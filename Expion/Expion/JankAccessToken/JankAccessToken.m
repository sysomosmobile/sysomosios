//
//  JankAccessToken.m
//  Expion
//
//  Created by Mike Bagwell on 3/29/17.
//  Copyright © 2017 Expion. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Webkit/WebKit.h>
#import "JankAccessToken.h"

//#import "Expion-Swift.h"

@interface JankAccessToken () <NSURLSessionDataDelegate, WKNavigationDelegate>

@property (nonatomic, strong, readwrite) NSURLSession * session;
@end

@implementation JankAccessToken

@synthesize delegate;
//@synthesize myUrl,
@synthesize companyId, debugMode;

-(void)ssoBegin:(NSString *)eUsername :(NSString *)ePW :(NSString *)sOauthDomain :(NSString *)sOauthPath :(NSString *)eUrl
{
    //NSLog(@"***-ssoBegin");
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
    
    
    debugMode = false;
    
    
    
    enteredUsername = [NSString stringWithFormat:@"%@", eUsername];
    enteredPassword = [NSString stringWithFormat:@"%@", ePW];
    
    hostDomain = [NSString stringWithFormat:@"%@", sOauthDomain];
    urlDomain = [NSString stringWithFormat:@"https://%@", sOauthDomain];
    urlOauth = [NSString stringWithFormat:@"https://%@%@", sOauthDomain, sOauthPath];
    
    expionHostUrl = [NSString stringWithFormat:@"%@", eUrl];
    expionDomainUrl = [NSString stringWithFormat:@"https://%@", eUrl];
    
    NSLog(@"***-hostDomain=>%@", hostDomain);
    NSLog(@"***-urlDomain=>%@", urlDomain);
    NSLog(@"***-urlOauth=>%@", urlOauth);
    NSLog(@"***-enteredUsername=>%@", enteredUsername);
    NSLog(@"***-enteredPassword=>%@", enteredPassword);
    NSLog(@"***-expionHostUrl=>%@", expionHostUrl);
    NSLog(@"***-expionDomainUrl=>%@", expionDomainUrl);
    
    NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
    NSString *version = [info objectForKey:@"CFBundleShortVersionString"];
    NSString *device = [NSString stringWithFormat:@"iPhone"];
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        device = [NSString stringWithFormat:@"iPad"]; /* Device is iPad */
    }
    
    userAgent = [NSString stringWithFormat:@"Expion-%@-%@", device, [version stringByReplacingOccurrencesOfString:@"." withString:@""]];
    
    if(enteredUsername != nil && enteredPassword != nil && hostDomain != nil && urlDomain != nil && urlOauth != nil){
        [self callOAuthGet];
    }
}

-(void)callOAuthGet
{
    //NSLog(@"***-callOAuthGet");
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.HTTPShouldSetCookies = YES;
    configuration.HTTPCookieAcceptPolicy = NSHTTPCookieAcceptPolicyAlways;
    //    configuration.HTTPCookieStorage?.cookieacceptPolicy = NSHTTPCookieAcceptPolicyAlways;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    
    
    NSURL *url = [NSURL URLWithString:expionDomainUrl]; //urlOauth];
    requestURL = [NSURL URLWithString:expionDomainUrl]; //urlOauth];
    
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    NSURLSessionTask *task = [session dataTaskWithRequest:request];
    NSString *urlStr = [NSString stringWithFormat:@"%@", url];
    NSLog(@"xx--callOAuthGet-url=>%@", urlStr);
    [task resume];
}

- (void)setCookies {
    //NSLog(@"!!!!----setCookies");
    
    
    
    
    
    [self postSysomosSSO];
}

-(void)postSysomosSSO
{
//    //NSLog(@"***-postSysomosSSO-cookieUrl=>%@", responseURL.absoluteString);
    NSString *cookieStr = [NSString stringWithFormat:@""];
    
    NSHTTPCookieStorage * cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray *cookies = [cookieStorage cookiesForURL:responseURL];
    for (NSHTTPCookie *cookie in cookies) {
        
//        NSLog(@"!!**-postSysomosSSO-cookie.name=>%@", cookie.name);
//        NSLog(@"!!**-postSysomosSSO-cookie.value=>%@", cookie.value);
        cookieStr = [cookieStr stringByAppendingString:[NSString stringWithFormat:@"%@=%@; ", cookie.name, cookie.value]];
        //        if([cookie.name rangeOfString:@"JSESSIONID" options:NSCaseInsensitiveSearch].location != NSNotFound){
        //            cookieStr = [NSString stringWithFormat:@"%@=%@", cookie.name, cookie.value];
        //        }
    }
    
    NSString *usernameData = [@"j_username=" stringByAppendingString: enteredUsername];
    NSString *passwordData = [@"&j_password=" stringByAppendingString: enteredPassword];
    NSString *loginData = [usernameData stringByAppendingString:passwordData];
    NSString *postData = [loginData stringByAppendingString:@"&login=Submit"];
    
    //    NSString *urlAsString = [NSString stringWithFormat:@"%@/Account/LogOn?ReturnUrl=%%2F", myUrl];

    NSString *urlAsString = [NSString stringWithFormat:@"%@/oauth/login.do", urlDomain];
    //    NSString *urlAsString2 = [NSString stringWithFormat:@"https://oauth.sysomos.com/oauth/oauth/authorize?response_type=token&state=oauth-test-5a02d3829317&client_id=sysomos-ui&scope=read&redirect_uri=https://www.google.com"];
    
    //NSLog(@"!**-postSysomosSSO-urlAsString=>%@", urlAsString);
    
    //NSLog(@"urlAsString=%@", urlAsString);
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];//@"CFBundleVersion"];//CFBundleShortVersionString =fix
    appVersion = [appVersion stringByReplacingOccurrencesOfString:@"." withString: @""];
    
    NSData *postData2 = [postData dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu" , (unsigned long)[postData length]];
    NSURL *url = [NSURL URLWithString:urlAsString];
    requestURL = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPShouldHandleCookies:YES];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData2];
    [request setValue:userAgent forHTTPHeaderField:@"User-Agent"];
    [request setValue:@"no-referrer-when-downgrade" forHTTPHeaderField:@"Referrer-Policy"];
    
    [request setValue:@"application/json, text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8" forHTTPHeaderField: @"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField: @"Content-Type"];
    
    [request setValue:cookieStr forHTTPHeaderField: @"Cookie"];
    [request setValue:@"1" forHTTPHeaderField: @"DNT"];
    [request setValue:hostDomain forHTTPHeaderField: @"Host"];
    [request setValue:urlDomain forHTTPHeaderField: @"Origin"];
    [request setValue:[NSString stringWithFormat:@"%@/oauth/login.jsp", urlDomain] forHTTPHeaderField: @"Referer"];
    [request setValue:@"1" forHTTPHeaderField: @"Upgrade-Insecure-Requests"];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.HTTPShouldSetCookies = YES;
    configuration.HTTPCookieAcceptPolicy = NSHTTPCookieAcceptPolicyAlways;
    //    configuration.HTTPCookieStorage?.cookieacceptPolicy = NSHTTPCookieAcceptPolicyAlways;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSURLSessionTask *task = [session dataTaskWithRequest:request];
    NSLog(@"xx--postSysomosSSO-url=>%@", urlAsString);
    [task resume];
}

-(void)parseAccessToken:(NSString *)responseUrlString {
    NSLog(@"!--parseAccessToken=>%@", responseUrlString);
    NSArray *parseAT = [responseUrlString componentsSeparatedByString:@"#access_token="];
    NSString *parseATStr = parseAT.count > 1 ? parseAT[1] : nil;
    NSString *errorMsg;
    if(parseATStr != nil) {
        NSArray *parseAT2 = [parseATStr componentsSeparatedByString:@"&"];
        NSString *accessTokenStr = parseAT2.count > 0 ? parseAT2[0] : nil;
        //NSLog(@"!--accessToken found=>%@", accessTokenStr);
        accessToken = accessTokenStr;
        if(accessToken != nil) {
            
            
            [self expionSysomosLogonPost:responseUrlString];
            return;
        } else {
            errorMsg = [NSString stringWithFormat:@"access token parse error"];
        }
    } else {
        errorMsg = [NSString stringWithFormat:@"access token not found"];
    }
    [self.delegate jankAccessTokenFailed:errorMsg];
}

-(void)expionSysomosLogonPost:(NSString *)responseUrlString
{
    
    NSString *cookieStr = [NSString stringWithFormat:@""];
    
    NSHTTPCookieStorage * cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    
    
    
    
    
    
    // expion.co=65a8ebb5-40d7-4fb7-8f82-f2139d126f42; ASP.NET_SessionId=uitf0uowoesumm4wzxvqdeah; ls.access={"access_token":"65a8ebb5-40d7-4fb7-8f82-f2139d126f42"}
    
    NSMutableDictionary *cookieProperties = [NSMutableDictionary dictionary];
    [cookieProperties setObject:@"expion.co" forKey:NSHTTPCookieName];
    [cookieProperties setObject:accessToken forKey:NSHTTPCookieValue];
    [cookieProperties setObject:@"t1-social.sysomos.com" forKey:NSHTTPCookieDomain];
//    [cookieProperties setObject:@"t1-social.sysomos.com" forKey:NSHTTPCookieOriginURL];
    [cookieProperties setObject:@"/" forKey:NSHTTPCookiePath];
//    [cookieProperties setObject:@"0" forKey:NSHTTPCookieVersion];
    
    // set expiration to one month from now or any NSDate of your choosing
    // this makes the cookie sessionless and it will persist across web sessions and app launches
    // if you want the cookie to be destroyed when your app exits, don't set this
//    [cookieProperties setObject:[[NSDate date] dateByAddingTimeInterval:2629743] forKey:NSHTTPCookieExpires];
    
    NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
    
    //2
    NSString *lsAccessStr = [NSString stringWithFormat:@"{'access_token':'%@'}", accessToken];
    NSMutableDictionary *cookieProperties2 = [NSMutableDictionary dictionary];
    [cookieProperties2 setObject:@"ls.access" forKey:NSHTTPCookieName];
    [cookieProperties2 setObject:lsAccessStr forKey:NSHTTPCookieValue];
    [cookieProperties2 setObject:@".sysomos.com" forKey:NSHTTPCookieDomain];
    [cookieProperties2 setObject:@"/" forKey:NSHTTPCookiePath];
    
    NSHTTPCookie *cookie2 = [NSHTTPCookie cookieWithProperties:cookieProperties2];
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie2];
    
    
    NSURL *cUrl = [NSURL URLWithString: @"sysomos.com"];//@"https://integration-oauth.sysomos.com/oauth/login.jsp"];
    
    NSArray *cookies = [cookieStorage cookies];
    for (NSHTTPCookie *cookie in cookies) {
        
//        NSLog(@"!!**--cookie.name=>%@", cookie.name);
//        NSLog(@"!!**--cookie.value=>%@", cookie.value);
        cookieStr = [cookieStr stringByAppendingString:[NSString stringWithFormat:@"%@=%@; ", cookie.name, cookie.value]];
    }
    
    NSString *expionOriginUrl = [NSString stringWithFormat:@"https://%@", expionHostUrl];
    NSString *expionRefererUrl = [NSString stringWithFormat:@"https://%@/content/SysLogCallBack.html?returnUrl=/", expionHostUrl];
    
    
//    //NSLog(@"***-expionSysomosLogonPost=>%@", accessToken);
    
    NSString *postData = [@"requestOrigin=" stringByAppendingString: responseUrlString];
    NSData *postData2 = [postData dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu" , (unsigned long)[postData length]];
    
    NSString *mefUrl = [NSString stringWithFormat:@"%@/Account/SysLogonPost", expionDomainUrl];
    NSURL *url = [NSURL URLWithString:mefUrl];
    requestURL = [NSURL URLWithString:mefUrl];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    //    NSString *mefUrl = [NSString stringWithFormat:@"https://t1-v6-5swat.crm.expion.us/API/ML/SSOUserRoles?accessToken=%@", accessToken];
    
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData2];
    [request setValue:userAgent forHTTPHeaderField:@"User-Agent"];
    
    
    [request setValue:@"application/json, text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8" forHTTPHeaderField: @"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField: @"Content-Type"];
    
    [request setValue:cookieStr forHTTPHeaderField: @"Cookie"];
    //TODO: CookieStr here is wrong
    [request setValue:@"1" forHTTPHeaderField: @"DNT"];
    [request setValue:expionHostUrl forHTTPHeaderField: @"Host"];
    [request setValue:expionOriginUrl forHTTPHeaderField: @"Origin"];
    [request setValue:[NSString stringWithFormat:expionRefererUrl, urlDomain] forHTTPHeaderField: @"Referer"];
    [request setValue:@"1" forHTTPHeaderField: @"Upgrade-Insecure-Requests"];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.HTTPShouldSetCookies = YES;
    configuration.HTTPCookieAcceptPolicy = NSHTTPCookieAcceptPolicyAlways;
    //    configuration.HTTPCookieStorage?.cookieacceptPolicy = NSHTTPCookieAcceptPolicyAlways;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSURLSessionTask *task = [session dataTaskWithRequest:request];
    NSLog(@"xx--expionSysomosLogonPost-url=>%@", mefUrl);
    [task resume];
}

-(void)getExpionCompanyId
{
    //NSLog(@"***-getExpionCompanyId");
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.HTTPShouldSetCookies = YES;
    configuration.HTTPCookieAcceptPolicy = NSHTTPCookieAcceptPolicyAlways;
    //    configuration.HTTPCookieStorage?.cookieacceptPolicy = NSHTTPCookieAcceptPolicyAlways;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSString *expionGetUrl = [NSString stringWithFormat:@"%@/api/ssologin?token=%@", expionDomainUrl, accessToken];
    
    NSURL *url = [NSURL URLWithString:expionGetUrl];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setValue:accessToken forHTTPHeaderField: @"token"];
    NSURLSessionTask *task = [session dataTaskWithRequest:request];
    NSLog(@"xx--getExpionCompanyId-url=>%@", expionGetUrl);
    [task resume];
}


- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task willPerformHTTPRedirection:(NSHTTPURLResponse *)response newRequest:(NSURLRequest *)request completionHandler:(void (^)(NSURLRequest *))completionHandler {
    NSLog(@"**-302 %@ from %@", request.URL, response.URL);
    if ([response respondsToSelector:@selector(allHeaderFields)]) {
        NSDictionary *dictionary = [response allHeaderFields];
        NSLog(@"%@", [dictionary description]);
    }
    completionHandler(request);
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler {
    
//    //NSLog(@"%@", response);
    
//    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
//    NSString *lastModifiedString = [[httpResponse allHeaderFields] objectForKey:@"Last-Modified"];
//    if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
//        NSDictionary *dictionary = [httpResponse allHeaderFields];
//        NSLog(@"%@", [dictionary description]);
//    }
    
    
    
    
    responseData = [[NSMutableData alloc] init];
    [responseData setLength:0];
    responseURL = [response URL];
    
    NSString *responseURLString = [[response URL] absoluteString];
    
    NSURL* url = [response URL];
    NSString* fragment = [url fragment];
    NSLog(@"!!-URLSession--response=>%@", responseURLString);
    NSLog(@"!!-fragment=>%@", fragment);
    
    if([responseURLString rangeOfString:@"#access_token" options:NSCaseInsensitiveSearch].location != NSNotFound){
        //NSLog(@"");
        //NSLog(@"!-URLSession--response-#access_token found!!--=>%@", responseURLString);
        //NSLog(@"");
//        [self.delegate jankAccessWebLoad:responseURLString];//] companyId:companyId];
//        [self parseAccessToken: responseURLString];
    }




    completionHandler(NSURLSessionResponseAllow);
}

//- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler {
//    //NSLog(@"!-URLSession--challenge %@", challenge.protectionSpace.authenticationMethod);
//    completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
//}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler
{
    completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]);
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    //NSLog(@"!-URLSession--done");
    
    
    if (responseData) {
        // my response is JSON; I don't know what yours is, though this handles both
        
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
        if (response) {
            //NSLog(@"response=>");
//            //NSLog(@"%@", response);
        } else {
            //NSLog(@"responseData=>");
//            //NSLog(@"%@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
        }
        
        //        [self.responsesData removeObjectForKey:@(task.taskIdentifier)];
    } else {
        //NSLog(@"responseData is nil");
    }
    
    if(responseURL){
        [self checkResponse];
    }
    
    //https://home.sysomos.com/
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data {
    NSLog(@"!-didReceiveData--=>");
    NSString* responseDataString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"!-responseDataString=>%@", responseDataString);
    [responseData appendData:data];
}

- (void)checkResponse {
    NSString *responseURLString = [responseURL absoluteString];
    NSString *requestURLString = [requestURL absoluteString];
    NSLog(@"!!--checkResponse-url=>%@", responseURLString);
    NSLog(@"!!--request-url=>%@", requestURLString);
    
//    if([responseURLString rangeOfString:requestURLString options:NSCaseInsensitiveSearch].location != NSNotFound){
    if([responseURLString rangeOfString:@"https" options:NSCaseInsensitiveSearch].location != NSNotFound){
        if([responseURLString rangeOfString:@"login.jsp" options:NSCaseInsensitiveSearch].location != NSNotFound){
            if([responseURLString rangeOfString:@"authentication_error=true" options:NSCaseInsensitiveSearch].location != NSNotFound){
                [self.delegate jankAccessTokenFailed:@"Invalid username and password combination.  Please check your entries and try again"];
            } else {
                [self setCookies];
            }
        } else if([responseURLString rangeOfString:@"login.do" options:NSCaseInsensitiveSearch].location != NSNotFound){
            [self callOAuthGet];
        } else if([responseURLString rangeOfString:@"SysLogCallBack.html?returnUrl=/#access_token" options:NSCaseInsensitiveSearch].location != NSNotFound){
//            [self sysLogCallBack];
            //[self.delegate jankAccessTokenComplete:accessToken companyId:companyId];
            
            //this html sets ls.access cookie for web
            [self parseAccessToken: responseURLString];
            
//        } else if([responseURLString rangeOfString:@"SysLogonPost" options:NSCaseInsensitiveSearch].location != NSNotFound){
//            [self getExpionCompanyId];
        } else if([responseURLString rangeOfString:@"Home/Home?" options:NSCaseInsensitiveSearch].location != NSNotFound){
            NSLog(@"!!--Home/Home");
//            [self getExpionCompanyId];
            [self setCompanyId:responseURLString];
//        } else if([responseURLString rangeOfString:@"api/ssologin?token=" options:NSCaseInsensitiveSearch].location != NSNotFound){
//            [self setCompanyId];
        } else if([responseURLString rangeOfString:@"SysLogCallBack.html?" options:NSCaseInsensitiveSearch].location != NSNotFound){
//            [self sysLogCallBack];
        }
    }
//    } else {
//        //NSLog(@"Login failed");
//    
//        //TODO: alert user here
//    }
}

-(void)sysLogCallBack {
    
    
    
//    NSString *responseURLString = [responseURL absoluteString];
//    NSString *requestURLString = [requestURL absoluteString];
//    //NSLog(@"!!--sysLogCallBack-responseURLString=>%@", responseURLString);
//    //NSLog(@"!!--sysLogCallBack-requestURLString=>%@", requestURLString);
//    WKPreferences *preferences = [[WKPreferences alloc] init];
//    preferences.javaScriptEnabled = YES;
//    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
//    configuration.preferences = preferences;
////    WKWebView *webView = [[WKWebView alloc] initWithFrame:self.view.frame configuration:theConfiguration];
//    WKWebView *webView =[[WKWebView alloc] initWithFrame:CGRectZero configuration:configuration];
//    webView.navigationDelegate = self;
////    NSURL *nsurl=[NSURL URLWithString:@"http://www.apple.com"];
//    NSURL *nsurl = responseURL;
//    NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
//    [webView loadRequest:nsrequest];
////    [self.view addSubview:webView];
}

- (void) webView: (WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler
{
    //NSLog(@"webView didReceiveAuthenticationChallenge");
    NSURLCredential * credential = [[NSURLCredential alloc] initWithTrust:[challenge protectionSpace].serverTrust];
    completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
}

- (void) webView: (WKWebView *) webView decidePolicyForNavigationAction: (WKNavigationAction *) navigationAction decisionHandler: (void (^)(WKNavigationActionPolicy)) decisionHandler
{
    //NSLog(@"webView decidePolicyForNavigationAction");
}

- (void) webView: (WKWebView *) webView didStartProvisionalNavigation: (WKNavigation *) navigation
{
    //NSLog(@"webView didStartProvisionalNavigation");
}

- (void) webView:(WKWebView *) webView didFailProvisionalNavigation: (WKNavigation *) navigation withError: (NSError *) error
{
    //NSLog(@"webView didFailNavigation");
    //NSLog(@"%@", error);
}

- (void) webView: (WKWebView *) webView didFailNavigation: (WKNavigation *) navigation withError: (NSError *) error
{
    //NSLog(@"webView didFailNavigation");
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    [webView evaluateJavaScript:@"document.getElementsByTagName('html')[0].outerHTML" completionHandler:^(id result, NSError *error) {
        //NSLog(@"Result %@", result);
    }];
}




-(void)setCompanyId:(NSString *)parseStr {
    
//    companyId = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    companyId = [NSString stringWithFormat:@"%@", [[[[parseStr componentsSeparatedByString:@"company_id="] objectAtIndex:1] componentsSeparatedByString:@"&"] objectAtIndex:0]];
    
//    NSMutableDictionary *lsAccess = [NSMutableDictionary dictionary];
//    [lsAccess setObject:accessToken forKey:@"access_token"];
//    
//    NSError *writeError = nil;
//    NSData *lsData = [NSJSONSerialization dataWithJSONObject:lsAccess options:NSJSONWritingPrettyPrinted error:&writeError];
//    NSString *lsDataString = [[NSString alloc] initWithData:lsData encoding:NSUTF8StringEncoding];
//    
//    
//    NSString *lsString = [NSString stringWithFormat:@"{'access_token':'%@'}", accessToken];
//    
//    NSMutableDictionary *dictCookieID = [NSMutableDictionary dictionary];
//    [dictCookieID setObject:@"ls.access" forKey:NSHTTPCookieName];
//    [dictCookieID setObject:lsString forKey:NSHTTPCookieValue];
//    [dictCookieID setObject:@"sysomos.com" forKey:NSHTTPCookieDomain];
//    [dictCookieID setObject:@"/" forKey:NSHTTPCookiePath];
//    //    [dictCookieID setObject:value forKey:NSHTTPCookieVersion];
//    
//    NSHTTPCookie *cookieID = [NSHTTPCookie cookieWithProperties:dictCookieID];
//    
//    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
//    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookieID];
    
    
    NSString *cookieStr = [NSString stringWithFormat:@""];
    
    NSHTTPCookieStorage * cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    NSURL *cUrl = [NSURL URLWithString: @"sysomos.com"];
    
    NSArray *cookies = [cookieStorage cookies];  //[cookieStorage cookiesForURL:cUrl];
    for (NSHTTPCookie *cookie in cookies) {
        
        NSLog(@"!!**--cookie.name=>%@", cookie.name);
        NSLog(@"!!**--cookie.value=>%@", cookie.value);
        NSLog(@"!!**--cookie.value=>%@", cookie.domain);
        cookieStr = [cookieStr stringByAppendingString:[NSString stringWithFormat:@"%@=%@; ", cookie.name, cookie.value]];
        //        if([cookie.name rangeOfString:@"JSESSIONID" options:NSCaseInsensitiveSearch].location != NSNotFound){
        //            cookieStr = [NSString stringWithFormat:@"%@=%@", cookie.name, cookie.value];
        //        }
    }
    
//    //NSLog(@"!!**--cookieStr=>%@", cookieStr);
    
    
    
    [self saveUserDefaults];
    
}

-(void)saveUserDefaults{
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setObject:expionDomainUrl forKey:@"myUrl"];
    //NSLog(@"saveUserDefaults 1");
//    [standardUserDefaults setObject:actionUrl forKey:@"actionUrl"];
    [standardUserDefaults setObject:companyId forKey:@"companyId"];
//    [standardUserDefaults setObject:expionName forKey:@"expionName"];
    
    [standardUserDefaults setObject:enteredUsername forKey:@"userName"];
    [standardUserDefaults setObject:[NSNumber numberWithBool:debugMode] forKey:@"debugMode"];
    
    [standardUserDefaults setObject:accessToken forKey:@"accessToken"];
    
//    [standardUserDefaults setBool:isAdmin forKey:@"isAdmin"];
//    //NSLog(@"saveUserDefaults 2");
//    [standardUserDefaults setBool:canViewPublish forKey:@"canViewPublish"];
//    [standardUserDefaults setBool:canMobileSuggestedContent forKey:@"canMobileSuggestedContent"];
//    [standardUserDefaults setBool:canMobileQuickDraft forKey:@"canMobileQuickDraft"];
//    [standardUserDefaults setBool:canViewModeration forKey:@"canViewModeration"];
//    [standardUserDefaults setBool:canMobileUpload forKey:@"canMobileUpload"];
//    [standardUserDefaults setBool:canMobileMediaCreate forKey:@"canMobileMediaCreate"];
//    [standardUserDefaults setBool:canMobileMediaApprove forKey:@"canMobileMediaApprove"];
//    
//    [standardUserDefaults setBool:canMediaGallery forKey:@"canMediaGallery"];
//    [standardUserDefaults setBool:canFBAlbumImagePublish forKey:@"canFBAlbumImagePublish"];
//    [standardUserDefaults setBool:canExpionImage forKey:@"canExpionImage"];
//    [standardUserDefaults setBool:canViewExpionImage forKey:@"canViewExpionImage"];
//    //NSLog(@"saveUserDefaults 3");
//    [standardUserDefaults setBool:isEImages forKey:@"isEImages"];
    
    //[standardUserDefaults setObject: cookiesData forKey: @"cookies"];
    //NSLog(@"saveUserDefaults sync");
    [standardUserDefaults synchronize];
    
    [self.delegate jankAccessTokenComplete:accessToken companyId:companyId];
}


//---------------------------------
//* End NSURLSession */
//---------------------------------










@end
