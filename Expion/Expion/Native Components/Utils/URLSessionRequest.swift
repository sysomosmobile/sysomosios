//
//  URLSessionRequest.swift
//  Expion
//
//  Created by Mike Bagwell on 10/5/17.
//  Copyright © 2017 Expion. All rights reserved.
//

import Foundation

@objc
protocol URLSessionRequestDelegate {
    @objc optional func URLSessionRequestComplete()
    @objc optional func URLSessionRequestFailed(errorMessage: String)
}

class URLSessionRequest: NSObject {
    
}
