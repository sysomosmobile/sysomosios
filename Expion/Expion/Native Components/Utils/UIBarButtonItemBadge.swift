//
//  UIBarButtonItemBadge.swift
//  Expion
//
//  Created by Mike Bagwell on 3/9/17.
//  Copyright © 2017 Expion. All rights reserved.
//

import Foundation

private var handle: UInt8 = 0;

extension UIBarButtonItem {
    
    private var badgeLayer: CAShapeLayer? {
        if let b: AnyObject = objc_getAssociatedObject(self, &handle) as AnyObject? {
            return b as? CAShapeLayer
        } else {
            return nil
        }
    }
    
    func addBadge(number: Int, withOffset offset: CGPoint = CGPoint.zero, andColor color: UIColor = UIColor.red, andFilled filled: Bool = true) {
        guard let view = self.value(forKey: "view") as? UIView else { return }
        
        badgeLayer?.removeFromSuperlayer()
        
        if(number == 0) {
            return
        }
        
        let numberStr = number > 999 ? "!" : String(number)
        let numberLength = CGFloat(numberStr.count > 0 ? numberStr.count : 1)
        
        // Initialize Badge
        let badge = CAShapeLayer()
        let radius = CGFloat(7)
        let location = CGPoint(x: view.frame.width - (radius + offset.x), y: (radius + offset.y))
        badge.drawCircleAtLocation(location: location, withRadius: radius, andColor: color, filled: filled)
        view.layer.addSublayer(badge)
        
        // Initialiaze Badge's label
        let label = CATextLayer()
        
        let fontSize = numberLength < 3 ? 11 : 8
        let width = numberLength * 10
        let offsetY = numberLength < 3 ? offset.y : (offset.y + 2)
        label.string = "\(numberStr)"
        label.alignmentMode = kCAAlignmentCenter
        label.fontSize = CGFloat(fontSize)
        label.frame = CGRect(origin: CGPoint(x: location.x - CGFloat(width/2), y: offsetY), size: CGSize(width: width, height: 16))
        label.foregroundColor = filled ? UIColor.white.cgColor : color.cgColor
        label.backgroundColor = UIColor.clear.cgColor
        label.contentsScale = UIScreen.main.scale
        badge.addSublayer(label)
        
        // Save Badge as UIBarButtonItem property
        objc_setAssociatedObject(self, &handle, badge, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    
    func updateBadge(number: Int) {
        DispatchQueue.main.async( execute: {
            CATransaction.begin()
            CATransaction.setDisableActions(true)
            self.removeBadge()
            if(number > 0) {
                self.addBadge(number: number)
            }
            CATransaction.commit()
        })
    }
    
    func removeBadge() {
        badgeLayer?.removeFromSuperlayer()
    }
    
}

extension CAShapeLayer {
    public func drawCircleAtLocation(location: CGPoint, withRadius radius: CGFloat, andColor color: UIColor, filled: Bool) {
        fillColor = filled ? color.cgColor : UIColor.white.cgColor
        strokeColor = color.cgColor
        let origin = CGPoint(x: location.x - radius, y: location.y - radius)
        path = UIBezierPath(ovalIn: CGRect(origin: origin, size: CGSize(width: radius * 2, height: radius * 2))).cgPath
    }
}
