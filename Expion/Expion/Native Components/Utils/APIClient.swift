//
//  APIClient.swift
//  Expion
//
//  Created by Mike Bagwell on 4/4/17.
//  Copyright © 2017 Expion. All rights reserved.
//

import Foundation

class APIClient {
    
    func login(email: String, password: String, completion: @escaping (_ success: Bool, _ message: String?) -> ()) {
        let loginObject = ["email": email, "password": password]
        
        post(request: clientURLRequest(path: "auth/local", params: loginObject as Dictionary<String, AnyObject>)) { (success, object) -> () in
            
            DispatchQueue.main.async { () -> Void in
                if success {
                    completion(true, nil)
                } else {
                    var message = "there was an error"
                    if let object = object, let passedMessage = object["message"] as? String {
                        message = passedMessage
                    }
                    completion(true, message)
                }
            }
        }
    }
    
    
    func getJsonResultForUrl(urlPath: String, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        //print(urlPath)
        get(request: clientURLRequest(path: urlPath, params: nil)) { (success, object) -> () in
            DispatchQueue.main.async { () -> Void in
                if success {
                    completion(true, object)
                } else {
                    var message = "there was an error"
                    if let object = object, let passedMessage = object["message"] as? String {
                        message = passedMessage
                    }
                    completion(true, object)
                }
            }
        }
    }
    
    private func get(request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        dataTask(request: request, method: "GET", completion: completion)
    }
    
    private func post(request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        dataTask(request: request, method: "POST", completion: completion)
    }

    private func put(request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        dataTask(request: request, method: "PUT", completion: completion)
    }

    private func dataTask(request: NSMutableURLRequest, method: String, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        request.httpMethod = method
        
//        let session = URLSession(configuration: URLSessionConfiguration.default)
        let session = URLSession.shared
        session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
//            let httpResponse = response as! HTTPURLResponse
//            //print(httpResponse)
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                if let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode {
                    //print(response)
                    completion(true, json as AnyObject)
                } else {
                    completion(false, json as AnyObject)
                }
            }
        }.resume()
    }
    
    private func clientURLRequest(path: String, params: Dictionary<String, AnyObject>? = nil) -> NSMutableURLRequest {
        let request = NSMutableURLRequest(url: URL(string: path)!)
        if let params = params {
            var paramString = ""
            for (key, value) in params {
                let escapedKey = key.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                let escapedValue = value.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                paramString += "\(String(describing: escapedKey))=\(String(describing: escapedValue))&"
            }
            
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpBody = paramString.data(using: String.Encoding.utf8)
        }
        
    //    if let token = token {
    //        request.addValue("Bearer "+token, forHTTPHeaderField: "Authorization")
    //    }
        
        return request
    }
}
