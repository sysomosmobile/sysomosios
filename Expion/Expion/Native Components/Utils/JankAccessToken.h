//
//  JankAccessToken.h
//  Expion
//
//  Created by Mike Bagwell on 3/29/17.
//  Copyright © 2017 Expion. All rights reserved.
//


@protocol JankAccessTokenDelegate <NSObject>
@optional
- (void)jankAccessTokenComplete:(NSString *)aToken companyId:(NSString *)cId;
- (void)jankAccessTokenFailed:(NSString *)errorMessage;

- (void)jankAccessWebLoad:(NSString *)tokenUrl;
@end

@interface JankAccessToken : NSObject {
    NSString *enteredUsername;
    NSString *enteredPassword;
    
    NSString *hostDomain;
    NSString *urlDomain;
    NSString *urlOauth;
    
    NSString *expionHostUrl;
    NSString *expionDomainUrl;
    
    NSString *userAgent;
    
    NSString *accessToken;
    
    NSMutableData *responseData;
    NSURL *requestURL;
    NSURL *responseURL;
    
//    NSString *myUrl;
    NSString *companyId;
    NSString *userName;
    
    BOOL debugMode;
    
    __weak id<JankAccessTokenDelegate> delegate;
}

@property (nonatomic, retain) NSString *enteredUsername;
@property (nonatomic, retain) NSString *enteredPassword;

@property (nonatomic, retain) NSString *hostDomain;
@property (nonatomic, retain) NSString *urlDomain;
@property (nonatomic, retain) NSString *urlOauth;

@property (nonatomic, retain) NSString *expionHostUrl;
@property (nonatomic, retain) NSString *expionDomainUrl;

@property (nonatomic, retain) NSString *userAgent;

@property (nonatomic, retain) NSString *accessToken;

@property (nonatomic, retain) NSURL *requestURL;
@property (nonatomic, retain) NSURL *responseURL;

//@property (nonatomic, retain) NSString *myUrl;
@property (nonatomic, retain) NSString *companyId;
@property (nonatomic, retain) NSString *userName;

@property (nonatomic, assign) BOOL debugMode;

@property (weak, nonatomic) id<JankAccessTokenDelegate> delegate;

- (void)ssoBegin:(NSString *)eUsername :(NSString *)ePW :(NSString *)sOauthDomain :(NSString *)sOauthPath :(NSString *)eUrl;

- (void)legacyBegin:(NSString *)eUsername :(NSString *)ePW :(NSString *)eUrl;

@end
