//
//  URLSessionConnect.swift
//  Expion
//
//  Created by Mike Bagwell on 5/10/17.
//  Copyright © 2017 Expion. All rights reserved.
//

import Foundation


@objc
protocol URLSessionConnectDelegate {
    @objc optional func apiConnComplete(returnData: NSMutableData)
}

class URLSessionConnect:NSObject { //: NSURLProtocol {
    
    var company_id: String!
    var requestUrl: String!
    var actionUrl: String!
    var responseData: NSMutableData!
    var delegate: URLSessionConnectDelegate?
    
    let opQueue = OperationQueue()
    var response:URLResponse?
    var session:URLSession?
    
    /*
     override func viewDidLoad() {
     super.viewDidLoad()
     //println("<< containerViewController getMLJSON <<")
     //println("getMLJSON")
     startConnection()
     }*/
    
    init(company_id: String, requestUrl: String, actionUrl: String){
        self.company_id = company_id
        self.requestUrl = requestUrl
        self.actionUrl = actionUrl
    }
    
    func startConnection(){
        //let company_id = "31"
        //let myUrl = "https://t1.crm.xpon.us"
        if(requestUrl != nil && company_id != nil){
            let urlPath: String = requestUrl
            let url: URL = URL(string: urlPath)!
            
            let cookies = HTTPCookieStorage.shared.cookies!
            let headers = HTTPCookie.requestHeaderFields(with: cookies)
            var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 60.0)
            
            
            request.setValue(ShareData.sharedInstance.userAgent, forHTTPHeaderField:"User-Agent")
            
            if let accessToken = UserDefaults.standard.object(forKey: "accessToken") as! String! {
                request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            }
            
            request.allHTTPHeaderFields = headers
            
            let sessionConfiguration = URLSessionConfiguration.default
            // disable the caching
            sessionConfiguration.urlCache = nil
            self.session = URLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: nil)
            if let task = self.session?.dataTask(with: request) {
                // start the task, tasks are not started by default
                task.resume()
            }
            
        }
    }
    
    func apiConnComplete() {
//        delegate?.apiConnComplete(returnData: self.data)
    }
}

extension URLSessionConnect:URLSessionDelegate {
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        // We've got a URLAuthenticationChallenge - we simply trust the HTTPS server and we proceed
        //print("CVC--didReceive challenge")
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
    
    func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        // We've got an error
        if let err = error {
            //print("CVC--Error: \(err.localizedDescription)")
        } else {
            //print("CVC--Error. Giving up")
        }
        //            PlaygroundPage.current.finishExecution()
    }
}

extension URLSessionConnect:URLSessionDataDelegate {
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didBecome streamTask: URLSessionStreamTask) {
        // The task became a stream task - start the task
        //print("CVC--didBecome streamTask")
        streamTask.resume()
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didBecome downloadTask: URLSessionDownloadTask) {
        // The task became a download task - start the task
        //print("CVC--didBecome downloadTask")
        downloadTask.resume()
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        // We've got a URLAuthenticationChallenge - we simply trust the HTTPS server and we proceed
        //print("CVC--didReceive challenge")
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: @escaping (URLRequest?) -> Void) {
        // The original request was redirected to somewhere else.
        // We create a new dataTask with the given redirection request and we start it.
        if let urlString = request.url?.absoluteString {
            //print("CVC--willPerformHTTPRedirection to \(urlString)")
        } else {
            //print("CVC--willPerformHTTPRedirection")
        }
        if let task = self.session?.dataTask(with: request) {
            task.resume()
        }
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        // We've got an error
        if let err = error {
            //print("CVC--Error: \(err.localizedDescription)")
        } else {
            //print("CVC--Session Complete")
            if(self.responseData != nil) {
                
                /*
                self.parseMLJSON(self.responseData) {
                    (parsedMLData: Dictionary<String, Array<MLItem>>) in
                    self.parseCompleted(parsedMLData)
                    //println("*X*X*parseMLJSON completed*X*X*")
                    //println("got back: (result)")
                }
                */
                //                do {
                //                    let json = try JSONSerialization.jsonObject(with: responseData as Data, options: .mutableContainers)
                //                    //                if let firstCategory = json["categories"]??[0]["name"] as? String {
                //                    //                    //print("The first category in the JSON list is \"\(firstCategory)\"")
                //                    //                }
                //                    //print("CVC--this is the json")
                //                    //print(json)
                //
                //                } catch let error as NSError {
                //                    //print("CVC--Error parsing JSON: \(error.localizedDescription)")
                //                }
            }
            
        }
        //            PlaygroundPage.current.finishExecution()
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        // We've got the response headers from the server.
        //print("CVC--didReceive response")
        self.response = response
        self.responseData = NSMutableData()
        completionHandler(URLSession.ResponseDisposition.allow)
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        // We've got the response body
        //print("CVC--didReceive data")
        if let responseText = String(data: data, encoding: .utf8) {
            //print(self.response ?? "")
            //print("\nCVC--Server's response text")
            //print(responseText)
            
        }
        responseData.append(data)
        
        self.session?.finishTasksAndInvalidate()
        //            PlaygroundPage.current.finishExecution()
    }
}
