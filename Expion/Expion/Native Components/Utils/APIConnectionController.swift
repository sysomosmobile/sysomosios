//
//  APIConnetionController.swift
//  Workspaces
//
//  Created by Mike Bagwell on 5/27/15.
//  Copyright (c) 2015 Expion. All rights reserved.
//

import Foundation

class APIConnectionController { //: NSURLProtocol {
    
    var company_id: String!
    var myUrl: String!
    var actionUrl: String!
    var data: NSMutableData!
    var delegate: ContainerViewController!
    
    /*
    override func viewDidLoad() {
        super.viewDidLoad()
        //println("<< containerViewController getMLJSON <<")
        //println("getMLJSON")
        startConnection()
    }*/
    
    init(company_id: String, myUrl: String, actionUrl: String){
        self.company_id = company_id
        self.myUrl = myUrl
        self.actionUrl = actionUrl
    }

    func startConnection(){
        //let company_id = "31"
        //let myUrl = "https://t1.crm.xpon.us"
        //let actionUrl = "/API/ML/User?company_id="
        var urlPath: String!
        if(company_id.count > 0) {
            urlPath = myUrl + "/" + actionUrl + "/" + company_id
        } else {
            urlPath = myUrl + "/" + actionUrl
        }
        //let urlPath: String = String(format:"%@/api/location/%@/%@?selectAll=true", myUrl, "FBPOSTV1", company_id);
        
        let url: URL = URL(string: urlPath)!
        
        //print(String(format: "!!--APIConnectionController startConnection urlPath=>%@", url as CVarArg))
        let request: URLRequest = URLRequest(url: url)
        let connection: NSURLConnection = NSURLConnection(request: request, delegate: self, startImmediately: false)!
        connection.start()
    }

    func connection(_ connection: NSURLConnection, willSendRequest request: URLRequest, redirectResponse response: URLResponse?) -> URLRequest? {
            if ((response) != nil) {
                // we don't use the new request built for us, except for the URL
                let newUrl: URL = request.url!
                // Previously, store the original request in _originalRequest.
                // We rely on that here!
                let newRequest: NSMutableURLRequest = (request as NSURLRequest).mutableCopy() as! NSMutableURLRequest
                newRequest.url = newUrl
                return newRequest as URLRequest
            } else {
                return request
            }
    }
    
//    func connection(connection: NSURLConnection, willSendRequest request: NSURLRequest, redirectResponse response: NSURLResponse?) -> NSURLRequest?
//    {
//        if let response = response {
//            client?.URLProtocol(self, wasRedirectedToRequest: request, redirectResponse: response)
//        }
//        return request
//    }
    
    func connection(_ didReceiveResponse: NSURLConnection!, didReceiveResponse response: URLResponse!) {
        // Received a new request, clear out the data object
        self.data = NSMutableData()
    }

    func connection(_ connection: NSURLConnection!, didReceiveData data: Data!){
        self.data.append(data)
//        var dataStr = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
//        println(String(format: "%@", dataStr!))
    }

    func connectionDidFinishLoading(_ connection: NSURLConnection!) {
//        var err: NSError
        // throwing an error on the line below (can't figure out where the error message is)
        apiConnComplete()
    }
    
    func apiConnComplete() {
        delegate?.apiConnComplete(self.data)
    }
}
