//
//  Serializable.swift
//  Workspaces
//
//  Created by Mike Bagwell on 5/28/15.
//  Copyright (c) 2015 Expion. All rights reserved.
//

import Foundation

@objc open class Serializable : NSObject
{

@objc func toDictionary() -> NSDictionary
{
    let aClass : AnyClass? = type(of: self)
    var propertiesCount : CUnsignedInt = 0
    let propertiesInAClass = class_copyPropertyList(aClass, &propertiesCount)
    //let propertiesInAClass : UnsafeMutablePointer<objc_property_t> = class_copyPropertyList(aClass, &propertiesCount)
    let propertiesDictionary : NSMutableDictionary = NSMutableDictionary()

    
    
    for i in (0 ..< Int(propertiesCount))
    {
        let property = propertiesInAClass?[i]
        let propName = NSString(cString: property_getName(property!), encoding: String.Encoding.utf8.rawValue)
        //var propType = property_getAttributes(property)
        let propValue : AnyObject! = self.value(forKey: propName! as String) as AnyObject?

        if(propValue is Serializable)
        {
            propertiesDictionary.setValue((propValue as! Serializable).toDictionary(), forKey: (propName as String?)!)
        }
        else if(propValue is Array<Serializable>)
        {
            var subArray = Array<NSDictionary>()
            for item in (propValue as! Array<Serializable>)
            {
                subArray.append(item.toDictionary())
            }
            propertiesDictionary.setValue(subArray, forKey: propName! as String)
        }
        else if(propValue is NSData)
        {
            propertiesDictionary.setValue((propValue as! Data).base64EncodedString(options: []), forKey: propName! as String)
        }
        /*
        else if(propValue is Bool)
        {
            propertiesDictionary.setValue((propValue as! Bool).boolValue, forKey: propName! as String)
        }*/
        else if(propValue is NSDate)
        {
            let date = propValue as! Date
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "Z"
            let dateString = NSString(format: "/Date(%.0f000%@)/", date.timeIntervalSince1970, dateFormatter.string(from: date))
            propertiesDictionary.setValue(dateString, forKey: propName! as String)
        }
        else
        {
            propertiesDictionary.setValue(propValue, forKey: propName! as String)
        }
    }

    return propertiesDictionary
}

    func toJson() -> Data!
    {
        let dictionary = self.toDictionary()
//        var err: NSError?
        do {
            return try JSONSerialization.data(withJSONObject: dictionary, options:JSONSerialization.WritingOptions(rawValue: 0))
        } catch _ {
            return nil
        }
    }

    func toJsonString() -> NSString!
    {
        //return NSString(data: self.toJson(), encoding: NSUTF8StringEncoding)
        return NSString(data: self.toJson(), encoding: String.Encoding.utf8.rawValue)
    }
    
    func toMLItemJsonString(_ mlItem: MLItem) -> NSString!
    {
        //return NSString(data: self.toJson(), encoding: NSUTF8StringEncoding)
        return NSString(data: mlItem.toJson(), encoding: String.Encoding.utf8.rawValue)
    }
    
    func toMLStackJsonString(_ mlStack: MLStack) -> NSString!
    {
        return NSString(data: mlStack.toJson(), encoding: String.Encoding.utf8.rawValue)
    }

    override init()
    {

    }
}


