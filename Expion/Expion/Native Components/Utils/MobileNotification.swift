//
//  MobileNotification.swift
//  Expion
//
//  Created by Mike Bagwell on 3/11/17.
//  Copyright © 2017 Expion. All rights reserved.
//

import Foundation
import UserNotifications
//import Playground

@objc
protocol MobileNotificationDelegate {
    @objc optional func deviceRegistrationComplete() //unused
    @objc optional func notificationsListRetreived(notifications: NSDictionary)
}

class MobileNotification: NSObject {
    typealias CallbackBlock = (_ result: String, _ error: String?) -> ()
    
//    var company_id: Int!
//    var myUrl: String!
    var delegate: MobileNotificationDelegate?
    
    let notifApiUrl = "http://34.199.227.10"

    let bsKey = "api-key"
    let bsValue = "7cf1b775e8674716b56efa82ade8b7a9"
    
//    let devDeviceToken = "9b81c11192581b6b2f7b95acbbf24c4f00bfc8558b3be907cf60dfd170f86863"
    let devDeviceToken = "edf1075a0c40025caf7291a9f80531855f33d5a322dc8725439ab06db5d82b0e"
    let demoDeviceToken = "edf1075a0c40025caf7291a9f80531855f33d5a322dc8725439ab06db5d82b0e"
    
    let opQueue = OperationQueue()
    var response:URLResponse?
    var session:URLSession?
    
    var notificationsEnabled: Bool = false
    
    var time:DispatchTime! {
        return DispatchTime.now() + 1.0 // seconds
    }
    
    let company_id = ShareData.sharedInstance.company_id!
    let myUrl = ShareData.sharedInstance.myUrl!
    
    func registerDevice(active_ind: Bool) {
        //print("Request Register Device...")
        let isLocalUrl = myUrl.contains("local-social")
        if(notificationsEnabled == true && (UserDefaults.standard.object(forKey: "apns_token") != nil) && !isLocalUrl) {
            // suspend the OperationQueue (operations will be queued but won't get executed)
            self.opQueue.isSuspended = true
            let sessionConfiguration = URLSessionConfiguration.default
            // disable the caching
            sessionConfiguration.urlCache = nil
            
            // initialize the URLSession
            self.session = URLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: self.opQueue)
            
            // create a URLRequest with the given URL and initialize a URLSessionDataTask
            
            let urlPath = myUrl + "/api/notificationproxy?nativeAction=registerDevice"
            //print("urlPath=>" + urlPath)
            if let url = URL(string: urlPath) {
                
//                //TODO: need to update notification settings for new changes
//                if #available(iOS 10.0, *) {
//                    UNUserNotificationCenter.current().getNotificationSettings(completionHandler: { (settings: UNNotificationSettings) in
//                        // settings.authorizationStatus == .authorized
//                    })
//                } else {
//                    var badges_ind = UIApplication.shared.currentUserNotificationSettings?.types.contains(UIUserNotificationType.badge) ?? false
//                    var sound_ind = UIApplication.shared.currentUserNotificationSettings?.types.contains(UIUserNotificationType.sound) ?? false
//                }
                
                
                var badges_ind = false //notificationType.contains(.badge) ? true : false
                var sound_ind = false //notificationType.contains(.sound) ? true : false
                
                UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                    if settings.authorizationStatus == .authorized {
                        // Notifications are allowed
                        badges_ind = true
                        sound_ind = true
                    }
                    else {
                        // Either denied or notDetermined
                    }
                }
                
                //let user_id: Int = 1725
                let sso_user_nm = ShareData.sharedInstance.userName
                
                var apns_token:String = ""
                if(UserDefaults.standard.object(forKey: "apns_token") != nil) {
                    apns_token = (UserDefaults.standard.object(forKey: "apns_token") as! String?)!
                } else if (AppSettings.isSimulator == true){
                    apns_token = devDeviceToken
                }
                
                var request = URLRequest(url: url)
                
                request.httpMethod = "POST"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                
                if let accessToken = UserDefaults.standard.object(forKey: "accessToken") as! String? {
                    if accessToken.count > 0 {
                        request.setValue(accessToken, forHTTPHeaderField: "Authorization")
                    }
                }
                
                let myUrlTrimmed = myUrl.components(separatedBy: "://")
                let myUrlArray = myUrlTrimmed[1].components(separatedBy: "-")
                let server_nm = myUrlArray[0]
                
                //print("sso_user_nm=>" + sso_user_nm!)
                //print("user_id=>" + String(user_id))
                //print("apns_token=>" + apns_token)
                //print("server_nm=>" + server_nm)
                let data = ["company_id":company_id,
                              //"user_id":user_id,
                              "sso_user_nm":sso_user_nm! as String,
                              "device_type":"apple",
                              "device_token":apns_token as String,
                              "server_nm":server_nm,
                              "badges_ind":badges_ind,
                              "sound_ind":sound_ind,
                              "active_ind":active_ind
                    ] as Dictionary<String, Any>
                
                let params = ["endpoint": "/api/user_devices",
                              "method": "post",
                              "data": data] as Dictionary<String, Any>
                
                //var error: NSError?
                var bodyData: NSData!
                
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted)

                    bodyData = jsonData as NSData
                    
                    if (try JSONSerialization.jsonObject(with: jsonData as Data, options: []) as? NSDictionary) != nil {
                        //print("register device=>")
                        //print(jsonData)
                    } else {
                        // here "jsonData" is the dictionary encoded in JSON data
                    }
                    
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
                request.httpBody = bodyData as Data?
                
                if let task = self.session?.dataTask(with: request) {
                    // start the task, tasks are not started by default
                    task.resume()
                }
            }
            
            // Open the operations queue after 1 second
            DispatchQueue.main.asyncAfter(deadline: self.time, execute: {[weak self] in
                //print("Opening the OperationQueue")
                self?.opQueue.isSuspended = false
            })
        }
    }
    
    func getNotifications(page_id: String = "1") {//(user_id: String, user_name: String, page_id: String) {
        /*
         GET /api/notifications - get paginated notifications
         user_id
         sso_user_nm
         page_id  - starts from 1
         */
        let isLocalUrl = myUrl.contains("local-social")
        if(notificationsEnabled == true && (UserDefaults.standard.object(forKey: "apns_token") != nil) && !isLocalUrl) {
            // suspend the OperationQueue (operations will be queued but won't get executed)
            self.opQueue.isSuspended = true
            let sessionConfiguration = URLSessionConfiguration.default
            // disable the caching
            sessionConfiguration.urlCache = nil
            
            // initialize the URLSession
            self.session = URLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: self.opQueue)
            
            // create a URLRequest with the given URL and initialize a URLSessionDataTask
            
            let urlPath = myUrl + "/api/notificationproxy?nativeAction=getNotifications"
            if let url = URL(string: urlPath) {
                
//                let notificationType = UIApplication.shared.currentUserNotificationSettings!.types
                
                let user_id: Int = 1725
                let sso_user_nm = ShareData.sharedInstance.userName
                
                var apns_token: String = ""
                if(UserDefaults.standard.object(forKey: "apns_token") != nil) {
                    apns_token = (UserDefaults.standard.object(forKey: "apns_token") as! String?)!
                } else {
                    apns_token = devDeviceToken
                }
                
//                let badges_ind = notificationType.contains(.badge) ? true : false
//                let sound_ind = notificationType.contains(.sound) ? true : false
                
                var request = URLRequest(url: url)
                
                request.httpMethod = "POST"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                
                if let accessToken = UserDefaults.standard.object(forKey: "accessToken") as! String? {
                    if accessToken.count > 0 {
                        request.setValue(accessToken, forHTTPHeaderField: "Authorization")
                    }
                }
                
                //print("sso_user_nm=>" + sso_user_nm!)
                //print("user_id=>" + String(user_id))
                //print("apns_token=>" + apns_token)
                let data = ["sso_user_nm":sso_user_nm! as String,
                    "user_id":user_id,
                    "page":1
                    ] as Dictionary<String, Any>
                
                let params = ["endpoint": "/api/notifications",
                              "method": "get",
                              "data": data] as Dictionary<String, Any>
                
                //var error: NSError?
                var bodyData: NSData!
                
                do {
                    if let jsonData = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted) as? NSData {
                        bodyData = jsonData
                        /*
                        if let jsonResult = try JSONSerialization.jsonObject(with: jsonData as Data, options: []) as? NSDictionary {
                            //print(jsonResult)
                        } else {
                            // here "jsonData" is the dictionary encoded in JSON data
                        }
                        */
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
                request.httpBody = bodyData as Data?
                
                if let task = self.session?.dataTask(with: request) {
                    // start the task, tasks are not started by default
                    task.resume()
                }
            }
            
            // Open the operations queue after 1 second
            DispatchQueue.main.asyncAfter(deadline: self.time, execute: {[weak self] in
                //print("Opening the OperationQueue")
                self?.opQueue.isSuspended = false
            })
        }
    }
    
    func getUserData() {
        var returnJSON: NSDictionary!

            //print("Request User Data")
            // suspend the OperationQueue (operations will be queued but won't get executed)
            self.opQueue.isSuspended = true
            let sessionConfiguration = URLSessionConfiguration.default
            // disable the caching
            sessionConfiguration.urlCache = nil
            
            // initialize the URLSession
            self.session = URLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: self.opQueue)
        
            let ssoUrl = ShareData.sharedInstance.myUrl
            let compId = ShareData.sharedInstance.company_id
            let urlPath = ssoUrl! + "/api/user/" + compId!
        
            //print("urlPath=>" + urlPath)
            if let url = URL(string: urlPath) {
                //                // Open the operations queue after 1 second
                //                DispatchQueue.main.asyncAfter(deadline: self.time, execute: {[weak self] in
                //                    //print("Opening the OperationQueue")
                //                    self?.opQueue.isSuspended = false
                //                })
                let urlRequest: NSMutableURLRequest = NSMutableURLRequest(url: url)
                urlRequest.setValue(bsValue, forHTTPHeaderField: bsKey)
                let session = URLSession.shared
                let task = session.dataTask(with: urlRequest as URLRequest) {
                    (data, response, error) -> Void in
                    
                    let httpResponse = response as! HTTPURLResponse
                    let statusCode = httpResponse.statusCode
                    
                    if (statusCode == 200) {
                        //print("success")
                        
                        do{
                            
                            returnJSON = try JSONSerialization.jsonObject(with: data! as Data, options:[]) as? NSDictionary
                            
                            
                        }catch {
                            //print("Error with Json: \(error)")
                        }
                        
                        if returnJSON != nil {
                            //print(returnJSON)
                            if (returnJSON["unread_count"] as? Int) != nil {
                                
                                //print("returnJSON[\"unreadCount\"] found")
                                //print(unreadCount)
                            }
                            self.delegate?.notificationsListRetreived!(notifications: returnJSON)
                        }
                    } else {
                        //print("Load error getting notifications")
                    }
                }
                
                task.resume()
            }
        }
}
    extension MobileNotification:URLSessionDelegate {
        
        func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
            // We've got a URLAuthenticationChallenge - we simply trust the HTTPS server and we proceed
            //print("didReceive challenge")
            completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
        }
        
        func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
            // We've got an error
            if error != nil {
                //print("Error: \(err.localizedDescription)")
            } else {
                //print("Error. Giving up")
            }
//            PlaygroundPage.current.finishExecution()
        }
    }
    
    extension MobileNotification:URLSessionDataDelegate {
        
        func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didBecome streamTask: URLSessionStreamTask) {
            // The task became a stream task - start the task
            //print("didBecome streamTask")
            streamTask.resume()
        }
        
        func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didBecome downloadTask: URLSessionDownloadTask) {
            // The task became a download task - start the task
            //print("didBecome downloadTask")
            downloadTask.resume()
        }
        
        func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
            // We've got a URLAuthenticationChallenge - we simply trust the HTTPS server and we proceed
            //print("didReceive challenge")
            completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
        }
        
        func urlSession(_ session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: @escaping (URLRequest?) -> Void) {
            // The original request was redirected to somewhere else.
            // We create a new dataTask with the given redirection request and we start it.
            if (request.url?.absoluteString) != nil {
                //print("willPerformHTTPRedirection to \(urlString)")
            } else {
                //print("willPerformHTTPRedirection")
            }
            if let task = self.session?.dataTask(with: request) {
                task.resume()
            }
        }
        
        func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
            // We've got an error
            if let err = error {
                print("Error: \(err.localizedDescription)")
            } else {
                //print("didCompleteWithError no error")
            }
//            PlaygroundPage.current.finishExecution()
        }
        
        func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
            // We've got the response headers from the server.
            //print("didReceive response")
            self.response = response
            completionHandler(URLSession.ResponseDisposition.allow)
        }
        
        func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
            // We've got the response body
            //print("didReceive data")
//            if let responseText = String(data: data, encoding: .utf8) {
//                print(self.response ?? "")
//                print("\nServer's response text")
//                print(responseText)
//            }
            
            var returnJSON: NSDictionary!
            
            do {
//                returnJSON = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                returnJSON = try JSONSerialization.jsonObject(with: data as Data, options:[]) as? NSDictionary
                
                
//                if let firstCategory = json["categories"]??[0]["name"] as? String {
//                    //print("The first category in the JSON list is \"\(firstCategory)\"")
//                }
            } catch let error as NSError {
                print("Error parsing JSON: \(error.localizedDescription)")
            }
            
            if returnJSON != nil {
                //print(returnJSON)
                if let unreadCount = returnJSON["unread_count"] as? Int {
                    
                    print("returnJSON[\"unreadCount\"] found")
                    print(unreadCount)
                    self.delegate?.notificationsListRetreived!(notifications: returnJSON)
                }
            }
            
            self.session?.finishTasksAndInvalidate()
//            PlaygroundPage.current.finishExecution()
        }
    }
    
//    PlaygroundPage.current.needsIndefiniteExecution = true
//    let requester = MobileNotification()
//    requester.registerDevice()

