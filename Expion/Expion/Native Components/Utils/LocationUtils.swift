//
//  LocationUtils.swift
//  Expion
//
//  Created by Mike Bagwell on 5/4/16.
//  Copyright © 2016 Expion. All rights reserved.
//

import Foundation

func forceLocListSingle(_ locList: Array<Dictionary<String, AnyObject>>) -> (Array<Dictionary<String, AnyObject>>, String) {
    let newLocList: Array<Dictionary<String, AnyObject>> = []
    var numSelected: Int = 0
    var titleTxt: String = "Select Locations"
    for var item in locList {
        if item["selected"] as? String == "1" {
            if(numSelected == 0) {
                numSelected += 1
                if(numSelected == 1) {
                    titleTxt = (item["name"] as? String)!
                } else {
                    titleTxt = String(numSelected) + " Locations"
                }
            } else {
                item.updateValue("0" as AnyObject, forKey: "selected")
            }
        }
    }
    return (newLocList, titleTxt)
}

func getTitleFromLocList(_ locList: Array<Dictionary<String, AnyObject>>) -> String {
    var numSelected: Int = 0
    var titleTxt: String = "Select Locations"
    for item in locList {
        if item["selected"] as? String == "1" {
            numSelected += 1
            if(numSelected == 1) {
                titleTxt = (item["name"] as? String)!
            } else {
                titleTxt = String(numSelected) + " Locations"
            }
        }
    }
    return titleTxt
}

func getSelectedFromLocList(_ locList: Array<Dictionary<String, AnyObject>>) -> Array<Int> {
    var selectedLocs: Array<Int> = []
    for item in locList {
        if item["selected"] as? String == "1" {
            if let idInt = item["id"] as? Int {
                selectedLocs.append(idInt)
            } else if let idStr = item["id"] as? String {
                selectedLocs.append(Int(idStr)!)
            }
        }
    }
    return selectedLocs
}

func validationCheck(_ locList: Array<Dictionary<String, AnyObject>>) -> Bool {
    var hasSelected: Bool = false
    for item in locList {
        if hasSelected == false || item["selected"] as? String == "1" {
            hasSelected = true
        }
    }
    return hasSelected
}
