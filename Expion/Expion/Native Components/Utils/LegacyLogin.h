//
//  LegacyLogin.h
//  Expion
//
//  Created by Mike Bagwell on 6/19/17.
//  Copyright © 2017 Expion. All rights reserved.
//

#ifndef LegacyLogin_h
#define LegacyLogin_h

#import "KeychainItemWrapper.h"

@protocol LegacyLoginDelegate <NSObject>
@optional
- (void)legacyLoginComplete:(NSString *)aToken companyId:(NSString *)cId;
- (void)legacyLoginFailed:(NSString *)errorMessage;

//- (void)jankAccessWebLoad:(NSString *)tokenUrl;
@end

@interface LegacyLogin : NSObject {
    BOOL debugMode;
    
    NSString *enteredUsername;
    NSString *enteredPassword;
    
    NSString *hostDomain;
    NSString *urlDomain;
    
    NSURL *requestURL;
    
    NSString *company_id;
    NSString *userName;
    
    NSString *userAgent;
    
    NSURLConnection *homeConnection;
    NSMutableData *responseData;
    NSURL *responseURL;
    
    BOOL isAdmin;
    BOOL canViewPublish;
    BOOL canViewContent;
    BOOL canMobileSuggestedContent;
    BOOL canViewModeration;
    BOOL canMobileQuickDraft;
    BOOL canMobileUpload;
    BOOL canMobileMediaCreate;
    BOOL canMobileMediaApprove;
    
    BOOL canMediaGallery;
    BOOL canFBAlbumImagePublish;
    BOOL canExpionImage;
    BOOL canViewExpionImage;
    
    BOOL isEImages;
    
    __weak id<LegacyLoginDelegate> delegate;
}

@property (nonatomic, assign) BOOL debugMode;

@property (nonatomic, retain) NSString *enteredUsername;
@property (nonatomic, retain) NSString *enteredPassword;

@property (nonatomic, retain) NSString *hostDomain;
@property (nonatomic, retain) NSString *urlDomain;

@property (nonatomic, retain) NSURL *requestURL;

@property (nonatomic, retain) NSString *company_id;
@property (nonatomic, retain) NSString *userName;

@property (nonatomic, retain) NSString *userAgent;

@property (nonatomic, retain) NSURLConnection *homeConnection;
@property (nonatomic, retain) NSURL *responseURL;

@property (nonatomic, assign) BOOL isAdmin;
@property (nonatomic, assign) BOOL canViewPublish;
@property (nonatomic, assign) BOOL canViewContent;
@property (nonatomic, assign) BOOL canMobileSuggestedContent;
@property (nonatomic, assign) BOOL canViewModeration;
@property (nonatomic, assign) BOOL canMobileQuickDraft;
@property (nonatomic, assign) BOOL canMobileUpload;
@property (nonatomic, assign) BOOL canMobileMediaCreate;
@property (nonatomic, assign) BOOL canMobileMediaApprove;
@property (nonatomic, assign) BOOL canMediaGallery;
@property (nonatomic, assign) BOOL canFBAlbumImagePublish;
@property (nonatomic, assign) BOOL canExpionImage;
@property (nonatomic, assign) BOOL canViewExpionImage;
@property (nonatomic, assign) BOOL isEImages;

@property (weak, nonatomic) id<LegacyLoginDelegate> delegate;

- (void)legacyBegin:(NSString *)eUsername :(NSString *)ePW :(NSString *)eUrl;

@end

#endif /* LegacyLogin_h */
