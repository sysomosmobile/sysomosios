//
//  LegacyLogin.m
//  Expion
//
//  Created by Mike Bagwell on 6/19/17.
//  Copyright © 2017 Expion. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Expion-Swift.h"
#import "LegacyLogin.h"

//#import "Expion-Swift.h"

@interface LegacyLogin () <NSURLSessionDataDelegate>

@property (nonatomic, strong, readwrite) NSURLSession * session;
@end

@implementation LegacyLogin

@synthesize debugMode;


@synthesize homeConnection;
@synthesize responseURL;

@synthesize enteredUsername, enteredPassword;
@synthesize hostDomain, urlDomain;


@synthesize company_id;
@synthesize userAgent;
@synthesize isAdmin;
@synthesize canViewPublish;
@synthesize canViewContent;
@synthesize canMobileSuggestedContent;
@synthesize canViewModeration;
@synthesize canMobileQuickDraft;
@synthesize canMobileUpload;
@synthesize canMobileMediaCreate;
@synthesize canMobileMediaApprove,
canMediaGallery,
canFBAlbumImagePublish,
canExpionImage,
delegate;
@synthesize isEImages;


//---------------------------------
//* Start Old Login */
//---------------------------------
-(void)legacyBegin:(NSString *)eUsername :(NSString *)ePW :(NSString *)eUrl
{
    //NSLog(@"***-ssoBegin");
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
    
    NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
    NSString *version = [info objectForKey:@"CFBundleShortVersionString"];
    NSString *device = [NSString stringWithFormat:@"iPhone"];
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        device = [NSString stringWithFormat:@"iPad"]; /* Device is iPad */
    }
    
    userAgent = [NSString stringWithFormat:@"Expion-%@-%@", device, [version stringByReplacingOccurrencesOfString:@"." withString:@""]];
    
    debugMode = false;
    
    
    
    enteredUsername = [NSString stringWithFormat:@"%@", eUsername];
    enteredPassword = [NSString stringWithFormat:@"%@", ePW];
    
    hostDomain = [NSString stringWithFormat:@"%@", eUrl];
    urlDomain = [NSString stringWithFormat:@"https://%@", eUrl];
    //urlOauth = [NSString stringWithFormat:@""];
    
    //expionHostUrl = [NSString stringWithFormat:@"%@", eUrl];
    //expionDomainUrl = [NSString stringWithFormat:@"https://%@", eUrl];
    
    NSLog(@"***-hostDomain=>%@", hostDomain);
    NSLog(@"***-urlDomain=>%@", urlDomain);
    //NSLog(@"***-urlOauth=>%@", urlOauth);
    NSLog(@"***-enteredUsername=>%@", enteredUsername);
    NSLog(@"***-enteredPassword=>%@", enteredPassword);
    //NSLog(@"***-expionHostUrl=>%@", expionHostUrl);
    //NSLog(@"***-expionDomainUrl=>%@", expionDomainUrl);
    
    if(enteredUsername != nil && enteredPassword != nil && hostDomain != nil && urlDomain != nil){
        [self postLegacy];
    }
}

-(void)postLegacy
{
    NSString *usernameData = [@"username=" stringByAppendingString: enteredUsername];
    NSString *passwordData = [@"&password=" stringByAppendingString: enteredPassword];
    NSString *loginData = [usernameData stringByAppendingString:passwordData];
    NSString *postData = [loginData stringByAppendingString:@"&rememberme=False"];
    
    NSString *urlAsString = [NSString stringWithFormat:@"%@/Account/LogOn?ReturnUrl=%%2F", urlDomain];
    NSLog(@"!**-postLegacy-urlAsString=>%@", urlAsString);
    
    //NSLog(@"urlAsString=%@", urlAsString);
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];//@"CFBundleVersion"];//CFBundleShortVersionString =fix
    appVersion = [appVersion stringByReplacingOccurrencesOfString:@"." withString: @""];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlAsString] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0];
    
    NSData *postData2 = [postData dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu" , (unsigned long)[postData length]];
    NSURL *url = [NSURL URLWithString:urlAsString];
    requestURL = [NSURL URLWithString:urlAsString];
    [request setHTTPShouldHandleCookies:YES];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData2];
    [request setValue:userAgent forHTTPHeaderField:@"User-Agent"];
    
    
    [request setValue:@"application/json, text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8" forHTTPHeaderField: @"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField: @"Content-Type"];
    
    //[request setValue:cookieStr forHTTPHeaderField: @"Cookie"];
    [request setValue:@"1" forHTTPHeaderField: @"DNT"];
    [request setValue:hostDomain forHTTPHeaderField: @"Host"];
    [request setValue:urlDomain forHTTPHeaderField: @"Origin"];
    //[request setValue:[NSString stringWithFormat:@"%@/oauth/login.jsp", urlDomain] forHTTPHeaderField: @"Referer"];
    [request setValue:@"1" forHTTPHeaderField: @"Upgrade-Insecure-Requests"];
    
    //NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    //configuration.HTTPShouldSetCookies = YES;
    //configuration.HTTPCookieAcceptPolicy = NSHTTPCookieAcceptPolicyAlways;
    //    configuration.HTTPCookieStorage?.cookieacceptPolicy = NSHTTPCookieAcceptPolicyAlways;
    
    NSLog(@"xx--postLegacy-url=>%@", urlAsString);
    
    //NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    //NSURLSessionTask *task = [session dataTaskWithRequest:request];
    //[task resume];
    
    homeConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void)logOffExpion {
    NSString *urlAsString = [NSString stringWithFormat:@"%@/Account/LogOff?ReturnUrl=%%2F", urlDomain];
    //NSLog(@"urlAsString=%@", urlAsString);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlAsString] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0];
    
    homeConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
    return nil;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    responseData = [[NSMutableData alloc] init];
    [responseData setLength:0];
    responseURL = [response URL];
    
    //    NSLog(@"didrecieveresponse url=%@", [responseURL absoluteString]);
    
    ExpionVersion *exVer = [ExpionVersion new];
    exVer.vc = self;
    [exVer checkResponseUrl:[responseURL absoluteString]];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    NSString *strResult = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    //    NSLog(@"didReceiveData=>%@", strResult);
    [responseData appendData:data];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    //    NSLog(@"Connection failed! Error - %@ %@",[error localizedDescription], [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    
    /* TODO: Alert- failed
     dispatch_async(dispatch_get_main_queue(), ^(void){
     [self showAlert: @"Connection Error" :@"Expion could not be reached.  Please check network connection and try again"];
     
     //TODO: call login screen to kill loading
     loginLoadingView.hidden = YES;
     loginButton.enabled=YES;
     logoutButton.enabled=NO;
     });
     */
    
    [self.delegate legacyLoginFailed:@"Invalid username and password combination.  Please check your entries and try again"];
    
    // inform the user
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // Use responseData
    //NSLog(@"Succeeded! Received %lu bytes of data",(unsigned long)[responseData length]);
    
    if(responseURL){
        NSString *responseURLString = [responseURL absoluteString];
        NSLog(@"responseURLString=>%@", responseURLString);
        if([responseURLString rangeOfString:@"Home" options:NSCaseInsensitiveSearch].location != NSNotFound) {
            
            if(responseURLString != nil && responseURLString.length > 0) {
                company_id = [NSString stringWithFormat:@"%@", [[[[responseURLString componentsSeparatedByString:@"company_id="] objectAtIndex:1] componentsSeparatedByString:@"&"] objectAtIndex:0]];
                [self setUserCreds];
                //NSLog(@"setUserCreds done");
                [self saveUserDefaults];
                //NSLog(@"saveUserDefaults done");
                //[self storeToKeyChain];
                //NSLog(@"storeToKeyChain done");
                
                //[self loginComplete];
                //            [self startViewController];
                //loginPage.hidden = YES;
                //homePage.hidden = NO;
                /*
                 dispatch_async(dispatch_get_main_queue(), ^(void){
                 loginButton.enabled=YES;
                 logoutButton.enabled=NO;
                 });
                 */
                //[self showAlert: @"Login Success" :@"Login was successful."];
            }
        }else if([responseURLString rangeOfString:@"MobileVersion" options:NSCaseInsensitiveSearch].location != NSNotFound){
            //            NSLog(@"MobileVersion");
            //            NSLog(@"MobileVersion");
            
        }else if([responseURLString rangeOfString:@"LogOff" options:NSCaseInsensitiveSearch].location != NSNotFound){
            //            NSLog(@"LogOff!!! Do nothing?");
        }else if([responseURLString rangeOfString:@"LogOn" options:NSCaseInsensitiveSearch].location != NSNotFound){
            //            NSLog(@"LogOff!!! Do nothing?");
            [self.delegate legacyLoginFailed:@"Invalid username and password combination.  Please check your entries and try again"];
        }else{
            /* TODO: error loading
             dispatch_async(dispatch_get_main_queue(), ^(void){
             [self showAlert: @"Login Failed" :@"Please Check Username and Password"];
             loginButton.enabled=YES;
             logoutButton.enabled=NO;
             loginLoadingView.hidden = YES;
             });
             */
            [self.delegate legacyLoginFailed:@"Expion could not be reached.  Please check network connection and try again"];
        }
    }
}

-(void)loginComplete {
    
}

//---------------------------------
//* End NSURLConnection */
//---------------------------------

-(void)showAlert:(NSString *)alertTitle :(NSString *)alertMessage{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: alertTitle message: alertMessage delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [alert show];
    });
    //[alert release];
}

-(void)setUserCreds{
    isAdmin = NO;
    canViewPublish = NO;
    canViewContent = NO;
    canViewModeration = NO;
    canMobileUpload = NO;
    canMobileMediaCreate = NO;
    canMobileMediaApprove = NO;
    
    canMediaGallery = NO;
    canFBAlbumImagePublish = NO;
    canExpionImage = NO;
    canViewExpionImage = NO;
    canMobileQuickDraft = NO;
    isEImages = NO;
    
    //NSString* responseDataString= [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
    NSString* responseDataString= [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    if ([responseDataString rangeOfString:@"canViewPublish=true"].location != NSNotFound) {
        canViewPublish = YES;
    }
    
    if ([responseDataString rangeOfString:@"canMobileQuickDraft=true"].location != NSNotFound) {
        canMobileQuickDraft = YES;
    }
    if ([responseDataString rangeOfString:@"canViewContent=true"].location != NSNotFound) {
        canViewContent = YES;
    }
    if ([responseDataString rangeOfString:@"canViewModeration=true"].location != NSNotFound) {
        canViewModeration = YES;
    }
    if ([responseDataString rangeOfString:@"canImageAlbumUpload=true"].location != NSNotFound) {
        canMobileUpload = YES;
    }
    
    //same as canMobileUpload
    //canImageAlbumUpload
    
    if ([responseDataString rangeOfString:@"canMobileMediaCreate=true"].location != NSNotFound) {
        canMobileMediaCreate = YES;
    }
    if ([responseDataString rangeOfString:@"canMobileMediaApprove=true"].location != NSNotFound) {
        canMobileMediaApprove = YES;
    }
    if ([responseDataString rangeOfString:@"canMediaGallery=true"].location != NSNotFound) {
        canMediaGallery = YES;
    }
    if ([responseDataString rangeOfString:@"canFBAlbumImagePublish=true"].location != NSNotFound) {
        canFBAlbumImagePublish = YES;
    }
    if ([responseDataString rangeOfString:@"canExpionImage=true"].location != NSNotFound) {
        canExpionImage = YES;
    }
    if ([responseDataString rangeOfString:@"canViewExpionImage=true"].location != NSNotFound) {
        canViewExpionImage = YES;
        //NSLog(@"canViewExpionImage=true");
    } else {
        //NSLog(@"canViewExpionImage=true NOT FOUND!!!!");
    }
    //NSLog(@"expionName: %@", expionName);
    //NSLog (@"isAdmin= %@", isAdmin ? @"YES" : @"NO");
    //NSLog (@"canViewPublish= %@", canViewPublish ? @"YES" : @"NO");
    //NSLog (@"canViewContent= %@", canViewContent ? @"YES" : @"NO");
    //NSLog (@"canViewModeration= %@", canViewModeration ? @"YES" : @"NO");
    //NSLog (@"canMobileUpload= %@", canMobileUpload ? @"YES" : @"NO");
    //NSLog (@"canMobileMediaCreate= %@", canMobileMediaCreate ? @"YES" : @"NO");
    //NSLog (@"canMobileMediaApprove= %@", canMobileMediaApprove ? @"YES" : @"NO");
}


-(void)saveUserDefaults{
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setObject:urlDomain forKey:@"myUrl"];
    //NSLog(@"saveUserDefaults 1");
    //    [standardUserDefaults setObject:actionUrl forKey:@"actionUrl"];
    [standardUserDefaults setObject:company_id forKey:@"companyId"];
    //    [standardUserDefaults setObject:expionName forKey:@"expionName"];
    
    [standardUserDefaults setObject:enteredUsername forKey:@"userName"];
    [standardUserDefaults setObject:[NSNumber numberWithBool:debugMode] forKey:@"debugMode"];
    
    //[standardUserDefaults setObject:@"" forKey:@"accessToken"];
    
    //    [standardUserDefaults setBool:isAdmin forKey:@"isAdmin"];
    //    //NSLog(@"saveUserDefaults 2");
    //    [standardUserDefaults setBool:canViewPublish forKey:@"canViewPublish"];
    //    [standardUserDefaults setBool:canMobileSuggestedContent forKey:@"canMobileSuggestedContent"];
    //    [standardUserDefaults setBool:canMobileQuickDraft forKey:@"canMobileQuickDraft"];
    //    [standardUserDefaults setBool:canViewModeration forKey:@"canViewModeration"];
    //    [standardUserDefaults setBool:canMobileUpload forKey:@"canMobileUpload"];
    //    [standardUserDefaults setBool:canMobileMediaCreate forKey:@"canMobileMediaCreate"];
    //    [standardUserDefaults setBool:canMobileMediaApprove forKey:@"canMobileMediaApprove"];
    //
    //    [standardUserDefaults setBool:canMediaGallery forKey:@"canMediaGallery"];
    //    [standardUserDefaults setBool:canFBAlbumImagePublish forKey:@"canFBAlbumImagePublish"];
    //    [standardUserDefaults setBool:canExpionImage forKey:@"canExpionImage"];
    //    [standardUserDefaults setBool:canViewExpionImage forKey:@"canViewExpionImage"];
    //    //NSLog(@"saveUserDefaults 3");
    //    [standardUserDefaults setBool:isEImages forKey:@"isEImages"];
    
    //[standardUserDefaults setObject: cookiesData forKey: @"cookies"];
    //NSLog(@"saveUserDefaults sync");
    [standardUserDefaults synchronize];
    
    [self.delegate legacyLoginComplete:@"" companyId:company_id];
}



//-----------------------------------------------------//


@end
