//
//  MLStackClass.swift
//  Workspaces
//
//  Created by Mike Bagwell on 8/20/15.
//  Copyright (c) 2015 Expion. All rights reserved.
//

import Foundation

@objc(MLStackClass) class MLStackClass: ExpionBaseClass, LegacyPagedScrollViewControllerDelegate { //PagedCollectionViewControllerDelegate {
    
    var pagedScrollViewController: LegacyPagedScrollViewController!
//    var pagedScrollViewController: PagedCollectionViewController!
    
    var pagedNavigationController: UINavigationController!
    
    var delegate: LegacyPagedScrollViewControllerDelegate?
//    var delegate: PagedCollectionViewControllerDelegate?

    
    required init(){
        super.init()
    }
    
    required init(xOverride: String, settingsOverrride: ExpionSettingsBaseClass){
        super.init(xOverride: xOverride, settingsOverrride: settingsOverrride)
        delegate = super.settings.delegate as! CenterViewController
    }
    
    override func Execute() {
        mlItemSelectedNative(super.settings.mlItem!, loadExpion: true)
    }
    
    func logWindowMsg(_ logTxt: String, timeCompare: Bool) {
        delegate?.logWindowMsg!(logTxt, timeCompare: timeCompare)
    }
    
    func mlItemHasFinished() {
        delegate?.mlItemHasFinished()
    }
    
    func logoutBtnClicked() {
        delegate?.logoutBtnClicked()
    }
    
    func sessionTimedOut() {
        delegate?.sessionTimedOut()
    }
    
    func addStackMLStackClass(){
        self.pagedScrollViewController.addStackToLayout()
    }
    
    func mlItemSelectedNative(_ mlItem: MLItem? = nil, loadExpion: Bool) {
        DispatchQueue.main.async {
            super.settings.contentView.isHidden = false
        }
        
//        //print("MLStack->mlItemSelectedNative")
//        let cookies = HTTPCookieStorage.shared.cookies!
//        for cookie in cookies {
//            let key: String = "ASPXAUTH"
//            //print("---mlstack---")
//            //print(cookie.name)
//            //print(cookie)
//        }
//        //print("end mlstack cookies")
        
        if ((pagedScrollViewController) != nil){
            DispatchQueue.main.async {
                self.pagedScrollViewController.clearScrollView()
                self.pagedScrollViewController.willMove(toParentViewController: nil)
                self.pagedNavigationController.willMove(toParentViewController: nil)
            }
        }
        
        DispatchQueue.main.async {
            if (super.settings.contentView.subviews.count > 0){
                let subViews = super.settings.contentView.subviews
//                DispatchQueue.main.async {
                    for subview in subViews{
                        //println("remove subview - cv")
                        subview.removeFromSuperview()
                    }
//                }
            }
        }
        
        //println(String(format:"mlItemSelected=>"))
        
        DispatchQueue.main.async{
            
            var storyboard = UIStoryboard() //(name: "Main", bundle: nil)
            if #available(iOS 11, *) {
                storyboard = UIStoryboard(name: "Main", bundle: nil)
            } else {
                storyboard = UIStoryboard(name: "MainiOS10", bundle: nil)
            }
            
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            self.pagedScrollViewController = storyboard.instantiateViewController(withIdentifier: "LegacyPagedScrollViewController") as! LegacyPagedScrollViewController
//            self.pagedScrollViewController = storyboard.instantiateViewController(withIdentifier: "PagedCollectionViewController") as! PagedCollectionViewController
            
            self.pagedScrollViewController.delegate = self
            self.pagedScrollViewController.company_id = super.settings.company_id
            self.pagedScrollViewController.myUrl = super.settings.myUrl
            
            self.pagedScrollViewController.mlItem = mlItem
            if(mlItem != nil) {
                let mlId: Int = (mlItem?.id)!
                let layout_id: Int = (mlItem?.layout_id)!
                self.pagedScrollViewController.mlId = String(mlId)
                self.pagedScrollViewController.layout_id = String(layout_id)
            }
            self.pagedScrollViewController.loadExpion = super.settings.loadExpion
            
            //var psvcFrame: CGRect!
            self.pagedScrollViewController.view.frame = super.settings.contentView.frame
            
            self.pagedNavigationController = UINavigationController(rootViewController: self.pagedScrollViewController)
            self.pagedNavigationController.view.frame = super.settings.contentView.bounds
            self.pagedNavigationController.view.frame.size.height = super.settings.contentView.bounds.height
            
            self.pagedNavigationController.navigationBar.isHidden = true
            self.pagedNavigationController.isNavigationBarHidden = true
            self.pagedNavigationController.automaticallyAdjustsScrollViewInsets = false
            
            super.settings.contentView.addSubview(self.pagedNavigationController.view)
            
//            let contentView = super.settings.contentView as UIView
//            let contentViewFrame = contentView.frame
//            
//            let topLC : NSLayoutConstraint = NSLayoutConstraint(
//                item: self.pagedNavigationController.view, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal,
//                toItem: super.settings.contentView, attribute: NSLayoutAttribute.Top, multiplier: 1.0, constant: 0)
//            
//            let leadingLC : NSLayoutConstraint = NSLayoutConstraint(
//                item: self.pagedNavigationController.view, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal,
//                toItem: super.settings.contentView, attribute: NSLayoutAttribute.Leading, multiplier: 1.0, constant: 0)
//            
//            let trailingLC : NSLayoutConstraint = NSLayoutConstraint(
//                item: self.pagedNavigationController.view, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal,
//                toItem: super.settings.contentView, attribute: NSLayoutAttribute.Trailing, multiplier: 1.0, constant: 0)
//            
//            let bottomLC : NSLayoutConstraint = NSLayoutConstraint(
//                item: self.pagedNavigationController.view, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal,
//                toItem: super.settings.contentView, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 0)
//            
//            let widthLC : NSLayoutConstraint = NSLayoutConstraint(
//                item: self.pagedNavigationController.view, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal,
//                toItem: super.settings.contentView, attribute: NSLayoutAttribute.Width, multiplier: 1.0, constant: contentViewFrame.size.width)
//            
//            let heightLC : NSLayoutConstraint = NSLayoutConstraint(
//                item: self.pagedNavigationController.view, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal,
//                toItem: super.settings.contentView, attribute: NSLayoutAttribute.Height, multiplier: 1.0, constant: contentViewFrame.size.height)
//            
//            super.settings.contentView.addConstraints([topLC,leadingLC,trailingLC,bottomLC,widthLC,heightLC])
//            super.settings.contentView.addConstraints([topLC,leadingLC,trailingLC,bottomLC])
        }
    }
}
