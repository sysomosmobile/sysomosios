//
//  MLGalleryClass.swift
//  Workspaces
//
//  Created by Mike Bagwell on 9/8/15.
//  Copyright (c) 2015 Expion. All rights reserved.
//

import Foundation

@objc(MLGalleryClass) class MLGalleryClass: ExpionBaseClass {
    
    var galleryViewController: GalleryViewController!
    
    var delegate: LegacyPagedScrollViewControllerDelegate?
//    var delegate: PagedCollectionViewControllerDelegate?
    
    required init(){
        super.init()
    }
    
    required init(xOverride: String, settingsOverrride: ExpionSettingsBaseClass){
        super.init(xOverride: xOverride, settingsOverrride: settingsOverrride)
        delegate = super.settings.delegate as! CenterViewController
    }
    
    override func Execute() {
        //mlItemSelectedNative(super.settings.mlItem!, loadExpion: true)
        launchGallery("approval")
    }
    
    func mlItemHasFinished() {
        delegate?.mlItemHasFinished()
    }
    
    func logoutBtnClicked() {
        delegate?.logoutBtnClicked()
    }
    
    func sessionTimedOut() {
        delegate?.sessionTimedOut()
    }
    
    func launchGallery(_ displayMode: String){
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "GalleryView", bundle: nil)
            self.galleryViewController = storyboard.instantiateViewController(withIdentifier: "GalleryViewController") as! GalleryViewController
            self.galleryViewController.displayMode = displayMode
            let frame = super.settings.contentView.bounds
            //print("++launchGallery++")
            //print(String(describing: frame.size.width))
            //print(String(describing: frame.size.height))
            self.galleryViewController.view.frame = super.settings.contentView.bounds
            
            //Add to CenterViewController
            super.settings.delegate.addChildViewController(self.galleryViewController)
            
            super.settings.contentView.addSubview(self.galleryViewController.view)
            self.galleryViewController.didMove(toParentViewController: super.settings.delegate)
        }
    }
}

@objc(MLGalleryUploadClass) class MLGalleryUploadClass: ExpionBaseClass {
    
    //var pagedScrollViewController: PagedScrollViewController!
    //var pagedNavigationController: UINavigationController!
    
    var galleryViewController: GalleryViewController!
    
    var delegate: LegacyPagedScrollViewControllerDelegate?
//    var delegate: PagedCollectionViewControllerDelegate?
    
    required init(){
        super.init()
    }
    
    required init(xOverride: String, settingsOverrride: ExpionSettingsBaseClass){
        super.init(xOverride: xOverride, settingsOverrride: settingsOverrride)
        delegate = super.settings.delegate as! CenterViewController
    }
    
    override func Execute() {
        //mlItemSelectedNative(super.settings.mlItem!, loadExpion: true)
        launchGallery("uploader")
    }
    
    func mlItemHasFinished() {
        delegate?.mlItemHasFinished()
    }
    
    func logoutBtnClicked() {
        delegate?.logoutBtnClicked()
    }
    
    func sessionTimedOut() {
        delegate?.sessionTimedOut()
    }
    
    func launchGallery(_ displayMode: String){
        
        
        
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "GalleryView", bundle: nil)
            self.galleryViewController = storyboard.instantiateViewController(withIdentifier: "GalleryViewController") as! GalleryViewController
            self.galleryViewController.displayMode = displayMode
            self.galleryViewController.view.frame = super.settings.contentView.bounds
            
            let frame = super.settings.contentView.bounds
            //print("++launchGallery2++")
            //print(String(describing: frame.size.width))
            //print(String(describing: frame.size.height))
            
            
            //Add to CenterViewController
            super.settings.delegate.addChildViewController(self.galleryViewController)
            
            super.settings.contentView.addSubview(self.galleryViewController.view)
            self.galleryViewController.didMove(toParentViewController: super.settings.delegate)
        }
    }
}
