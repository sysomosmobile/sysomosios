//
//  ExpionBaseClass.swift
//  Workspaces
//
//  Created by Mike Bagwell on 6/11/15.
//  Copyright (c) 2015 Expion. All rights reserved.
//

import Foundation

@objc(ExpionBaseClass) class ExpionBaseClass: NSObject {
    
    var x: String!
    var settings: ExpionSettingsBaseClass!
    
    override required init(){
        
    }
    
    required init(xOverride: String, settingsOverrride: ExpionSettingsBaseClass){
        self.x = xOverride
        self.settings = settingsOverrride
        //println("ExpionBaseClass init x=>" + x)
    }
    
    func Execute(){
        //println("ExpionBaseClass Execute()")
    }
}

@objc(ExpionSettingsBaseClass) class ExpionSettingsBaseClass: NSObject {
    var company_id: String!
    var myUrl: String!
    var userAgent: String!
    var isPad: NSNumber!
    //var injectionScriptStr: String!
    
    var mlItem: MLItem!
    var delegate: UIViewController!
    var loadExpion: Bool!
    var debugMode: Bool!
    
    @IBOutlet weak var contentView: UIView!
    
    required init(company_id: String, myUrl: String, userAgent: String, isPad: NSNumber, mlItem: MLItem? = nil, contentView: UIView, delegate: UIViewController, loadExpion: Bool, debugMode: Bool){
        self.company_id = company_id
        self.myUrl = myUrl
        self.userAgent = userAgent
        self.isPad = isPad
        self.mlItem = mlItem
        self.contentView = contentView
        self.delegate = delegate
        self.loadExpion = loadExpion
        self.debugMode = debugMode
    }
}


