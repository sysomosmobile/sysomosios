//
//  MLWebViewClass.swift
//  Workspaces
//
//  Created by Mike Bagwell on 8/20/15.
//  Copyright (c) 2015 Expion. All rights reserved.
//

import Foundation


@objc(MLWebViewClass) class MLWebViewClass: ExpionBaseClass, StackViewFrameControllerDelegate {
    
    //var pagedScrollViewController: PagedScrollViewController!
    //var pagedNavigationController: UINavigationController!
    
    var delegate: StackViewFrameControllerDelegate?
    
    var loadExpion: Bool!
    //var settingsSring: String!
    //var injectionScriptStr: String!
    
    var stackPage: Int = 0
    
    required init(){
        super.init()
    }
    
    required init(xOverride: String, settingsOverrride: ExpionSettingsBaseClass){
        super.init(xOverride: xOverride, settingsOverrride: settingsOverrride)
        delegate = super.settings.delegate as! CenterViewController
    }
    
    override func Execute() {
        launchWebView(super.settings.mlItem!, loadExpion: true)
    }
    
    func logWindowMsg(_ logTxt: String, timeCompare: Bool) {
        delegate?.logWindowMsg!(logTxt, timeCompare: timeCompare)
    }
    
//    func logoutBtnClicked() {
//        delegate?.logoutBtnClicked()
//    }
//
//    func sessionTimedOut() {
//        delegate?.sessionTimedOut()
//    }
    
    func launchWebView(_ mlItem: MLItem, loadExpion: Bool) {
        
        var loadiFrameUrl: String!
        
        var storyboard = UIStoryboard() //(name: "Main", bundle: nil)
        if #available(iOS 11, *) {
            storyboard = UIStoryboard(name: "Main", bundle: nil)
        } else {
           storyboard = UIStoryboard(name: "MainiOS10", bundle: nil)
        }
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let webViewFrameCntlr: StackViewFrameController = storyboard.instantiateViewController(withIdentifier: "StackViewFrameController") as! StackViewFrameController
        //stackCntlr.dataSource = self
        webViewFrameCntlr.delegate = self

        //stackCntlr.view.frame = self.view.frame
        
        //println(String(format: "loadPage=>set defaults"))
        webViewFrameCntlr.isStack = false
        webViewFrameCntlr.company_id = super.settings.company_id
        webViewFrameCntlr.myUrl = super.settings.myUrl
        webViewFrameCntlr.loadiFrameUrl = mlItem.mlStack.directive.webUrl
        webViewFrameCntlr.loadExpion = loadExpion
        webViewFrameCntlr.stackPage = stackPage
        webViewFrameCntlr.mlId = String(super.settings.mlItem.id)
        webViewFrameCntlr.layoutId = String(super.settings.mlItem.layout_id)
        webViewFrameCntlr.settingsSring = super.settings.mlItem.settings
        webViewFrameCntlr.injectionScriptStr = "loadXPComponent('" + super.settings.mlItem.settings + "');"
//        webViewFrameCntlr.shouldInject = true
        DispatchQueue.main.async {
            let newStackFrame = CGRect(x: 0, y: 0, width: super.settings.contentView.bounds.width, height: super.settings.contentView.bounds.height)
            webViewFrameCntlr.view.frame = newStackFrame
            
            super.settings.delegate.addChildViewController(webViewFrameCntlr)
            
            super.settings.contentView.addSubview(webViewFrameCntlr.view)
            webViewFrameCntlr.didMove(toParentViewController: super.settings.delegate)
            
            webViewFrameCntlr.initializeWebView()
        }
        //super.settings.delegate.
    }
    
    func logoutTimedOut() {
        //delegate?.logoutBtnClicked()
    }
}
