//
//  StackViewFrameController.swift
//  Expion
//
//  Created by Mike Bagwell on 4/10/17.
//  Copyright © 2017 Expion. All rights reserved.
//

import UIKit
import WebKit
import Darwin

@objc
protocol StackViewFrameControllerDelegate {
    @objc optional func stackDetailFinishedLoading(_ sPage: Int, triggerDir: Bool)
    @objc optional func logoutTimedOut()
    @objc optional func collapseSidePanels()
    @objc optional func logWindowMsg(_ logTxt: String, timeCompare: Bool)
    @objc optional func stackInserted(_ sPage: Int, stackDetails: NSDictionary)
    @objc optional func stackAddedToLayout(_ sPage: Int, stack_id: Int)
    @objc optional func stackDeleted(_ sPage: Int)
    @objc optional func layoutUpdated(_ sPage: Int)
}

class StackViewFrameController: UIViewController, WKNavigationDelegate, JSONLocationDelegate, FlatTreeViewControllerDelegate, WKHTTPCookieStoreObserver {
    
    //@IBOutlet weak var contentView: UIView!
    @IBOutlet weak var frameLoadingView: UIView!
    @IBOutlet weak var frameContentView: UIView!
    @IBOutlet weak var frameLoadFailView: UIView!
    
    @IBOutlet weak var reloadButton: UIButton!
    @IBOutlet weak var frameLoadFailTextView: UITextView!
    
    @IBOutlet weak var stackPageLabel: UILabel!
    
    var stackTest: Bool = false
    
    var isStack: Bool = true
    var loadScriptInjected: Bool = false
    var preloadScriptReady: Bool = false
    var loadScriptAttemptCount: Int = 0
    var loadCompletedCalled: Bool = false
    var isLoadedCalled: Bool = false
    var loadScriptCompleteCount: Int = 0
    var webViewLoadAttemptCount: Int = 0
    var webViewLoadCompleteCount: Int = 0
    var webViewFinished: Bool = false
    var timeoutForceReload: Bool = false
    var stackDetailData: Array<StackDetailData> = []
    var stackDetailIndex: Int = 0
    
    var delegate: StackViewFrameControllerDelegate?
    
    var company_id: String!
    var myUrl: String!
    var loadiFrameUrl: String!
    var loadExpion: Bool!
    var stackId: String!
    var mlId: String!
    var layoutId: String!
    var settingsSring: String!
    var injectionScriptStr: String!
    var jsCallGuid: String = ""
    
    var stackPage: Int!
    var stackActive: Bool!
    
    lazy var detailRequestData = NSMutableData()
    
    var postItems: NSMutableArray = []
    
    var requestData: String!
    var vfwkWebView: WKWebView!
    
    var webLoadStart: Date!
    var debugMode: Bool = false
    
    var cookieGuid: String = ""
    var isProgressObservered: Bool = false
    
    lazy var htmlRequestData = NSMutableData()
    
    @IBOutlet weak var myProgressView: UIProgressView!
    var myContext = 0
    
    @IBAction func reloadAction(_ sender: UIButton){
        reloadStack()
    }
    
    var isiOS11Enabled: Bool = false
    
    func reloadStack() {
        dumpWebview()
        initializeWebView()
    }
    
    func stackBecameActive() {
        //print("SVF-\(self.stackPage!)-stackBecameActive")
        DispatchQueue.main.async {
            
            self.view.setNeedsDisplay()
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            
            self.frameContentView.setNeedsDisplay()
            self.frameContentView.setNeedsLayout()
            self.frameContentView.layoutIfNeeded()
            
            self.vfwkWebView.setNeedsFocusUpdate()
            self.vfwkWebView.setNeedsDisplay()
            self.vfwkWebView.setNeedsLayout()
            self.vfwkWebView.layoutIfNeeded()
        }
        if(timeoutForceReload == true){
            reloadStack()
        }
    }
    
    var timerActive: Bool!
    var myTimer: Timer!
    var loadAsWK: Bool = true
    
    var userString: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setAppVersion()
        setupLoadingView()
    }
    
    func setStackPageLabel(sPage: Int){
        DispatchQueue.main.async {
            self.stackPageLabel.text = String(sPage)
            self.stackPageLabel.isHidden = (ShareData.sharedInstance.debugMode == true ? false : true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        //print("!!!-stacViewFrame-\(self.stackPage!)- didReceiveMemoryWarning")
        // Dispose of any resources that can be recreated.
    }
    
    func dumpWebview() {
        if(stackTest == false) {
            if(loadAsWK == true && self.vfwkWebView != nil){
                //print("WW--loadAsWK")
                DispatchQueue.main.async {
                    self.removeProgressObserver()
                    self.vfwkWebView.loadHTMLString("", baseURL: nil)
                    self.vfwkWebView.stopLoading()
                    self.vfwkWebView.navigationDelegate = nil
                    self.vfwkWebView.removeFromSuperview()
                    self.vfwkWebView = nil
                }
            }
        }
    }
    
    func initializeWebView() {
        HTTPCookieStorage.shared.cookieAcceptPolicy = HTTPCookie.AcceptPolicy.always
        
        if #available(iOS 11.0, *) {
            print("SVF--ios11 available")
            isiOS11Enabled = true
        } else {
            print("SVF--ios11 !! NOT AVAILABLE")
        }
        
        setAppVersion()
        setupLoadingView()
        
        setStackPageLabel(sPage: self.stackPage)
        
        if(stackTest == true) {
            wvFinished()
            showLoadFailView(errorMsg: String(stackPage))
        } else {
        //        usleep(1420024)
        //        delegate?.stackDetailFinishedLoading!(stackPage, triggerDir: true)
        
            loadAsWkWebView()
        }
    }

    func injectScript(loadUrl: String){
        if(loadScriptInjected == false && injectionScriptStr != nil && injectionScriptStr.count > 0){
            if(debugMode == true) {
                if(webLoadStart != nil) {
                    webLoadStart = nil
                }
                webLoadStart = Date()
            }
            //print("-\(stackPage)-injectionScriptStr=>" + injectionScriptStr)
            //usleep(420024) //1000000 = 1 sec
            delegate?.logWindowMsg!(String(format:"SVF-\(self.stackPage!)-injectScript=>%@", ""), timeCompare: false)
//            vfWebView.stringByEvaluatingJavaScript(from: injectionScriptStr)
            loadScriptInjected = true
            loadScriptAttemptCount += 1
            self.hideLoading()
        }
    }
    
    func updateInjectScript(updateScript: String) {
        //usleep(1000000)
        //print("-\(stackPage)-updateInjectScript=>" + updateScript)
        injectionScriptStr = updateScript
        attemptUpdateInject()
    }
    
    func updateStackData(updateScript: String, updateStackId: String) {
        //usleep(1000000)
        //print("-\(stackPage)-updateInjectScript=>" + updateScript)
        injectionScriptStr = updateScript
        stackId = updateStackId
        if(self.preloadScriptReady == true) {
            injectStackScriptWK()
        }
    }
    
    func updateStackPage(sPage: Int) {
        stackPage = sPage
        setStackPageLabel(sPage: sPage)
    }
    
    var updateScriptRetry: Int = 0
    func attemptUpdateInject() {
        if(isLoadedCalled == true) {
            injectScript(loadUrl: "Doesn'tDoShit")//TODO:remove loadUrl from injectScript()
        }
    }
    
    func injectAlert(_ webView: WKWebView, loadUrl: String){
        let refreshAlert = UIAlertController(title: "js-", message: loadUrl, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Inject", style: .default, handler: { (action: UIAlertAction) in
            //println("@A@A@A--injectScript")
            self.injectScript(loadUrl: loadUrl)
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction) in
            //println("@A@A@A--Cancel Alert")
        }))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    
    func checkLoadUrl(loadUrl: String) -> Bool {
        DispatchQueue.main.async {
            UIView.animate(withDuration: ShareData.sharedInstance.transitionSpeed, animations: {
                self.frameLoadFailView.alpha = 0.0
            })
            self.frameLoadFailTextView.text = ""
            self.frameLoadFailView.isHidden = true
            self.frameLoadFailTextView.isHidden = true
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        print("WebView--loadUrl=>" + loadUrl)
        let testLoadUrl = loadUrl.lowercased()
        
        switch testLoadUrl {
            case let str where str.contains("logon"),
              let str where str.contains("logofff"),
              let str where str.contains("logout"): //let x where x.range(of: "logout") != nil:
                print("!!--!!--logout--!!--!!")
                delegate?.logoutTimedOut!()
                return false
            case let str where str.contains("sysomos.com/oauth/oauth/authorize?"):
                print("!!--!!--not authorized--!!--!!")
//                delegate?.logoutTimedOut!()
                return true
            case let str where str.contains("updatestackorder"):
                print("updatestackorder")
                break
            case let str where str.contains(String(format: "%@/ml/index", myUrl.lowercased())):
                loadScriptInjected = false
                reloadStack()
                return false
            case let str where str.contains(String(format: "%@/ml/componentloader", myUrl.lowercased())):
                loadScriptInjected = false
                return true
            case let str where str.contains("js-"):
                print("**** js- ****")
                handleJsBridge(loadUrl: loadUrl)
                return false
            default:
                break
        }
        
        return true
    }
    
    func handleJsBridge(loadUrl: String) {
        //injectAlert(webView, loadUrl: loadUrl)
        //            self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
        
        var jsCall = loadUrl.components(separatedBy: "::")//split(loadUrl){$0 == ":"}
        //let jsType: String = jsCall[0]
        let jsClassName: String = jsCall[1]
        let jsData: String = jsCall[2] //.decodeUrl()
        
        delegate?.logWindowMsg!(String(format:"SVF-\(self.stackPage!)-js-call:=>%@", jsClassName), timeCompare: false)
        
        print("!!-\(self.stackPage!)-jsClassName=>" + jsClassName)
        print("!!--js-call-jsData=>\(jsData)")
        
        if(jsClassName.range(of: "is_loaded") != nil) {
            isLoadedCalled = true
            
            let decodedData = Data(base64Encoded: jsData, options: NSData.Base64DecodingOptions(rawValue: 0))
            if let decodedString = NSString(data: decodedData!, encoding: String.Encoding.utf8.rawValue) {
                print("decodedString=>" + String(describing: decodedString))
            }
            
            //                let decodedData = jsData.data(using: String.Encoding.utf8, allowLossyConversion: false)
            
            if (decodedData != nil) {
                var jsCallJSON: NSDictionary!
                do {
                    jsCallJSON = try JSONSerialization.jsonObject(with: decodedData!, options: []) as? NSDictionary
                } catch {
                    print("fail")
                }
                
                print(jsCallJSON)
                
                if let guid = jsCallJSON["guid"] as? String {
                    //                        self.jsCallGuid = guid
                    cookieGuid = guid
                    //print("cookieGuid=>" + cookieGuid)
                }
                if (jsCallJSON["data"] as? String) != nil {
                }
            }
            
            
            if(loadScriptInjected == false) {
                self.showLoading()
                //                    usleep(420024)
                if(loadAsWK == false) {
                    injectScript(loadUrl: loadUrl)
                } else {
                    
                    if (isiOS11Enabled == true) {
                        injectNativeRequestCompletion()
                        //                            injectStackScriptWK()
                    } else {
                        injectCookieScriptWK()
                    }
                    //                        injectCookie()
                }
            } else {
                //print("!!=>is_loaded called stack-" + String(stackPage))
            }
        } else if(jsClassName.range(of: "load_completed") != nil) {
            loadScriptCompleteCount += 1
            //no need for this here if iOS can handle webViewDidFinishLoad
            delegate?.logWindowMsg!("SVF-\(self.stackPage!)- Render Complete-WebTime", timeCompare: true)
            delegate?.logWindowMsg!("SVF-\(self.stackPage!)- Render-Total Time", timeCompare: true)
            if(loadCompletedCalled == false) {
                loadCompletedCalled = true
            } else {
                //print("!!=>load_completed stack-" + String(stackPage) + " loadScriptAttemptCount-" + String(loadScriptCompleteCount))
            }
        } else if(jsClassName.range(of: "FlatTree") != nil) {
            decodeJSCallDataString(jsData)
            //                frameLoadingView.hidden = false
            //                requestData = jsData
            //                getLocJson()
        } else if(jsClassName.range(of: "IGMobile") != nil) {
            decodeJSCallDataStringIG(jsData)
        } else if(jsClassName.range(of: "stack_id_inserted") != nil) {
            decodeJSCallDataStringStackInserted(jsData)
            //                delegate?.stackInserted!(stackPage)
        } else if(jsClassName.range(of: "stack_deleted") != nil) {
            stackActive = false
            usleep(420024)
            delegate?.stackDeleted!(stackPage)
        } else if(jsClassName.range(of: "layout_updated") != nil && stackActive == true) {
            usleep(420024)
            //                delegate?.layoutUpdated!(stackPage)
        }
    }
    
    func wvFinished() {
        self.hideLoading()
        showContentView()
        delegate?.logWindowMsg!(String(format:"SVF--webViewDidFinishLoad=>%@", ""), timeCompare: false)

        webViewLoadCompleteCount+=1
        if(webViewFinished == false) {
            webViewFinished = true
            pvWebViewFinished()
        } else {

        }
    }
    
    func wvFailedLoad(error: Error) {
    
        let errorMsg: String = error.localizedDescription.count > 0 ? String(format:"An error occured: %@", error.localizedDescription) : ""
        showLoadFailView(errorMsg: errorMsg)
    }
    
    func showLoadFailView(errorMsg: String) {
        //print("errorMsg=>" + errorMsg)
        DispatchQueue.main.async {
            //self.frameLoadFailView.backgroundColor = UIColor.clear.withAlphaComponent(0.0)
            self.frameLoadFailTextView.text = errorMsg
            self.frameLoadFailView.isHidden = false
            self.frameLoadFailTextView.isHidden = false
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.timeoutForceReload = true
            //        UIView.animateWithDuration(1.0, animations: {
            //            self.frameLoadFailView.aplha = 1.0
            //        })
            
            UIView.animate(withDuration: ShareData.sharedInstance.transitionSpeed, animations: {
//                self.frameLoadFailView.backgroundColor = UIColor.clear.withAlphaComponent(1.0)
                self.frameLoadFailView.alpha = 1.0
            })
            //        if(loadScriptCompleteCount < 1){
            //            triggerReload(msg: errorMsg)
            //        }
        }
    }
    
    
    func connection(connection: NSURLConnection, canAuthenticateAgainstProtectionSpace protectionSpace: URLProtectionSpace) -> Bool{
        //print("canAuthenticateAgainstProtectionSpace method Returning True")
        return true
    }
    
    //-------------WKWebView--------------
    
    
    func setWKWebViewConfig() {
        
    }


    func loadAsWkWebView() {
//        self.setUpWKWebView()
//        self.setWKCookies()
        self.writeCookies()
    }
    
    func writeCookies() {
        if #available(iOS 11.0, *) {
            //You have to add self to httpCookieStore before creating wkwebview
            //This is currently a bug with apple
            //Do not refactor this until this apple bug is resolved
            WKWebsiteDataStore.default().httpCookieStore.add(self)
        } else {
            // Fallback on earlier versions
        }

        DispatchQueue.main.async {
            self.vfwkWebView = WKWebView(frame: self.frameContentView.bounds)
 
            if #available(iOS 11, *) {
                let cookiesStore = self.vfwkWebView.configuration.websiteDataStore.httpCookieStore
                let cookies = HTTPCookieStorage.shared.cookies!
                cookies.forEach { (cookie) in
    //                guard let cookie = HTTPCookie.init(properties: cookie) else {
    //                    return
    //                }
                    print("")
                    print(cookie.name)
                    print("attempt setCookie")
                    cookiesStore.setCookie(cookie, completionHandler: {
                        print ("!!WK--cookie added: \(cookie.name)")
                        print("-WK-")
                    })
                    print("")
                }
            }
            self.setUpWKWebView()
        }
    }
    
    @available(iOS 11.0, *) //fuck2
    func cookiesDidChange(in cookieStore: WKHTTPCookieStore) {
        print("Cookies did change")
//        cookieStore.getAllCookies { (cookies) in
//            print("Cookie exists?: \(cookies.contains(where: { $0.name == "OAuth.AccessToken" }))")
//        }
        DispatchQueue.main.async {
            cookieStore.getAllCookies { (cookies) in
                print("-------Cookie exists on main thread------")
                print(cookies)
                print("----cookies----")
            }
        }
    }
    
    
//    func cookiesDidChange(in cookieStore: WKHTTPCookieStore) {
//        DispatchQueue.main.async {
//            cookieStore.getAllCookies { cookies in
//                // Process cookies
////                print("!-WK-cookieDidChange")
////                print(cookies)
//            }
//        }
//    }
    
    func setUpWKWebView() {
        print("setUpWKWebView")
        
        DispatchQueue.main.async {
            self.vfwkWebView.navigationDelegate = self
            self.vfwkWebView.isOpaque = true //should be true
            //            self.vfwkWebView.backgroundColor = UIColor.clear
            self.vfwkWebView.scrollView.bounces = false
            self.vfwkWebView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: &self.myContext)
            self.isProgressObservered = true
            //            self.vfwkWebView.scalesPageToFit = true
            self.vfwkWebView.clipsToBounds = true
            self.vfwkWebView.scrollView.bounds = self.frameContentView.bounds
            self.vfwkWebView.scrollView.clipsToBounds = true
            
            if #available(iOS 11.0, *) {
                self.vfwkWebView.scrollView.contentInsetAdjustmentBehavior = .never;
            }
            //            self.frameContentView.backgroundColor = UIColor.clear
            self.frameContentView.addSubview(self.vfwkWebView)
            //
            //            //self.frameContentView.translatesAutoresizingMaskIntoConstraints = false
            self.vfwkWebView.translatesAutoresizingMaskIntoConstraints = false
            
            //Constrain webView to frameContentView
            let topLC : NSLayoutConstraint = NSLayoutConstraint(item: self.vfwkWebView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self.frameContentView, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0)
            let leadingLC : NSLayoutConstraint = NSLayoutConstraint(item: self.vfwkWebView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: self.frameContentView, attribute: NSLayoutAttribute.leading, multiplier: 1.0, constant: 0)
            let trailingLC : NSLayoutConstraint = NSLayoutConstraint(item: self.vfwkWebView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: self.frameContentView, attribute: NSLayoutAttribute.trailing, multiplier: 1.0, constant: 0)
            let bottomLC : NSLayoutConstraint = NSLayoutConstraint(item: self.vfwkWebView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self.frameContentView, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0)
            self.frameContentView.addConstraints([topLC,leadingLC,trailingLC,bottomLC])
            self.WkWebViewSetRequest()
        }
    }
    
    func WkWebViewSetRequest() {
        //        print("WW-loadAsWKWebView-stackPage=>\(stackPage)")
        stackActive = true
        timeoutForceReload = false
        //        URLCache.shared.removeAllCachedResponses()
        //        URLCache.shared.diskCapacity = 0
        //        URLCache.shared.memoryCapacity = 0
        
        var urlString: String!
        
        if(isStack == false && loadiFrameUrl != nil && loadiFrameUrl.count > 0) {
            urlString = loadiFrameUrl
        } else {
            urlString = myUrl + "/ML/ComponentLoader?company_id=" + company_id //+ (AppSettings.isPad() == true ? "&xTablet=True" : "&xMobile=True")  + "&StackPage=" + String(stackPage)
        }
        
        var stackUrlPath = URL(string: urlString)
        
        if(loadExpion == false){
            stackUrlPath = URL(string: "http://google.com")
        }
        //print("stackUrlPath=>\(urlString)")
        
        
        
        let cacheP: NSURLRequest.CachePolicy = .useProtocolCachePolicy //.reloadIgnoringLocalAndRemoteCacheData
        let request = NSMutableURLRequest(url: stackUrlPath!, cachePolicy: cacheP, timeoutInterval: 60.0)
        
        request.setValue(ShareData.sharedInstance.userAgent, forHTTPHeaderField:"User-Agent")
        
        
        let cookieStr: String = buildCookieStr()
        if(cookieStr.count > 0) {
            print("")
            print("cookieStr=>")
            print(cookieStr)
            print("")
            request.setValue(cookieStr, forHTTPHeaderField: "Cookie")
        }
        
        
        if let accessToken = UserDefaults.standard.object(forKey: "accessToken") as! String? {
            if accessToken.count > 0 {
                request.setValue(accessToken, forHTTPHeaderField: "Authorization")
            }
        }
        
        loadWKWebviewRequest(request: request as URLRequest)
    }
    
    func loadWKWebviewRequest(request: URLRequest) {
        DispatchQueue.main.async {
            self.vfwkWebView.load(request)
        }
    }
    
    func buildCookieStr() -> String {
        var cookieStr: String = ""
        let key: String = "ASPXAUTH"
        
        if let aspxCookie = UserDefaults.standard.object(forKey: key) as! String? {
            cookieStr.append(String(format: "%@=%@; ", ".ASPXAUTH", aspxCookie))
            print("loadAsWkWebView-cookieStr=>" + cookieStr)
        }
        
//        let cookies = HTTPCookieStorage.shared.cookies!
//        cookies.forEach { cookie in
//            if(cookie.name.contains("ls.access")
//                || cookie.name.contains("ASPXAUTH")
//                || cookie.name.contains("expion.co"))
//            {
//                cookieStr.append(String(format: "%@=%@; ", cookie.name, cookie.value))
//            }
//        }
        
        return cookieStr
    }
    
    //: MARK observer
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        guard let change = change else { return }
        if context != &myContext {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
        
        if keyPath == "estimatedProgress" {
            guard let progress = (change[NSKeyValueChangeKey.newKey] as AnyObject).floatValue else { return }
            print("----myProgressView=>\(progress)")
            DispatchQueue.main.async {
                self.myProgressView.setProgress(progress, animated: true)
//                        self.myProgressView.progress = progress
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                if self.myProgressView.progress == 1.0 {
                    self.myProgressView.isHidden = true
//                        self.removeProgressObserver()
                }
            }
        }
    }
    
    func removeProgressObserver() {
        if(isProgressObservered == true) {
            self.vfwkWebView.removeObserver(self, forKeyPath: "estimatedProgress")
        }
    }
    
    
    //----------------------------
    
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
//        //print("!!--didStartProvisionalNavigation--!!")
        pvStartProgressBar()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        //print("!!--didFail navigation--!!")
        wvFailedLoad(error: error)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping ((WKNavigationActionPolicy) -> Void)) {
        guard
            let url = navigationAction.request.url else {
            decisionHandler(.cancel)
            return
        }
        print("decidePolicyFor")
        print(url.absoluteString)
        if navigationAction.navigationType == WKNavigationType.linkActivated {
            print("---link---")
            
            if(!url.absoluteString.contains(ShareData.sharedInstance.myUrl)){
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            
            decisionHandler(.allow)
            return
        } else {
            print("--- not a link action ---")
            let shouldLoad: Bool = checkLoadUrl(loadUrl: (navigationAction.request.url?.absoluteString)!)
            if(shouldLoad == true) {
                decisionHandler(.allow)
                return
            }
            decisionHandler(.cancel)
            return
        }
    }
    
//    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Swift.Void) {
    //fuck1
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        guard
            let response = navigationResponse.response as? HTTPURLResponse,
            let url = navigationResponse.response.url
            else {
                decisionHandler(.cancel)
                return
        }
        
        
        
//        if let headerFields = response.allHeaderFields as? [String: String] {
//            let cookies = HTTPCookie.cookies(withResponseHeaderFields: headerFields, for: url)
//        let cookies = HTTPCookieStorage.shared.cookies!
//        if(cookies != nil) {
//            print("navigationResponse cookies")
//            cookies.forEach { (cookie) in
//                print("")
//                print(cookie)
//                HTTPCookieStorage.shared.setCookie(cookie)
//            }
//            print("")
//        }
        
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        wvFinished()
    }
    
    func reloadWKWebVeiw(){
        //print("reloadWKWebVeiw")
        vfwkWebView.reload()
    }
    
    func injectCookieScriptWK(){
        //print("!!--injectCookieScriptWK")
        let key: String = "ASPXAUTH"
        if let aspxCookie = UserDefaults.standard.object(forKey: key) as! String? {
            let injectionCookieStr = "document.cookie=\".ASPXAUTH=" + aspxCookie + "; path=/\""
            //print("injectionCookieStr=>" + injectionCookieStr)
            vfwkWebView.evaluateJavaScript(injectionCookieStr){ (result, error) in
                if error == nil {
                    // ***** JANK COOKIE SETTING ******
//                    self.injectCookie()
                    self.injectNativeRequestCompletion()
//                    self.injectStackScriptWK()
                    // ***** JANK COOKIE SETTING ******
                } else {
                    //print("error in evaluatejs=>\(String(describing: error))")
                }
            }
        }
    }
    
    var cookieInjected: Bool = false
    func injectNativeRequestCompletion() {
        print("wWwWW---injectNativeRequestCompletion")
        var injectionCookieStr: String = ""
        if #available(iOS 11.0, *) {
            
        } else {
            if let accessToken = UserDefaults.standard.object(forKey: "accessToken") as? String {
                injectionCookieStr = "document.cookie='ls.access={\"access_token\":\"" + accessToken + "\"}; path=/'; "
                self.cookieInjected = true
            }
        }
        let injectionCookieStrAdd = "window.completeNativeRequest(\"" + cookieGuid + "\", true)"
        injectionCookieStr.append(injectionCookieStrAdd)
        vfwkWebView.evaluateJavaScript(injectionCookieStr){ (result, error) in
            if error == nil {
                if(self.loadScriptInjected == false) {
                    self.showLoading()
                    usleep(420024)
                    self.injectStackScriptWK()
                } else {
                    //print("!!=>is_loaded called stack-" + String(stackPage))
                }
            } else {
                //print("error in evaluatejs=>\(String(describing: error))")
            }
        }
    }
    
    
    func injectStackScriptWK(){
        //print("injectStackScriptWK")
        if(loadScriptInjected == false && injectionScriptStr != nil && injectionScriptStr.count > 0){
            if(debugMode == true) {
                if(webLoadStart != nil) {
                    webLoadStart = nil
                }
                webLoadStart = Date()
            }
            print("")
            print("injectStackScriptWK-\(stackPage)-injectionScriptStr=>" + injectionScriptStr)
            print("")
//            usleep(420024) //1000000 = 1 sec
            usleep(50000)
            delegate?.logWindowMsg!(String(format:"SVF-\(self.stackPage!)-injectScript=>%@", ""), timeCompare: false)
            vfwkWebView.evaluateJavaScript(injectionScriptStr){ (result, error) in
                if error == nil {
                    self.loadScriptInjected = true
                    self.loadScriptAttemptCount += 1
                    self.hideLoading()
                } else {
                    //print("error in evaluatejs=>\(String(describing: error))")
                }
            }//(from: injectionScriptStr)
        } else {
            self.preloadScriptReady = true
            //print("no injectionScriptStr, waiting...")
        }
    }
    
    
    
    func dothings(){
        var injectionCookieStr: String = ""
        if (self.isiOS11Enabled == true) {
            
        } else {
            if let accessToken = UserDefaults.standard.object(forKey: "accessToken") as! String? {
                injectionCookieStr = "document.cookie='ls.access={\"access_token\":\"" + accessToken + "\"}; path=/'; "
            }
        }
        let injectionCookieStrAdd = "window.completeNativeRequest(\"" + cookieGuid + "\", true)"
        injectionCookieStr.append(injectionCookieStrAdd)

        vfwkWebView.evaluateJavaScript(injectionCookieStr){ (result, error) in
            if error == nil {
                if(self.loadScriptInjected == false) {
                    self.showLoading()
                    //                    usleep(420024)
                    
                    if (self.isiOS11Enabled == true) {
                        self.injectStackScriptWK()
                    } else {
                        self.injectCookieScriptWK()
                    }
                    //                        injectCookie()
                } else {
                    //print("!!=>is_loaded called stack-" + String(stackPage))
                }
            } else {
                //print("error in evaluatejs=>\(String(describing: error))")
            }
        }
    }
    
    //----------------------------
    
    func triggerReload(msg: String) {
        for index in 1...3 {
            let msgTxt = String(format: "%@ retrying in %@...", msg, String(index))
            self.frameLoadFailTextView.text = msgTxt
            sleep(1)
        }
        reloadStack()
    }
    
    func setAppVersion() {
        var appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        appVersion = appVersion.replacingOccurrences(of: ".", with: "", options: NSString.CompareOptions.literal, range: nil)
        UserDefaults.standard.register(defaults: ["User-Agent": ShareData.sharedInstance.userAgent])
    }
    
    func setupLoadingView() {
        DispatchQueue.main.async {
            if(self.stackTest == false) {
                self.frameLoadingView.isHidden = false
                self.frameContentView.isHidden = false
                self.frameLoadFailView.isHidden = true
            } else {
                self.frameLoadFailView.isHidden = false
                self.frameLoadFailTextView.text = String(self.stackPage)
            }
            
            self.frameLoadingView.frame = self.view.bounds
            self.frameContentView.frame = self.view.bounds
            self.frameLoadFailView.frame = self.view.bounds
        }
    }
    
    func showLoading() {/*
        //print("WW--showLoading")
        DispatchQueue.main.async {
            
            self.frameLoadingView.isHidden = false
            UIView.animate(withDuration: ShareData.sharedInstance.transitionSpeed, animations: {
                self.frameLoadingView.alpha = 1.0
//                self.frameContentView.alpha = 0.0
            })
            
        }*/
    }
    
    func hideLoading() {
        //print("WW--hideLoading")
        DispatchQueue.main.async {
            if(self.injectionScriptStr != nil && self.injectionScriptStr.count > 0) {
                UIView.animate(withDuration: ShareData.sharedInstance.transitionSpeed, animations: {
                    self.frameLoadingView.alpha = 0.0
                    self.frameLoadingView.isHidden = true
                })
            }
        }
    }
    
    func showContentView() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: ShareData.sharedInstance.transitionSpeed * 2, animations: {
                self.frameContentView.alpha = 1.0
            })
        }
    }
    
    //:MARK Instagram stuff ----------IG------
    func decodeJSCallDataString(_ jsDataStr: String){
        let decodedData = Data(base64Encoded: jsDataStr, options: NSData.Base64DecodingOptions(rawValue: 0))
        let decodedString = NSString(data: decodedData!, encoding: String.Encoding.utf8.rawValue)
        print("decodedString=>" + String(describing: decodedString))
        if (decodedData != nil) {
            parseJSData(decodedData!)
        }
    }
    
    func parseJSData(_ jsonData: Data) {
        var jsCallJSON: NSDictionary!
        do {
            jsCallJSON = try JSONSerialization.jsonObject(with: jsonData, options: []) as? NSDictionary
        } catch {
            
        }
        if let guid = jsCallJSON["guid"] as? String {
            self.jsCallGuid = guid
        }
        if let jsonDataDictStr = jsCallJSON["data"] as? String {
            let dataStr = jsonDataDictStr.data(using: String.Encoding.utf8, allowLossyConversion: false)
            if let dataDict = (try? JSONSerialization.jsonObject(with: dataStr!, options: [])) as? NSDictionary {
                var locsArray: Array<Int>!
                var locSource: String!
                if let locsArrayTemp = dataDict["selected_locs"] as? NSArray {
                    if locsArrayTemp.count > 0 {
                        if locsArrayTemp[0] is String {
                            locsArray = locsArrayTemp.map { Int($0 as! String)!}
                        } else {
                            locsArray = locsArrayTemp as! Array<Int>
                        }
                    }
                }
                if let sourcesStr = dataDict["sources"] as? String {
                    locSource = sourcesStr
                }
                getLocJson(locSource, selectedLocsArray: locsArray)
            }
        }
    }
    
    func decode(_ s: String) -> String? {
        let data = s.data(using: .utf8)!
        return String(data: data, encoding: .nonLossyASCII)
    }
    
    func decodeJSCallDataStringIG(_ jsDataStr: String){
//        if let decodedData = Data(base64Encoded: jsDataStr, options: NSData.Base64DecodingOptions(rawValue: 0)) {
//            if let decodedString = NSString(data: decodedData, encoding: String.Encoding.utf8.rawValue) {
//                print("decodedString=>" + String(describing: decodedString))
//            }
        let decodedString = jsDataStr.decodeUrl()
        print("decodedString=>" + String(describing: decodedString))
        
        if let decodedData = decodedString.data(using: String.Encoding.utf8, allowLossyConversion: false) {
            parseJSDataIG(decodedData)
        } else {
            print("String encode fail!!")
        }
    }
    
    func parseJSDataIGTestAPI(_ jsonData: Data){
        jsBridgeGetMessageData(messageId: "539367965") //539152659
    }
    
    func parseJSDataIG(_ jsonData: Data) {
        var jsCallJSON: NSDictionary!

        do {
            if let responseJSON = try JSONSerialization.jsonObject(with: jsonData, options: []) as? NSDictionary {
                //try JSONSerialization.jsonObject(with: jsonData) as? NSDictionary
                print("responseJSON success")
                print(responseJSON)
                jsCallJSON = responseJSON
            }
        } catch {
            let errorMsg = error.localizedDescription
            print("Failed to load: \(errorMsg)")
            if #available(iOS 11, *) {
                self.showAlert(title: "Message Load Error", message: errorMsg)
            } else {
                self.showAlert(title: "Message Load Error", message: "Please upgrade iOS to use this feature.")
            }
        }
        
        if(jsCallJSON != nil) {
            if let guid = jsCallJSON["guid"] as? String {
                self.jsCallGuid = guid
            }
            if let jsonDataDictStr = jsCallJSON["data"] as? String {
                let dataStr = jsonDataDictStr.data(using: String.Encoding.utf8, allowLossyConversion: false)
                if let dataDict = (try? JSONSerialization.jsonObject(with: dataStr!, options: [])) as? NSDictionary {
                    var imageUrl: String!
                    var videoUrl: String!
                    var msgTxt: String!
                    
                    var attachType: String!
                    
                    if let atTemp = dataDict["attach_type"] as? String {
                        attachType = atTemp
                    }
                    if let imgUrlTemp = dataDict["image_url"] as? String {
                        imageUrl = imgUrlTemp
                    }
                    if let vidUrlTemp = dataDict["video_url"] as? String {
                        videoUrl = vidUrlTemp
                    }
                    if let msgTxtTemp = dataDict["msg_txt"] as? String {
                        msgTxt = msgTxtTemp
                    } else {
                        msgTxt = ""
                    }
                    if(attachType == "image") {
                        loadInstagramImage(msgTxt: msgTxt, imageUrl: imageUrl)
                    }
                    if(attachType == "video") {
                        loadInstagramVideo(msgTxt: msgTxt, videoUrl: videoUrl)
                    }
                }
            }
        } else {
            print("could not convert to json object")
        }
    }
    
    func decodeJSCallDataStringStackInserted(_ jsDataStr: String){
        let decodedData = Data(base64Encoded: jsDataStr, options: NSData.Base64DecodingOptions(rawValue: 0))
//        let decodedString = NSString(data: decodedData!, encoding: String.Encoding.utf8.rawValue)
//        print("decodedString=>" + String(describing: decodedString))
        if (decodedData != nil) {
            parseJSDataStackInserted(decodedData!)
        }
    }
    
    func parseJSDataStackInserted(_ jsonData: Data) {
        var jsCallJSON: NSDictionary!
        do {
            jsCallJSON = try JSONSerialization.jsonObject(with: jsonData, options: []) as? NSDictionary
        } catch {
            
        }
//        //print("jsCallJSON=>")
//        //print(jsCallJSON)
//        //print("")
        
        if let guid = jsCallJSON["guid"] as? String {
            self.jsCallGuid = guid
        }
        if let jsonDataDictStr = jsCallJSON["data"] as? String {
            let dataStr = jsonDataDictStr.data(using: String.Encoding.utf8, allowLossyConversion: false)
            if let jsonData = (try? JSONSerialization.jsonObject(with: dataStr!, options: [])) as? NSDictionary {
                if let layoutData = jsonData["layoutData"] as? NSDictionary {
                    if let stack_id = layoutData["stack_id"] as? Int {
                        //print("stack_id success stackPage=\(stackPage)")
                        //print("stack_id found=>\(stack_id)")
                        delegate?.stackAddedToLayout!(stackPage, stack_id: stack_id)
                    }
                }
            }
        }
    }
    
    func jsBridgeGetMessageData(messageId: String) {
        if(messageId.count > 0){
            print("messageId=>")
            print(messageId)
            let urlString = String(format:"%@/api/stack/alldata/%@/%@", myUrl, messageId, company_id)
            print("urlString=>" + urlString)
            
            guard let url = URL(string: urlString) else { return }

            let urlRequest: NSMutableURLRequest = NSMutableURLRequest(url: url)
//            let session = URLSession.shared
            
                
            guard let accessToken = UserDefaults.standard.object(forKey: "accessToken") as! String? else { return }
                    //print("accessToken=>" + accessToken)
            if accessToken.count > 0 {
                //                    request.addValue(accessToken, forHTTPHeaderField: "Authorization")
                urlRequest.addValue(accessToken, forHTTPHeaderField: "authorization")
            }
            
            URLSession.shared.dataTask(with: url) { data, response, error in
                
                let httpResponse = response as! HTTPURLResponse
                let statusCode = httpResponse.statusCode
                
                if (statusCode == 200) {
                    guard let data = data, error == nil else { return }
                    print("message data=>")
                    let dataStr = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                    print(String(format: "!!~~dataStr=>%@", dataStr!))
                    print("")
                    print(httpResponse)
                }  else {
                    var returnJSON: NSDictionary!
                    
                    do {
                        returnJSON = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    } catch {
                        
                    }
                    
                    if returnJSON != nil {
                        if let errorMsg = returnJSON["Message"] as? String {
                            //print("^^^==errorMsg=>" + errorMsg)
                            self.showAlert(title: "Could not retrieve message data", message: errorMsg)
                        }
                    }
                }
            }.resume()
        }
    }
    
    func loadInstagramVideo(msgTxt: String?, videoUrl: String?) {
        DispatchQueue.main.async(execute: { () -> Void in
            if((videoUrl?.count)! > 0) {
                //print("videoUrl != nil")
//                InstagramManager.sharedManager.postVideoToInstagramFromDIC(videoUrlStr: videoUrl!, instagramCaption: msgTxt!, view: self.view)
                InstagramManager.sharedManager.postVideoToInstagramFromDIC(videoUrlStr: videoUrl!, instagramCaption: msgTxt!, view: self.view)
            } else {
                //print("videoUrl data nil!!")
            }
            
        })
    }
    
    func loadInstagramImage(msgTxt: String?, imageUrl: String?) {
        if(imageUrl != nil && (imageUrl?.count)! > 0){
            print("imageUrl=>")
            print(imageUrl ?? "nil")
            let urlString: String = ((imageUrl?.contains("http://"))! || (imageUrl?.contains("https://"))!) ? imageUrl! : (myUrl + imageUrl!)
//            let igImg: UIImage!
            print("urlString=>" + urlString)
            
            if let url = URL(string: urlString) {
                let urlRequest: NSMutableURLRequest = NSMutableURLRequest(url: url)
//                let session = URLSession.shared
                
                
                if let accessToken = UserDefaults.standard.object(forKey: "accessToken") as! String? {
                    //print("accessToken=>" + accessToken)
                    if accessToken.count > 0 {
                        //                    request.addValue(accessToken, forHTTPHeaderField: "Authorization")
                        urlRequest.addValue(accessToken, forHTTPHeaderField: "authorization")
                    }
                } else {
                    //print("!!!NO AUTH TOKEN")
                }
                
                guard let url = URL(string:urlString) else { return }
                URLSession.shared.dataTask(with: url) { data, response, error in
                    
                    let httpResponse = response as! HTTPURLResponse
                    let statusCode = httpResponse.statusCode
                    
                    if (statusCode == 200) {
                        guard let data = data, error == nil else { return }
                        let tmpURL = FileManager.default.temporaryDirectory
                            .appendingPathComponent(response?.suggestedFilename ?? "share.csv")
                        do {
                            try data.write(to: tmpURL)
                        } catch {
                            print("error downloading file")
                            print(error)
                        }
//                        DispatchQueue.main.async {
//                            self.share(url: tmpURL)
//                        }
                        InstagramManager.sharedManager.postImageToInstagramFromDIC(fileURL: tmpURL, instagramCaption: msgTxt!, view: self.view)
                    }  else {
                        var returnJSON: NSDictionary!
                        
                        do {
                            returnJSON = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        } catch {
                            
                        }
                        
                        if returnJSON != nil {
                            if let errorMsg = returnJSON["Message"] as? String {
                                //print("^^^==errorMsg=>" + errorMsg)
                                self.showAlert(title: "Image Load Error", message: errorMsg)
                            }
                        }
                    }
                }.resume()
            }
        }
    }
    
    let documentInteractionController = UIDocumentInteractionController()
    func share(url: URL) {
        documentInteractionController.url = url
        documentInteractionController.uti = url.typeIdentifier ?? "public.data, public.content"
        documentInteractionController.name = url.localizedName ?? url.lastPathComponent
        documentInteractionController.presentOptionsMenu(from: view.frame, in: view, animated: true)
    }
    
    func showAlert(title: String, message: String){
        //print("mlItemTimedOut")
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title: "okay", style: UIAlertActionStyle.default, handler: { action in self.reloadLayout()}))
            alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    //---------location stuff
    
    func getLocJson(_ source: String?, selectedLocsArray: Array<Int>?)
    {
        let jsonLoc: JSONLocation = JSONLocation()
        jsonLoc.company_id = company_id
        jsonLoc.myUrl = myUrl
        jsonLoc.enableNoLocSelect = isStack == true ? true : false
        jsonLoc.source = source
        jsonLoc.delegate = self
        
        if(selectedLocsArray != nil && (selectedLocsArray?.count)! > 0) {
            jsonLoc.selectedLocs = selectedLocsArray!
            //            locJSONReturned(jsonLoc)
        } else {
            
        }
        
        jsonLoc.getLocJSON()
    }
    
    func locJSONReturned(_ jsonLocReturn: JSONLocation) {
        jsonLocReturn.delegate = self
        //TODO: use jsonLocReturn to set loc data
        //        if(self.locationArray != nil){
        //            self.locationArray.removeAll()
        //        }
        //        var locationArray = jsonLocReturn.locationList
        showFlatLocTree(jsonLocReturn)
    }
    
    func showFlatLocTree(_ jsonLocReturn: JSONLocation) {
        var storyboard = UIStoryboard() //(name: "Main", bundle: nil)
        if #available(iOS 11, *) {
            storyboard = UIStoryboard(name: "Main", bundle: nil)
        } else {
            storyboard = UIStoryboard(name: "MainiOS10", bundle: nil)
        }
//        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let locSelect: FlatTreeViewController = storyboard.instantiateViewController(withIdentifier: "FlatTreeViewController") as! FlatTreeViewController
        
        locSelect.treeItemArray = jsonLocReturn.locationList //Array<Dictionary<String, AnyObject>>! //self.locationArray
        locSelect.canSelectMultiples = true
        locSelect.enableNoLocSelect = isStack == true ? true : false
        locSelect.delegate = self
        
        let nav = UINavigationController(rootViewController: locSelect)
        
        nav.navigationBar.isHidden = true
        nav.isNavigationBarHidden = true
        
        if(AppSettings.isPad() == true) {
            nav.modalPresentationStyle = UIModalPresentationStyle.formSheet
            nav.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            //            locSelect.modalPresentationStyle = UIModalPresentationStyle.FormSheet
            //            locSelect.modalTransitionStyle = UIModalTransitionStyle.CrossDissolve
        } else {
            //            locSelect.modalPresentationStyle = UIModalPresentationStyle.FormSheet
            //            locSelect.modalTransitionStyle = UIModalTransitionStyle.CrossDissolve
            nav.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            //            nav.modalTransitionStyle = UIModalTransitionStyle.CrossDissolve
        }
        
        //        self.presentViewController(locSelect, animated: true, completion: nil)
        DispatchQueue.main.async {
            self.view.window?.rootViewController!.present(nav, animated: false, completion: nil)
        }
        //        self.albumLocBtn.setTitle("  ...", forState: UIControlState.Normal)
    }
    
    func locViewControllerDismissed(_ returnedLocs: Array<Dictionary<String, AnyObject>>, withTitle titleTxt: String) { //, locSelect: FlatTreeViewController?){
        
        //        self.dismissViewControllerAnimated(true, completion: {
        //            locSelect?.delegate = nil
        //            self.delegate?.locViewControllerDismissed!(self.treeItemArray, withTitle: self.treeTitle)
        //            self.delegate = nil
        //        })
        
        self.hideLoading()
        
        //print("****locViewControllerDismissed--!!")
        
        let locUtils = LocationUtils()
        let locIdArray: Array<Int> = locUtils.getSelectedFromLocList(returnedLocs)
        
        let locDataDict: NSMutableDictionary = ["locationIds": locIdArray, "locationsTxt": titleTxt]
        let locData = try? JSONSerialization.data(withJSONObject: locDataDict, options: [])
        let locDataStr = NSString(data: locData!, encoding: String.Encoding.utf8.rawValue)
        
        let script: String = "completeNativeRequest('" + jsCallGuid + "'," + (locDataStr! as String) + ")"
        //print("****inject script=>" + script)
        self.vfwkWebView.evaluateJavaScript(script){ (result, error) in
            if error == nil {
                
            } else {
            
            }
        }
    }
    
    func pvStartProgressBar() {
//        DispatchQueue.main.async {
//            self.myProgressView.progress = 0.0
//        }
//        self.timerActive = false
//        self.myTimer = Timer.scheduledTimer(timeInterval: 0.01667, target: self, selector: #selector(StackViewFrameController.timerCallback), userInfo: nil, repeats: true)
    }
    
    func pvWebViewFinished() {
        //print("WW-0-0-pvWebViewFinished")
        self.timerActive = true
        if isStack == true {
            delegate?.stackDetailFinishedLoading!(stackPage, triggerDir: true)
        } else {
            //delegate?.mlItemHasFinished()
        }
        //showContentView()
    }
    
    //@objc
    @objc func timerCallback() {
//        DispatchQueue.main.async {
//
//            if (self.timerActive != nil) {
//                if self.myProgressView.progress >= 1 {
//                    self.myProgressView.isHidden = true
//                    self.myTimer.invalidate()
//                } else {
//                    self.myProgressView.progress += 0.1
//                }
//            } else {
//                self.myProgressView.progress += 0.05
//                if self.myProgressView.progress >= 0.95 {
//                    self.myProgressView.progress = 0.95
//                }
//            }
//
//        }
    }
    
    func clearContentView(){
        //println("clearContentView")
        
        let sViews = self.frameContentView.subviews
        for sView in sViews{
            //println("remove subview - clearContentView")
            sView.removeFromSuperview()
        }
    }
    
}

extension String
{
    func encodeUrl() -> String
    {
        return self.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
    }
    func decodeUrl() -> String
    {
        return self.removingPercentEncoding!
//        if #available(iOS 11.0, *) {
//            return self.removingPercentEncoding!
//        }
    }
}

extension URL {
    var typeIdentifier: String? {
        return (try? resourceValues(forKeys: [.typeIdentifierKey]))?.typeIdentifier
    }
    var localizedName: String? {
        return (try? resourceValues(forKeys: [.localizedNameKey]))?.localizedName
    }
}
