//
//  LegacyPagedScrollViewController.swift
//  Expion
//
//  Created by Mike Bagwell on 1/27/16.
//  Copyright © 2016 Expion. All rights reserved.
//

import UIKit

@objc
protocol LegacyPagedScrollViewControllerDelegate {
    @objc optional func collapseSidePanels()
    @objc optional func logWindowMsg(_ logTxt: String, timeCompare: Bool)
    func mlItemHasFinished()
    func logoutBtnClicked()
    func sessionTimedOut()
}

class LegacyPagedScrollViewController: UIViewController, UIScrollViewDelegate, StackViewFrameControllerDelegate { //StackDetailViewControllerDelegate { //, UIWebViewDelegate {
    
    @IBOutlet var scrollView: UIScrollView!
    var scrollContainerView: UIView = UIView(frame: CGRect.zero)
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var noStacksLbl: UILabel!
    @IBOutlet var noStacksLblView: UIView!
    
    var repeatLoad: Bool = false
    var shouldInject: Bool = true
    var stackMax: Int = 12
    var stackLayoutData: Array<StackLayoutData> = []
    var stackLayoutIndex: Int = 0
    
    var pageViews: [UIView?] = []
    var pageViewControllers: [StackViewFrameController?] = []
    //var stackXPos: CGFloat = 0
    var stackLayoutColumnWidth: Int = 0
    var delegate: LegacyPagedScrollViewControllerDelegate?
    //var colors: Array<UIColor>! = []
    
    var pageCount: Int = 0
    var pagesToShow: Int = 0
    var visiblePageLower: Int = 0
    var visiblePageUpper: Int = 0
    var pageCountThreshold: Int = 4
    
    var stack_detailsArray: Array<NSDictionary?> = []
    var stackIdArray: Array<String> = []
    
    var startContentOffset: CGFloat = 0.0
    
    var company_id: String!
    var myUrl: String!
    var layout_id: String!
    var mlItem: MLItem!
    var mlId: String!
    var settingsString: String!
    var debugMode: Bool = false
    
    var stackWidth: CGFloat!
    var loadExpion: Bool!
    var isDragging: Bool!
    
    var testUrlArray: Array<String> = []
    
    lazy var layoutRequestData = NSMutableData()
    var webViewPage: Int = 0
    
    var startIndex: Int = 0
    private var svTrailingLC: NSLayoutConstraint!
    private var svWidthLC: NSLayoutConstraint!
    private var svcWidthLC: NSLayoutConstraint!
    
    let serialQueue = DispatchQueue(label:"stackQueue")
    
    var shouldPreload: Bool = true
    //    var accessToken: String!
    let opQueue = OperationQueue()
    var response:URLResponse?
//    var session:URLSession?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ///Setup ScrollView
        delegate?.logWindowMsg!("LegacyPagedScrollViewController didLoad", timeCompare: false)
        scrollView.delegate = self
        DispatchQueue.main.async {
            self.pageControl.isHidden = true
        }
        
        let ramSize = ProcessInfo.processInfo.physicalMemory
        
        if(ramSize < 2000000000) {  //2105016320
            pageCountThreshold = 4
        } else {
            pageCountThreshold = 12
        }
    
        //print("ramSize=>\(ramSize)")
        //print("pageCountThreshold=>\(pageCountThreshold)")
        
        isDragging = false
        self.pageCount = 0
        stackWidth = (self.view.bounds.size.width > 700) ? 320 : self.view.bounds.size.width
        pagesToShow = Int(ceil(self.view.bounds.size.width / stackWidth))
        
        ///Get Stack Data
        //print("PVC--viewDidLoad")
        clearScrollView({
            self.getStackLayoutJSON()
            if(self.shouldPreload == true) {
                self.preloadStackLayout()
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        //print("!!!--LPVC didReceiveMemoryWarning")
        //print("")
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: get data methods
    func getStackLayoutJSON(){
        //print("getStackLayoutJSON")
        startConnection()
    }
    
    func startConnection(){
        let urlPath: String = String(format:"%@/api/stacklayout/%@?layout_id=%@", myUrl, company_id, layout_id)
        //print("getStackLayoutJSON=>\(urlPath)")
        guard let url = URL(string: urlPath) else {
            print("failed url")
            return
        }
        
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(url: url)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: urlRequest as URLRequest) {
            (data, response, error) -> Void in
            guard error == nil else {
                print("error in startConnection() to \(urlPath)")
                print(error!)
                return
            }
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            guard let httpResponse = response as? HTTPURLResponse else {
                print("Error: did not receive response")
                return
            }
            let statusCode = httpResponse.statusCode
            if (statusCode == 200) {
                self.layoutRequestData = NSMutableData()
                self.layoutRequestData.append(responseData)
                self.parseStackLayoutJSON()

            } else {
                //print("xx--fail--xx")
            }
        }
        task.resume()

    }
    
    func updateStacksOrder(){
//        //print("updateStacksOrder")
        // suspend the OperationQueue (operations will be queued but won't get executed)
//        self.opQueue.isSuspended = true
        
        let urlPath: String = String(format:"%@/api/stack/stackactions/UpdateStackOrder/%@/%@", myUrl, layout_id, company_id)
//        //print(urlPath)
        
        if (stackIdArray.count > 0) {
            if let url = URL(string: urlPath) {
    //            var request = URLRequest(url: url)
                
                let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
                
                request.httpMethod = "POST"
                request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                request.addValue("json", forHTTPHeaderField: "dataType")
                
                request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
                request.addValue(ShareData.sharedInstance.userAgent, forHTTPHeaderField: "User-Agent")
                
                if let accessToken = UserDefaults.standard.object(forKey: "accessToken") as! String? {
                    if accessToken.count > 0 {
                        request.setValue(accessToken, forHTTPHeaderField: "Authorization")
                    }
                }
                
                var data: Array<Dictionary<String, Int>> = []
                
                for i in 0 ..< stackIdArray.count {
                    let stackId = Int(stackIdArray[i])
                    let dict: Dictionary<String, Int> = ["stack_id" : stackId!, "sort_index" : i ]
                    data.append(dict)
                }
                
    //            //print("data=>")
    //            //print(data)
//                let params = ["data": data] as Dictionary<String, Any>
                
                //var error: NSError?
                var bodyData: NSData!
                
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
//                    if let jsonData = try JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted) as? NSData {
                        bodyData = jsonData as NSData
                        if (try JSONSerialization.jsonObject(with: jsonData as Data, options: []) as? NSDictionary) != nil {
    //                        //print("updateStacksOrder=>")
    //                        //print(jsonData)
                        } else {
                            // here "jsonData" is the dictionary encoded in JSON data
                        }
//                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
                request.httpBody = bodyData as Data?
                
                let session = URLSession.shared
                
                let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
//                    if let response = response {
//                        //print(response)
//                    }
                    if let data = data {
                        do {
//                            let json = try JSONSerialization.jsonObject(with: data, options: [])
//                            print(json)
                        } catch {
                            //print(error)
                        }
                    }
                })
                task.resume()
            }
        }
        
//        // Open the operations queue after 1 second
//        DispatchQueue.main.asyncAfter(deadline: self.time, execute: {[weak self] in
//            //print("Opening the OperationQueue")
//            self?.opQueue.isSuspended = false
//        })
        
    }
    
    func parseStackLayoutJSON(){
        //print("PVC--parseStackLayoutJSON=>")
        let string1 = String(data: layoutRequestData as Data, encoding: String.Encoding.utf8) ?? "Data could not be printed"
//        //print(string1)
        delegate?.logWindowMsg!("PVC parseStackLayoutJSON", timeCompare: false)
        var json: NSArray!
        do {
            json = try JSONSerialization.jsonObject(with: layoutRequestData as Data, options: []) as? NSArray
        } catch {
            //print("JSON Processing Failed")
        }
        
        if json != nil && json.count > 0 {
            if let layoutData = json[0] as? NSDictionary {
//                //print(String(format:"*J*S*O*N* json=>%@", json))
//                //print("-==-")
//                //print(String(format:"*J*S*O*N* layoutData=>%@", layoutData))
//                //print("")
                if let stack_detailsData = layoutData["stack_details"] as? NSArray {
                    
                    var stackLayoutColumnWidthUpdate = 0
                    //print("!!--before-stackLayoutColumnWidth=\(stackLayoutColumnWidth)")
                    var stack_detailsArrayUpdate: Array<NSDictionary?> = []
                    var stackIdArrayUpdate: Array<String> = []
                    
                    for stack_columns in stack_detailsData {
                        if let stack_detailsDict = stack_columns as? NSDictionary {
                            if let stack_detail_id = stack_detailsDict["id"] as? Int {
                                stack_detailsArrayUpdate.append(stack_detailsDict)
                                stackIdArrayUpdate.append(String(stack_detail_id))
                            }
//                            if let column_count = stack_detailsDict["column_count"] as? Int {
//                                print("!!--column_count=\(column_count)")
//                                stackLayoutColumnWidthUpdate += column_count
//                            }
                            stackLayoutColumnWidthUpdate += 1
                        }
                    }
                    
                    self.stack_detailsArray.removeAll()
                    self.stackIdArray.removeAll()
                
                    self.stack_detailsArray = stack_detailsArrayUpdate
                    self.stackIdArray = stackIdArrayUpdate
                    self.stackLayoutColumnWidth = stackLayoutColumnWidthUpdate
                        
                    //TODO: FIX THIS JANK
                    let stacksInLayout: Int = stack_detailsArrayUpdate.count > 0 ? stack_detailsArrayUpdate.count : 0
                    if(stacksInLayout > 0) {
                        //print("!!--setPageCount => \(stacksInLayout)")
                        //self.pageCount = stack_detailsArrayUpdate.count don't set pageCount until page is added to screen
                        DispatchQueue.main.async {
                            self.noStacksLblView.isHidden = true
                        }
                        //print("==>startIndex=>\(startIndex)")
                        
                        if(startIndex > 0) {
                            //loadInsertedStack()
//                            stackInsertedData()
                        } else {
                            //fuck
//                            setupStacksLayout(stacksInLayout: stacksInLayout, loadStack0: false)
                            setStackCount(stacksInLayout: stacksInLayout)
                        }
                    } else {
                        //no stacks in layout
                        //print("==>no stacks in layout!!")
                        showNoStacks()
                        delegate?.mlItemHasFinished()
                    }
                    
                    
                } else {
                    
                }
            }
        } else {
            //print("!!json nil")
            DispatchQueue.main.async {
                self.noStacksLblView.isHidden = false
            }
            delegate?.mlItemHasFinished()
        }
    }
    
    func showNoStacks() {
        DispatchQueue.main.async {
            self.noStacksLblView.isHidden = false
        }
    }
    
    func preloadStackLayout(){
        //print("preloadStackLayout")
        pageViews.append(nil)
        pageViewControllers.append(nil)
        setupStacksLayout(stacksInLayout: 1, loadStack0: true)
    }
    
    func setStackCount(stacksInLayout: Int) {
//        let stackCount: Int = (shouldPreload == true && stacksInLayout > 0) ? (stacksInLayout - 1) : stacksInLayout
        for _ in pageViews.count ..< stacksInLayout {
            pageViews.append(nil)
            pageViewControllers.append(nil)
        }
        
        //print("setStackCount--pageViews.count=>\(pageViews.count) == stacksInLayout=>\(stacksInLayout)")
        //stack layout already preloaded for 1
        if(stacksInLayout > 0) {
            if(shouldPreload == true) {
                updateStackCount(stacksInLayout: stacksInLayout)
            }
        }
        if(stacksInLayout > 1) {
            setupStacksLayout(stacksInLayout: stacksInLayout, loadStack0: false)
        }
    }
    
    func updateStackCount(stacksInLayout: Int){
        updatePreloadedPage(updatePage: 0)
        //TODO: Adjust layout size
    }
    
    func setupStacksLayout(stacksInLayout: Int, loadStack0: Bool) {
        //print("!!--setupStacksLayout")
        delegate?.logWindowMsg!("PVC loadStacksLayout", timeCompare: false)
        
        DispatchQueue.main.async {
            self.pageControl.currentPage = 0
            self.pageControl.numberOfPages = stacksInLayout
        
            let pagesScrollViewSize = self.view.frame
            let scHeight = pagesScrollViewSize.height //- 64
            let scrollViewPages: Int = self.repeatLoad == true ? self.stackMax : self.pageCount
            
            let contentWidth: CGFloat = self.stackWidth * CGFloat(self.stackLayoutColumnWidth)
            let scContentSize = CGSize(width: contentWidth, height: (self.view.frame.height))
            
            //print("SS--LPSVC frame height=>\(self.view.frame.height)")
            //print("SS--LPSVC bounds height=>\(self.view.bounds.height)")
            
            var paging: Bool = false

            if(AppSettings.isPad() == false && self.view.bounds.size.width < 700) {
                paging = true
            }
            let pcHidden: Bool = (self.view.bounds.size.width > 700) ? true : false
            
            self.automaticallyAdjustsScrollViewInsets = false
            
            self.scrollView.frame = pagesScrollViewSize
            self.scrollView.frame.size.height = scHeight
            
            //print("!*-PVC--scHeight=>\(scHeight)")
            self.scrollView.contentSize = scContentSize
            
            self.scrollView.isPagingEnabled = paging
            self.pageControl.isHidden = pcHidden
            self.scrollView.bounces = false
            
            let stacksPerPage: Int = Int(ceil(self.view.bounds.size.width / self.stackWidth))
        
            self.visiblePageLower = 0//-1
    //            self.visiblePageUpper = self.pageCount < stacksPerPage ? self.pageCount : stacksPerPage
            self.visiblePageUpper = ((stacksPerPage) > self.stackIdArray.count) ? (self.stackIdArray.count > 0 ? self.stackIdArray.count : 1) : (stacksPerPage)
            
            //print("visiblePageUpper=>\(self.visiblePageUpper) stacksPerPage=>\(stacksPerPage) stackIdArray.count=>\(self.stackIdArray.count)")
            
            if(loadStack0 == true) {
                self.preLoadPage()
            } else if(self.shouldPreload == false) {
                if let stack_detailsLoad = self.stack_detailsArray[0] as NSDictionary! {
                    self.installScrollViewContainerWithConstraints()
//                    self.loadPage(0, stack_details: stack_detailsLoad, addNewStack: "False")
                }
            }
        }
    }
    
    func installScrollViewContainerWithConstraints() {
        //print("installScrollViewContainerWithConstraints")
        // Prevent system from creating layout constraints //
        /*
         DispatchQueue.main.async {
         self.scrollContainerView.translatesAutoresizingMaskIntoConstraints = false
         
         self.scrollView.addSubview(self.scrollContainerView);
         
         }*/
        setScrollViewContraints()
    }
    
    func setScrollViewContraints() {
        /*
         DispatchQueue.main.async {
         let scrollViewContentWidth = self.stackWidth * CGFloat(self.stackLayoutColumnWidth)
         //print("PVC-scrollViewContentWidth=>\(scrollViewContentWidth)")
         self.scrollView.translatesAutoresizingMaskIntoConstraints = false
         
         let topLC : NSLayoutConstraint = NSLayoutConstraint(item: self.scrollContainerView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self.scrollView, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0)
         let leadingLC : NSLayoutConstraint = NSLayoutConstraint(item: self.scrollContainerView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: self.scrollView, attribute: NSLayoutAttribute.leading, multiplier: 1.0, constant: 0)
         let trailingLC = NSLayoutConstraint(item: self.scrollContainerView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: self.scrollView, attribute: NSLayoutAttribute.trailing, multiplier: 1.0, constant: 0)
         let bottomLC : NSLayoutConstraint = NSLayoutConstraint(item: self.scrollContainerView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self.scrollView, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0)
         //            let widthLC : NSLayoutConstraint = NSLayoutConstraint(item: self.scrollContainerView, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: self.scrollView, attribute: NSLayoutAttribute.Width, multiplier: widthRatio, constant: 0)
         
         self.svWidthLC = NSLayoutConstraint(item: self.scrollContainerView, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: scrollViewContentWidth)
         
         let heightLC : NSLayoutConstraint = NSLayoutConstraint(item: self.scrollContainerView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: self.scrollView, attribute: NSLayoutAttribute.height, multiplier: 1.0, constant: 0)
         
         self.scrollView.addConstraints([topLC,leadingLC,trailingLC,bottomLC,self.svWidthLC,heightLC])
         
         //            self.scrollContainerView.backgroundColor = UIColor.clear
         }*/
        if let stack_detailsLoad = stack_detailsArray[0] as NSDictionary! {
            loadPage(0, stack_details: stack_detailsLoad, addNewStack: "False")
        }
    }
    
    func preLoadPage() {
        //print("preLoadPage")
        loadPage(0, stack_details: nil, addNewStack: "False")
    }
    
    func loadVisiblePages() {
        
        // First, determine which page is currently visible
        let pageWidth = scrollView.frame.size.width
        let page = Int(floor((scrollView.contentOffset.x * 2.0 + pageWidth) / (pageWidth * 2.0)))
        
        // Update the page control
        DispatchQueue.main.async {
            self.pageControl.currentPage = page
        }
        
        // Work out which pages you want to load
        let firstPage =  visiblePageLower > 0 ? visiblePageLower - 1 : 0
        let lastPage = (visiblePageUpper + 1) > self.stackIdArray.count ? visiblePageUpper : visiblePageUpper + 1 //page + 1
        
        if(stackIdArray.count > 0) {
            // Purge anything before the first page
            for index in 0 ..< firstPage { //LOOP CHECK SHOULD THIS BE 0 or 1
                //print("PURGE before-" + String(firstPage) + "=>" + String(index))
                purgePage(index)
            }
            
            // Load pages in our range
            /*
             for var index = firstPage; index <= lastPage; ++index {
             loadPage(index)
             }
             */
            //        if let stack_detailsLoad = stack_detailsArray[0] as NSDictionary! {
            //            loadPage(0, stack_details: stack_detailsLoad, addNewStack: "False")
            //        }
            
            // Purge anything after the last page
            //print("loadVisiblePages-firstPage=>\(firstPage)--lastPage=>\(lastPage)")
            //print("loadVisiblePages-lastPage=>\(lastPage)--visiblePageUpper=>\(visiblePageUpper)")
            for index in lastPage ..< stackIdArray.count {
                //print("PURGE after-" + String(lastPage) + "=>" + String(index))
                purgePage(index)
            }
        }
    }
    
    func purgePage(_ page: Int) {
        //print("purgePage")
        //print("purgePage")
        if (page < 0 || page >= pageViewControllers.count || pageViews[page] == nil) {
            // If it's outside the range of what you have to display, then do nothing
            return
        }
        
        //pageViewControllers[page]?.encodeRestorableStateWithCoder(self.coder)
        
        // Remove a page from the scroll view and reset the container array
        if let pageView = pageViews[page] {
            DispatchQueue.main.async {
                pageView.removeFromSuperview()
                self.pageViews[page] = nil
            }
        }
        
        
        
        let deleteIndex = page
        
        
        
        DispatchQueue.main.async {
            
//            let sDeleteIndex = deleteIndex + 10
//            if let svcView = self.scrollView.viewWithTag(sDeleteIndex) {
//                //print("clear sv Views")
//                let svcViewSVs = svcView.subviews
//                for svcViewSV in svcViewSVs{
//                    for sContraint in svcViewSV.constraints {
//                        svcViewSV.removeConstraint(sContraint)
//                    }
//                    svcViewSV.removeFromSuperview()
//                }
//                for sContraint in svcView.constraints {
//                    svcView.removeConstraint(sContraint)
//                }
//                svcView.removeFromSuperview()
//            }
            
            
            self.pageCount = self.pageCount > 0 ? (self.pageCount - 1) : 0
            //print("pageCount=>\(self.pageCount)")
            //print("purgePage=>\(deleteIndex) of \(self.pageViewControllers.count)")
            if(deleteIndex < self.pageViewControllers.count) {
                if let pViewCntl = self.pageViewControllers[deleteIndex] {
                    //            pViewCntl?.clearContentView()
                    pViewCntl.dumpWebview()
                    pViewCntl.delegate = nil
                    pViewCntl.willMove(toParentViewController: nil)
                }
            }
            
            
            if let pageViewC = self.pageViewControllers[page] {
                DispatchQueue.main.async {
                    self.pageViewControllers[page] = nil
                }
            }
            
            if(deleteIndex < self.pageViews.count) {
                if let pView = self.pageViews[deleteIndex] {
                    let sViews = pView.subviews
                    for subView in sViews{
                        //print("clearScrollViews")
                        //                        DispatchQueue.main.async {
                        subView.removeFromSuperview()
                        //                        }
                    }
                }
            }
            
//            if(deleteIndex < self.pageViews.count) {
//                self.pageViews.remove(at: deleteIndex)
//            }
            
        }
        
        
    }
    
    func redrawScrollViewContent() {
        //print("redrawScrollViewContent")
        if(pageViewControllers.count > 0) {
            for index in 0 ..< pageViewControllers.count { //***LOOP CHECK SHOULD THIS BE 0 or 1
                let stackFrame = pageViewControllers[index]!.view.frame
                let newStackFrame = CGRect(x: stackFrame.origin.x, y: stackFrame.origin.y, width: stackFrame.size.width, height: scrollView.bounds.height)
                DispatchQueue.main.async {
                    self.pageViewControllers[index]!.view.frame = newStackFrame
                }
            }
        }
        if(pageViews.count > 0) {
            for index in 0 ..< pageViews.count { //***LOOP CHECK SHOULD THIS BE 0 or 1
                let pageFrame = pageViews[index]!.frame
                let newFrame = CGRect(x: pageFrame.origin.x, y: pageFrame.origin.y, width: pageFrame.size.width, height: scrollView.bounds.height)
                DispatchQueue.main.async {
                    self.pageViews[index]?.frame = newFrame
                }
            }
        }
        
        let svViews = scrollView.subviews
        for svView in svViews{
            //print("clear sv Views")
            if !(svView is UIImageView) {
                if let svView = svView as? UIView {
                    let svFrame = svView.frame
                    let svNewFrame = CGRect(x: svFrame.origin.x, y: svFrame.origin.y, width: svFrame.size.width, height: scrollView.bounds.height)
                    DispatchQueue.main.async {
                        svView.frame = svNewFrame
                    }
                    
                    let subViews = svView.subviews
                    for sfView in subViews {
                        let sfFrame = sfView.frame
                        let sfNewFrame = CGRect(x: sfFrame.origin.x, y: sfFrame.origin.y, width: sfFrame.size.width, height: scrollView.bounds.height)
                        DispatchQueue.main.async {
                            sfView.frame = sfNewFrame
                        }
                    }
                }
            }
        }
        //print("done")
    }
    
    func insertStack() {
        let pageWidth = scrollView.frame.size.width
        let page = Int(floor((scrollView.contentOffset.x * 2.0 + pageWidth) / (pageWidth * 2.0)))
//        let insertPage = page + 1
//        let nilDict: NSDictionary = [:]
        stackAddedToLayout(page, stack_id: 0)
    }
    
    func stackAddedToLayout(_ sPage: Int, stack_id: Int) {
        let insertIndex = sPage + 1
        //print("stackAddedToLayout-stack_id=>\(stack_id)")
        serialQueue.sync {
            //print("!!-1-serialQueue")
            //print("!!-2-stackInserted-serialQueue-insertIndex=>\(insertIndex)")
            self.setStackPrimerData(insertIndex: insertIndex, stackDetails: [:], stack_id: stack_id)
        }
    }
    
    func setStackPrimerData(insertIndex: Int, stackDetails: NSDictionary, stack_id: Int) {
        let addIndex = insertIndex //startIndex
        
        let sdArray = NSDictionary()
        //print("(addIndex < stackIdArray.count //pageCount)")
        //print("(\(addIndex) >< \(pageCount)")
        //print("stackLayoutColumnWidth=>\(self.stackLayoutColumnWidth)")
        if(addIndex < stackIdArray.count) {
            //print("(\(addIndex) < \(stackIdArray.count))")
            pageViews.insert(nil, at: addIndex)
            pageViewControllers.insert(nil, at: addIndex)
            stackIdArray.insert(String(stack_id), at: addIndex)
            stack_detailsArray.insert(stackDetails, at: addIndex)
        } else {
            //print("(\(addIndex) >< \(stackIdArray.count))")
            pageViews.append(nil)
            pageViewControllers.append(nil)
            stackIdArray.append(String(stack_id))
            stack_detailsArray.append(sdArray)
        }
//        self.pageCount += 1  don't add page count until page is added to screen
        self.stackLayoutColumnWidth += 1
        //print("stackLayoutColumnWidth=>\(self.stackLayoutColumnWidth)")
        DispatchQueue.main.async {
            self.pageControl.currentPage = addIndex
            self.pageControl.numberOfPages = self.pageCount
        }
        
        //????
        
        //print("!-2-insertStackPrimer end")
        self.resetScrollViewContraints(insertIndex: insertIndex, stackDetails: stackDetails)
    }
    
    func stackInserted2(insertIndex: Int, stackDetails: NSDictionary) {
        //adjust scrollview
        serialQueue.sync {
            //print("!!-3-serialQueue")
            self.resetScrollViewContraints(insertIndex: insertIndex, stackDetails: stackDetails)
        }

//        serialQueue.sync {
//            //print("!!-4-serialQueue")
//            //Force layoutUpdate when it does not return from stack-js side
//            //self.layoutUpdated(sPage)
//        }
    }

    func stackInsertedLoadPage(insertIndex: Int, stackDetails: NSDictionary) {
        serialQueue.sync {
            //print("!!-5-serialQueue-stackInserted-insertIndex=>\(insertIndex)")
            let nilDict: NSDictionary = [:]
            self.loadPage(insertIndex, stack_details: stackDetails, addNewStack: "False")
            //}
        }
        
        serialQueue.sync {
            //print("!!-5-serialQueue")
            self.checkInsertScroll(scrollToPage: insertIndex)
        }
        
        serialQueue.sync {
            //print("!!-5-serialQueue")
            self.updateStacksOrder()
        }
        
    }
    
    func stackDeleted(_ sPage: Int) {
        //print("stackDeleted-sPage=deleteIndex=>\(sPage)")
        deletedScrollLeft(deleteIndex: sPage)
    }
    
    func deletedScrollLeft(deleteIndex: Int) {
        //print("PVC--deletedScrollLeft=\(deleteIndex)")
        serialQueue.sync {
//            var newIndex: Int = 0
//
//            if(deleteIndex == 0 && pageViews.count > 1) {
//                newIndex = 0
//            } else {
//                newIndex = deleteIndex > 0 ? (deleteIndex - 1) : 0
//            }
            
            let newIndex = deleteIndex > 0 ? (deleteIndex - 1) : 0
            
            //            //print("!!-5-serialQueue-newIndex=>\(newIndex) deleteIndex=>\(deleteIndex)")
            self.deleteStackAtIndex(scrollToPage: newIndex, deleteIndex: deleteIndex)
        }
    }
    
    func resetScrollViewContraints(insertIndex: Int, stackDetails: NSDictionary){
//        let scrollViewContentWidth = stackWidth * CGFloat(stackLayoutColumnWidth)
        DispatchQueue.main.async {
            self.scrollView.translatesAutoresizingMaskIntoConstraints = false
            
            let oldWidth = self.scrollView.contentSize.width
            let newWidth = self.stackWidth * CGFloat(self.stackLayoutColumnWidth)
            
            //print("oldWidth=>\(oldWidth) newWidth=>\(newWidth) stackWidth=>\(self.stackWidth) stackLayoutColumnWidth=>\(self.stackLayoutColumnWidth)")
            
            self.scrollView.contentSize.width = newWidth
            
            //scrollView.contentSize = CGSize(width: stackWidth * CGFloat(stackLayoutColumnWidth), height: (self.view.frame.height))
            /*
            self.svWidthLC.constant = scrollViewContentWidth
            */
            
            for index in (insertIndex ..< self.pageViews.count).reversed() {
                //print("insert-move--for " + String(index) + " in " + String(self.startIndex) + " ..< " + String(self.pageViews.count))
//                if let pv = self.scrollView.viewWithTag(index) {
//                    pv.tag = index + 1
//                }
                if let pvc = self.pageViewControllers[index] {
                    pvc.view.tag = index + 1 + 10
                }
            }

//            fuck this right here
            //print("resetScrollViewContraints startIndex=>\(self.startIndex)")
            //print("resetScrollViewContraints insertIndex=>\(insertIndex)")
            //self.startIndex) {
            if(self.pageViews.count > insertIndex) {
                for index in insertIndex ..< self.pageViews.count {
                    //print("**--\(index) of \(insertIndex) ..< \(self.pageViews.count)")
                    if let pv = self.pageViews[index] {
                        pv.frame.origin.x += self.stackWidth
                    }
                    if let pvc = self.pageViewControllers[index] {
//                        pvc.stackPage = index
                        pvc.updateStackPage(sPage: index)
                    }
                }
            }

            self.scrollView.layoutIfNeeded()
            
            self.stackInsertedLoadPage(insertIndex: insertIndex, stackDetails: stackDetails)
        }
        //print("!-3-resetScrollViewContraints-startIndex=>\(startIndex)")
        
        //updateLayoutJSON
    }
    
    func loadInsertedStack() {
        let currentPage = startIndex
        if (!(currentPage < 0) && pageViewControllers.count > currentPage) {
            let stackVC = pageViewControllers[currentPage] as StackViewFrameController!
            let injectionStr = buildInjectionScript(currentPage)
            if (injectionStr != nil) {
                stackVC?.updateInjectScript(updateScript: injectionStr!)
            }
        }
    }
    
    //    func updateJSON() {
    //        //print("=loadInsertedStack=>startIndex=>\(startIndex)")
    //        if(stack_detailsArray.count > (startIndex) && stack_detailsArray[startIndex] != nil) {
    //            if let stack_detailsLoad = stack_detailsArray[startIndex] as NSDictionary! {
    //                //print("loadInsertedStack=loadPage=>startIndex=>\(startIndex)")
    //                loadPage(startIndex, stack_details: stack_detailsLoad, addNewStack: "False")
    //            }
    //        } else {
    //            //print("!*-stack_detailsArray count=>\(stack_detailsArray.count)")
    //            if(retryInsertLoadCount < 3){
    //                usleep(1420)
    //                retryInsertLoadCount += 1
    //                //print("!*-less than expected retry \(retryInsertLoadCount)")
    //                self.getStackLayoutJSON()
    //            } else {
    //                //print("!*-gave up after retry \(retryInsertLoadCount)")
    //                retryInsertLoadCount = 0
    //            }
    //        }
    //    }
    
    func buildInjectionScript(_ page: Int) -> String! {
        var injectionStr: String!
        let stackId = stackIdArray[page] as String
        let addNewStack = "False" //also unused
        if(stackId.count > 0) {
            let attributeValues: Array<MLAttributesKeyValue> =
                [MLAttributesKeyValue(key: "layout_id", value: layout_id),
                 MLAttributesKeyValue(key: "module_id", value: mlId),
                 MLAttributesKeyValue(key: "data-single-stack-id", value: stackId),
                 MLAttributesKeyValue(key: "class", value: "xMobile mobilize"),
                 MLAttributesKeyValue(key: "add-new-stack", value: addNewStack)]
            
            let mlAttr: MLAttributes = MLAttributes(layout_id: Int(layout_id)!, attributeValues: attributeValues)
            let mlDir: MLDirective = MLDirective(directive_name: "stacks-layout", attributes: mlAttr)
            
            let mlStack: MLStack = MLStack(directive: mlDir)
            
            let serializer: SerializableMLItem = SerializableMLItem()
            let serializedStr: NSString = serializer.toMLStackJsonString(mlStack)
            injectionStr = "loadXPComponent('" + (serializedStr as String) + "');"
        }
        return injectionStr
    }
    
    func loadPage(_ page: Int, stack_details: NSDictionary?, addNewStack: String) { //TODO: remove addNewStack always "False"
        delegate?.logWindowMsg!("PVC loadPage-" + String(page), timeCompare: false)
        //print("loadPage=>\(page) of \(self.pageCount)")
        //print(String(format: "LLL-loadPage=>%@ of self.pageCount=>%@ -LLL", String(page), String(self.pageCount)))
        if(page < self.pageViews.count){
            self.webViewPage = page
        }
        if page < 0 || self.pageViews.count < page {
            // If it's outside the range of what you have to display, then do nothing
            return
        }
        let preload: Bool = (page == 0 && stack_details == nil) ? true : false
        if(self.pageViews.count > page) {
            //print("loadPage=>stop")
            if (preload == false && pageViews[page] != nil) {
                // Do nothing. The view is already loaded.
                //print("!!*--load \(page) Do nothing. The view is already loaded=>")
            } else {
                pageViews[page] = UIView()
                //print("LOAD--\(page)")
//                if(self.pageViews.count > page) { //if(page < self.pageCount){
                    /* Getting the page View controller */
                
                var storyboard = UIStoryboard() //(name: "Main", bundle: nil)
                if #available(iOS 11, *) {
                    storyboard = UIStoryboard(name: "Main", bundle: nil)
                } else {
                    storyboard = UIStoryboard(name: "MainiOS10", bundle: nil)
                }
                
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                    //TODO: Tablet might need this changed
                    var injectionStr: String!
                
                var stackId: String!
                if(preload == false) {
                    injectionStr = buildInjectionScript(page)
                    stackId = stackIdArray[page]
                }
                    let stackCntlr: StackViewFrameController = storyboard.instantiateViewController(withIdentifier: "StackViewFrameController") as! StackViewFrameController
                    
                    stackCntlr.delegate = self
                    //print("PSV--company_id=>" + company_id)
                    
                    stackCntlr.company_id = company_id
                    stackCntlr.myUrl = myUrl
                    stackCntlr.loadExpion = loadExpion
                    stackCntlr.stackPage = Int(page)
                    //stackCntlr.stack_details = stack_details
                    stackCntlr.mlId = mlId
                    stackCntlr.layoutId = layout_id
                    stackCntlr.injectionScriptStr = injectionStr
                    stackCntlr.stackId = stackId
                    stackCntlr.debugMode = self.debugMode
                    stackCntlr.restorationIdentifier = "stackPage" + String(page)
                    
                    var columnSize: CGFloat = 1
                    if let cc = stack_details?["column_count"] as? Int {
                        columnSize = CGFloat(cc)
                    }
//                    DispatchQueue.main.async {
//                        //self.scrollView.backgroundColor = UIColor.red
//                        //self.scrollContainerView.backgroundColor = UIColor.purple //UIColor.clear
//                    }
                    let stackXPos = self.stackWidth * CGFloat(page)//(stackWidth * columnSize)
                    //print("PVC-page=>\(describing: page))
                    //print("PVC-stackXPos=>" + String(describing: stackXPos))
                    
                    let svcWidth: CGFloat = self.stackWidth * columnSize
                    //print("PVC-svcWidth=>" + String(describing: svcWidth))
                    //                        //print("PVC-self.scrollView.bounds.height=>" + String(describing: self.scrollView.bounds.height))
                    
                
                    DispatchQueue.main.async {
//                        //print("stackCntlr set frame-stackXPos=>\(stackXPos)")
                        let newStackFrame = CGRect(x: stackXPos, y: 0, width: svcWidth, height: self.scrollView.bounds.height)
                        stackCntlr.view.frame = newStackFrame
                        stackCntlr.view.tag = page + 10
                    }
                        
                        
                        //let newPageView = UIView()
                        //                let newPageFrame = CGRectMake(stackWidth * CGFloat(page), 0, stackWidth, scrollView.bounds.height)
                        //                        let newPageFrame = CGRect(x: stackXPos, y: 0, width: (self.stackWidth * columnSize), height: self.scrollView.bounds.height)
                        
                        
                        //                        newPageView.frame = newPageFrame  //!important for frame.origin
                        //                        newPageView.translatesAutoresizingMaskIntoConstraints = true
                        
                        //                let heightConstraint = NSLayoutConstraint(item: newPageView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: scrollView, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 100)
                        
                        /////////////
                        //                        self.scrollContainerView.translatesAutoresizingMaskIntoConstraints = false
                        //                        stackCntlr.view.translatesAutoresizingMaskIntoConstraints = false
                        //
                        //                        let topLC : NSLayoutConstraint = NSLayoutConstraint(
                        //                            item: stackCntlr.view, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal,
                        //                            toItem: self.scrollContainerView, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0)
                        //
                        //                        let leadingLC : NSLayoutConstraint = NSLayoutConstraint(
                        //                            item: stackCntlr.view, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal,
                        //                            toItem: self.scrollContainerView, attribute: NSLayoutAttribute.leading, multiplier: 1.0, constant: stackXPos)
                        //
                        //                        let bottomLC : NSLayoutConstraint = NSLayoutConstraint(
                        //                            item: stackCntlr.view, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal,
                        //                            toItem: self.scrollContainerView, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0)
                        //
                        //                        self.svcWidthLC = NSLayoutConstraint(
                        //                            item: stackCntlr.view, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal,
                        //                            toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: svcWidth)
                        
                    DispatchQueue.main.async {
                        self.addChildViewController(stackCntlr)
                        
//                        //print("if page > self.scrollView.subviews.count")
//                        if(page > self.scrollView.subviews.count) {
//                        //print("load page=>\(page) of pageViews.count=>\(self.pageViews.count)")
                        
                        if(self.pageCount > page) {
                            //pageCount > page means inserted Stack
                            //print("page < self.pageViews.count--insert")
                            self.scrollView.insertSubview(stackCntlr.view, at: page)
                        } else {
                            //pageCount > page means inserted Stack
                            //print("else--!!--append")
                            self.scrollView.addSubview(stackCntlr.view)
                        }
                        
                        self.pageCount += 1
                        self.pageViewControllers[page] = stackCntlr
                        self.pageViews[page] = stackCntlr.view
                        
                        self.scrollView.layoutIfNeeded()
                    }
                        
                    //                        self.scrollContainerView.addConstraints([topLC,leadingLC,bottomLC,self.svcWidthLC])
                    //    /////////////
                    //
                    stackCntlr.didMove(toParentViewController: self)

                    //print("!-4-loadPage")

                    stackCntlr.initializeWebView()
                }
//            }
        }
    }
    
    func updatePreloadedPage(updatePage: Int) {
        if (updatePage < pageViewControllers.count) {
            let stackVC = pageViewControllers[updatePage] as StackViewFrameController?
            let injectionStr = buildInjectionScript(updatePage)
            let stackId = stackIdArray[updatePage]
            if (injectionStr != nil) {
                stackVC?.updateStackData(updateScript: injectionStr!, updateStackId: stackId)
            }
        }
    }
    
    func checkInsertScroll(scrollToPage: Int){
        //print("!-5-checkInsertScroll-startIndex=>\(scrollToPage)")
        //        if(scrollToPage != current){
        
        let scrollX = CGFloat(scrollToPage) * stackWidth
        let scrollY = scrollView.frame.origin.y
        //print("!-5-checkInsertScroll-scrollX=>\(scrollX)--stackWidth=>\(stackWidth)")
        DispatchQueue.main.async {
            UIView.animate(withDuration: ShareData.sharedInstance.transitionSpeed, animations: {
                self.scrollView.setContentOffset(CGPoint(x: scrollX, y: scrollY), animated: true)
            }, completion: { finished in
                //print("animate=>complete")
                //self.insertStackPrimer()
                self.startIndex = 0
            })
        }
        
        //        }
    }
    func deleteStackAtIndex(scrollToPage: Int, deleteIndex: Int){
        //print("!-5-checkInsertScroll-scrollToPage=>\(scrollToPage) deleteIndex=>\(deleteIndex)")
        //print("!-5 of pageViews.count=>\(self.pageViews.count)")
        
        //        if(scrollToPage != current){
        DispatchQueue.main.async {
            //removes the stack view
            let sDeleteIndex = deleteIndex + 10
            if let svcView = self.scrollView.viewWithTag(sDeleteIndex) {
                //print("clear sv Views")
                let svcViewSVs = svcView.subviews
                for svcViewSV in svcViewSVs{
                    for sContraint in svcViewSV.constraints {
                        svcViewSV.removeConstraint(sContraint)
                    }
                    svcViewSV.removeFromSuperview()
                }
                for sContraint in svcView.constraints {
                    svcView.removeConstraint(sContraint)
                }
                svcView.removeFromSuperview()
            }
            
            
            self.pageCount = self.pageCount > 0 ? (self.pageCount - 1) : 0
            //print("pageCount=>\(self.pageCount)")
            //print("stackDeletedRemoveViews=>\(deleteIndex) of \(self.pageViewControllers.count)")
            
            
            if(deleteIndex < self.pageViewControllers.count) {
                if let pViewCntl = self.pageViewControllers[deleteIndex] {
                    //            pViewCntl?.clearContentView()
                    pViewCntl.dumpWebview()
                    pViewCntl.delegate = nil
                    pViewCntl.willMove(toParentViewController: nil)
                }
            }
            
            if(deleteIndex < self.pageViewControllers.count) {
                self.pageViewControllers.remove(at: deleteIndex)
            }
            
            if(deleteIndex < self.pageViews.count) {
                if let pView = self.pageViews[deleteIndex] {
                    let sViews = pView.subviews
                    for subView in sViews{
                        //print("clearScrollViews")
//                        DispatchQueue.main.async {
                            subView.removeFromSuperview()
//                        }
                    }
                }
            }
            
            if(deleteIndex < self.pageViews.count) {
                self.pageViews.remove(at: deleteIndex)
            }
            
            if(self.pageViews.count == 0) {
//                self.noStacksLbl.isHidden = false
                self.showNoStacks()
            }
            
            self.scrollView.setNeedsFocusUpdate()
//            self.scrollView.setNeedsLayout()
            self.scrollView.setNeedsDisplay()
            self.scrollView.layoutIfNeeded()

            
            self.stackLayoutColumnWidth = self.stackLayoutColumnWidth > 0 ? (self.stackLayoutColumnWidth - 1) : 0
            
            let oldWidth = self.scrollView.contentSize.width
            let newWidth = self.stackWidth * CGFloat(self.stackLayoutColumnWidth)
            
            //print("!-5-oldWidth=>\(oldWidth) newWidth=>\(newWidth) stackWidth=>\(self.stackWidth) stackLayoutColumnWidth=>\(self.stackLayoutColumnWidth)")
            //print("!!-5-(self.view.frame.height)=\(self.view.frame.height)")
            let scContentSize = CGSize(width: newWidth, height: (self.view.frame.height))
            self.scrollView.contentSize = scContentSize
            
            for sViewSubView in self.scrollView.subviews {
                let viewIndexStart = deleteIndex + 10
                let sViewSubViewTag = sViewSubView.tag
                print("sViewSubViewTag=>\(sViewSubViewTag)")
                print("viewIndexStart=>\(viewIndexStart)")
                if(sViewSubViewTag > viewIndexStart) {
                    let pvcIndex = sViewSubViewTag - 10
                    print("pvcIndex=>\(pvcIndex)")
                    if(self.pageViewControllers.count > pvcIndex) {
                        if let pvc = self.pageViewControllers[pvcIndex] {
                            pvc.view.tag = sViewSubViewTag - 1
                            sViewSubView.tag = sViewSubViewTag - 1
                            //print("new pvc.view.tag=>\(pvc.view.tag) -- sViewSubView.tag=\(sViewSubView.tag)")
                        }
                    }
                } else {
                    //print("new tag value=> else keep value")
                }
            }

                ////////////////
            
            
            
            //default behavior on delete should be to move to the next item on the left
            if (deleteIndex == 0) {
                //this means that the 0 index of pageViews was just removed
                //now you would want to view old index 1, now at index 0
                //since no item on the left then the new object 0 should be in focus
            }
            if(deleteIndex > 0) {
                //example pageViews = [0,1,2]
                //deleteIndex == 1
                //newViewIndexPos would be 0 (deleteIndex > 0 ? deleteindex - 1 : 0)
                //now pageViews objects = [0,2] but reindexed to [0,1]
                //in this scenario move old object 2 the index 1, it becomes new object 1
            }
            
            
//            for i in (0 ..< 3) {
//                //print("i = \(i)")
//            }
//
//            for x in (3 ..< 6) {
//                //print("x = \(x)")
//            }
            
            
            let pageCount = self.pageViews.count
            //print("!-5 new pageViews.count=>\(self.pageViews.count)")
            let newInViewIndex = deleteIndex > 0 ? (deleteIndex - 1) : 0
//                if (deleteIndex < pageEnd && pageEnd > 0) {
            
            var needsMove: Bool = false
            var needsScroll: Bool = false
            if(deleteIndex == 0 && newInViewIndex == 0) {
                //no scroll just move
                needsMove = true
            } else if(deleteIndex > 0 && deleteIndex < self.pageViews.count) {
                //scroll left and move deleteIndex - pageViews.count
                needsMove = true
                needsScroll = true
            } else if(deleteIndex == self.pageViews.count) {
                //last stack in layout, scroll left no move
                needsScroll = true
            }
            let tranSpeed = (ShareData.sharedInstance.transitionSpeed / 2)
            
            
            
            UIView.animate(withDuration: tranSpeed, animations: {
                if(needsScroll == true) {
                    let scrollX = CGFloat(scrollToPage) * self.stackWidth
                    let scrollY = self.scrollView.frame.origin.y
                    //print("!-5-needsScroll-scrollX=>\(scrollX)--stackWidth=>\(self.stackWidth)")
                    self.scrollView.setContentOffset(CGPoint(x: scrollX, y: scrollY), animated: true)
                }
                if(needsMove == true) {
                    //print("newInViewIndex=> \(newInViewIndex)")
                    for index in (deleteIndex ..< pageCount) {
                        //only move xpos of delete to end
                        //print("deleteMove-xpos--for=> \(index) of \(pageCount)")
                        //print("**--\(index) of \(newInViewIndex) ..< \(pageCount)")
    //                        let viewTag = index + 10
    //                        if let pv = self.scrollView.viewWithTag(viewTag) {

                        //print("self.pageViews=>\(self.pageViews.count)")
                        
                        if let pvc = self.pageViewControllers[index] {
//                            pvc.stackPage = index
                            pvc.updateStackPage(sPage: index)
                        }
                        
                        if(index > 0) {
                            if let pv = self.pageViews[index] as UIView? {
                                //print("shift-xpos-=> for \(index) of \(pageCount)")
    //                            let columnSize: CGFloat = 1
    //                            let svcWidth: CGFloat = self.stackWidth * columnSize
                                let stackXPos = self.stackWidth * CGFloat(index)
                                //print("stackXPos=>\(stackXPos)")
    //                            let moveStackFrame = CGRect(x: stackXPos, y: 0, width: svcWidth, height: self.view.frame.height)
                                
                                let pvFrame = pv.frame
                                //print("pvFrame xPos =>\(pvFrame.origin.x) for index=>\(index)")
    //                            pv.frame.origin.x -= self.stackWidth
    //                            pv.frame = moveStackFrame
                                
                                let offset = CGFloat(self.stackWidth * -1)
                                pv.frame = pv.frame.offsetBy(dx: offset, dy: CGFloat(0))

                            }
                        }
                    }
                }

            }, completion: { finished in
                if(self.pageViewControllers.count == 0) {
                    DispatchQueue.main.async {
                        self.noStacksLblView.isHidden = false
                    }
                } else {
                    if let pvc = self.pageViewControllers[newInViewIndex] {
                        pvc.stackBecameActive()
                    }
                }
                self.updateStacksOrder()
            })
        }
    }
    
    func addStackToLayout() {
        let pageWidth = scrollView.frame.size.width
        let page = Int(floor((scrollView.contentOffset.x * 2.0 + pageWidth) / (pageWidth * 2.0)))
        let addIndex: Int = page + 1
        //stack_detailsArrayAdd.append(stackDetails)
        //print("++-addStackToLayout- stackWidth=>" + String(stringInterpolationSegment: stackWidth))
        //self.scrollView.contentSize = CGSizeMake(stackWidth * stackLayoutColumnWidth, (pagesScrollViewSize.height))
        
        
        let sdArray = NSDictionary()
        if(addIndex < pageCount) {
            pageViews.insert(nil, at: addIndex)
            pageViewControllers.insert(nil, at: addIndex)
            stackIdArray.insert("", at: addIndex)
            stack_detailsArray.insert(sdArray, at: addIndex)
        } else {
            pageViews.append(nil)
            pageViewControllers.append(nil)
            stackIdArray.append("")
            stack_detailsArray.append(sdArray)
        }
        //print("++--stack_detailsArray.count=>" + String(stack_detailsArray.count))
        
        var columnSize: CGFloat = 1
        let stackXPos = self.stackWidth * CGFloat(addIndex)
        let svcWidth: CGFloat = self.stackWidth * columnSize
        
        DispatchQueue.main.async {
            self.scrollView.contentSize = CGSize(width: self.stackWidth * CGFloat(self.pageCount), height: (self.view.frame.height))// - 64))
            /*
            let newStackFrame = CGRect(x: stackXPos, y: 0, width: svcWidth, height: self.scrollView.bounds.height)
            
            let emptyView = UIView()
            emptyView.backgroundColor = UIColor.green
            emptyView.frame = newStackFrame
            self.scrollContainerView.insertSubview(emptyView, at: addIndex)
            */
            if(self.pageViews.count > addIndex) {
                for index in addIndex ..< self.pageViews.count {
                    if let pv = self.pageViews[index] as? UIView {
                        pv.frame.origin.x += svcWidth
                    }
                }
            }
            
            self.scrollView.layoutIfNeeded()
        }
        
        //print("++--stack_detailsArray.count=>" + String(stack_detailsArray.count))
        //stackDetailStartLoad(stack_detailsArray.count)
        let loadIndex: Int = pageCount
        //print("++--loadIndex=>" + String(loadIndex))
        pageCount += 1
        //stackDetailStartLoad(loadIndex, addNewStack: "True")
    }
    
    func removeStackFromLayout() {
        //print("--!!-removeStackFromLayout- stackWidth=>" + String(stringInterpolationSegment: stackWidth))
        let pageWidth = self.scrollView.frame.size.width
        let page = Int(floor((scrollView.contentOffset.x * 2.0 + pageWidth) / (pageWidth * 2.0)))
        stackDeleted(page)
    }
    
    // MARK: ScrollView methods
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //print("scrollViewDidScroll")
        showPageControl()
        checkScrollPosition()
    }
    
    private func scrollViewDidBeginDecelerating(_ scrollView: UIScrollView) {
        //print("scrollViewDidBeginDecelerating")
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidBeginDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        isDragging = true
        //print("scrollViewDidBeginDragging")
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        //print("scrollViewDidEndDragging")
        isDragging = false
//        var timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(LegacyPagedScrollViewController.checkForPageControl), userInfo: nil, repeats: true)
    }
    
    func checkScrollPosition(){
        //print("START--scrollViewDidScroll>checkScrollPosition")
        
        let triggerOffsetUpper: CGFloat = CGFloat(visiblePageUpper) * CGFloat(stackWidth)
        let triggerOffsetLower: CGFloat = CGFloat(visiblePageLower) * CGFloat(stackWidth)
        
        
        let scrollStartPos = scrollView.contentOffset.x
        let scrollEndPos = scrollView.frame.size.width + scrollView.contentOffset.x
//        //print(String(format:">>---------scrollEndPos=>%@", String(describing: scrollEndPos)))
//        //print(String(format:">>>--triggerOffsetUpper=>%@", String(describing: triggerOffsetUpper)))
//        //print(String(format:">>-------scrollStartPos=>%@", String(describing: scrollStartPos)))
//        //print(String(format:">>>--triggerOffsetLower=>%@", String(describing: triggerOffsetLower)))
        
        let currentPage: Int = Int(scrollStartPos / scrollView.frame.size.width)
//        //print(String(format:"currentPage=>%@", String(describing: currentPage)))
        
        
//        if(scrollEndPos >= triggerOffsetUpper && (visiblePageUpper) <= self.stackIdArray.count){
//            //print("TRIGGER->Upper scrollView.contentOffset.x >= triggerOffset")
//            scrollTriggerOffset(directionNext: true, currentPage: currentPage)
//        } else if(scrollStartPos <= triggerOffsetLower && -1 <= (visiblePageLower)){
//            //print("TRIGGER<-Lower scrollView.contentOffset.x <= triggerOffsetLower")
//            scrollTriggerOffset(directionNext: false, currentPage: currentPage)
//        } else {
//            //print(String(format:"ELSE scrollEndPos=>%@", String(describing: scrollEndPos)))
//        }
        
        if(scrollEndPos >= triggerOffsetUpper && visiblePageUpper <= self.stackIdArray.count){
            //print("TRIGGER->Upper scrollView.contentOffset.x >= triggerOffset")
            scrollTriggerOffset(directionNext: true, currentPage: currentPage)
        } else if(scrollEndPos <= triggerOffsetLower && 0 <= visiblePageLower){
            //print("TRIGGER<-Lower scrollView.contentOffset.x <= triggerOffsetLower")
            scrollTriggerOffset(directionNext: false, currentPage: currentPage)
        } else {
            //print(String(format:"ELSE scrollEndPos=>%@", String(describing: scrollEndPos)))
        }
        
//        notifyActiveStack(currentPage: currentPage)
        
        
        
//        //print(String(format:">>END------scrollEndPos=>%@", String(describing: scrollEndPos)))
        if (isDragging == false) {
            hidePageControl(1.0)
        }
    }
    
    func scrollTriggerOffset(directionNext: Bool, currentPage: Int) {
//        directionNext = true = + moving right or upper
//        directionNext = false = - moving left or lower
        //print("!!!--start---directionNext=>\(directionNext)")
        //print(String(format:">>--start--visiblePageLower=>%@", String(visiblePageLower)))
        //print(String(format:">>--start---visiblePageUpper=>%@", String(visiblePageUpper)))
        
        if(directionNext == true) {
            //print("TRIGGER->Upper scrollView.contentOffset.x >= triggerOffset")
            let lPage = visiblePageUpper
            visiblePageUpper = visiblePageUpper + 1 > self.stackIdArray.count ? visiblePageUpper : visiblePageUpper + 1
            if(visiblePageUpper - visiblePageLower > pageCountThreshold){
                visiblePageLower = visiblePageLower + 1
            }
            stackDetailFinishedLoading(lPage, triggerDir: true)
        } else {
            //print("TRIGGER<-Lower scrollView.contentOffset.x <= triggerOffsetLower")
            let lPage = visiblePageLower
            visiblePageLower = visiblePageLower > 0 ? visiblePageLower - 1 : 0
            if(visiblePageUpper - visiblePageLower > pageCountThreshold){
                visiblePageUpper = visiblePageUpper - 1
            }
            if(lPage >= 0) {
                stackDetailFinishedLoading(lPage, triggerDir: false)
            }
        }
        
        //print(String(format:">>>----currentPage=>%@", String(currentPage)))
        //print(String(format:">>----visiblePageLower=>%@", String(visiblePageLower)))
        //print(String(format:">>-----visiblePageUpper=>%@", String(visiblePageUpper)))
    }
    
    func notifyActiveStack(currentPage: Int){
        //print("notifyActiveStack=>\(currentPage)")
        if (!(currentPage < 0) && pageViewControllers.count > currentPage) {
            if let stackVC = pageViewControllers[currentPage] as StackViewFrameController! {
                stackVC.vfwkWebView.setNeedsFocusUpdate()
                stackVC.vfwkWebView.updateFocusIfNeeded()
                stackVC.stackBecameActive()
            }
        }
    }
    
    // MARK: PageControl methods
    //@objc
    @objc func checkForPageControl() {
        //        //print("checkForPageControl")
        if(pageControl.isHidden == false){
            hidePageControl(0.0)
        }
    }
    
    func showPageControl() {
        //        //print("showPageControl")
        DispatchQueue.main.async {
            self.pageControl.isHidden = false
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.pageControl.alpha = 1.0
            }, completion: nil)
        }
    }
    
    func hidePageControl(_ delay: TimeInterval) {
        //print("hidePageControl")
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5, delay: delay, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.pageControl.alpha = 0.0
            }, completion: {
                (value: Bool) in
                self.pageControl.isHidden = true
            })
        }
    }
    
    // MARK: Manage loading methods
    func stackDetailStartLoad(_ slPage: Int, addNewStack: String) {
        //print(String(format: "*>*>*>--stackDetailStartLoad slPage=>%@", String(slPage)))
        if(0 <= slPage && slPage < stack_detailsArray.count){
            if let stack_detailsLoad = stack_detailsArray[slPage] as NSDictionary! {
                loadPage(slPage, stack_details: stack_detailsLoad, addNewStack: addNewStack)
            }
        }
    }
    
    func stackDetailFinishedLoading(_ sPage: Int, triggerDir: Bool) {
//        //print(String(format: "PSVC--stackDetailFinishedLoading sPage=>%@ count=>%@ lower=>%@ upper=>%@", String(sPage), String(self.stackIdArray.count), String(visiblePageLower), String(visiblePageUpper)))
        
        for pIndex in 0 ..< self.stackIdArray.count {
            if(pIndex < visiblePageLower || pIndex > visiblePageUpper) {
//                clearview
            }
        }
        
        loadVisiblePages()
        
        if(triggerDir == true && (sPage+1) < self.stackIdArray.count && (sPage+1) <= visiblePageUpper) { // + 1) {
            //print("stackDetailFinishedLoading=>startLoad ++")
            stackDetailStartLoad(sPage+1, addNewStack: "False")
        } else if(triggerDir == false && (sPage-1) >= 0 && self.stackIdArray.count >= 0 && (sPage-1) <= visiblePageLower) {
            //print("stackDetailFinishedLoading=>startLoad --")
            stackDetailStartLoad(sPage-1, addNewStack: "False")
        } else {
            hidePageControl(1.0)
            //manageViewablePages()
            //            loadVisiblePages()
            delegate?.mlItemHasFinished()
        }
    }
    
    func logWindowMsg(_ logTxt: String, timeCompare: Bool) {
        delegate?.logWindowMsg!(logTxt, timeCompare: timeCompare)
    }
    
    func clearScrollView(_ completion: (()-> Void)? = nil){
        //print("QQQQ--clearScrollViews--LegacyPagedScrollViewController--QQQQ")
        DispatchQueue.main.async {
            self.noStacksLblView.isHidden = true
        }
        for pViewCntl in pageViewControllers {
            //            pViewCntl?.clearContentView()
            pViewCntl?.dumpWebview()
            pViewCntl?.delegate = nil
            DispatchQueue.main.async {
                pViewCntl?.willMove(toParentViewController: nil)
            }
        }
        
        //        pageViewControllers[page] = stackCntlr
        //        pageViews[page] = stackCntlr.view //newPageView
        
        //print("QQQQ--svView in svViews{--QQQQ")
        
        var svcViewCount = 0
        for svcView in scrollContainerView.subviews {
            //print("clear-\(svcViewCount) svc Views")
            let svcViewSVs = svcView.subviews
            for svcViewSV in svcViewSVs{
                DispatchQueue.main.async {
                    svcViewSV.removeFromSuperview()
                }
            }
            DispatchQueue.main.async {
                svcView.removeFromSuperview()
            }
            //print(String(svcViewSVs.count))
            svcViewCount += 1
        }
        DispatchQueue.main.async {
            self.scrollContainerView.removeConstraints(self.scrollContainerView.constraints)
        }
        var svViewCount = 0
        for svView in self.scrollView.subviews {
            //print("clear-\(svViewCount) sv Views")
            let svViewSVs = svView.subviews
            for svViewSV in svViewSVs{
                DispatchQueue.main.async {
                    svViewSV.removeFromSuperview()
                }
            }
            DispatchQueue.main.async {
                svView.removeFromSuperview()
            }
            svViewCount += 1
        }
        DispatchQueue.main.async {
            self.scrollContainerView = UIView(frame: CGRect.zero)
        }
        
        for pView in self.pageViews {
            if(pView != nil){
                let sViews = pView!.subviews
                for subView in sViews{
                    //print("clearScrollViews")
                    DispatchQueue.main.async {
                        subView.removeFromSuperview()
                    }
                }
            }
        }

    
        layoutRequestData.setData(Data())
        //print("QQQQ--loop completed--QQQQ")
        pageViews.removeAll(keepingCapacity: false)
        pageViewControllers.removeAll(keepingCapacity: false)
        stack_detailsArray.removeAll(keepingCapacity: false)
        stackIdArray.removeAll(keepingCapacity: false)
        if((completion) != nil) {
            completion!()
        }
        
        //print("clearScrollView--subviews.count=>\(self.scrollView.subviews.count)")
        //print("")
        
    }
    
    func logoutTimedOut() {
        //print("PSVC--logoutTimedOut")
        delegate?.sessionTimedOut()
    }
}
/*
extension LegacyPagedScrollViewController:URLSessionDelegate {
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        // We've got a URLAuthenticationChallenge - we simply trust the HTTPS server and we proceed
        //print("PVC--didReceive challenge")
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
    
    func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        // We've got an error
        if let err = error {
            //print("PVC--Error: \(err.localizedDescription)")
        } else {
            //print("PVC--Error. Giving up")
        }
        //            PlaygroundPage.current.finishExecution()
    }
}

extension LegacyPagedScrollViewController:URLSessionDataDelegate {
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didBecome streamTask: URLSessionStreamTask) {
        // The task became a stream task - start the task
        //print("PVC--didBecome streamTask")
        streamTask.resume()
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didBecome downloadTask: URLSessionDownloadTask) {
        // The task became a download task - start the task
        //print("PVC--didBecome downloadTask")
        downloadTask.resume()
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        // We've got a URLAuthenticationChallenge - we simply trust the HTTPS server and we proceed
        //print("PVC--didReceive challenge")
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: @escaping (URLRequest?) -> Void) {
        // The original request was redirected to somewhere else.
        // We create a new dataTask with the given redirection request and we start it.
        if let urlString = request.url?.absoluteString {
            //print("PVC--willPerformHTTPRedirection to \(urlString)")
        } else {
            //print("PVC--willPerformHTTPRedirection")
        }
        let session
        if let task = self.session?.dataTask(with: request) {
            task.resume()
        }
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        // We've got an error
        if let err = error {
            //print("PVC--Error: \(err.localizedDescription)")
        } else {
            //print("PVC--Session Complete")
            self.parseStackLayoutJSON()
        }
        //            PlaygroundPage.current.finishExecution()
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        // We've got the response headers from the server.
        //print("PVC--didReceive response")
        //print(response)
        self.response = response
        self.layoutRequestData = NSMutableData()
        completionHandler(URLSession.ResponseDisposition.allow)
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        // We've got the response body
        //print("PVC--didReceive data")
        if let responseText = String(data: data, encoding: .utf8) {
            //print(self.response ?? "")
            //print("\nServer's response text")
            //print(responseText)
        }
        
        self.layoutRequestData.append(data)
    }
    
}
 */
