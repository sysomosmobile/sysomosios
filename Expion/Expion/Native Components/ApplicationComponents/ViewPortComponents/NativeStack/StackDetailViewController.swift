//
//  StackLayoutViewController.swift
//  Workspaces
//
//  Created by Mike Bagwell on 4/13/15.
//  Copyright (c) 2015 Expion. All rights reserved.
//

import UIKit

@objc
protocol StackDetailViewControllerDelegate {
    func stackDetailFinishedLoading(_ sPage: Int)
}

class StackDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    //@IBOutlet weak var tableViewRowHeightConstraint : NSLayoutConstraint!

    //@IBOutlet weak var contentView: UIView!
    @IBOutlet weak var detailLoadingView: UIView!
    @IBOutlet weak var detailTableView: UIView!
    @IBOutlet weak var detailLoadFailView: UIView!
    
    /*
    //let stackLayoutData = StackLayoutData()
    var stackLayoutData: Array<StackLayoutData> = []
    var stackLayoutIndex: Int = 0
    */
    
    //let stackDetailData = StackDetailData()
    var stackDetailData: Array<StackDetailData> = []
    var stackDetailIndex: Int = 0
    
    var stack_details: NSDictionary!
    
    var delegate: StackDetailViewControllerDelegate?
    //var stackView: UIView!
    //var stackHeaderView: UIView!
    
    var company_id: String!
    var myUrl: String!
    //var controllerMsg: String!
    var mlId: String!
    var layoutId: String!
    
    var stackPage: Int!
    
    lazy var detailRequestData = NSMutableData()
    
    var postItems: NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //print(String(format: "StackLayoutViewController=>%@", self.view.frame.height))
        //println(String(format: "stack_details=>%@", stack_details))
        //println(String(format: "!!! stackPage =>%@", self.stackPage))
        self.automaticallyAdjustsScrollViewInsets = false
        
        detailLoadingView.isHidden = false
        detailTableView.isHidden = true
        detailLoadFailView.isHidden = true
        
        let stackCellNib = UINib(nibName: "StackPostCell", bundle: nil)
        tableView.register(stackCellNib, forCellReuseIdentifier:"StackPostCell")
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.reloadData()
        //tableView.bounces = false
        //tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        //tableView.estimatedRowHeight = 360.0
        //println(String(format: "StackLayoutViewController viewDidLoad finish"))
        
        getStackData()
    }

    // MARK: get data methods
    func getStackData(){
        /*
        for var index = 0; index < 30; ++index {
            let data = StackDetailData(NativeUserPicture: "photo1", UserName: "Dark Wing Duck", LatestDateTime: "Apr 9, 2015 09:35 AM to Liberty Ca", Message: "Fan Message text will go here and it will go on on and on and on and on and apodudl aljd ou alkdnelr yd oadfi adsjlenpfoiy da dypfo iadfoi neapone pafya dfn apoefy adof anefp naof aydof nepfoneof yfoaneopfyef fad fdasf yasdofy adsoifue", NativePictureUrl: "http://m2.i.pbase.com/g6/36/718136/2/71754072.pn7yNHAu.jpg")
            stackDetailData.append(data)
            
        }*/
    
        //print("getStackData")
        startConnection()
    }
    
    func startConnection(){
        //let company_id = "31"
        //let myUrl = "https://t1.crm.xpon.us"
        
        //let urlPath: String = myUrl + "/API/ML/User?company_id=" + company_id
        //for details in stack_details {
        
        if (stack_details["stack_type_id"] as? Int) != nil {
            //if(stack_type_id == 1){
                if let stack_detail_id = stack_details["id"] as? Int {
                    let urlPath: String = String(format: "%@/api/stack/data/%@/%@/false/false", myUrl, String(stack_detail_id))
                    //print(String(format: "getStackDetailJSON urlPath=>%@", urlPath))
                    let url: URL = URL(string: urlPath)!
                    let request: URLRequest = URLRequest(url: url)
                    let connection: NSURLConnection = NSURLConnection(request: request, delegate: self, startImmediately: false)!
                    connection.start()
                } else {
                    //print("no stack_detail_id!!!")
                }
            //}
        } else {
            
        }
        //}
    }
    
//    func connection(didReceiveResponse: NSURLConnection!, didReceiveResponse response: NSURLResponse!) {
//        // Received a new request, clear out the data object
//        if let httpResponse = response as? NSHTTPURLResponse {
//            //print(httpResponse.statusCode)
//            if(httpResponse.statusCode == 200){
//                
//            } else {
//                delegate?.sessionTimedOut()
//            }
//        } else {
//            assertionFailure("unexpected response")
//        }
//    }
    
    func connection(_ connection: NSURLConnection!, didReceiveData data: Data!){
//        let dataStr = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
//        print(String(format: "%@", dataStr!))
        self.detailRequestData.append(data)
    }
    
    func connectionDidFinishLoading(_ connection: NSURLConnection!) {
//        var err: NSError
        // throwing an error on the line below (can't figure out where the error message is)
        parseStackDetailJSON()
    }
    
    func parseStackDetailJSON(){
//        var jsonError: NSError?
//        var last_selected: String!
//        var last_selected_match: Bool = false
//        var shouldLoadId: String!
//        var loadMLItemIndex: Int!
        //if let json = NSJSONSerialization.JSONObjectWithData(detailRequestData, options: nil, error: &jsonError) as? NSArray {
        
        var json: NSArray!
        
        do{
            json = try JSONSerialization.jsonObject(with: detailRequestData as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSArray
        } catch {
            
        }
        if json != nil {
            //print(String(format: "parseStackDetailJSON json=>%@", json))
            
            self.postItems = json.mutableCopy() as! NSMutableArray
            //println(String(format: "self.postItems=>%@", self.postItems))
            tableView.reloadData()
            detailTableView.isHidden = false
            detailLoadFailView.isHidden = true
        } else {
            detailTableView.isHidden = true
            detailLoadFailView.isHidden = false
        }
        detailLoadingView.isHidden = true
        delegate?.stackDetailFinishedLoading(stackPage)
    }
    
    func parseStackDetailDataMess(){
        
    }
    
    func heightForView(_ text:String, charPerLine:CGFloat, lineHeight:CGFloat) -> CGFloat{
        let cellHeight: CGFloat! = CGFloat(text.count) / charPerLine * lineHeight
        //println(String(format: "heightForView h=>%@", cellHeight))
        return cellHeight
    }

    
    func configureTableView() {
        //tableView.rowHeight = UITableViewAutomaticDimension
        //tableView.estimatedRowHeight = 360.0
        //tableView.contentSize.height = 3000
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        //let postData = stackDetailData.posts[indexPath.row]
        //let font = UIFont(name: "Helvetica", size: 20.0)
        //let charPerLine = CGFloat(20.0)
        //let lineHeight = CGFloat(20.0)
        //println(String(format: "heightForView i=>%@", indexPath.row))
        if isLandscapeOrientation() {
            //return hasImageAtIndexPath(indexPath) ? 140.0 : 120.0
            return 120.0
            //return heightForView(postData.Message, charPerLine: charPerLine, lineHeight: lineHeight)
        } else {
            //return hasImageAtIndexPath(indexPath) ? 235.0 : 155.0
            return 93.0
            //return heightForView(postData.Message, charPerLine: charPerLine, lineHeight: lineHeight)
        }
        //return CGFloat(320.0)
    }


    func isLandscapeOrientation() -> Bool {
        return UIInterfaceOrientationIsLandscape(UIApplication.shared.statusBarOrientation)
    }


    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print("numberOfRowsInSection")
        return self.postItems.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //println("cellForRowAtIndexPath")
        let cell = tableView.dequeueReusableCell(withIdentifier: "StackPostCell", for: indexPath) as! StackPostCell
        
        /*
        let postData = stackDetailData[indexPath.row]
        let image = UIImage(named: postData.NativeUserPicture)
        cell.postImage.image = image
        cell.headerLabel1.text = postData.UserName
        cell.headerLabel2.text = postData.LatestDateTime
        cell.messageTxt.text = postData.Message
        */
        
        let image = UIImage(named: "photo1")
        cell.postImage.image = image
        
        if let postData = self.postItems[(indexPath as NSIndexPath).row] as? NSDictionary {
            //println(String(format: "cell postData"))
            
            if let UserName = postData["UserName"] as? String {
                cell.headerLabel1.text = UserName
            } else if let UserName = postData["Name"] as? String {
                cell.headerLabel1.text = UserName
            } else if let UserName = postData["CalculatedUserName"] as? String {
                cell.headerLabel1.text = UserName
            } else {
                cell.headerLabel1.text = "UserName/Name not found"
            }
            if let LatestDateTime = postData["LatestDateTime"] as? String {
                cell.headerLabel2.text = LatestDateTime
            } else if let LatestDateTime = postData["SuggestedScheduledDate"] as? String {
                cell.headerLabel2.text = LatestDateTime
            } else if let LatestDateTime = postData["NativeDate"] as? String {
                cell.headerLabel2.text = LatestDateTime
            } else {
                cell.headerLabel2.text = "LatestDateTime not found"
            }
            if let Message = postData["Message"] as? String {
                cell.messageTxt.text = Message
            } else {
                cell.messageTxt.text = "Message not found"
            }
        }

        cell.messageTxt.numberOfLines = 0
        //cell.separatorInset = UIEdgeInsetsZero
        cell.layoutMargins = UIEdgeInsets.zero
        //tableView.contentSize.height = tableView.contentSize.height + cell.frame.height
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(self.tableView.responds(to: #selector(setter: UITableViewCell.separatorInset))){
            self.tableView.separatorInset = UIEdgeInsets.zero
        }
            
        if(self.tableView.responds(to: #selector(setter: UIView.layoutMargins))){
            self.tableView.layoutMargins = UIEdgeInsets.zero
        }
            
        if(cell.responds(to: #selector(setter: UIView.layoutMargins))){
            cell.layoutMargins = UIEdgeInsets.zero
        }     
    }

    // Mark: Table View Delegate
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        //print("didDeselectRowAtIndexPath")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //print("didSelectRowAtIndexPath")
    }

}
