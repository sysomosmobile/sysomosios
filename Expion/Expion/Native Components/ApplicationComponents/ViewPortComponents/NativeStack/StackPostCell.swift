//
//  StackPostCell.swift
//  Workspaces
//
//  Created by Mike Bagwell on 4/13/15.
//  Copyright (c) 2015 Expion. All rights reserved.
//

import UIKit

class StackPostCell: UITableViewCell {

    @IBOutlet weak var ContentView: UIView!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var headerLabel1: UILabel!
    @IBOutlet weak var headerLabel2: UILabel!
    @IBOutlet weak var messageTxt: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
