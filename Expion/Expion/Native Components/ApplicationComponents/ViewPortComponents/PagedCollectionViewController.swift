//
//  PagedCollectionView.swift
//  Expion
//
//  Created by Mike Bagwell on 10/16/17.
//  Copyright © 2017 Expion. All rights reserved.
//

import Foundation
////////////////////
import UIKit

@objc
protocol PagedCollectionViewControllerDelegate {
    //optional func toggleLeftPanel()
    //optional func toggleRightPanel()
    @objc optional func collapseSidePanels()
    @objc optional func logWindowMsg(_ logTxt: String, timeCompare: Bool)
    func mlItemHasFinished()
    func logoutBtnClicked()
    func sessionTimedOut()
}

class PagedCollectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, StackViewFrameControllerDelegate {
    
    var company_id: String!
    var myUrl: String!
    var layout_id: String!
    var mlItem: MLItem!
    var mlId: String!
    var settingsString: String!
    var debugMode: Bool = false
    var loadExpion: Bool!
    
    var pageViews: [UIView?] = []
    var pageViewControllers: [StackViewFrameController?] = []
    
    var stackLayoutColumnWidth: Int = 0
    var stacksInLayout: Int = 0
    
    var delegate: PagedCollectionViewControllerDelegate?
    
    var stackIdArray: Array<String> = []
    
    @IBOutlet var collectionView: UICollectionView! //UIScrollView!
    
    override func viewDidLoad() {
//        for i in 0 ..<  4 {
//            stackIdArray.append(String(i))
//        }
//        let wwidth = UIScreen.main.bounds.width
//        print("PCVC--screen width=>\(wwidth)")
        
        getStackLayoutJSON()
    }
    
    func getStackLayoutJSON() {
        //let loginObject = ["email": email, "password": password]
        
        let urlPath: String = String(format:"%@/api/stacklayout/%@?layout_id=%@", myUrl, company_id, layout_id)
        let client = APIClient()
        client.getJsonResultForUrl(urlPath: urlPath) { (success, object) -> () in
            if success {
                //print("get JSON success")
                if let json = object as? NSArray {
                    //print(String(format:"*J*S*O*N* json=>%@", json))
                    self.parseStackLayoutJSON(json: json)
                }
            } else {
//                let message = object as! String
//                print("there was an error:" + message)
            }
        }
    }
    
    func parseStackLayoutJSON(json: NSArray){
        //        //print("PVC--parseStackLayoutJSON=>")
        
        if json.count > 0 {
            if let layoutData = json[0] as? NSDictionary {
                //                //print(String(format:"*J*S*O*N* json=>%@", json))
                //                //print("-==-")
                //                //print(String(format:"*J*S*O*N* layoutData=>%@", layoutData))
                //                //print("")
                if let stack_detailsData = layoutData["stack_details"] as? NSArray {
                    
                    var stackLayoutColumnWidthUpdate = 0
                    //print("!!--before-stackLayoutColumnWidth=\(stackLayoutColumnWidth)")
                    var stack_detailsArrayUpdate: Array<NSDictionary?> = []
                    var stackIdArrayUpdate: Array<String> = []
                    
                    for stack_columns in stack_detailsData {
                        if let stack_detailsDict = stack_columns as? NSDictionary {
                            if let stack_detail_id = stack_detailsDict["id"] as? Int {
                                stack_detailsArrayUpdate.append(stack_detailsDict)
                                stackIdArrayUpdate.append(String(stack_detail_id))
                            }
//                            if let column_count = stack_detailsDict["column_count"] as? Int {
//                                print("!!--column_count=\(column_count)")
//                                stackLayoutColumnWidthUpdate += column_count
//                            }
                            stackLayoutColumnWidthUpdate += 1
                        }
                    }
                    
                    //if(stackIdArrayUpdate != stackIdArray){
//                    self.stack_detailsArray.removeAll()
                    self.stackIdArray.removeAll()
                    
//                    self.stack_detailsArray = stack_detailsArrayUpdate
                    self.stackIdArray = stackIdArrayUpdate
                    self.stackLayoutColumnWidth = stackLayoutColumnWidthUpdate
                    
                    //print("!!--after-stackLayoutColumnWidth=\(stackLayoutColumnWidth)")
                    //print("stack_detailsArray count=>" + String(stack_detailsArray.count))
                    //print("stackIdArray count=>" + String(stackIdArray.count))
                    //}
                    
                    //TODO: FIX THIS JANK
                    let stacksInLayout: Int = stack_detailsArrayUpdate.count > 0 ? stack_detailsArrayUpdate.count : 0
                    if(stacksInLayout > 0) {
                        //print("!!--setPageCount => \(stacksInLayout)")
                        //self.pageCount = stack_detailsArrayUpdate.count don't set pageCount until page is added to screen
//                        DispatchQueue.main.async {
//                            self.noStacksLbl.isHidden = true
//                        }
                        //print("==>startIndex=>\(startIndex)")
                        
//                        if(startIndex > 0) {
//                            //loadInsertedStack()
//                            //                            stackInsertedData()
//                        } else {
                            self.setupStacksLayout(stacksInLayout: stacksInLayout)
//                        }
                    } else {
                        //no stacks in layout
                        //print("==>no stacks in layout!!")
//                        DispatchQueue.main.async {
//                            self.noStacksLbl.isHidden = false
//                        }
                        delegate?.mlItemHasFinished()
                        
                    }
                } else {
                    
                }
            }
        } else {
            //print("!!json nil")
//            DispatchQueue.main.async {
//                self.noStacksLbl.isHidden = false
//            }
            delegate?.mlItemHasFinished()
        }
    }
    
    func setupStacksLayout(stacksInLayout: Int){
//        DispatchQueue.main.async {
//            self.pageControl.currentPage = 0
//            self.pageControl.numberOfPages = stacksInLayout
//        }
        
        for _ in pageViews.count ..< stacksInLayout {
            pageViews.append(nil)
            pageViewControllers.append(nil)
        }
        
        if(stackIdArray.count > 0) {
            self.stacksInLayout = 1
            self.collectionView.reloadData()
        }
    }
    
    
    func addStackToLayout() {
//        collectionView.reloadItems(at: <#T##[IndexPath]#>)
    }
    
    func clearScrollView() {
        
    }
    
    func removeStackFromLayout() {
        
    }
    
    func buildInjectionScript(_ page: Int) -> String! {
        var injectionStr: String!
        let stackId = stackIdArray[page] as String
        let addNewStack = "False" //also unused
        if(stackId.count > 0) {
            let attributeValues: Array<MLAttributesKeyValue> =
                [MLAttributesKeyValue(key: "layout_id", value: layout_id),
                 MLAttributesKeyValue(key: "module_id", value: mlId),
                 MLAttributesKeyValue(key: "data-single-stack-id", value: stackId),
                 MLAttributesKeyValue(key: "class", value: "xMobile mobilize"),
                 MLAttributesKeyValue(key: "add-new-stack", value: addNewStack)]
            
            let mlAttr: MLAttributes = MLAttributes(layout_id: Int(layout_id)!, attributeValues: attributeValues)
            let mlDir: MLDirective = MLDirective(directive_name: "stacks-layout", attributes: mlAttr)
            
            let mlStack: MLStack = MLStack(directive: mlDir)
            
            let serializer: SerializableMLItem = SerializableMLItem()
            let serializedStr: NSString = serializer.toMLStackJsonString(mlStack)
            injectionStr = "loadXPComponent('" + (serializedStr as String) + "');"
        }
        return injectionStr
    }
    
    func stackDetailFinishedLoading(_ sPage: Int, triggerDir: Bool) {
        //print(String(format: "PSVC--stackDetailFinishedLoading sPage=>%@ count=>%@ lower=>%@ upper=>%@", String(sPage), String(self.pageCount), String(visiblePageLower), String(visiblePageUpper)))
        
        
        stackDetailStartLoad(sPage+1, addNewStack: "False")
        
//        if(triggerDir == true && (sPage+1) < self.pageViews.count && (sPage+1) <= visiblePageUpper) { // + 1) {
//            //print("stackDetailFinishedLoading=>startLoad ++")
//            stackDetailStartLoad(sPage+1, addNewStack: "False")
//        } else if(triggerDir == false && (sPage-1) >= 0 && self.pageViews.count >= 0 && (sPage-1) <= visiblePageLower) {
//            //print("stackDetailFinishedLoading=>startLoad --")
//            stackDetailStartLoad(sPage-1, addNewStack: "False")
//        } else {
//            hidePageControl(1.0)
//            //manageViewablePages()
//            //            loadVisiblePages()
//            delegate?.mlItemHasFinished()
//        }
    }
    
    func stackDetailStartLoad(_ slPage: Int, addNewStack: String) {
        //print(String(format: "*>*>*>--stackDetailStartLoad slPage=>%@", String(slPage)))
        if(0 <= slPage && slPage < stackIdArray.count && pageViews[slPage] == nil){
            self.stacksInLayout += 1
            let indexPath = IndexPath(row: slPage, section: 0)
            self.collectionView.insertItems(at: [indexPath])
        }
    }
    
    func logWindowMsg(_ logTxt: String, timeCompare: Bool) {
        delegate?.logWindowMsg!(logTxt, timeCompare: timeCompare)
    }
//}
//
//// MARK: - UICollectionViewDataSource
//extension PagedCollectionViewController {
    //1
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //2
    func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return stacksInLayout //searches[section].searchResults.count
    }
    
    let reuseIdentifier: String = "PagedCollectionViewCell"
    //3
    func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier,
                                                      for: indexPath)
        let page = indexPath.row
        //print("<<--cellForItemAt=>\(page)")
        if pageViews[page] != nil {
            //print("<<--do nothing=>\(page) is loaded")
        } else {
            //print("<<--load stack=>\(page)")
            cell.backgroundColor = UIColor.black
            
            let injectionStr = buildInjectionScript(page)
            var storyboard = UIStoryboard() //(name: "Main", bundle: nil)
            if #available(iOS 11, *) {
                storyboard = UIStoryboard(name: "Main", bundle: nil)
            } else {
                storyboard = UIStoryboard(name: "MainiOS10", bundle: nil)
            }
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let stackCntlr: StackViewFrameController = storyboard.instantiateViewController(withIdentifier: "StackViewFrameController") as! StackViewFrameController
            stackCntlr.stackTest = false
            stackCntlr.delegate = self
            //print("PSV--company_id=>" + company_id)
            
            stackCntlr.company_id = company_id
            stackCntlr.myUrl = myUrl
            stackCntlr.loadExpion = true //loadExpion
            stackCntlr.stackPage = Int(page)
            //stackCntlr.stack_details = stack_details
            stackCntlr.stackId = stackIdArray[page]
            stackCntlr.mlId = mlId
            stackCntlr.layoutId = layout_id
            stackCntlr.injectionScriptStr = injectionStr
            stackCntlr.debugMode = self.debugMode
            stackCntlr.restorationIdentifier = "stackPage" + String(page)
            
            DispatchQueue.main.async {
                let newStackFrame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                stackCntlr.view.frame = newStackFrame
                self.addChildViewController(stackCntlr)
                cell.addSubview(stackCntlr.view)
                
                stackCntlr.initializeWebView()
                self.pageViewControllers[page] = stackCntlr
                self.pageViews[page] = stackCntlr.view
            }
            
            stackCntlr.didMove(toParentViewController: self)
        }
        
        // Configure the cell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sWidth = self.view.bounds.width
        let sHeight = self.view.bounds.height
//        let wwidth = UIScreen.main.bounds.width
//        print("PCVC--screen width=>\(wwidth)--cell width=>\(sWidth)")
        return CGSize(width: sWidth, height: sHeight);
    }
}




import WebKit

class PagedCollectionViewCell: UICollectionViewCell, WKNavigationDelegate {
    @IBOutlet var frameContentView: UIView!
    var vfwkWebView: WKWebView!
    
    let urlString: String = "https://www.google.com/search?q=moose&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjKnd2H9ffWAhVq4YMKHYO-DuMQ_AUICigB&biw=1303&bih=702"
    
    override func didMoveToSuperview() {
//        self.loadAsWkWebView()
//        self.loadStackView()
    }
    
    func loadStackView() {
        
    }
    
//    func display(contentController content: UIViewController, on view: UIView) {
//        self.addChildViewController(content)
//        content.view.frame = view.bounds
//        view.addSubview(content.view)
//        content.didMove(toParentViewController: self)
//    }
    
    func loadAsWkWebView() {
//        var urlString: String!
        
        let stackUrlPath = URL(string: urlString)
        
        let cacheP: NSURLRequest.CachePolicy = .useProtocolCachePolicy //.reloadIgnoringLocalAndRemoteCacheData
        
    
        let request = NSMutableURLRequest(url: stackUrlPath!, cachePolicy: cacheP, timeoutInterval: 60.0)
        
//        let sWidth = self.bounds.width
//        let wwidth = UIScreen.main.bounds.width
        //print("PCVCell--screen width=>\(wwidth)--cell width=>\(sWidth)")
        
        DispatchQueue.main.async {
            let viewFrame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
//            self.frameLoadingView.frame = viewFrame
            self.frameContentView.frame = viewFrame
//            self.frameLoadFailView.frame = viewFrame
            
            let wvFrame = CGRect(x: 0, y: 0, width: self.frameContentView.bounds.width, height: self.frameContentView.bounds.height)
            
            self.vfwkWebView = WKWebView(frame: wvFrame)
            self.vfwkWebView.navigationDelegate = self
            self.vfwkWebView.isOpaque = true //should be true
            //            self.vfwkWebView.backgroundColor = UIColor.clear
            self.vfwkWebView.scrollView.bounces = false
            
            self.frameContentView.addSubview(self.vfwkWebView)

            
            self.vfwkWebView.load(request as URLRequest)
//            self.webViewLoadAttemptCount += 1
            
        }
        
//        delegate?.logWindowMsg!("SVF--Native Complete Web Handoff", timeCompare: true)
//        delegate?.logWindowMsg!(String(format:"SVF--vfwkWebView.load=>%@", urlString), timeCompare: false)
        
    }
}


