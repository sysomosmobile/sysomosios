//
//  WKStackFrameController.swift
//  Expion
//
//  Created by Mike Bagwell on 5/31/18.
//  Copyright © 2018 Expion. All rights reserved.
//

import UIKit
import WebKit
import Darwin

@objc
protocol WKStackViewFrameControllerDelegate {
    @objc optional func stackDetailFinishedLoading(_ sPage: Int, triggerDir: Bool)
    @objc optional func logoutTimedOut()
    @objc optional func collapseSidePanels()
    @objc optional func logWindowMsg(_ logTxt: String, timeCompare: Bool)
    @objc optional func stackInserted(_ sPage: Int, stackDetails: NSDictionary)
    @objc optional func stackAddedToLayout(_ sPage: Int, stack_id: Int)
    @objc optional func stackDeleted(_ sPage: Int)
    @objc optional func layoutUpdated(_ sPage: Int)
}

class WKStackViewFrameController: UIViewController, WKNavigationDelegate, JSONLocationDelegate, FlatTreeViewControllerDelegate {
    
    
    @IBOutlet weak var frameLoadingView: UIView!
    @IBOutlet weak var frameContentView: UIView!
    @IBOutlet weak var frameLoadFailView: UIView!
    
    @IBOutlet weak var reloadButton: UIButton!
    @IBOutlet weak var frameLoadFailTextView: UITextView!
    
    @IBOutlet weak var stackPageLabel: UILabel!
    @IBOutlet weak var myProgressView: UIProgressView!
    
    var isiOS11Enabled: Bool = false
    var stackTest: Bool = false
    var stackPage: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //print("WW--viewDidLoad--=>" + String(stackPage))
        //print("-\(stackPage)-injectionScriptStr=>" + injectionScriptStr)
        
        HTTPCookieStorage.shared.cookieAcceptPolicy = HTTPCookie.AcceptPolicy.always
        
        if #available(iOS 11.0, *) {
            isiOS11Enabled = true
        }
        
        var appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        appVersion = appVersion.replacingOccurrences(of: ".", with: "", options: NSString.CompareOptions.literal, range: nil)
        UserDefaults.standard.register(defaults: ["User-Agent": ShareData.sharedInstance.userAgent])
        DispatchQueue.main.async {
            if(self.stackTest == false){
                self.frameLoadingView.isHidden = false
                self.frameContentView.isHidden = false
                self.frameLoadFailView.isHidden = true
            } else {
                self.frameLoadFailView.isHidden = false
                self.frameLoadFailTextView.text = String(self.stackPage)
            }
            
            self.frameLoadingView.frame = self.view.bounds
            self.frameContentView.frame = self.view.bounds
            self.frameLoadFailView.frame = self.view.bounds
            
        }
        
        setStackPageLabel(sPage: self.stackPage)
    }
    
    func setStackPageLabel(sPage: Int){
        DispatchQueue.main.async {
            self.stackPageLabel.text = String(sPage)
            self.stackPageLabel.isHidden = (ShareData.sharedInstance.debugMode == true ? false : true)
        }
    }
    
    
    
    //: MARK JSONLocationDelegate
    func locJSONReturned(_ jsonLocReturn: JSONLocation) {
        
    }
    
    
}
