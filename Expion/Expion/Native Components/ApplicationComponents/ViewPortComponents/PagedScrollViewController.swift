//
//  PagedScrollViewController.swift
//  Expion
//
//  Created by Mike Bagwell on 1/27/16.
//  Copyright © 2016 Expion. All rights reserved.
//

import UIKit

@objc
protocol PagedScrollViewControllerDelegate {
    //optional func toggleLeftPanel()
    //optional func toggleRightPanel()
    @objc optional func collapseSidePanels()
    @objc optional func logWindowMsg(_ logTxt: String, timeCompare: Bool)
    func mlItemHasFinished()
    func logoutBtnClicked()
    func sessionTimedOut()
}

class PagedScrollViewController: UIViewController, UIScrollViewDelegate, StackViewFrameControllerDelegate {//StackDetailViewControllerDelegate { //, UIWebViewDelegate {
    
    @IBOutlet var scrollView: UIScrollView!
    var scrollContainerView: UIView = UIView(frame: CGRect.zero)
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var noStacksLbl: UILabel!
    
    var repeatLoad: Bool = false
    var shouldInject: Bool = true
    var stackMax: Int = 12
    var stackLayoutData: Array<StackLayoutData> = []
    var stackLayoutIndex: Int = 0
    
    var pageViews: [UIView?] = []
    var pageViewControllers: [StackViewFrameController?] = []
    //var stackXPos: CGFloat = 0
    var stackLayoutColumnWidth: Int = 0
    var delegate: PagedScrollViewControllerDelegate?
    //var colors: Array<UIColor>! = []
    
    var pageCount: Int = 0
    var pagesToShow: Int = 0
    var visiblePageLower: Int = 0
    var visiblePageUpper: Int = 0
    var pageCountThreshold: Int = 10
    
    var stack_detailsArray: Array<NSDictionary?> = []
    var stackIdArray: Array<String> = []
    
    var startContentOffset: CGFloat = 0.0
    
    var company_id: String!
    var myUrl: String!
    var layout_id: String!
    var mlItem: MLItem!
    var mlId: String!
    var settingsString: String!
    var debugMode: Bool = false
    
    var stackWidth: CGFloat!
    var loadExpion: Bool!
    var isDragging: Bool!
    
    var testUrlArray: Array<String> = []
    
    lazy var layoutRequestData = NSMutableData()
    var webViewPage: Int = 0
    
    var startIndex: Int = 0
    private var svTrailingLC: NSLayoutConstraint!
    private var svWidthLC: NSLayoutConstraint!
    private var svcWidthLC: NSLayoutConstraint!
    
    let serialQueue = DispatchQueue(label:"stackQueue")
    
//    var accessToken: String!
    let opQueue = OperationQueue()
    var response:URLResponse?
    var session:URLSession?
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ///Setup ScrollView
        delegate?.logWindowMsg!("PagedScrollViewController didLoad", timeCompare: false)
        scrollView.delegate = self
        DispatchQueue.main.async {
            self.pageControl.isHidden = true
        }
        isDragging = false
        self.pageCount = 0
        stackWidth = (self.view.bounds.size.width > 700) ? 320 : self.view.bounds.size.width
        pagesToShow = Int(ceil(self.view.bounds.size.width / stackWidth))
        
        ///Get Stack Data
        //print("PVC--viewDidLoad")
        clearScrollView({
            self.getStackLayoutJSON()
        })
    }
    
    // MARK: get data methods
    func getStackLayoutJSON(){
        //print("getStackLayoutJSON")
        startConnection()
    }
    
    func startConnection(){
        delegate?.logWindowMsg!("PVC getStackLayoutJSON", timeCompare: false)
        let urlPath: String = myUrl + "/api/stacklayout/" + company_id + "?layout_id=" + layout_id
        //print("PVC--urlPath=>" + urlPath)
        let url: URL = URL(string: urlPath)!
        
        let cookies = HTTPCookieStorage.shared.cookies!
        let headers = HTTPCookie.requestHeaderFields(with: cookies)
        
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 60.0)
        
        request.setValue(ShareData.sharedInstance.userAgent, forHTTPHeaderField:"User-Agent")
        
        if let accessToken = UserDefaults.standard.object(forKey: "accessToken") as! String? {
            if accessToken.count > 0 {
                request.setValue(accessToken, forHTTPHeaderField: "Authorization")
            }
        }
        
        request.allHTTPHeaderFields = headers
        
        //print("getStackLayoutJSON--cookies=>")
//        //print(HTTPCookieStorage.shared.cookies!)
        
        let sessionConfiguration = URLSessionConfiguration.default
        // disable the caching
        sessionConfiguration.urlCache = nil
        self.session = URLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: nil)
        
        if let task = self.session?.dataTask(with: request) {
            // start the task, tasks are not started by default
            task.resume()
        }
    }
    
    func parseStackLayoutJSON(){
        delegate?.logWindowMsg!("PVC parseStackLayoutJSON", timeCompare: false)
        //print("PVC--parseStackLayoutJSON=>%@", layoutRequestData)
        var json: NSArray!
        do {
            json = try JSONSerialization.jsonObject(with: layoutRequestData as Data, options: []) as? NSArray
        } catch {
            
        }
        
        if json != nil && json.count > 0 {
            if let layoutData = json[0] as? NSDictionary {
                //print(String(format:"*J*S*O*N* json=>%@", json))
                //print(String(format:"*J*S*O*N* layoutData=>%@", layoutData))
                if let stack_detailsData = layoutData["stack_details"] as? NSArray {
                    
                    var stackLayoutColumnWidthUpdate = 0
                    //print("!!--before-stackLayoutColumnWidth=\(stackLayoutColumnWidth)")
                    var stack_detailsArrayUpdate: Array<NSDictionary?> = []
                    var stackIdArrayUpdate: Array<String> = []
                    
                    for stack_columns in stack_detailsData {
                        if let stack_detailsDict = stack_columns as? NSDictionary {
                            if let stack_detail_id = stack_detailsDict["id"] as? Int {
                                stack_detailsArrayUpdate.append(stack_detailsDict)
                                stackIdArrayUpdate.append(String(stack_detail_id))
                            }
//                            if let column_count = stack_detailsDict["column_count"] as? Int {
//                                print("!!--column_count=\(column_count)")
//                                stackLayoutColumnWidthUpdate += column_count
//                            }
                            stackLayoutColumnWidthUpdate += 1
                        }
                    }
                    
                    if(stackIdArrayUpdate != stackIdArray){
                        stack_detailsArray.removeAll()
                        stackIdArray.removeAll()
                        
                        stack_detailsArray = stack_detailsArrayUpdate
                        stackIdArray = stackIdArrayUpdate
                        stackLayoutColumnWidth = stackLayoutColumnWidthUpdate
                        
                        //print("!!--after-stackLayoutColumnWidth=\(stackLayoutColumnWidth)")
                        //print("stack_detailsArray count=>" + String(stack_detailsArray.count))
                        //print("stackIdArray count=>" + String(stackIdArray.count))
                        
                        if(stack_detailsArray.count > 0) {
                            self.pageCount = stack_detailsArray.count
                            DispatchQueue.main.async {
                                self.noStacksLbl.isHidden = true
                            }
                            //print("==>startIndex=>\(startIndex)")
                            
                            if(startIndex > 0) {
                                loadInsertedStack()
                            } else {
                                setupStacksLayout()
                            }
                        } else {
                            //no stacks in layout
                            //print("==>no stacks in layout!!")
                            DispatchQueue.main.async {
                                self.noStacksLbl.isHidden = false
                            }
                            delegate?.mlItemHasFinished()
                                
                        }
                    }
                }
            }
        } else {
            //print("!!json nil")
        }
    }
    
    func setupStacksLayout() {
        delegate?.logWindowMsg!("PVC loadStacksLayout", timeCompare: false)
        
        DispatchQueue.main.async {
            self.pageControl.currentPage = 0
            self.pageControl.numberOfPages = self.pageCount
        }
        
        //print("before pageViews.count=>" + String(pageViews.count))
        //print("before pageViewControllers.count=>" + String(pageViewControllers.count))
        
        for _ in (pageViews.count)..<self.pageCount {
            pageViews.append(nil)
            pageViewControllers.append(nil)
        }
        
        //print("PVC--pageViews.count=>" + String(pageViews.count))
        //print("PVC--pageViewControllers.count=>" + String(pageViewControllers.count))
        
        let scrollViewPages: Int = repeatLoad == true ? stackMax : self.pageCount
        
        DispatchQueue.main.async {
            self.automaticallyAdjustsScrollViewInsets = false
            let pagesScrollViewSize = self.view.frame
            self.scrollView.frame = pagesScrollViewSize
            self.scrollView.frame.size.height = pagesScrollViewSize.height //- 64

            
            let contentWidth: CGFloat = self.stackWidth * CGFloat(self.stackLayoutColumnWidth)
            //print("!*-PVC--contentWidth=>" + String(describing: contentWidth))
            self.scrollView.contentSize = CGSize(width: contentWidth, height: (self.view.frame.height))
            
            var paging: Bool = false
            if(AppSettings.isPad() == false && self.view.bounds.size.width < 700) {
                 paging = true
            }
            self.scrollView.isPagingEnabled = paging
            self.pageControl.isHidden = (self.view.bounds.size.width > 700) ? true : false
            self.scrollView.bounces = false
        }
        
        //loadVisiblePages()
        let stacksPerPage: Int = Int(ceil(self.view.bounds.size.width / stackWidth))
        
        visiblePageLower = 0//-1
        visiblePageUpper = pageCount < stacksPerPage ? pageCount : stacksPerPage
        
        //loadVisiblePages()
        //for var index = 0; index < visiblePageUpper; index += 1 {
        if let stack_detailsLoad = stack_detailsArray[0] as NSDictionary! {
//            if(startIndex == 0) {
//                self.installScrollViewContainerWithConstraints()
//            } else {
//                self.resetScrollViewContraints()
//            }
            self.installScrollViewContainerWithConstraints()
        }
        //}
    }
    
    func loadVisiblePages() {
        
        // First, determine which page is currently visible
        let pageWidth = scrollView.frame.size.width
        let page = Int(floor((scrollView.contentOffset.x * 2.0 + pageWidth) / (pageWidth * 2.0)))
        
        // Update the page control
        DispatchQueue.main.async {
            self.pageControl.currentPage = page
        }
        
        // Work out which pages you want to load
        let firstPage =  visiblePageLower//page - 1
        let lastPage = (visiblePageUpper + 1) > self.pageCount ? visiblePageUpper : visiblePageUpper + 1 //page + 1
    
        
        // Purge anything before the first page
        for index in 0 ..< firstPage { //LOOP CHECK SHOULD THIS BE 0 or 1
            //print("PURGE before-" + String(firstPage) + "=>" + String(index))
            purgePage(index)
        }
        
        // Load pages in our range
        /*
         for var index = firstPage; index <= lastPage; ++index {
         loadPage(index)
         }
         */
        //        if let stack_detailsLoad = stack_detailsArray[0] as NSDictionary! {
        //            loadPage(0, stack_details: stack_detailsLoad, addNewStack: "False")
        //        }
        
        // Purge anything after the last page
        //print("loadVisiblePages-firstPage=>\(firstPage)--lastPage=>\(lastPage)")
        //print("loadVisiblePages-lastPage=>\(lastPage)--visiblePageUpper=>\(visiblePageUpper)")
        for index in lastPage ..< pageViews.count {
            //print("PURGE after-" + String(lastPage) + "=>" + String(index))
            purgePage(index)
        }
    }
    
    func redrawScrollViewContent() {
        //print("redrawScrollViewContent")
        if(pageViewControllers.count > 0) {
            for index in 0 ..< pageViewControllers.count { //***LOOP CHECK SHOULD THIS BE 0 or 1
                let stackFrame = pageViewControllers[index]!.view.frame
                let newStackFrame = CGRect(x: stackFrame.origin.x, y: stackFrame.origin.y, width: stackFrame.size.width, height: scrollView.bounds.height)
                DispatchQueue.main.async {
                    self.pageViewControllers[index]!.view.frame = newStackFrame
                }
            }
        }
        if(pageViews.count > 0) {
            for index in 0 ..< pageViews.count { //***LOOP CHECK SHOULD THIS BE 0 or 1
                let pageFrame = pageViews[index]!.frame
                let newFrame = CGRect(x: pageFrame.origin.x, y: pageFrame.origin.y, width: pageFrame.size.width, height: scrollView.bounds.height)
                DispatchQueue.main.async {
                    self.pageViews[index]?.frame = newFrame
                }
            }
        }
        
        let svViews = scrollView.subviews
        for svView in svViews{
            //print("clear sv Views")
            if !(svView is UIImageView) {
                if let svView = svView as? UIView {
                    let svFrame = svView.frame
                    let svNewFrame = CGRect(x: svFrame.origin.x, y: svFrame.origin.y, width: svFrame.size.width, height: scrollView.bounds.height)
                    DispatchQueue.main.async {
                        svView.frame = svNewFrame
                    }
                    
                    let subViews = svView.subviews
                    for sfView in subViews {
                        let sfFrame = sfView.frame
                        let sfNewFrame = CGRect(x: sfFrame.origin.x, y: sfFrame.origin.y, width: sfFrame.size.width, height: scrollView.bounds.height)
                        DispatchQueue.main.async {
                            sfView.frame = sfNewFrame
                        }
                    }
                }
            }
        }
       //print("done")
    }
    
    
    func installScrollViewContainerWithConstraints() {
        
        // Prevent system from creating layout constraints //
        DispatchQueue.main.async {
            self.scrollContainerView.translatesAutoresizingMaskIntoConstraints = false
        
        // The width of the scrollable area is defined as one //
        // and a half times that of the scroll view's width  //
//        let widthRatio : CGFloat = 1.5;
        
        //Resize for tablet
        
//        let scrollViewContentWidth = stackWidth * CGFloat(self.pageCount)
        
        // Add the container view and set its constraints so that   //
        // it resizes with the same proportions on screen rotations //
            self.scrollView.addSubview(self.scrollContainerView);
        }
        setScrollViewContraints()
    }
    
    func setScrollViewContraints() {
        DispatchQueue.main.async {
            let scrollViewContentWidth = self.stackWidth * CGFloat(self.stackLayoutColumnWidth)
            //print("PVC-scrollViewContentWidth=>\(scrollViewContentWidth)")
            self.scrollView.translatesAutoresizingMaskIntoConstraints = false
        
            let topLC : NSLayoutConstraint = NSLayoutConstraint(item: self.scrollContainerView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self.scrollView, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0)
            let leadingLC : NSLayoutConstraint = NSLayoutConstraint(item: self.scrollContainerView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: self.scrollView, attribute: NSLayoutAttribute.leading, multiplier: 1.0, constant: 0)
            let trailingLC = NSLayoutConstraint(item: self.scrollContainerView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: self.scrollView, attribute: NSLayoutAttribute.trailing, multiplier: 1.0, constant: 0)
            let bottomLC : NSLayoutConstraint = NSLayoutConstraint(item: self.scrollContainerView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self.scrollView, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0)
//            let widthLC : NSLayoutConstraint = NSLayoutConstraint(item: self.scrollContainerView, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: self.scrollView, attribute: NSLayoutAttribute.Width, multiplier: widthRatio, constant: 0)
        
            self.svWidthLC = NSLayoutConstraint(item: self.scrollContainerView, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: scrollViewContentWidth)

            let heightLC : NSLayoutConstraint = NSLayoutConstraint(item: self.scrollContainerView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: self.scrollView, attribute: NSLayoutAttribute.height, multiplier: 1.0, constant: 0)
        
            self.scrollView.addConstraints([topLC,leadingLC,trailingLC,bottomLC,self.svWidthLC,heightLC])
        
            self.scrollContainerView.backgroundColor = UIColor.lightGray //UIColor.clear
        }
        if let stack_detailsLoad = stack_detailsArray[0] as NSDictionary! {
            loadPage(0, stack_details: stack_detailsLoad, addNewStack: "False")
        }
    }
    
    
    func stackInserted(_ sPage: Int) {
        //insertNil
        
        //print("!!-1-stackInserted-sPage=>\(sPage)")
        serialQueue.sync {
            //print("!!-1-serialQueue")
            //print("!!-1-stackInserted-serialQueue-sPage=>\(sPage)")
            startIndex = sPage + 1
            //print("==>startIndex=>\(startIndex)")
            
//            
//    //        self.visiblePageUpper += 1
//    //        self.visiblePageLower += 1
//            
//            if(visiblePageUpper <= self.pageCount){
//                //print("TRIGGER->Upper scrollView.contentOffset.x >= triggerOffset")
//    //            let lPage = visiblePageUpper
//                visiblePageUpper = visiblePageUpper + 1 > self.pageCount ? visiblePageUpper : visiblePageUpper + 1
//                if(visiblePageUpper - visiblePageLower >= pageCountThreshold){
//                    visiblePageLower = visiblePageLower + 1
//                }
//    //            stackDetailFinishedLoading(lPage, triggerDir: true)
//            }
        
        }
        
        serialQueue.sync {
            //print("!!-2-serialQueue")
            if(startIndex > 0) {
                self.insertStackPrimer()
            }
        }
        
        //adjust scrollview
        serialQueue.sync {
            //print("!!-3-serialQueue")
            resetScrollViewContraints()
        }
        
        serialQueue.sync {
            //print("!!-4-serialQueue")
            let nilDict: NSDictionary = [:]
            //if let stack_detailsLoad = stack_detailsArray[startIndex] as NSDictionary! {
            //print("!-4-insertStackPrimer=loadPage=>startIndex=>\(startIndex)")
            loadPage(startIndex, stack_details: nilDict, addNewStack: "False")
            //}
        }
        
        serialQueue.sync {
            //print("!!-5-serialQueue")
            checkInsertScroll()
        }
        
        //slideStackOver
        
        //getLayoutJSON update
        
        //decide whether to do anything about other stacks or not
//        layoutUpdated(sPage)
    }
    
    func stackDeleted(_ sPage: Int) {
        //print("!-1-stackDeleted-stackLayoutColumnWidth=>\(stackLayoutColumnWidth)")
        if(sPage > 0){
            //self.startIndex = sPage
            
            let scrollX = CGFloat(sPage-1) * stackWidth
            let scrollY = scrollView.frame.origin.y
            //print("stackDeleted-sPage=>\(sPage)")
            //print("stackDeleted-scrollX=>\(scrollX)")
            
            //print("!!-1-serialQueue")
            //print("!!-1-stackInserted-serialQueue-sPage=>\(sPage)")
            //print("==>sPage=>\(sPage)")
            
            stackLayoutColumnWidth = stackLayoutColumnWidth > 0 ? stackLayoutColumnWidth - 1 : 0
            
            let scrollViewContentWidth = stackWidth * CGFloat(stackLayoutColumnWidth)
            
            //print("!-1-stackWidth=>\(stackWidth)-stackLayoutColumnWidth=>\(stackLayoutColumnWidth)")
            //print("!-1-resetScrollViewContraints-scrollViewContentWidth=>\(scrollViewContentWidth)")
            
            DispatchQueue.main.async {
                self.scrollView.translatesAutoresizingMaskIntoConstraints = false
            
                UIView.animate(withDuration: ShareData.sharedInstance.transitionSpeed, animations: {
                    
                    self.scrollView.contentSize.width = self.stackWidth * CGFloat(self.stackLayoutColumnWidth)
                    
                    self.scrollView.setContentOffset(CGPoint(x: scrollX, y: scrollY), animated: true)
                    self.svWidthLC.constant = scrollViewContentWidth
                    self.svcWidthLC.constant = scrollViewContentWidth
                }, completion: { finished in
                    //print("!!-1-animateCompleted")
                })
            }
            
            
            serialQueue.sync {
                //print("!!-2-serialQueue")
                if(startIndex > 0) {
                    self.pageCount = self.pageCount > 0 ? self.pageCount - 1 : 0
                    
                    //print("!!-3-stackDeleted=>startIndex=>\(self.startIndex)")
                    
                    //TODO: have to resize both scrollView and scrollContainerView
                    DispatchQueue.main.async {
                        self.pageControl.currentPage = sPage > 0 ? sPage - 1 : 0
                        self.pageControl.numberOfPages = self.pageCount

                    
//                    scrollView.addSubview(scrollContainerView);
//                    self.scrollContainerView.addSubview(stackCntlr.view)
//                    scrollView < scrollContainerView < stackView
                    

                        if let svcView = self.scrollContainerView.subviews[sPage] as? UIView{
                            //print("clear sv Views")
                            let svcViewSVs = svcView.subviews
                            for svcViewSV in svcViewSVs{
                                svcViewSV.removeFromSuperview()
                            }
                            self.scrollContainerView.willRemoveSubview(svcView)
                            svcView.removeFromSuperview()
                            //print(String(svcViewSVs.count))
                        }
                        
                        
                    }
                        
                    if let pView = pageViews[sPage]! as? UIView {
                        if(pView != nil){
                            let sViews = pView.subviews
                            DispatchQueue.main.async {
                                for subView in sViews{
                                    //print("clearScrollViews")
                                    subView.removeFromSuperview()
                                }
                            }
                        }
                    }
                    
                    if let pViewCntl = pageViewControllers[sPage]! as? StackViewFrameController {
                        //            pViewCntl?.clearContentView()
                        pViewCntl.dumpWebview()
                        pViewCntl.delegate = nil
                        DispatchQueue.main.async {
                            pViewCntl.willMove(toParentViewController: nil)
                        }
                    }
                    
                    pageViews.remove(at: sPage)
                    pageViewControllers.remove(at: sPage) //.insert(nil, at: self.startIndex)
                    
                    stack_detailsArray.remove(at: sPage)
                    stackIdArray.remove(at: sPage)
                    
                    DispatchQueue.main.async {
                        self.scrollView.setNeedsLayout()
                        self.scrollContainerView.setNeedsLayout()
                    }
                    self.deleteStackPrimer()
                    //print("!-2-deleteStackPrimer end")
                }
            }

            //adjust scrollview
            serialQueue.sync {
                //print("!!-3-serialQueue")
                //print("!-3-resetScrollViewContraints-startIndex=>\(startIndex)")
            }
//
//            serialQueue.sync {
//                //print("!!-4-serialQueue")
//                let nilDict: NSDictionary = [:]
//                //if let stack_detailsLoad = stack_detailsArray[startIndex] as NSDictionary! {
//                //print("!-4-insertStackPrimer=loadPage=>startIndex=>\(startIndex)")
//                loadPage(startIndex, stack_details: nilDict, addNewStack: "False")
//                //}
//            }
//            
//            serialQueue.sync {
//                //print("!!-5-serialQueue")
//                checkInsertScroll()
//            }
            
            
            
            
            
//            self.visiblePageUpper -= 1
//            self.visiblePageLower -= 1

//            if(-1 <= (visiblePageLower)){
//                //print("TRIGGER<-Lower scrollView.contentOffset.x <= triggerOffsetLower")
//                //            let lPage = visiblePageLower
//                visiblePageLower = visiblePageLower > 0 ? visiblePageLower - 1 : 0
//                if(visiblePageUpper - visiblePageLower >= pageCountThreshold){
//                    visiblePageUpper = visiblePageUpper - 1
//                }
//                //            if(lPage >= 0) {
//                //                stackDetailFinishedLoading(lPage, triggerDir: false)
//                //            }
//            }

            
        }
    }
    
    func insertStackPrimer() {
        DispatchQueue.main.async {
            self.pageCount += 1
            self.pageControl.currentPage = self.startIndex
            self.pageControl.numberOfPages = self.pageCount
        }
        pageViews.insert(nil, at: startIndex)
        pageViewControllers.insert(nil, at: startIndex)
        
        stackLayoutColumnWidth += 1
        
        let newDict = NSDictionary()
        stack_detailsArray.insert(newDict, at: startIndex)
        stackIdArray.insert("", at: startIndex)
        
        //print("!-2-insertStackPrimer end")
    }
    
    func deleteStackPrimer(){
        
    }
    
    func layoutUpdated(_ sPage: Int){
        //print("!!-6-layoutUpdated-sPage=>\(sPage)")
        serialQueue.sync {
            //print("!!-6-layoutUpdated-serialQueue-sPage=>\(sPage)")
            DispatchQueue.main.async {
                //print("!!-6-layoutUpdated-DispatchQueue.main-sPage=>\(sPage)")
                self.startIndex = sPage + 1
                if(self.startIndex != 0){
                    //print("!-6-layoutUpdated")
                    self.layoutRequestData.setData(Data())
                    self.getStackLayoutJSON()
                }
            }
        }
    }
    
    func resetScrollViewContraints(){
        let scrollViewContentWidth = stackWidth * CGFloat(stackLayoutColumnWidth)
        DispatchQueue.main.async {
            self.scrollView.translatesAutoresizingMaskIntoConstraints = false
            
            //print("!-3-resetScrollViewContraints-scrollViewContentWidth=>\(scrollViewContentWidth)")
            
            //        svWidthLC = NSLayoutConstraint(item: self.scrollContainerView, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: scrollViewContentWidth)
            
            self.scrollView.contentSize.width = self.stackWidth * CGFloat(self.stackLayoutColumnWidth)
            
            //scrollView.contentSize = CGSize(width: stackWidth * CGFloat(stackLayoutColumnWidth), height: (self.view.frame.height))
            self.svWidthLC.constant = scrollViewContentWidth
            
            self.scrollView.setNeedsLayout()
            self.scrollContainerView.setNeedsLayout()
        }
        //print("!-3-resetScrollViewContraints-startIndex=>\(startIndex)")
        
        //updateLayoutJSON
    }
    
    func loadInsertedStack() {
        let currentPage = startIndex
        if (!(currentPage < 0) && pageViewControllers.count > currentPage) {
            let stackVC = pageViewControllers[currentPage] as StackViewFrameController!
            let injectionStr = buildInjectionScript(currentPage)
            if (injectionStr != nil) {
                stackVC?.updateInjectScript(updateScript: injectionStr!)
            }
        }
    }
    
//    func updateJSON() {
//        //print("=loadInsertedStack=>startIndex=>\(startIndex)")
//        if(stack_detailsArray.count > (startIndex) && stack_detailsArray[startIndex] != nil) {
//            if let stack_detailsLoad = stack_detailsArray[startIndex] as NSDictionary! {
//                //print("loadInsertedStack=loadPage=>startIndex=>\(startIndex)")
//                loadPage(startIndex, stack_details: stack_detailsLoad, addNewStack: "False")
//            }
//        } else {
//            //print("!*-stack_detailsArray count=>\(stack_detailsArray.count)")
//            if(retryInsertLoadCount < 3){
//                usleep(1420)
//                retryInsertLoadCount += 1
//                //print("!*-less than expected retry \(retryInsertLoadCount)")
//                self.getStackLayoutJSON()
//            } else {
//                //print("!*-gave up after retry \(retryInsertLoadCount)")
//                retryInsertLoadCount = 0
//            }
//        }
//    }
    
    func buildInjectionScript(_ page: Int) -> String! {
        var injectionStr: String!
        let stackId = stackIdArray[page] as String
        let addNewStack = "False" //also unused
        if(stackId.count > 0) {
            let attributeValues: Array<MLAttributesKeyValue> =
                [MLAttributesKeyValue(key: "layout_id", value: layout_id),
                 MLAttributesKeyValue(key: "module_id", value: mlId),
                 MLAttributesKeyValue(key: "data-single-stack-id", value: stackId),
                 MLAttributesKeyValue(key: "class", value: "xMobile mobilize"),
                 MLAttributesKeyValue(key: "add-new-stack", value: addNewStack)]
            
            let mlAttr: MLAttributes = MLAttributes(layout_id: Int(layout_id)!, attributeValues: attributeValues)
            let mlDir: MLDirective = MLDirective(directive_name: "stacks-layout", attributes: mlAttr)
            
            let mlStack: MLStack = MLStack(directive: mlDir)
            
            let serializer: SerializableMLItem = SerializableMLItem()
            let serializedStr: NSString = serializer.toMLStackJsonString(mlStack)
            injectionStr = "loadXPComponent('" + (serializedStr as String) + "');"
        }
        return injectionStr
    }
    
    func loadPage(_ page: Int, stack_details: NSDictionary?, addNewStack: String) { //TODO: remove addNewStack always "False"
        delegate?.logWindowMsg!("PVC loadPage-" + String(page), timeCompare: false)
        //print("loadPage=>\(page)")
        //print(String(format: "LLL-loadPage=>%@ of self.pageCount=>%@ -LLL", String(page), String(self.pageCount)))
        if(page < self.pageCount){
            self.webViewPage = page
        }
        if page < 0 || self.pageCount < page {
            // If it's outside the range of what you have to display, then do nothing
            return
        }
        if(pageViews.count > page) {
            //print("loadPage=>stop")
            if pageViews[page] != nil {
                // Do nothing. The view is already loaded.
               //print("!!*--Do nothing. The view is already loaded=>")
            } else {
                if(page < self.pageCount){
                    /* Getting the page View controller */
                    var storyboard = UIStoryboard() //(name: "Main", bundle: nil)
                    if #available(iOS 11, *) {
                        storyboard = UIStoryboard(name: "Main", bundle: nil)
                    } else {
                        storyboard = UIStoryboard(name: "MainiOS10", bundle: nil)
                    }
//                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    //TODO: Tablet might need this changed
                    var injectionStr: String!
                    //print("stack_details=>")
                    
                    injectionStr = buildInjectionScript(page)
                    
                        //println(String(format:"III--injectionStr=>%@", injectionStr))
                    
                    let stackCntlr: StackViewFrameController = storyboard.instantiateViewController(withIdentifier: "StackViewFrameController") as! StackViewFrameController
                    
                    stackCntlr.delegate = self
                    //print("PSV--company_id=>" + company_id)
                    
                    stackCntlr.company_id = company_id
                    stackCntlr.myUrl = myUrl
                    stackCntlr.loadExpion = loadExpion
                    stackCntlr.stackPage = Int(page)
                    //stackCntlr.stack_details = stack_details
                    stackCntlr.stackId = stackIdArray[page]
                    stackCntlr.mlId = mlId
                    stackCntlr.layoutId = layout_id
                    stackCntlr.injectionScriptStr = injectionStr
                    stackCntlr.debugMode = self.debugMode
                    stackCntlr.restorationIdentifier = "stackPage" + String(page)
                    
                    DispatchQueue.main.async {
                        
                        var columnSize: CGFloat = 1
                        if let cc = stack_details?["column_count"] as? Int {
                            columnSize = CGFloat(cc)
                        }
                        self.scrollView.backgroundColor = UIColor.lightGray  //UIColor.clear
                        self.scrollContainerView.backgroundColor = UIColor.lightGray //UIColor.clear
                        
                        let stackXPos = self.stackWidth * CGFloat(page)//(stackWidth * columnSize)
                        //print("PVC-page=>" + String(describing: page))
                        //print("PVC-stackXPos=>" + String(describing: stackXPos))
                        
                        let svcWidth: CGFloat = self.stackWidth * columnSize
                        //print("PVC-svcWidth=>" + String(describing: svcWidth))
//                        //print("PVC-self.scrollView.bounds.height=>" + String(describing: self.scrollView.bounds.height))
                        
                        let newStackFrame = CGRect(x: stackXPos, y: 0, width: svcWidth, height: self.scrollView.bounds.height)
                        //print("stackCntlr set frame")
                        stackCntlr.view.frame = newStackFrame
                        
                        let newPageView = UIView()
                        //                let newPageFrame = CGRectMake(stackWidth * CGFloat(page), 0, stackWidth, scrollView.bounds.height)
//                        let newPageFrame = CGRect(x: stackXPos, y: 0, width: (self.stackWidth * columnSize), height: self.scrollView.bounds.height)
                        
                        
//                        newPageView.frame = newPageFrame  //!important for frame.origin
//                        newPageView.translatesAutoresizingMaskIntoConstraints = true

                        //                let heightConstraint = NSLayoutConstraint(item: newPageView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: scrollView, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 100)
                        
                          /////////////
//                        self.scrollContainerView.translatesAutoresizingMaskIntoConstraints = false
//                        stackCntlr.view.translatesAutoresizingMaskIntoConstraints = false
//
//                        let topLC : NSLayoutConstraint = NSLayoutConstraint(
//                            item: stackCntlr.view, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal,
//                            toItem: self.scrollContainerView, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0)
//                        
//                        let leadingLC : NSLayoutConstraint = NSLayoutConstraint(
//                            item: stackCntlr.view, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal,
//                            toItem: self.scrollContainerView, attribute: NSLayoutAttribute.leading, multiplier: 1.0, constant: stackXPos)
//                        
//                        let bottomLC : NSLayoutConstraint = NSLayoutConstraint(
//                            item: stackCntlr.view, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal,
//                            toItem: self.scrollContainerView, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0)
//                        
//                        self.svcWidthLC = NSLayoutConstraint(
//                            item: stackCntlr.view, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal,
//                            toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: svcWidth)


                        self.addChildViewController(stackCntlr)
                        
                        if(self.startIndex == 0) {
                            self.scrollContainerView.addSubview(stackCntlr.view)
                        } else {
                            self.scrollContainerView.insertSubview(stackCntlr.view, at: self.startIndex)
                        }
                        
//                        self.scrollContainerView.addConstraints([topLC,leadingLC,bottomLC,self.svcWidthLC])
                        self.scrollView.layoutIfNeeded()

                        
//    /////////////
//
                        stackCntlr.didMove(toParentViewController: self)

                        //print("!-4-loadPage")
                        self.pageViewControllers[page] = stackCntlr
                        self.pageViews[page] = stackCntlr.view //newPageView
                    
                        stackCntlr.initializeWebView()
                }
                    //stackXPos += (stackWidth * columnSize)
                    //print("!!-4-loadStack end")
                }
            }
        }
    }
    
    func checkInsertScroll(){
        //print("!-5-checkInsertScroll-startIndex=>\(startIndex)")
        if(startIndex != 0){
            
            let scrollX = CGFloat(startIndex) * stackWidth
            let scrollY = scrollView.frame.origin.y
            //print("!-5-checkInsertScroll-scrollX=>\(scrollX)--stackWidth=>\(stackWidth)")
            DispatchQueue.main.async {
                UIView.animate(withDuration: ShareData.sharedInstance.transitionSpeed, animations: {
                    self.scrollView.setContentOffset(CGPoint(x: scrollX, y: scrollY), animated: true)
                }, completion: { finished in
                    //print("animate=>complete")
                    //self.insertStackPrimer()
                    self.startIndex = 0
                })
            }
            
        }
    }
    
    func purgePage(_ page: Int) {
        //print("purgePage")
        //print("purgePage")
        if page < 0 || page >= pageViewControllers.count {
            // If it's outside the range of what you have to display, then do nothing
            return
        }
        
        //pageViewControllers[page]?.encodeRestorableStateWithCoder(self.coder)
        
        // Remove a page from the scroll view and reset the container array
        if let pageView = pageViews[page] {
            DispatchQueue.main.async {
                pageView.removeFromSuperview()
                self.pageViews[page] = nil
            }
        }
        
    }
    
    
    func addStackToLayout() {
        //stack_detailsArrayAdd.append(stackDetails)
        //print("++-addStackToLayout- stackWidth=>" + String(stringInterpolationSegment: stackWidth))
        //self.scrollView.contentSize = CGSizeMake(stackWidth * stackLayoutColumnWidth, (pagesScrollViewSize.height))
        DispatchQueue.main.async {
            self.scrollView.contentSize = CGSize(width: self.stackWidth * CGFloat(self.pageCount + 1), height: (self.view.frame.height))// - 64))
        }
        pageViews.append(nil)
        pageViewControllers.append(nil)
        stackIdArray.append("")
        
        //print("++--stack_detailsArray.count=>" + String(stack_detailsArray.count))
        let sdArray = NSDictionary()
        stack_detailsArray.append(sdArray)
        
        //print("++--stack_detailsArray.count=>" + String(stack_detailsArray.count))
        //stackDetailStartLoad(stack_detailsArray.count)
//        let loadIndex: Int = pageCount
        //print("++--loadIndex=>" + String(loadIndex))
        pageCount += 1
        //stackDetailStartLoad(loadIndex, addNewStack: "True")
    }
    
    // MARK: ScrollView methods
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //print("scrollViewDidScroll")
        showPageControl()
        checkScrollPosition()
    }
    
    private func scrollViewDidBeginDecelerating(_ scrollView: UIScrollView) {
       //print("scrollViewDidBeginDecelerating")
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

    }
    
    func scrollViewDidBeginDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        isDragging = true
        //print("scrollViewDidBeginDragging")
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        //print("scrollViewDidEndDragging")
        isDragging = false
        var timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(PagedScrollViewController.checkForPageControl), userInfo: nil, repeats: true)
    }
    
    func checkScrollPosition(){
        //print("START--scrollViewDidScroll>checkScrollPosition")

        let triggerOffsetUpper: CGFloat = CGFloat(visiblePageUpper) * CGFloat(stackWidth)
        let triggerOffsetLower: CGFloat = CGFloat(visiblePageLower) * CGFloat(stackWidth)

        
        let scrollStartPos = scrollView.contentOffset.x
        let scrollEndPos = scrollView.frame.size.width + scrollView.contentOffset.x
        //print(String(format:">>---------scrollEndPos=>%@", String(describing: scrollEndPos)))
        //print(String(format:">>>--triggerOffsetUpper=>%@", String(describing: triggerOffsetUpper)))
        //print(String(format:">>-------scrollStartPos=>%@", String(describing: scrollStartPos)))
        //print(String(format:">>>--triggerOffsetLower=>%@", String(describing: triggerOffsetLower)))

        let currentPage: Int = Int(scrollStartPos / scrollView.frame.size.width)
        //print(String(format:"ELSE scrollEndPos-lPage=>%@", String(describing: currentPage)))
        
        if(scrollEndPos >= triggerOffsetUpper && (visiblePageUpper) <= self.pageCount){
            //print("TRIGGER->Upper scrollView.contentOffset.x >= triggerOffset")
            let lPage = visiblePageUpper
            visiblePageUpper = visiblePageUpper + 1 > self.pageCount ? visiblePageUpper : visiblePageUpper + 1
            if(visiblePageUpper - visiblePageLower >= pageCountThreshold){
                visiblePageLower = visiblePageLower + 1
            }
            stackDetailFinishedLoading(lPage, triggerDir: true)
        } else if(scrollStartPos <= triggerOffsetLower && -1 <= (visiblePageLower)){
            //print("TRIGGER<-Lower scrollView.contentOffset.x <= triggerOffsetLower")
            let lPage = visiblePageLower
            visiblePageLower = visiblePageLower > 0 ? visiblePageLower - 1 : 0
            if(visiblePageUpper - visiblePageLower >= pageCountThreshold){
                visiblePageUpper = visiblePageUpper - 1
            }
            if(lPage >= 0) {
                stackDetailFinishedLoading(lPage, triggerDir: false)
            }
        } else {
            //print(String(format:"ELSE scrollEndPos=>%@", String(describing: scrollEndPos)))
        }
        
        notifyActiveStack(currentPage: currentPage)
        
        
        
//       //print(String(format:">>END------scrollEndPos=>%@", String(scrollEndPos)))
//       //print(String(format:">>>----visiblePageLower=>%@", String(visiblePageLower)))
//       //print(String(format:">>-----visiblePageUpper=>%@", String(visiblePageUpper)))
        
        if (isDragging == false) {
            hidePageControl(1.0)
        }
    }
    
    func notifyActiveStack(currentPage: Int){
        //print("notifyActiveStack=>\(currentPage)")
        if (!(currentPage < 0) && pageViewControllers.count > currentPage) {
            let stackVC = pageViewControllers[currentPage] as StackViewFrameController!
            stackVC?.stackBecameActive()
        }
    }
    
    // MARK: PageControl methods
    //@objc
    @objc func checkForPageControl() {
//        //print("checkForPageControl")
        if(pageControl.isHidden == false){
            hidePageControl(0.0)
        }
    }
    
    func showPageControl() {
//        //print("showPageControl")
        DispatchQueue.main.async {
            self.pageControl.isHidden = false
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.pageControl.alpha = 1.0
                }, completion: nil)
        }
    }
    
    func hidePageControl(_ delay: TimeInterval) {
        //print("hidePageControl")
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5, delay: delay, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.pageControl.alpha = 0.0
                }, completion: {
                    (value: Bool) in
                    self.pageControl.isHidden = true
            })
        }
    }
    
    // MARK: Manage loading methods
    func stackDetailStartLoad(_ slPage: Int, addNewStack: String) {
        //print(String(format: "*>*>*>--stackDetailStartLoad slPage=>%@", String(slPage)))
        if(0 <= slPage && slPage < stack_detailsArray.count){
            if let stack_detailsLoad = stack_detailsArray[slPage] as NSDictionary! {
                loadPage(slPage, stack_details: stack_detailsLoad, addNewStack: addNewStack)
            }
        }
    }
    
    func stackDetailFinishedLoading(_ sPage: Int, triggerDir: Bool) {
//       //print(String(format: "PSVC--stackDetailFinishedLoading sPage=>%@ count=>%@ lower=>%@ upper=>%@", String(sPage), String(self.pageCount), String(visiblePageLower), String(visiblePageUpper)))
        
        if(triggerDir == true && (sPage+1) < self.pageCount && (sPage+1) <= visiblePageUpper) { // + 1) {
           //print("stackDetailFinishedLoading=>startLoad ++")
            stackDetailStartLoad(sPage+1, addNewStack: "False")
        } else if(triggerDir == false && (sPage-1) >= 0 && self.pageCount >= 0 && (sPage-1) <= visiblePageLower) {
           //print("stackDetailFinishedLoading=>startLoad --")
            stackDetailStartLoad(sPage-1, addNewStack: "False")
        } else {
            hidePageControl(1.0)
            //manageViewablePages()
//            loadVisiblePages()
            delegate?.mlItemHasFinished()
        }
    }
    
    func logWindowMsg(_ logTxt: String, timeCompare: Bool) {
        delegate?.logWindowMsg!(logTxt, timeCompare: timeCompare)
    }
    
    func clearScrollView(_ completion: (()-> Void)? = nil){
        //print("QQQQ--clearScrollViews--PagedScrollViewController--QQQQ")
        DispatchQueue.main.async {
            self.noStacksLbl.isHidden = true
        }
        for pViewCntl in pageViewControllers {
//            pViewCntl?.clearContentView()
            pViewCntl?.dumpWebview()
            pViewCntl?.delegate = nil
            DispatchQueue.main.async {
                pViewCntl?.willMove(toParentViewController: nil)
            }
        }
        
//        pageViewControllers[page] = stackCntlr
//        pageViews[page] = stackCntlr.view //newPageView
        
        //print("QQQQ--svView in svViews{--QQQQ")
        for svcView in scrollContainerView.subviews {
            //print("clear sv Views")
            let svcViewSVs = svcView.subviews
            DispatchQueue.main.async {
                for svcViewSV in svcViewSVs{
                    svcViewSV.removeFromSuperview()
                }
                svcView.removeFromSuperview()
            }
           //print(String(svcViewSVs.count))
        }
        DispatchQueue.main.async {
            self.scrollContainerView.removeConstraints(self.scrollContainerView.constraints)
            for svView in self.scrollView.subviews {
                //print("clear sv Views")
                let svViewSVs = svView.subviews
                for svViewSV in svViewSVs{
                    svViewSV.removeFromSuperview()
                }
                svView.removeFromSuperview()
            }
            self.scrollContainerView = UIView(frame: CGRect.zero)
        }
        //print("QQQQ--pView in pageViews--QQQQ")
        DispatchQueue.main.async {
            for pView in self.pageViews {
                if(pView != nil){
                    let sViews = pView!.subviews
                    for subView in sViews{
                        //print("clearScrollViews")
                        subView.removeFromSuperview()
                    }
                }
            }
        }
//        for view in self.view.subviews {
//            view.removeFromSuperview()
//        }
        layoutRequestData.setData(Data())
        //print("QQQQ--loop completed--QQQQ")
        pageViews.removeAll(keepingCapacity: false)
        pageViewControllers.removeAll(keepingCapacity: false)
        stack_detailsArray.removeAll(keepingCapacity: false)
        stackIdArray.removeAll(keepingCapacity: false)
        if((completion) != nil) {
            completion!()
        }
    }
    
    func logoutTimedOut() {
        //print("PSVC--logoutTimedOut")
        delegate?.sessionTimedOut()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension PagedScrollViewController:URLSessionDelegate {
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        // We've got a URLAuthenticationChallenge - we simply trust the HTTPS server and we proceed
        //print("PVC--didReceive challenge")
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
    
    func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        // We've got an error
        if let err = error {
            //print("PVC--Error: \(err.localizedDescription)")
        } else {
            //print("PVC--Error. Giving up")
        }
        //            PlaygroundPage.current.finishExecution()
    }
}

extension PagedScrollViewController:URLSessionDataDelegate {
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didBecome streamTask: URLSessionStreamTask) {
        // The task became a stream task - start the task
        //print("PVC--didBecome streamTask")
        streamTask.resume()
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didBecome downloadTask: URLSessionDownloadTask) {
        // The task became a download task - start the task
        //print("PVC--didBecome downloadTask")
        downloadTask.resume()
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        // We've got a URLAuthenticationChallenge - we simply trust the HTTPS server and we proceed
        //print("PVC--didReceive challenge")
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: @escaping (URLRequest?) -> Void) {
        // The original request was redirected to somewhere else.
        // We create a new dataTask with the given redirection request and we start it.
        if let urlString = request.url?.absoluteString {
            //print("PVC--willPerformHTTPRedirection to \(urlString)")
        } else {
            //print("PVC--willPerformHTTPRedirection")
        }
        if let task = self.session?.dataTask(with: request) {
            task.resume()
        }
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        // We've got an error
        if let err = error {
            //print("PVC--Error: \(err.localizedDescription)")
        } else {
            //print("PVC--Session Complete")
            self.parseStackLayoutJSON()
        }
        //            PlaygroundPage.current.finishExecution()
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        // We've got the response headers from the server.
        //print("PVC--didReceive response")
        self.response = response
        self.layoutRequestData = NSMutableData()
        completionHandler(URLSession.ResponseDisposition.allow)
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        // We've got the response body
        //print("PVC--didReceive data")
        if let responseText = String(data: data, encoding: .utf8) {
            //print(self.response ?? "")
            //print("\nServer's response text")
            //print(responseText)
        }
        
        self.layoutRequestData.append(data)
    }
    
}
