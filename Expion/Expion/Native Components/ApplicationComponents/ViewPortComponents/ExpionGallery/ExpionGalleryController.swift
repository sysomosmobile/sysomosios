//
//  ExpionGallery.swift
//  Expion
//
//  Created by Mike Bagwell on 9/14/15.
//  Copyright (c) 2015 Expion. All rights reserved.
//

import Foundation
import UIKit
import WebKit
import Darwin

@objc
protocol ExpionGalleryControllerDelegate {
    @objc optional func stackDetailFinishedLoading(_ sPage: Int)
    @objc optional func logoutTimedOut()
    @objc optional func collapseSidePanels()
}

class ExpionGalleryController: UIViewController {
    
    //@IBOutlet weak var contentView: UIView!
    @IBOutlet weak var frameLoadingView: UIView!
    @IBOutlet weak var frameContentView: UIView!
    @IBOutlet weak var frameLoadFailView: UIView!
    
    @IBOutlet weak var reloadButton: UIButton!
    
    var isStack: Bool = true
    var delegate: ExpionGalleryControllerDelegate?
    
    var company_id: String!
    var myUrl: String!
    var loadExpion: Bool!
    var stackId: String!
    var mlId: String!
    var layoutId: String!
    var settingsSring: String!
    var injectionScriptStr: String!
    var shouldInject: Bool = false
    
//    var userString: String!
    
    var stackPage: Int!
    
    lazy var detailRequestData = NSMutableData()
    
    var postItems: NSMutableArray = []
    
    var requestData: String!
    var vfWebView: UIWebView!
    
    lazy var htmlRequestData = NSMutableData()
    
    @IBOutlet weak var myProgressView: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        var deviceStr: String!
//        if(UIDevice.currentDevice().userInterfaceIdiom == .Pad){
//            userString = "&mobilize=True&xMobile=True&xTablet=True"
//            deviceStr = "iPad"
//        } else {
//            userString = "&mobilize=True&xMobile=True"
//            deviceStr = AppSettings.deviceStr//"iPhone"
//        }
        
        var appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        appVersion = appVersion.replacingOccurrences(of: ".", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        UserDefaults.standard.register(defaults: ["User-Agent": ShareData.sharedInstance.userAgent])
    }
    
    
}
