//
//  LeftViewController.swift
//  SlideOutNavigation
//
//  Created by Mike Bagwell on 12/16/14.
//  Copyright (c) 2014 Expion. All rights reserved.
//

import UIKit

@objc
protocol SidePanelViewControllerDelegate {
    //func mlItemSelected(mlItem: MLItem)
    func logoutBtnClicked()
}

class SidePanelViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var logoutBtn: UIButton!
    
    var delegate: SidePanelViewControllerDelegate?
    
    var mlItems: Array<MLItem>!
    //var home_items: Array<MLItem>!
    //var user_home_items: Array<MLItem>!
    //var shared_home_items: Array<MLItem>!
    //var suggested_home_items: Array<MLItem>!

    //let logoutBtn: UIButton!
    
    struct TableView {
        struct CellIdentifiers {
            static let MLCell = "MLCell"
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.reloadData()
        /*
        var loadIndex: Int!
        for var index:Int = 1; index < user_home_items.count; index++
        {
            let MLItem = user_home_items[index]
            //println(String(format:"selected =>%@", MLItem.selected!))
            if(MLItem.selected==true){
                loadIndex = index
            }
        }
        if(loadIndex != nil){
            let selectedMlItem = user_home_items[loadIndex]
            delegate?.mlitemselected(selectedMlItem)
        } else {
            //println(String(format:"no loadIndex"))
        }
        */
        
        //print("SidePanelViewController viewDidLoad")
        
        /*
        if(user_home_items == nil){
            //println("user_home_items nil")
            user_home_items = []
        }*/
    }

    // MARK: Table View Data Source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mlItems.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /*
        if(user_home_items.count > indexPath.row){
            let cell = tableView.dequeueReusableCellWithIdentifier(TableView.CellIdentifiers.MLCell, forIndexPath: indexPath) as! MLCell
            cell.configureForMLItem(user_home_items[indexPath.row])
            return cell
        } else {
            return UITableViewCell()
        }
        */
        return UITableViewCell()
        //return cell
    }
    
    // Mark: Table View Delegate
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cellToDeSelect = tableView.cellForRow(at: indexPath)! as! MLCell
        //let cell = tableView.dequeueReusableCellWithIdentifier(TableView.CellIdentifiers.MLCell, forIndexPath: indexPath) as MLCell
        cellToDeSelect.contentView.backgroundColor = RenderUtils().colorWithHexString("#edf1f5")
        cellToDeSelect.imageNameLabel.textColor = RenderUtils().colorWithHexString("#333333")
        cellToDeSelect.imageCreatorLabel.textColor = RenderUtils().colorWithHexString("#333333")
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell = tableView.cellForRow(at: indexPath)! as! MLCell
        selectedCell.contentView.backgroundColor = RenderUtils().colorWithHexString("#ffffff")
        selectedCell.imageNameLabel.textColor = RenderUtils().colorWithHexString("#2199d0")
        selectedCell.imageCreatorLabel.textColor = RenderUtils().colorWithHexString("#2199d0")
        
        let selectedMlItem = mlItems[(indexPath as NSIndexPath).row]
        //delegate?.mlItemSelected(selectedMlItem)
    }
    
    /*
    func buttonAction(sender:UIButton!) {
        var btnsendtag:UIButton = sender
        if btnsendtag.tag == 22 {
            //println("Button tapped tag 22")
        }
    }*/
    @IBAction func logoutBtnAction(_ sender:UIButton) {
        //print("logoutBtn tapped")
        delegate?.logoutBtnClicked()
    }


}
