//
//  LeftMenuViewController.swift
//  SlideOutNavigation
//
//  Created by Mike Bagwell on 12/16/14.
//  Copyright (c) 2014 Expion. All rights reserved.
//

import UIKit

@objc
protocol LeftMenuViewControllerDelegate {
    func mlItemSelected(_ mlItem: MLItem)
    func refreshMLMenu()
    func logoutBtnClicked()
    @objc optional func deselectLastItem()
}

class LeftMenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    //@IBOutlet var tableView: UITableView!
    @IBOutlet weak var tableView: UITableView!
    //@IBOutlet weak var logoutBtn: UIButton!
    
    //var tableViewController = UITableViewController()
    var refreshControl = UIRefreshControl()
    @IBOutlet weak var shadowView: UIView!
    
    var delegate: LeftMenuViewControllerDelegate? //LeftPanelViewControllerDelegate?
    var mlData: Dictionary<String, Array<MLItem>>!
    
    //var mlItems: Array<MLItem>!
    var last_selected: Int!
    var user_home_items: Array<MLItem>!
    var home_items: Array<MLItem>!
    var shared_home_items: Array<MLItem>!
    
    var sectionCount: Int = 0
    var homeItemsSecStart: Int = 0
    var homeItemsSecEnd: Int = 0
    
//    var collapsedInd: Array<Bool> = []
    var selectedIndex: IndexPath! // = NSIndexPath(forRow: 1, inSection: 0)
    var showTableText: Bool = false
    var menuOffset: CGFloat = 0
    @IBOutlet weak var leftMenuConstraint: NSLayoutConstraint!
    //let logoutBtn: UIButton!
    
    
    //var peterDemoStr = loadXPComponent('{"directive":{"directive_name":"stacks-layout","attributes":{"attributeValues":[{"key":"layout_id","value":"9489"},{"key":"module_id","value":"10151"},{"key":"data-single-stack-id","value":"134419"},{"key":"class","value":"xMobile mobilize"},{"key":"add-new-stack","value":"False"}],"layout_id":9489},"webUrl":""}}');"
    
    struct TableView {
        struct CellIdentifiers {
            static let MLCell = "MLCell"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.view.clipsToBounds = true
            self.leftMenuConstraint.constant = self.menuOffset
            
            let topInset: CGFloat = 0.0
            //var rightInset: CGFloat = 0.0
            if(AppSettings.IS_IPAD == false){
    //            topInset = UIApplication.sharedApplication().statusBarFrame.size.height
                //rightInset = 10.0
            }
            let insets = UIEdgeInsets(top: topInset, left: 0, bottom: 0, right: 0)
            self.tableView.clipsToBounds = true
            
            self.tableView.contentInset = insets
            self.tableView.scrollIndicatorInsets = insets
            
            //print("LeftMenuViewController viewDidLoad")
            
            self.refreshControl = UIRefreshControl()
            self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
            self.refreshControl.addTarget(self, action: #selector(LeftMenuViewControllerDelegate.refreshMLMenu), for: UIControlEvents.valueChanged)
            self.tableView.addSubview(self.refreshControl)
        }
        setupLeftPanel(false)
    }
    
    func toggleTableText(_ shouldShow: Bool){
        /*
        for cell in self.tableView.visibleCells {
        let mlCell = cell as! MLCell
        mlCell.imageNameLabel.hidden = shouldShow
        }
        */
        self.showTableText = shouldShow
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func setupLeftPanel(_ isRefresh: Bool){
       //print("LeftMenuViewController setupLeftPanel")
        user_home_items = mlData["user_home_items"]
        home_items = mlData["home_items"]
        shared_home_items = mlData["shared_home_items"]
        
        var homeItemsStartIndex: Int = -1
        var homeItemsEndIndex: Int = -1
        var secCount: Int = 0
        
       //print("LMV-mlData.count=>" + String(mlData.count))
        
        for s in 0 ..< mlData.count
        {
            let key: String = Array(mlData.keys)[s]
            //initial 4 bucket loop, this should only fire once and only for home_items
            if(key.lowercased().range(of: "home_items") != nil && key.lowercased().range(of: "user_home_items") == nil && key.lowercased().range(of: "shared_home_items") == nil){
                //is home_items
                let mlItems: Array<MLItem> = mlData[key]!
                
                if(homeItemsStartIndex < 0) {
                   //print("LMV--mlItems.count=>" + String(mlItems.count))
                    secCount += mlItems.count
                    homeItemsStartIndex = s
                    homeItemsEndIndex = (secCount) - 1// + homeItemsStartIndex //****** this is the thing right here
                   //print("LMV--set homeItemsStartIndex=>" + String(s) + "-" + String(homeItemsStartIndex))
                   //print("LMV--set homeItemsEndIndex=>" + String(homeItemsEndIndex))
                }
               //print("LMV--mlItems.count=>" + String(mlItems.count))
            } else {
                //workspaces of any kind
                secCount += 1
            }
           //print("LMV--s=>" + String(s))
        }
       //print("LMV--homeItemsStartIndex=>" + String(homeItemsStartIndex))
       //print("LMV--homeItemsEndIndex=>" + String(homeItemsEndIndex))
       //print("LMV--secCount=>" + String(secCount))
        
        //print("MM--setupLeftPanel secCount=>" + String(secCount))
        
        if(sectionCount != secCount || homeItemsSecStart != homeItemsStartIndex || homeItemsSecEnd != homeItemsEndIndex) {
            sectionCount = secCount
            homeItemsSecStart = homeItemsStartIndex
            homeItemsSecEnd = homeItemsEndIndex
            
//            if(collapsedInd.count > 0){
//                collapsedInd.removeAll(keepCapacity: false)
//            }
//            
//            for var sC = 0; sC < sectionCount; ++sC
//            {
//                collapsedInd.append(false)
//            }
        }
        
        if(isRefresh == true){
            //println(">> refreshMLMenu() >>")
            DispatchQueue.main.async {
                if(self.tableView != nil) {
                    self.tableView.reloadData()
                }
                //print(">> tableView.reloadData() >>")
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    func refreshMLMenu() {
        delegate?.refreshMLMenu()
    }
    
    func refreshMLMenuCompleted(_ updatedMLData: Dictionary<String, Array<MLItem>>, updated_last_selected: Int) {
        //println("~L~ refreshMLMenuCompleted count=>" + String(updatedMLData.count))
        mlData.removeAll(keepingCapacity: false)
        mlData = updatedMLData
        last_selected = updated_last_selected
        setupLeftPanel(true)
    }
    
    
    // MARK: Table View Data Source
    
    //override 
    func numberOfSections(in tableView: UITableView) -> Int {
        //return 1
       //print("numberOfSectionsInTableView=>" + String(sectionCount))
        return sectionCount
    }
    
    func getMLItemBySectionRow(_ section: Int, row: Int) -> MLItem! {
//       //print("!!--getMLItemBySectionRow=>" + String(section) +  "-row=>" + String(row))
        
        //0 should never be my workspaces
        
        var mlItem: MLItem!
        var mlDataIndex: Int!
        
//        //first get data bucket
//        if(homeItemsSecStart <= section && section <= homeItemsSecEnd) { //--should be home_items
//            mlDataIndex = homeItemsSecStart
////           //print("N mlDataIndex=>1")
//        } else if (section < homeItemsSecStart) {
//            mlDataIndex = section //--My Workspace needs help
//            //force my workspace to be after home_items
////           //print("N mlDataIndex=>2")
//        } else if (section > homeItemsSecEnd) {
//            mlDataIndex = section - (homeItemsSecEnd - homeItemsSecStart)
////           //print("N mlDataIndex=>3")
//        }
        
        /////////////
        if(mlData != nil && mlData.count > 0) {
            //MenuSection Home Items Index (No Folder) = (0 - length of home_items)
            //MenuSection My Workspaces Folder Index = (home_items length - 1 - startIndex) + 1
            //MenuSection Shared Workspaces Folder Index = home_items length + 1 for My Workspaces Folder
            
            //mlDataIndex should point to the correct MenuSection
            
            
//            let homeItemSectionCount = (homeItemsSecEnd - homeItemsSecStart)

            /*
            
             home_items calculation
             
             [0,1] - mlItems array
             0 = home_items -- count = 4
             1 = user_home_items
             [0,1,2,3,4]    - flattened mlItems array
             [0,1,2,3]      - home_items
             [4]            - user_home_items
             0 = homeItemsSecStart
             3 = homeItemsSecEnd
            
            
            
            */
            
            let homeItemDifference = homeItemsSecStart - 0
            
//           //print("!!--getMLItemBySectionRow-homeItemsSecStart=>" + String(homeItemsSecStart))
//           //print("!!--getMLItemBySectionRow-homeItemsSecEnd=>" + String(homeItemsSecEnd))
//           //print("!!--getMLItemBySectionRow-homeItemSectionCount=>" + String(homeItemSectionCount))
//           //print("!!--getMLItemBySectionRow-homeItemDifference=>" + String(homeItemDifference))
//           //print("!!--getMLItemBySectionRow-section" + String(section) + " < ? =>" + String(homeItemSectionCount + homeItemDifference))
            
            /*
             homeItemSectionCount = 4
             [0, 1, 2, 3, 4]
             
             Should force 0-3 = homeItems all of the time
             4 should be mapped to first ML outside of home_items
             
             section = 0
             homeItemDifference = 0
             true && (3 - 0)=3 true 0-3
             
             section = 1
             homeItemDifference = 1
             true && (4 - 1)=3 true 0-3
             
             section = 2
             homeItemDifference = 1
             true && (4 - 1)=3 true 0-3
             
             section = 3
             homeItemDifference = 1
             true && (4 - 1)=3 true 0-3
            
             section = 4
             homeItemDifference = 1
             true && (4 - 1)=3 false
             
             */
            
            if (homeItemsSecStart >= 0 && 0 <= section && section <= (homeItemsSecEnd - homeItemDifference)) { //--force to home_items
//            if (homeItemsSecStart <= section && section <= homeItemsSecEnd) { //--force to home_items
                mlDataIndex = homeItemsSecStart
            } else {
                if(homeItemsSecStart > 0 || homeItemsSecStart < 0) {
                    mlDataIndex = 0
                } else {
                    mlDataIndex = 1
                }
            }
        }
        
//       //print("!!--mlDataIndex=>" + String(mlDataIndex))

//        [home_items, user_home_items]
//        [user_home_items, home_items]
//        
//        [home_items, user_home_items, 3rd]
//        [user_home_items, home_items, 3rd]
//        [3rd, home_items, user_home_items]
        /////////////
        
        let keys = Array(mlData.keys)
        
//        //print("keys got=>")
//        //print(keys)
//        //print(keys.count)
        
        
        var key: String = ""
        if (keys.count > 0 && keys.count > mlDataIndex){
            let keyStr = keys[mlDataIndex]
//            //print("keyStr=>")
//            //print(keyStr)
            if (keyStr.count > 0) {
                key = keyStr
            }
//           //print("!!--key=>" + key)
        }
        if (key.count > 0){
        
            var mlItems: Array<MLItem> = mlData[key]!
//            var adjustedSection
            //println("getMLItemBySectionRow key=> " + key + "==" + String(mlItems.count))
            //next --if home_items get section for item in bucket
            
            var mlItemData: MLItem!
            if(key.lowercased().range(of: "home_items") != nil && key.lowercased().range(of: "user_home_items") == nil && key.lowercased().range(of: "shared_home_items") == nil){
                //is home_items
                let sectionDifference = homeItemsSecStart > 0 ? (homeItemsSecStart - 0) : 0
//                let sectionHomeItems = sectionDifference > 0 && section > sectionDifference ? (section - sectionDifference) : section
                let sectionHomeItems = section > homeItemsSecEnd ? section - sectionDifference : section
                
                if(mlItems.count > sectionHomeItems){
                    mlItemData = mlItems[sectionHomeItems]
                } else {
                   //print("out of range else=>")
                }
                
            } else {
                //println("not home_item else=>") //no need for adjusting section
                mlItemData = mlItems[0]
            }
            /*
            if(key.lowercaseString.rangeOfString("user") != nil){
            //println("=<>= row=>" + String(row) + "=<>=")
            //println(currentSectionML.folder_ind)
            //println(">>>children count=>" + String(currentSectionML.children.count))
            }
            */
            
//           //print("mlItemData")
            
            if(mlItemData != nil && mlItemData.folder_ind == true && mlItemData.children.count > 0 && row >= 0){
                var mlChildren: Array<MLItem> = mlItemData.children
                //println("children count=>" + String(mlChildren.count))
                mlItem = mlChildren[row]
            } else {
                //println("row else=>")
                mlItem = mlItemData
            }
        } else {
           //print("!!--key was nil--!!")
        }
//        if(mlItem != nil){
//            let mlItemName = mlItem.name
//           //print("mlItem name=>" + (mlItemName != nil ? mlItemName : "WAS NIL!"))
//        }
        return mlItem
    }
    
    //override 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount: Int = 0
        //var mlDataIndex: Int = 0
        
        var mlItem: MLItem!
        mlItem = getMLItemBySectionRow(section, row:-1)
        if(mlItem != nil){
            if(mlItem.folder_ind == true){
                //println("mlItem.folder_ind == true")
            } else {
                //println("mlItem.folder_ind else")
            }
            if(mlItem.children.count > 0){
                //println("**NumRows**children count=>" + String(mlItem.children.count))
                rowCount = mlItem.children.count
            }
        } else {
            //println("mlItem was nil")
        }
        
        //println("**N section " + String(section) + "-row Count=>" + String(rowCount))
        return rowCount
    }
    
    //override 
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        //var key: String = Array(mlData.keys)[section]
        //println("titleForHeaderInSection")
        return "section " + String(section)
    }
    
    //override 
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        //println("titleForHeaderInSection")
        return 50
    }
    
    //override 
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    //override
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        //print("+++heightForRowAtIndexPath")
        //        //print("+++indexPath--" + String(indexPath.section) + "-" + String(indexPath.row))
        //
        var mlItem: MLItem!
        mlItem = getMLItemBySectionRow((indexPath as NSIndexPath).section, row:-1)
        //println("back from getMLItem")
        if(mlItem != nil){
            //            //print("+++" + String(mlItem.selected_ind))
            //if(sectionCount > indexPath.section && collapsedInd.count > indexPath.section){
            if(mlItem.selected_ind == true){
                return 44
            } else {
                return 0
            }
            //}
        } else {
            return 0
        }
        
    }
    
    //override 
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //print("---LMVC-table-------viewForHeaderInSection")
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 40))
        
        var headerStr: String = ""
        
        var mlItemData: MLItem!
//        var mlDataIndex: Int = 0
        
        //var mlItem: MLItem!
        mlItemData = getMLItemBySectionRow(section, row:-1)
        if(mlItemData != nil) {
            //Start styling for section header
            
            if mlItemData != nil {
                headerStr = mlItemData.name
            }
           
            
            var selected: Bool = false
            //print("----Section=>" + String(section) + "-=>" + mlItemData.name + "-=>" + String(selected))
            if mlItemData != nil && mlItemData.selected_ind == true {  //collapsedInd[section]
                selected = true
                let row: Int = -1
                if (mlItemData.folder_ind == false) {
                    let indexPath : IndexPath = IndexPath(row: row, section:section)
                    selectedIndex = indexPath
                }
            } else {
                
            }
            let testBundle = Bundle(for: type(of: self))
            var imageStr: String = "IconGlobe"
            if mlItemData != nil {
                let cssString: String = "navIcons/" + mlItemData.css_class_name + (selected == true ? "-Blue" : "")
                let fileURL = testBundle.url(forResource: cssString, withExtension: "png")
                if(fileURL != nil) {
                    //println("*>*>fileURL=>" + String(stringInterpolationSegment: fileURL))
                    imageStr = "navIcons/" + mlItemData.css_class_name + (selected == true ? "-Blue" : "")
                }
            }
            
            //print("*>*>imageStr=>" + imageStr)
            
            //DispatchQueue.main.async {

                headerView.backgroundColor = UIColor.white
                headerView.tag = section
                
                let headerTxt = UILabel(frame: CGRect(x: 38, y: 10, width: self.tableView.frame.size.width-38, height: 30)) as UILabel
            
                let image: UIImage = UIImage(named: imageStr)!
                let imageView: UIImageView = UIImageView(frame: CGRect(x: 10, y: 15, width: 20, height: 20))
                imageView.image = image
                headerView.addSubview(imageView)
                
                headerTxt.text = headerStr
                headerTxt.textColor = (selected == true ? RenderUtils().colorWithHexString("#2199d0") : RenderUtils().colorWithHexString("#333333"))
                headerTxt.isHidden = self.showTableText
                headerView.addSubview(headerTxt)
                
                let headerTapped = UITapGestureRecognizer (target: self, action:#selector(self.sectionHeaderTapped(_:)))
                headerView.addGestureRecognizer(headerTapped)
                //headerView.addTarget(self, action: #selector(self.sectionHeaderTapped(_:)), forControlEvents: .TouchUpInside)
            //}
        }
        
        return headerView
    }
    
    func deselectMLforIndexPath(_ section: Int, row: Int) {
        //var mlItemData: MLItem!
       //print("deselectMLforIndexPath=>" + String(section) + " - " + String(row))
        
        var mlItem: MLItem!
        var mlDataIndex: Int!
        
        //first get data bucket
//        if(homeItemsSecStart <= section && section <= homeItemsSecEnd) {
//            mlDataIndex = homeItemsSecStart
//        } else if (section < homeItemsSecStart) {
//            mlDataIndex = section
//        } else if (section > homeItemsSecEnd) {
//            mlDataIndex = section - (homeItemsSecEnd - homeItemsSecStart)
//        }
        
        /////////////
        if(mlData != nil && mlData.count > 0) {
            //MenuSection Home Items (No Folder) = 0 - length of home_items
            //MenuSection My Workspaces Folder = home_items length + 1
            //MenuSection Shared Workspaces Folder = home_items length + 1 for My Workspaces Folder
            
            //mlDataIndex should point to the correct MenuSection
            
            let homeItemSectionCount = (homeItemsSecEnd - homeItemsSecStart)
            let homeItemDifference = homeItemsSecStart - 0
            
            if (0 <= section && section < (homeItemSectionCount + homeItemDifference)) { //--force to home_items
                mlDataIndex = homeItemsSecStart
            } else {
                if(mlData.keys.count <= 1 || homeItemsSecStart > 0) {
                    mlDataIndex = 0
                } else {
                    mlDataIndex = 1
                }
            }
        }
        
        let key: String = Array(mlData.keys)[mlDataIndex]
        var mlItems: Array<MLItem> = mlData[key]!
    
        var mlItemData: MLItem!
        
        if(key.lowercased().range(of: "home_items") != nil && key.lowercased().range(of: "user_home_items") == nil && key.lowercased().range(of: "shared_home_items") == nil){
            //is home_items
            if(mlItems.count > section){
                mlItemData = mlItems[section]
            } else {
                //print("out of range else=>")
            }
            
        } else {
            //println("not home_item else=>")  
            //Workspaces!!
            mlItemData = mlItems[0]
        }
        
        if(mlItemData != nil) {
            //print(mlItemData)
            if(mlItemData.folder_ind == true && mlItemData.children.count > 0 && row >= 0){
                var mlChildren: Array<MLItem> = mlItemData.children
                //println("children count=>" + String(mlChildren.count))
                mlItem = mlChildren[row]
            } else {
                mlItem = mlItemData
                mlItem.selected_ind = false
            }
            
            if((key.lowercased().range(of: "user_home_items")) != nil && row >= 0) {
                mlItem.selected_ind = false
            }
        }
    }
    
    func deselectLastItem() {
        if(selectedIndex != nil) {
            deselectMLforIndexPath(selectedIndex.section, row: selectedIndex.row)
            handleTableSelectionReload(selectedIndex, newIndexPath: nil)
        }
    }
    
    //@objc
    @objc func sectionHeaderTapped(_ sender: UITapGestureRecognizer) {
        //print("section Header Tapped")
        
        
        let section: Int = (sender.view?.tag as Int?)!
        let row: Int = -1
        let indexPath : IndexPath = IndexPath(row: row, section:section) // row was 0
        
        var mlItemData: MLItem!
        mlItemData = getMLItemBySectionRow(section, row: row)
        
        
        if(mlItemData != nil){
            if let oldIndex = selectedIndex as IndexPath? {
                var selected: Bool = true
                if(mlItemData.folder_ind == true) {
                    selected = !mlItemData.selected_ind
                }
                //this tries to mark data selected -- probably doesn't
                mlItemData.selected_ind = selected
                
                
                
                if(mlItemData.folder_ind == false) {
                    //set new selected
                    if(selectedIndex != nil && selectedIndex != indexPath
                        && selectedIndex.section != (indexPath as NSIndexPath).section) {
                        
                        deselectMLforIndexPath(oldIndex.section, row: oldIndex.row)
                    } else {
                        //print("nil--selectedIndex--nil!!")
                    }
                    selectedIndex = indexPath
                    delegate?.mlItemSelected(mlItemData)
                }
                
                handleTableSelectionReload(oldIndex, newIndexPath: indexPath)
            }
        }
    }
        
//        @available(iOS 2.0, *)
//        public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//            <#code#>
//        }
    
    //override 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var isChild: Bool = false
        var isVis: Bool = true
        //var mlDataIndex: Int = 0
        
        var mlItem: MLItem!
        mlItem = getMLItemBySectionRow((indexPath as NSIndexPath).section, row:(indexPath as NSIndexPath).row)
        
        var mlItemParent: MLItem!
        if(mlItem.folder_ind == true && mlItem.children.count > 0 && mlItem.children.count > (indexPath as NSIndexPath).row) {
            isChild = false
        } else { // if(mlItem.children.count > indexPath.section){
            isChild = true
            mlItemParent = getMLItemBySectionRow((indexPath as NSIndexPath).section, row:-1)
            //println("back from getMLItem")
            if(mlItemParent != nil){
                if(mlItemParent.selected_ind == true){
                    isVis = true
                } else {
                    isVis = false
                }
            }
        }
        
        //if(mlItems.count > indexPath.row){
        //print("TV--cellForRowAtIndexPath=>" + String(indexPath.section) + "-" + String(indexPath.row))
        
        if(mlItem != nil) {
            //print("TV--mlItem has value")
            let cell = tableView.dequeueReusableCell(withIdentifier: TableView.CellIdentifiers.MLCell, for: indexPath) as! MLCell
            //let selectedCell: Bool = (last_selected == mlItem.id) ? true : false
            
            if(selectedIndex == nil && (last_selected == mlItem.id || mlItem.selected_ind == true)){
                selectedIndex = indexPath
                //print("TV--last_selected==mlItem.id=>" + String(mlItem.id))
            }
            
            cell.configureForMLItem(indexPath, mlItem: mlItem, childCell: isChild, selectedCell: mlItem.selected_ind, sectionCollapsed: isVis, showTableText: !showTableText)//collapsedInd[indexPath.section])
            
            return cell
            
        } else {
            //print("TV--!!--mlItem NULL--!!")
            return UITableViewCell()
        }
        
    }
    
    // Mark: Table View Delegate
    //override 
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        let cellToDeSelect = tableView.cellForRowAtIndexPath(indexPath)! as! MLCell
//        cellToDeSelect.imageNameLabel.textColor = RenderUtils().colorWithHexString("#333333")//("#333333")
//        cellToDeSelect.imageCreatorLabel.textColor = RenderUtils().colorWithHexString("#333333")//("#333333")
    }
    
    //override 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedMlItem: MLItem = getMLItemBySectionRow((indexPath as NSIndexPath).section, row: (indexPath as NSIndexPath).row) as MLItem
        selectedMlItem.selected_ind = true
        delegate?.mlItemSelected(selectedMlItem)
        
        if (selectedIndex != nil) {
            if(indexPath.section == selectedIndex.section && indexPath.row == selectedIndex.row) {
                //selected current ml item
            } else {
                deselectMLforIndexPath(selectedIndex.section, row: selectedIndex.row)
                let oldIndex: IndexPath = selectedIndex
                let oldMlItem: MLItem = getMLItemBySectionRow((oldIndex as NSIndexPath).section, row: (oldIndex as NSIndexPath).row) as MLItem
                oldMlItem.selected_ind = false
                handleTableSelectionReload(oldIndex, newIndexPath: indexPath)
            }
        } else {
            //needs to force deselect all mlitems that are not folders
            handleTableSelectionReload(nil, newIndexPath: indexPath)
        }
        
        selectedIndex = indexPath
    }
    
    func handleTableSelectionReload(_ oldIndexPath: IndexPath?, newIndexPath: IndexPath?) {
        if (oldIndexPath != newIndexPath && oldIndexPath != nil) {
            animateTableSelectionForIndex(oldIndexPath!)
        }
        if (newIndexPath != nil) {
            animateTableSelectionForIndex(newIndexPath!)
        }
    }
    
    func animateTableSelectionForIndex(_ indexPath: IndexPath){
        DispatchQueue.main.async {
            if ((indexPath as NSIndexPath).row >= 0) {
                //print("^^-reloadRows")
                UIView.performWithoutAnimation {
                    self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
                }
            } else {
                //print("^^-reloadSections")
                let range = NSMakeRange((indexPath as NSIndexPath).section, 1)
                let sectionToReload = IndexSet(integersIn: Range(range) ?? 0..<0)
                self.tableView.reloadSections(sectionToReload, with:UITableViewRowAnimation.fade)
            }
        }
    }
    
    /*
    func buttonAction(sender:UIButton!) {
    var btnsendtag:UIButton = sender
    if btnsendtag.tag == 22 {
    //println("Button tapped tag 22")
    }
    }*/
    func logoutBtnAction(_ sender:UIButton) {
        //println("logoutBtn tapped")
        delegate?.logoutBtnClicked()
    }
    
    
}

class MLCell: UITableViewCell {
    @IBOutlet weak var MLImageView: UIImageView!
    @IBOutlet weak var imageNameLabel: UILabel!
    @IBOutlet weak var imageCreatorLabel: UILabel!
    
    func configureForMLItem(_ selectedIndex: IndexPath, mlItem: MLItem, childCell: Bool, selectedCell: Bool, sectionCollapsed: Bool, showTableText: Bool) {
        //print("LMVC--configureForMLItem")
//        self.indentationLevel = 0
//        self.layoutMargins = UIEdgeInsets.zero
        DispatchQueue.main.async {
            if(selectedCell == true){
                self.imageNameLabel.textColor = RenderUtils().colorWithHexString("#2199d0")
                self.imageCreatorLabel.textColor = RenderUtils().colorWithHexString("#2199d0")
            } else {
                self.imageNameLabel.textColor = RenderUtils().colorWithHexString("#333333")
                self.imageCreatorLabel.textColor = RenderUtils().colorWithHexString("#333333")
            }
            
            let nameLabelTxt = (showTableText == true ? mlItem.name : "")
           //print(">>" + nameLabelTxt + "=>" + String(mlItem.selected_ind))
            
            if(childCell == false){
                self.imageNameLabel.text = nameLabelTxt
                self.contentView.backgroundColor = RenderUtils().colorWithHexString("#ffffff")
            } else {
                //(showTableText == true ? "  • " + mlItem.name : "  •  ")
                self.imageNameLabel.text = "  • " + nameLabelTxt
                self.contentView.backgroundColor = RenderUtils().colorWithHexString("#edf1f5")
                
                if(sectionCollapsed == false){
                    self.contentView.isHidden = true
                } else {
                    self.contentView.isHidden = false
                }
            }
        }
    }
}
