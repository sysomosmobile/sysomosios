//
//  RightPanelViewController.swift
//  Workspaces
//
//  Created by Mike Bagwell on 4/27/15.
//  Copyright (c) 2015 Expion. All rights reserved.
//

import UIKit

@objc
protocol RightPanelViewControllerDelegate {
    //func mlItemSelected(mlItems: MLItem)
    func logoutBtnClicked()
}

class RightPanelViewController: UIViewController {
    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var maskView: UIView!
    @IBOutlet weak var logoutBtn: UIButton!
    @IBOutlet weak var userNameLabel: UILabel!
    
    var delegate: RightPanelViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateUserNameLabel()
    }

    func updateUserNameLabel() {
        let userName:String = UserDefaults.standard.object(forKey: "userName") as! String
        userNameLabel.text = userName
    }
    
    @IBAction func logoutBtnAction(_ sender:UIButton) {
        //print("logoutBtn tapped")
        delegate?.logoutBtnClicked()
    }
    
    
}
