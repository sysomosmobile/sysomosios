//
//  CenterViewController.swift
//  SlideOutNavigation
//
//  Created by Michael Bagwell on 12/16/14.
//  Copyright (c) 2014 Expion. All rights reserved.
//

import UIKit

@objc
protocol CenterViewControllerDelegate {
    @objc optional func toggleLeftPanel()
    @objc optional func toggleRightPanel()
    @objc optional func collapseSidePanels()
    @objc optional func hideUserMenu()
    @objc optional func launchGallery(_ displayMode: String)
    func returnToLogin()
    func userLoggedOut()
    func sessionTimedOut()
    func getMLJSON()
    func updateLastSelected(_ mlItem: MLItem)
    @objc optional func loadInsightView()
    @objc optional func hasConnectivity() -> Bool
    @objc optional func showNotConnectedAlert()
    @objc optional func logWindowMsg(_ logTxt: String, timeCompare: Bool)
}

class CenterViewController: UIViewController, LeftMenuViewControllerDelegate, RightPanelViewControllerDelegate, StackViewFrameControllerDelegate, MobileNotificationDelegate, UIScrollViewDelegate, LegacyPagedScrollViewControllerDelegate { //PagedCollectionViewControllerDelegate { //LeftPanelViewControllerDelegate
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var leftMenuView: UIView!
    @IBOutlet weak var loadingView: UIView!
    
//    var company_id: String!
//    var myUrl: String!
//    var deviceStr: String!
//    var debugMode: NSNumber!
//    var peterDemoMode: NSNumber!
    
    
    let company_id = ShareData.sharedInstance.company_id!
    let myUrl = ShareData.sharedInstance.myUrl!
    let debugMode = ShareData.sharedInstance.debugMode!
    
    var logWindowVis: Bool = false
    var loadExpion: Bool = true
    var delegate: CenterViewControllerDelegate?

    var pagedNavigationController: UINavigationController!
    var LegacyPagedScrollViewController: LegacyPagedScrollViewController!
//    var LegacyPagedScrollViewController: PagedCollectionViewController!
    var MLStackBaseClassObj: MLStackClass!
    var MLGalleryBaseClassObj: MLGalleryClass!
    var MLGalleryUploadBaseClassObj: MLGalleryUploadClass!
    
    lazy var workspaceRequestData = NSMutableData()
    // MARK: Button actions
    var fuckWeb: Bool = true
    var currentMLItem: MLItem!

    var mlItemTimer: Timer!
    var mlStartTime: Date!
    
    @IBOutlet weak var leftMenuBtn: UIBarButtonItem!
    @IBOutlet weak var rightMenuBtn: UIBarButtonItem!
    @IBOutlet weak var reloadBtn: UIBarButtonItem!
    @IBOutlet weak var insightBtn: UIBarButtonItem!
    
    @IBOutlet weak var addStackBtn: UIBarButtonItem!
    @IBOutlet weak var removeStackBtn: UIBarButtonItem!
    
    @IBOutlet weak var logWindowBtn: UISwitch!
    @IBOutlet weak var urlOverrideBtn: UISwitch!
    @IBOutlet weak var logWindowView: UIView!
    @IBOutlet weak var logWindowTxtView: UITextView!
    @IBOutlet weak var logWindowSegmentedCntrl: UISegmentedControl!
    @IBOutlet weak var logWindowHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var logWindowCopyBtn: UIBarButtonItem!
    
    @IBAction func indexChanged(sender: UISegmentedControl)
    {
        self.logWindowViewAdjustHeight()
    }
    
    func logWindowViewAdjustHeight() {
        let viewMode = logWindowSegmentedCntrl.selectedSegmentIndex
        let viewSize = viewMode + 1
        let newHeight = CGFloat(viewSize) * 0.25 * (self.view.frame.height)
        //print("viewSize=>\(viewSize) newHeight=>\(newHeight)")
        DispatchQueue.main.async {
            UIView.animate(withDuration: (ShareData.sharedInstance.transitionSpeed / 2), animations: {
                self.logWindowHeightConstraint.constant = newHeight
                self.view.layoutIfNeeded()
            }, completion: { finished in
                self.logWindowScrollToBottom()
            })
        }
    }
    
    func logWindowScrollToBottom() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: (0.1), animations: {
                let bottom = NSMakeRange(self.logWindowTxtView.text.count - 1, 1)
                self.logWindowTxtView.scrollRangeToVisible(bottom)
            })
        }
    }
    
    @IBOutlet weak var contentViewLeftConstraint: NSLayoutConstraint!
    
    @IBAction func leftMenuTapped(_ sender: AnyObject) {
        delegate?.hideUserMenu?()
        delegate?.toggleLeftPanel?()
    }

    @IBAction func rightMenuTapped(_ sender: AnyObject) {
        delegate?.toggleRightPanel?()
//        loadInstagram2()
    }
    
    @IBAction func reloadBtn(_ sender: AnyObject) {
//        delegate?.hideUserMenu?()
        reloadLayout()
//        loadInstagram2()
//        reloadBtn.tintColor = UIColor.lightGray
    }
    
    @IBAction func insightBtn(_ sender: AnyObject) {
        //checkInsightsBadge()
        delegate?.loadInsightView!()
        
        //stack insert tester
//        if(self.MLStackBaseClassObj != nil && self.MLStackBaseClassObj.pagedScrollViewController != nil) {
//            self.MLStackBaseClassObj.pagedScrollViewController.insertStack()
//        }
        
        //stack delete tester
//        if(self.MLStackBaseClassObj != nil && self.MLStackBaseClassObj.pagedScrollViewController != nil) {
//            self.MLStackBaseClassObj.pagedScrollViewController.removeStackFromLayout()
//        }
        
        //insightBtn.tintColor = UIColor.white
    }
    
    @IBAction func addStackBtn(_ sender: AnyObject) {
        if(self.MLStackBaseClassObj != nil && self.MLStackBaseClassObj.pagedScrollViewController != nil) {
            self.MLStackBaseClassObj.pagedScrollViewController.addStackToLayout()
        }
        /*
        //mlItemSelected(currentMLItem)
        DispatchQueue.main.async {
            self.addStackBtn.tintColor = UIColor.lightGray
            self.reloadBtn.tintColor = UIColor.lightGray
        }
        MLStackBaseClassObj.addStackMLStackClass()
        */
    }
    
    @IBAction func removeStackBtn(_ sender: AnyObject) {
        if(self.MLStackBaseClassObj != nil && self.MLStackBaseClassObj.pagedScrollViewController != nil) {
            self.MLStackBaseClassObj.pagedScrollViewController.removeStackFromLayout()
        }
    }
    
//    func loadInstagram() {
//        let imageStr: String = "mqdefault.jpg"
//        let image: UIImage = UIImage(named: imageStr)!
//        //        let image = self.photoImageView.image
//        //        InstagramManager.sharedManager.postImageToInstagram(image: image, instagramCaption: "Will do moose things for money...", view: self.view)
//        InstagramManager.sharedManager.postImageToInstagramWithCaption(image: image, instagramCaption: "Will do moose things for money...", view: self.view)
//        //        InstagramManager.sharedManager.postImageToInstagramFromDIC(image: image, instagramCaption: "Will do moose things for money...", view: self.view)
//    }
//    
//    func loadInstagram2() {
//        let imageStr: String = "mqdefault.jpg"
//        let image: UIImage = UIImage(named: imageStr)!
//        //        let image = self.photoImageView.image
//        //        InstagramManager.sharedManager.postImageToInstagram(image: image, instagramCaption: "Will do moose things for money...", view: self.view)
////        InstagramManager.sharedManager.postImageToInstagramWithCaption(image: image, instagramCaption: "Will do moose things for money...", view: self.view)
//        InstagramManager.sharedManager.postImageToInstagramFromDIC(image: image, instagramCaption: "Will do moose things for money...", view: self.view)
//    }
    
    @IBAction func logWindowBtn(_ sender: AnyObject) {
        logWindowVis = logWindowBtn.isOn
        showLogWindow()
        //logWindowMsg("debug Mode=>" + String(logWindowVis), timeCompare: false)
    }
    
    @IBAction func logWindowClearBtn(_ sender: AnyObject) {
        self.logWindowClear()
    }
    
    @IBAction func logWindowCopyBtn(_sender: AnyObject)
    {
        self.logWindowCopyTxt()
    }
    
    @IBAction func urlOverrideBtn(_ sender: AnyObject) {
        loadExpion = !urlOverrideBtn.isOn
        reloadLayout()
    }
    
    override var shouldAutorotate : Bool {
        return false
    }
    
    override func viewDidLoad() {
//        //print("CVC viewDidLoad")
//        let cookies = HTTPCookieStorage.shared.cookies!
//        //print("POST--CVC viewDidLoad cookies=>")
//        for cookie in cookies {
//            //print("--c--" + cookie.name + "=" + cookie.value)
//            
//        }
//        //print("END---CVC viewDidLoad cookies")
        
        //print("SS--CenterView Frame height=>\(self.contentView.frame.height)")
        
        refreshMLMenu()
    }
    
    func reloadLayout(){
        delegate?.hideUserMenu?()
        DispatchQueue.main.async {
            self.reloadBtn.tintColor = UIColor.lightGray
        }
        mlItemSelected(currentMLItem)
    }
    
    func setupCenterView(){
        //print("Center--setupCenterView")
        showDebugControls()
    }
    /*
    func setUpContentView(){
        if(AppSettings.IS_IPAD == true){
            //print("Center--setUpContentView isIpad=true")
            
            contentViewLeftConstraint.constant = 320
            self.contentView.layoutIfNeeded()
            //UIView.animateWithDuration(0.5, animations: { [weak self] in
            //    self?.view.layoutIfNeeded() ?? ()
            //})
            /*
            self.contentViewLeftConstraint.constant = 420
            UIView.animateWithDuration(0.5, animations: {
                self.contentViewLeftConstraint.constant = 420
                self.contentView.layoutIfNeeded()
                self.contentView.updateConstraints()
            })*/
            //contentView.frame = CGRectMake(320, 0, self.view.frame.width - 320, self.view.frame.height)
        }
    }
    */
    
    func showDebugControls(){
        DispatchQueue.main.async {
            //print("debugMode=>\(self.debugMode)")
            if(self.debugMode == 1){
                self.logWindowBtn.isHidden = false
                self.logWindowBtn.isEnabled = true
                self.logWindowBtn.setOn(true, animated: false)
                self.logWindowVis = self.logWindowBtn.isOn
                self.showLogWindow()
                self.urlOverrideBtn.isHidden = false
                self.urlOverrideBtn.isEnabled = true
            } else {
                self.logWindowBtn.isHidden = true
                self.logWindowBtn.isEnabled = false
                self.urlOverrideBtn.isHidden = true
                self.urlOverrideBtn.isEnabled = false
            }
            
            //TODO:  This stuff is for turning on insights
            if(ShareData.sharedInstance.notifEnabled == true){
                //print("showDebugControls=>alert on")
//                insightBtn.isHidden = false
//                let btnImg = UIImage(named: "lighting bolt.png")
                self.insightBtn.image = UIImage(named: "AlertBellWhite20px")
                self.insightBtn.width = 20

//                self.insightBtn.addBadge(number: 4)
                self.checkInsightsBadge()
            } else {
                //print("showDebugControls=>alert off")
                self.insightBtn.image = nil
                self.insightBtn.width = 0
            }
        }
    }
    
    func checkInsightsBadge(){
        //print("checkInsightsBadge=>" + String(describing: self.peterDemoMode))
        var apns_token: String!
        if (UserDefaults.standard.object(forKey: "apns_token") != nil) {
            apns_token = UserDefaults.standard.object(forKey: "apns_token") as! String!
        }
        if((apns_token != nil && apns_token.count > 0) || AppSettings.isSimulator == true) {
            let mobNot: MobileNotification = MobileNotification()
            mobNot.delegate = self
            mobNot.getNotifications()//(user_id: ShareData.sharedInstance.userId, user_name: ShareData.sharedInstance.userName, page_id: "1")()
        }
    }
    
    func notificationsListRetreived(notifications: NSDictionary) {
//        if let notifs = notifications["data"] as? NSArray {
//        }
        if let unreadCount = notifications["unread_count"] as? Int {
            
//            //print("notifications[\"unreadCount\"] found")
//            //print(unreadCount)
            insightBtn.updateBadge(number: unreadCount)
            DispatchQueue.main.async {
                let application = UIApplication.shared
                application.applicationIconBadgeNumber = unreadCount
            }
        } else {
            //print("no data")
        }
    }
    
    func showLogWindow(){
        self.logWindowViewAdjustHeight()
        DispatchQueue.main.async {
            self.logWindowView.isHidden = !self.logWindowVis
        }
    }
    
    func logWindowCopyTxt() {
        let consoleMsgTxt = self.logWindowTxtView.text
        UIPasteboard.general.string = consoleMsgTxt
    }
    
    func logWindowMsg(_ logTxt: String, timeCompare: Bool){
        if(logWindowVis == true && self.debugMode == 1) {
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm:ss.SSS"
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            formatter.locale = Locale(identifier: "en_US_POSIX")
            
            let timeStamp: String = (formatter.string(from: date))
            var logMsg: String = String(format:"\n%@=>%@", timeStamp, logTxt)
            
//            let color: UIColor = UIColor.blue
            
            DispatchQueue.main.async {
                self.logWindowTxtView.textColor = UIColor.cyan
                
//                logMsg.addAttribute(NSAttributedStringKey.foregroundColor, value: color, range: range)
                

                
//                myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: NSRange(location:2,length:4))
                
                if(timeCompare == true) {
                    let elapsed = Date().timeIntervalSince(self.mlStartTime)
                    self.logWindowTxtView.text = self.logWindowTxtView.text.appending(logMsg + "==>" + String(elapsed))
                    
//                    let str = String(format:"%@==>%@", logMsg, elapsed)
//                    let range = (str as NSString).range(of: str)
//                    let attributedString = NSMutableAttributedString(string:str)
//                    attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.blue , range: range)
//
//                    self.logWindowTxtView.attributedText = self.logWindowTxtView.attributedText = attributedString
                } else {
                    self.logWindowTxtView.text = self.logWindowTxtView.text.appending(logMsg)
                }
                
                self.logWindowScrollToBottom()
            }
        }
    }
    
    func logWindowClear() {
        DispatchQueue.main.async {
            self.logWindowTxtView.text = ""
            self.logWindowScrollToBottom()
        }
    }
    
    func refreshMLMenu() {
        //print("<< centerViewController refreshMLMenu >>")
        delegate?.getMLJSON()
    }
    
    func mlItemSelected(_ mlItem: MLItem) {
        logWindowMsg("CVC--mlItemSelected--company_id=>" + company_id, timeCompare: false)
        
//        DispatchQueue.main.async {
            if(AppSettings.IS_IPAD == false){
                self.delegate?.collapseSidePanels?()
            }
//        }
        
        
//        //print("CVC->mlItemSelected")
//        let cookies = HTTPCookieStorage.shared.cookies!
//        for cookie in cookies {
//            //print("--c--" + cookie.name + "=" + cookie.value)
//        }
//        //print("--end mlItemSelected cookies--")
        
        
        //print("CVC--mlItemSelected--company_id=>" + company_id)
        if(debugMode == 1) {
            if(mlStartTime != nil) {
                mlStartTime = nil
            }
            mlStartTime = Date()
        }
        if(delegate?.hasConnectivity!() == true){
            mlItemHasFinished()
            if(mlItem.deployed_status == "DEPLOYED"){
                getDeployedLayoutId(mlItem)
            } else {
                loadMLItem(mlItem)
            }
        } else {
            delegate?.showNotConnectedAlert!()
            mlItemHasFinished()
        }
    }
    
    func getDeployedLayoutId(_ mlItem: MLItem){

        //if let currentImageId = itemData["image_id"] as? Int {
            let deployedUrlString: String = String(format:"%@/api/ml/deployws/clonewsforuser/%@/%@?clone=", myUrl, String(mlItem.id), company_id)
            
            //print("* * *deployedUrlString=>" + deployedUrlString)
            
            let URL = Foundation.URL(string: deployedUrlString)!
            let request = NSMutableURLRequest(url:URL)
            request.httpMethod = "GET"
            NSURLConnection.sendAsynchronousRequest(
                request as URLRequest,
                queue: OperationQueue.main,
                completionHandler: { response, data, err in
                    if err != nil {
                        //error?(e)
                        //print("error occured=>" + err.debugDescription)
//                            self.hideLoading()
                    } else {
                        if(data != nil) {
//                            var dataStr = NSString(data: data!, encoding: NSUTF8StringEncoding)
//                            //print(dataStr)
                            if let deployedJSON = (try? JSONSerialization.jsonObject(with: data!, options: [])) as? NSDictionary{
                                if let ClonedStackID = deployedJSON["ClonedStackID"] as? Int {
                                    //print("ClonedStackID=>" + String(ClonedStackID))
                                    
                                    let deployedMl = mlItem
                                    
                                    let mlAttributes = MLAttributes(layout_id: ClonedStackID)
                                    
                                    let mlDirective = MLDirective(directive_name: deployedMl.mlStack.directive.directive_name , attributes: mlAttributes)
                                    
                                    let mlStack = MLStack(directive: mlDirective)
                                    
                                    let ml = MLItem(folder_ind: deployedMl.folder_ind,
                                        selected_ind: deployedMl.selected_ind,
                                        id: deployedMl.id,
                                        company_id: deployedMl.company_id,
                                        name: deployedMl.name,
                                        order_ind: deployedMl.order_ind,
                                        settings: deployedMl.settings,
                                        suggested_ind: deployedMl.suggested_ind,
                                        shd_v: deployedMl.shd_v,
                                        shd_e: deployedMl.shd_e,
                                        shd_d: deployedMl.shd_d,
                                        shd_c: deployedMl.shd_c,
                                        set_hw_c: deployedMl.set_hw_c,
                                        set_hw_v: deployedMl.set_hw_v,
                                        code_class_name: deployedMl.code_class_name,
                                        css_class_name: deployedMl.css_class_name,
                                        deployed_status: deployedMl.deployed_status,
                                        layout_id: ClonedStackID,
                                        children: deployedMl.children,
                                        mlStack: mlStack)
                                    
                                    self.loadMLItem(ml)
                                }
                            }
//                                self.doApprovalAction(actionUrlString)
                        } else {
                            //ERROR
//                                self.hideLoading()
                        }
                        //print("success")
                    }
            })
        //}
    }

    func preloadMLStackClass() {
        let settingsOverride = ExpionSettingsBaseClass(company_id: self.company_id, myUrl: self.myUrl, userAgent: ShareData.sharedInstance.userAgent, isPad: AppSettings.IS_IPAD as NSNumber, mlItem: nil, contentView: self.contentView, delegate: self, loadExpion: loadExpion, debugMode: debugMode == 1 ? true : false)
        
        let expionBaseClass = MLStackClass.init(xOverride: "thing", settingsOverrride: settingsOverride)
//        let expionBaseClass = expionBaseClassObj.init(xOverride: "thing", settingsOverrride: settingsOverride)
        expionBaseClass.Execute();
        MLStackBaseClassObj = expionBaseClass
    }
    
    func preloadStackLayout() {
        
    }
    
    func loadMLItem(_ mlItem: MLItem) {
        mlItemTimer = Timer.scheduledTimer(timeInterval: 120, target: self, selector: #selector(self.mlItemTimedOut), userInfo: nil, repeats: true)
        
        if(reloadBtn.isEnabled == true){
            clearCenterViewController()
            
            currentMLItem = mlItem
            var classString: String!
            
            if NSClassFromString(mlItem.code_class_name + "Class") != nil {
                //use code_name_class if it exists
                classString = mlItem.code_class_name + "Class"
            } else {
                //fallback to MLWebViewClass
                classString = "MLWebViewClass"
            }
            //print("classString=>" + classString)
            
            DispatchQueue.main.async {
                self.reloadBtn.isEnabled = false
            }
            delegate?.updateLastSelected(mlItem)
            
            if((mlItem.code_class_name.lowercased().range(of: "mlstack") != nil)){
                //---------------------------------------
                let settingsOverride = ExpionSettingsBaseClass(company_id: self.company_id, myUrl: self.myUrl, userAgent: ShareData.sharedInstance.userAgent, isPad: AppSettings.IS_IPAD as NSNumber, mlItem: mlItem, contentView: self.contentView, delegate: self, loadExpion: loadExpion, debugMode: debugMode == 1 ? true : false)
                
                let expionBaseClassObj = NSClassFromString(classString) as! ExpionBaseClass.Type
                let expionBaseClass = expionBaseClassObj.init(xOverride: "thing", settingsOverrride: settingsOverride)
                expionBaseClass.Execute();
                MLStackBaseClassObj = expionBaseClass as! MLStackClass
                //mlItemHasFinished()
                //---------------------------------------
            } else if(mlItem.code_class_name.lowercased().range(of: "homeworkspace") != nil){
                //---------------------------------------
                getUserHomeWorkspaceJSON()
                
            } else if((mlItem.code_class_name.lowercased().range(of: "mobile-approval")) != nil){
                let settingsOverride = ExpionSettingsBaseClass(company_id: self.company_id, myUrl: self.myUrl, userAgent: ShareData.sharedInstance.userAgent, isPad: AppSettings.IS_IPAD as NSNumber, mlItem: mlItem, contentView: self.contentView, delegate: self, loadExpion: loadExpion, debugMode: debugMode == 1 ? true : false)
                
                let expionBaseClassObj = NSClassFromString("MLGalleryClass") as! ExpionBaseClass.Type
                let expionBaseClass = expionBaseClassObj.init(xOverride: "thing", settingsOverrride: settingsOverride)
                expionBaseClass.Execute();
                MLGalleryBaseClassObj = expionBaseClass as! MLGalleryClass
                mlItemHasFinished()
            } else if((mlItem.code_class_name.lowercased().range(of: "mobile-uploader")) != nil){
                let settingsOverride = ExpionSettingsBaseClass(company_id: self.company_id, myUrl: self.myUrl, userAgent: ShareData.sharedInstance.userAgent, isPad: AppSettings.IS_IPAD as NSNumber, mlItem: mlItem, contentView: self.contentView, delegate: self, loadExpion: loadExpion, debugMode: debugMode == 1 ? true : false)
                
                let expionBaseClassObj = NSClassFromString("MLGalleryUploadClass") as! ExpionBaseClass.Type
                let expionBaseClass = expionBaseClassObj.init(xOverride: "thing", settingsOverrride: settingsOverride)
                expionBaseClass.Execute();
                MLGalleryUploadBaseClassObj = expionBaseClass as! MLGalleryUploadClass
                mlItemHasFinished()
            } else {
                //---------------------------------------
                let settingsOverride = ExpionSettingsBaseClass(company_id: self.company_id, myUrl: self.myUrl, userAgent: ShareData.sharedInstance.userAgent, isPad: AppSettings.IS_IPAD as NSNumber, mlItem: mlItem, contentView: self.contentView, delegate: self, loadExpion: loadExpion, debugMode: debugMode == 1 ? true : false)
                
                //let expionBaseClassObj = NSClassFromString(classString) as! ExpionBaseClass.Type
                let expionBaseClassObj = NSClassFromString("MLWebViewClass") as! ExpionBaseClass.Type
                let expionBaseClass = expionBaseClassObj.init(xOverride: "thing", settingsOverrride: settingsOverride)
                expionBaseClass.Execute();
                //MLStackBaseClassObj = expionBaseClass as! MLStackClass
                mlItemHasFinished()
                //---------------------------------------
            }
            
        }
    }
    
    //@objc
    @objc func mlItemTimedOut(){
        //print("mlItemTimedOut")
        clearMLTimer()
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Loading Timed Out", message: "The application timed out before the previous load finished.  Would you like to reload the last selected item?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Reload", style: UIAlertActionStyle.default, handler: { action in self.reloadLayout()}))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { action in self.mlItemHasFinished()}))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func clearMLTimer() {
        if(mlItemTimer != nil){
            mlItemTimer.invalidate()
            mlItemTimer = nil
        }
    }
    
    //---------Home Workspace----------------------
    
    
    func getUserHomeWorkspaceJSON(){
        //print("****getUserHomeWorkspaceJSON")
        startConnection()
    }
    
//    func startConnection(){
//        //let company_id = "31"
//        //let myUrl = "https://t1.crm.xpon.us"
//        
//        //let urlPath: String = myUrl + "/API/ML/User?company_id=" + company_id
//        let urlPath: String = myUrl + "/api/ml/GetUserHomeWorkspace/" + company_id
//        //print(String(format: "****getUserHomeWorkspaceJSON urlPath=>%@", urlPath))
//        let url: URL = URL(string: urlPath)!
//        
//        let cookies = HTTPCookieStorage.shared.cookies!
//        let headers = HTTPCookie.requestHeaderFields(with: cookies)
//        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 60.0)
//        //print("userAgent=>" + AppSettings.originalUserAgent)
//        request.setValue(AppSettings.originalUserAgent, forHTTPHeaderField:"User-Agent")
//        
//        if let accessToken = UserDefaults.standard.object(forKey: "accessToken") as! String! {
//            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
//        }
//        
//        request.allHTTPHeaderFields = headers
//        
//        let sessionConfiguration = URLSessionConfiguration.default
//        // disable the caching
//        sessionConfiguration.urlCache = nil
//        self.session = URLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: nil)
//        if let task = self.session?.dataTask(with: request) {
//            // start the task, tasks are not started by default
//            task.resume()
//        }
//    }
    
    
    func startConnection() {
//        let urlPath: String = myUrl + "/API/ML/User?company_id=" + company_id
        let urlPath: String = myUrl + "/api/ml/GetUserHomeWorkspace/" + company_id

        //print(String(format: "****getUserHomeWorkspaceJSON urlPath=>%@", urlPath))
        let url = NSURL(string: urlPath)!
        let request = URLRequest(url: url as URL)
        let session = URLSession(configuration: URLSessionConfiguration.default)
        //print("THIS LINE IS PRINTED")
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) -> Void in
            if let data = data {
                //print("THIS ONE IS PRINTED, TOO")
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                if let response = response as? HTTPURLResponse , 200...299 ~= response.statusCode {
//                    taskCallback(true, json as AnyObject?)
                    //print("true=>%@", json as AnyObject? ?? "")
//                    self.workspaceRequestData.append(data)
                    self.parseUserLayoutJSON(layoutData: data)
                } else {
//                    taskCallback(false, json as AnyObject?)
                    //print("false=>%@", json as AnyObject? ?? "")
                }
            }
        })
        task.resume()
    }
    
    func parseUserLayoutJSON(layoutData: Data){
        var jsonError: NSError?
        var shouldLoadId: String!
        var loadMLItemIndex: Int!
        
        var layout_id: Int = 0
        var directive_name: String = ""
        
        //if let json = NSJSONSerialization.JSONObjectWithData(workspaceRequestData, options: nil, error: &jsonError) as? NSArray {
            //if let layoutData = json[0] as? NSDictionary {
                if let settingsJSON = (try? JSONSerialization.jsonObject(with: layoutData, options: [])) as? NSDictionary{
                    //println(String(format:"settingsJSON success=>%@", settingsJSON))
                    
                    if let directive = settingsJSON["directive"] as? NSDictionary {
                        //print(String(format:"directive success=>%@", directive))
                        
                        if let JSON_directive_name = directive["directive"] as? String {
                            directive_name = JSON_directive_name
                        }
                        
                        if let attributes = directive["attributes"] as? NSDictionary {
                            //println(String(format:"attributes success=>%@", attributes))
                            
                            if let layout_idInt = attributes["layout_id"] as? Int {
                                //println(String(format:"layout_idInt success=>%@", String(layout_idInt)))
                                layout_id = layout_idInt
                            }
                        }
                    }
                }
            //}
        //}
        
        let mlAttributes = MLAttributes(layout_id: layout_id)
        
        let mlDirective = MLDirective(directive_name: directive_name, attributes: mlAttributes)
        
        let mlStack = MLStack(directive: mlDirective)
        
        let ml = MLItem(folder_ind: currentMLItem.folder_ind,
            selected_ind: currentMLItem.selected_ind,
            id: currentMLItem.id,
            company_id: currentMLItem.company_id,
            name: currentMLItem.name,
            order_ind: currentMLItem.order_ind,
            settings: currentMLItem.settings,
            suggested_ind: currentMLItem.suggested_ind,
            shd_v: currentMLItem.shd_v,
            shd_e: currentMLItem.shd_e,
            shd_d: currentMLItem.shd_d,
            shd_c: currentMLItem.shd_c,
            set_hw_c: currentMLItem.set_hw_c,
            set_hw_v: currentMLItem.set_hw_v,
            code_class_name: currentMLItem.code_class_name,
            css_class_name: currentMLItem.css_class_name,
            deployed_status: currentMLItem.deployed_status,
            layout_id: layout_id,
            children: currentMLItem.children,
            mlStack: mlStack)
        
        currentMLItem = ml
        
        loadUserWSAsStackLayout(ml)
    }
    
    func loadUserWSAsStackLayout(_ mlItem: MLItem){
        let settingsOverride = ExpionSettingsBaseClass(company_id: self.company_id, myUrl: self.myUrl, userAgent: ShareData.sharedInstance.userAgent, isPad: AppSettings.IS_IPAD as NSNumber, mlItem: mlItem, contentView: self.contentView, delegate: self, loadExpion: loadExpion, debugMode: debugMode == 1 ? true : false)
        
        //let expionBaseClassObj = NSClassFromString(classString) as! ExpionBaseClass.Type
        let expionBaseClassObj = NSClassFromString("MLStackClass") as! ExpionBaseClass.Type
        let expionBaseClass = expionBaseClassObj.init(xOverride: "thing", settingsOverrride: settingsOverride)
        expionBaseClass.Execute();
        MLStackBaseClassObj = expionBaseClass as! MLStackClass
        
        workspaceRequestData.setData(Data())
    }
    
    
    //--------------------------------
    
    func updateLastSelected() {
        
    }
    
    func collapseSidePanels() {
        delegate?.collapseSidePanels?()
    }
    
    func clearCenterViewController() {
        //print("QQQQ--clearScrollViews--CenterViewController--QQQQ")
        DispatchQueue.main.async {
            UIView.animate(withDuration: ShareData.sharedInstance.transitionSpeed, animations: {
                self.contentView.alpha = 0.0
            })
        }
        if (self.MLStackBaseClassObj != nil && self.MLStackBaseClassObj.pagedScrollViewController != nil){
            self.MLStackBaseClassObj.pagedScrollViewController.clearScrollView()
//            DispatchQueue.main.async {
//                self.MLStackBaseClassObj.LegacyPagedScrollViewController.willMove(toParentViewController: nil)
//            self.MLStackBaseClassObj.pagedNavigationController.willMove(toParentViewController:     nil)
//            }
            self.MLStackBaseClassObj = nil
        }
        DispatchQueue.main.async {
            let subViews = self.contentView.subviews
            if (subViews.count > 0){
                for subview in subViews{
//                    DispatchQueue.main.async {
                    //print("remove subview - cv")
                        subview.removeFromSuperview()
//                    }
                }
            }
        }
    
        DispatchQueue.main.async {
            UIView.animate(withDuration: ShareData.sharedInstance.transitionSpeed, animations: {
                self.contentView.alpha = 1.0
            })
        }
    }
    
    func mlItemHasFinished() {
        //print("mlItemHasFinished")
        clearMLTimer()
        
        DispatchQueue.main.async {
            self.leftMenuBtn.isEnabled = true
            self.reloadBtn.isEnabled = true
            //addStackBtn.enabled = true
            //addStackBtn.tintColor = UIColor.whiteColor()
            self.reloadBtn.tintColor = UIColor.white
        }
    }
    
    func sessionTimedOut() {
        delegate?.sessionTimedOut()
    }
    
    func logoutBtnClicked() {
        logOut()
    }
    
    func logOut() {
        if(self.debugMode == 1){
            self.logWindowBtn.setOn(false, animated: false)
            self.logWindowVis = self.logWindowBtn.isOn
            self.showLogWindow()
        }
        //print(String(format:"logoutBtnClicked"))
        delegate?.collapseSidePanels?()
        clearCenterViewController()
        delegate?.userLoggedOut()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        //println(String(format:"scrollViewWillDrag height=>%@ width=>%@", scrollView.frame.height, scrollView.frame.width))
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

    }
}
