//
//  ContainerViewController.swift
//  SlideOutNavigation
//
//  Created by Mike Bagwell on 12/16/14.
//  Copyright (c) 2014 Expion. All rights reserved.
//

import UIKit
import QuartzCore
import Foundation

enum SlideOutState {
  case bothCollapsed
  case leftPanelExpanded
  case rightPanelExpanded
}

class ShareData {
    
//    private static var __once: () = {
//            Static.instance = ShareData()
//        }()
//    class var sharedInstance: ShareData {
//        struct Static {
//            static var instance: ShareData?
//            static var token: Int = 0
//        }
//        
//        _ = ShareData.__once
//        
//        return Static.instance!
//    }

    static let sharedInstance : ShareData = {
        let instance = ShareData()
        return instance
    }()
    
    var company_id: String!
    var myUrl: String!
    var debugMode: NSNumber!
    var notificationsEnabled: NSNumber!
    var isAdmin: NSNumber!
    var userName: String!
    
    
    var userId: NSNumber!
//    var apns_token: String!
    var locationLabel: String = "Location"
    var transitionSpeed: TimeInterval = 0.42 //1.0
    
    var userAgent: String!
    var notifEnabled: Bool = false
}
struct AppSettings {
    var userAgent: String
    
    static var TheCurrentDevice: UIDevice {
        struct Singleton {
            static let device = UIDevice.current
        }
        return Singleton.device
    }
    static var IS_IPAD: Bool {
        return isPad()
    }
    static func isPhone() -> Bool {
        return TheCurrentDevice.userInterfaceIdiom == .phone
    }
    
    static func isPad() -> Bool {
        return TheCurrentDevice.userInterfaceIdiom == .pad
    }
    
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
            isSim = true
        #endif
        return isSim
    }()
    
    
}

@objc
protocol ContainerViewControllerDelegate {
    
}

class ContainerViewController: UIViewController, LoginViewControllerDelegate, CenterViewControllerDelegate, UIGestureRecognizerDelegate, UIPopoverPresentationControllerDelegate {
    /**
    company_id brief
    - parameter one: param one description
    - parameter two: param two description
    - returns: company_id return description
    */
    var firstLoad: Bool = true
    
//    var company_id: String!
//    var myUrl: String!
//    var debugMode: NSNumber!
    
//    var notificationsEnabled: NSNumber!
//
//    var isAdmin: NSNumber!
//    var userName: String!

    var userMenuVis: Bool = false
    //var expionViewController: ExpionViewController!
    //var loginViewController: LoginViewController!
    var centerNavigationController: UINavigationController!
    var centerViewController: CenterViewController!
    
//    var accessToken: String!
    let opQueue = OperationQueue()
    var response:URLResponse?
    var session:URLSession?

    var currentState: SlideOutState = .bothCollapsed {
        didSet {
            let shouldShowShadow = currentState != .bothCollapsed
            showShadowForCenterViewController(shouldShowShadow)
        }
    }
    
    var leftViewController: LeftMenuViewController?
    var rightViewController: RightPanelViewController?
    var loginViewController: LoginViewController?
    
    var gStartX: Int!
    var gStartY: Int!
    var gThreshold = 5
    var gShouldLoad: Bool!
    var gMenuPan: Bool!
    
    var imageView = UIImageView(frame: CGRect(x: 50, y: 27, width: 97, height: 30)); // set as you want
    var image = UIImage(named: "logo_mobile_header_bar.png")
    
    let menuOffset: CGFloat = AppSettings.IS_IPAD == true ? 0 : 20
    let menuWidth: CGFloat = AppSettings.IS_IPAD == true ? 320 : 330 //(desired menu width: 320 + menuOffset)
    let menuWidthCollapsed: CGFloat = 40
    let centerPanelExpandedOffset: CGFloat = 60

    //lazy var data = NSMutableData()
    var responseData: NSMutableData = NSMutableData()
    var mlData = Dictionary<String, Array<MLItem>>()
    
    var home_items: Array<MLItem> = []
    
    var last_selected: Int!
//    var last_selected_is_workspace: Bool = false
    var last_selectedML: MLItem!
    
    var insightML: MLItem!
    
    var user_home_items: Array<MLItem> = []
    
    //var shared_home_items: Array<MLItem> = []
    //var suggested_home_items: Array<MLItem> = []
    
    var mlTryCount: Int = 0
    var mlItemDataIndex: IndexPath = IndexPath(row: 1, section: 0)
    var mlItemSelectedIndex: IndexPath = IndexPath(row: 1, section: 0)
    
    let shareData = ShareData.sharedInstance
    
    let notificationInWork: Bool = false
    let serialQueue = DispatchQueue(label:"containerQueue")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerObserver()
        
        let deviceStr = (AppSettings.isPad() == true ? "iPad" : "iPhone")
        
        let versionNumber = (Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String).replacingOccurrences(of: ".", with: "")
//        let versionNumber = "620100" //"650100"
        let originalUserAgent = UIWebView().stringByEvaluatingJavaScript(from: "navigator.userAgent")!
        let userAgentStr = "Expion-" + deviceStr + "-" + versionNumber + "-" + originalUserAgent

//        let userAgentStr = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36"
        
        print("")
        print(userAgentStr)
        print("")
        
        shareData.userAgent = userAgentStr
        
        UserDefaults.standard.register(defaults: ["User-Agent": userAgentStr])
        
        let notificationName = Notification.Name("NotificationReceived")
        NotificationCenter.default.addObserver(self, selector: #selector(ContainerViewController.parseNotification(notification:)), name: notificationName, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ContainerViewController.parseNotification(notification:)), name: Notification.Name("NotificationOpened"), object: nil)
        
        if(firstLoad == true){
            showLogin(true)
            firstLoad = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        //print("!!!--ContainerView didReceiveMemoryWarning")
        // Dispose of any resources that can be recreated.
    }

    //@objc
    @objc func parseNotification(notification: NSNotification) {
        //        let userInfo:[NSObject : AnyObject] = notification.userInfo!
        //        self.eventTitle = userInfo["aps"]!["alert"] as! String
        //        self.eventDescription = userInfo["aps"]!["description"] as! String
        //print("notificatonOpened")
        //print("name=>" + String(describing: notification.name))
        //print("info=>" + String(describing: notification.userInfo))
//        let notif = notification
        //print(notif)
        
        if let alertInfo = notification.object as? NSDictionary {
            var msgTxt: String!
            var image_url: String!
            if let aps = alertInfo["aps"] as? NSDictionary {
                if let alert = aps["alert"] as? NSDictionary {
                    if let message = alert["message"] as? NSString {
                        //Do stuff
                        print(message)
                    }
                } else if (aps["alert"] as? NSString) != nil {
                    //Do stuff

                }
            }
            if let message = alertInfo["xMsg"] as? String {
                //Do stuff
                msgTxt = message
            }
            if let imgUrl = alertInfo["xImgUrl"] as? String {
                //Do stuff
                image_url = imgUrl
            }
            
            var type: String!
            if let notification_type = alertInfo["notification_type"] as? String {
                //print("notification_type=>" + notification_type)
                type = notification_type
            }
        
//            if(type != nil && type == "insights") {
//                
//            }
            
            if(type != nil && type == "ig-post") {
                if (notification.name.rawValue == "NotificationOpened") {
                    //print("Opened")
                    //pushToMobileShare
                } else if (notification.name.rawValue == "NotificationReceived") {
                    //pushToMobileShare
                }
                if(msgTxt != nil && image_url != nil && image_url.count > 0) {
                    loadInstagram(msgTxt: msgTxt, imageUrl: image_url)
                }
            } else {
                if (notification.name.rawValue == "NotificationOpened") {
                    //print("Opened")
                    loadInsightView()
                } else if (notification.name.rawValue == "NotificationReceived") {
                    //print("else=>" + notification.name.rawValue)
                    if(centerViewController != nil) {
                        centerViewController.checkInsightsBadge()
                    }
                }
            }
        }
        
    }
    
    func loadInstagram(msgTxt: String?, imageUrl: String?) {
        if(imageUrl != nil && (imageUrl?.count)! > 0){
            let url = URL(string: imageUrl!)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                if (data != nil) {
                    guard let data = data else { return }
                    let tmpURL = FileManager.default.temporaryDirectory
                        .appendingPathComponent(self.response?.suggestedFilename ?? "fileName.csv") //"share.igo")
                    do {
                        try data.write(to: tmpURL)
                    } catch { print(error) }
                    InstagramManager.sharedManager.postImageToInstagramFromDIC(fileURL: tmpURL, instagramCaption: msgTxt!, view: self.view)
                }
            }
        }
    }
        
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        if(firstLoad == true){
//            showLogin(true)
//            firstLoad = false
//        }
    }
    
    
    var stateTest: String = "nothing"
    override func encodeRestorableState(with coder: NSCoder) {
        //1
        let stateOverride: String = "Test Value"
//        if let stateTest = stateTest {
            coder.encode(stateOverride, forKey: "stateTest")
//        }
        
        //2
        super.encodeRestorableState(with: coder)
    }
    
    override func decodeRestorableState(with coder: NSCoder) {
        stateTest = coder.decodeObject(forKey: "stateTest") as! String
        
        super.decodeRestorableState(with: coder)
    }
    
    override func applicationFinishedRestoringState() {
        //guard let stateTest = stateTest else { return "" }
        //currentPet = MatchedPetsManager.sharedManager.petForId(petId)
        //print("CoVC--applicationFinishedRestoringState-stateTest=>" + stateTest)
        //print("!!!")
    }
    
    func showLogin(_ autoLogin: Bool) {
        //print("showLogin")
        
        
        let cookieStore = HTTPCookieStorage.shared
        for cookie in cookieStore.cookies ?? [] {
            cookieStore.deleteCookie(cookie)
        }
        
//        let cstorage = HTTPCookieStorage.shared
//        if let cookies = cstorage.cookies {
//            for cookie in cookies {
//                cstorage.deleteCookie(cookie)
//            }
//        }
        
        if (UserDefaults.standard.object(forKey: "accessToken") as? String) != nil {
//            UserDefaults.standard.setValue("", forKey: "accessToken")
            UserDefaults.standard.removeObject(forKey: "accessToken")
        }
        if (UserDefaults.standard.object(forKey: "ASPXAUTH") as? String) != nil {
//            UserDefaults.standard.setValue("", forKey: "ASPXAUTH")
            UserDefaults.standard.removeObject(forKey: "ASPXAUTH")
        }
        
        UserDefaults.standard.setValue("", forKey: "myUrl")
        UserDefaults.standard.setValue("", forKey: "companyId")
        UserDefaults.standard.setValue("", forKey: "userName")
//        UserDefaults.standard.setValue(debugMode.isOn, forKey: "debugMode")
        UserDefaults.standard.synchronize()
        
        if(self.loginViewController != nil) {
            self.loginViewController = nil
        }
        
        self.loginViewController = UIStoryboard.loginViewController()
        self.loginViewController?.delegate = self
        self.loginViewController?.autoLogin = autoLogin
        
        DispatchQueue.main.async {
            let vc = self.view?.window?.rootViewController
            vc?.present(self.loginViewController!, animated: true, completion: nil)
        }
    }
    
    //test
    func loginComplete(){
        serialQueue.sync {
            self.setUserInfo()
        }
        serialQueue.sync {
            self.setUpMobNotif()
        }
        serialQueue.sync {
            self.setupContainerView()
        }
    }
    
    func setUserInfo() {
        self.loginViewController = nil
        
        let getUrl:String = UserDefaults.standard.object(forKey: "myUrl") as! String
        let getCompany_id:String = UserDefaults.standard.object(forKey: "companyId") as! String
        let getUserName:String = UserDefaults.standard.object(forKey: "userName") as! String
        let getDebugMode:NSNumber = UserDefaults.standard.object(forKey: "debugMode") as! NSNumber
        
        
        var shouldResetCenterView = true

        if(shareData.myUrl != nil && shareData.myUrl == getUrl && shareData.company_id != nil && shareData.company_id == getCompany_id && shareData.userName != nil && shareData.userName == getUserName){
            shouldResetCenterView = false
        }
        if(shouldResetCenterView == true){
//            clearContainerView()
        }
        
        self.shareData.myUrl = getUrl
        self.shareData.company_id = getCompany_id

        self.shareData.userName = getUserName
        self.shareData.debugMode = getDebugMode

        //print("END--loginComplete cookies")
    }
    func setupContainerView() {
        
        if(self.rightViewController != nil) {
            self.rightViewController?.updateUserNameLabel()
        }
        
        if(centerViewController != nil){
            refreshMLJSON()
            centerViewController.setupCenterView()
            DispatchQueue.main.async {
                UIView.animate(withDuration: ShareData.sharedInstance.transitionSpeed, animations: {
                    self.centerViewController.view.alpha = 1.0
                })
            }
        } else {
            initCenterView()
        }
    }
    
    func setUpMobNotif() {
        var apns_token: String!
        if (UserDefaults.standard.object(forKey: "apns_token") != nil) {
            apns_token = UserDefaults.standard.object(forKey: "apns_token") as! String?
        }
        if ((apns_token != nil && apns_token.count > 0) || AppSettings.isSimulator == true) {
            let application = UIApplication.shared
            application.applicationIconBadgeNumber = 0
//            var pDemo: NSNumber!
        
            //            pDemo = 1
            //        } else {
            //            pDemo = 0
            
            let mobNot: MobileNotification = MobileNotification()
//            mobNot.company_id = Int(company_id)
//            mobNot.myUrl = self.myUrl
            mobNot.registerDevice(active_ind: true)
        }
    }
    
    func notifRegComplete() {
        
    }
    
    func clearContainerView(){
        //print("CVC--clearContainerView")
        
        if(centerViewController != nil){
            centerViewController.clearCenterViewController()
            DispatchQueue.main.async {
                self.centerViewController.willMove(toParentViewController: nil)
                self.centerViewController = nil
                self.centerNavigationController = nil
            }
            
        }
        DispatchQueue.main.async {
            if(self.leftViewController != nil){
                self.leftViewController!.willMove(toParentViewController: nil)
                self.leftViewController = nil
            }
            if(self.rightViewController != nil){
                self.rightViewController!.willMove(toParentViewController: nil)
                self.rightViewController = nil
            }
            
        }
        for subview in self.view.subviews {
            DispatchQueue.main.async {
                subview.removeFromSuperview()
            }
        }
    }
    
    func initCenterView(){
        centerViewController = UIStoryboard.centerViewController()
        centerViewController.delegate = self
        centerViewController.view.frame = self.view.frame
        
//        centerViewController.company_id = self.company_id
//        centerViewController.myUrl = self.myUrl
//        centerViewController.debugMode = self.debugMode
        //if(debugMode == 1 || peterDemoMode == 1){
        centerViewController.setupCenterView()
        //}
        
        // wrap the centerViewController in a navigation controller, so we can push views to it
        centerNavigationController = UINavigationController(rootViewController: centerViewController)
        centerNavigationController.view.frame = self.view.frame
        
        if(AppSettings.IS_IPAD == true){
            //set menu to expanded
            currentState = .leftPanelExpanded
            DispatchQueue.main.async {
                self.centerViewController.contentViewLeftConstraint.constant = self.menuWidth
                self.centerViewController.contentView.layoutIfNeeded()
            }
        } else {
            currentState = .bothCollapsed
        }
        
        DispatchQueue.main.async {
            self.view.addSubview(self.centerNavigationController.view)
            self.addChildViewController(self.centerNavigationController)
            
    //        imageView.image = image;
    //        centerNavigationController.view.addSubview(imageView);
            
            //centerNavigationController.navigationBar.barTintColor = UIColor(red: 0.129, green:0.6, blue:0.816, alpha:1)
            self.centerNavigationController.navigationBar.isHidden = true
            self.centerNavigationController.isNavigationBarHidden = true
//            self.centerNavigationController.automaticallyAdjustsScrollViewInsets = false
            self.centerNavigationController.didMove(toParentViewController: self)
        
        }
        
//        getMLJSON()
    }
    
    func registerObserver(){
        let notificationCenter = NotificationCenter.default

        // Add observer:
        notificationCenter.addObserver(self,
          selector:#selector(ContainerViewController.applicationDidBecomeActive),
          name:NSNotification.Name.UIApplicationDidBecomeActive,
          object:nil)

//        // Remove observer:
//        notificationCenter.removeObserver(self,
//          name:UIApplicationWillResignActiveNotification,
//          object:nil)
//
//        // Remove all observer for all notifications:
//        notificationCenter.removeObserver(self)
        notificationCenter.addObserver(self,
          selector:#selector(ContainerViewController.deviceRotated),
          name: NSNotification.Name.UIDeviceOrientationDidChange,
          object: nil)
    }
    
    // Callback:
    //@objc
    @objc func applicationDidBecomeActive() {
        // Handle application will resign notification event.
        //print("applicationDidBecomeActive")
        if(hasConnectivity() == true){
            if(centerViewController != nil){
                centerViewController.mlItemHasFinished()
                if(mlData.count > 0){
                    //centerViewController.reloadLayout()
                    //TODO: this needs to be check reloadLayout
                } else {
                    startMLJsonConnection()
                }
            } else {
//                showLogin(true)
            }
        } else {
            showInternetConnectionLost()
            if(centerViewController != nil){
                centerViewController.mlItemHasFinished()
            }
        }
    }

    //@objc
    @objc func deviceRotated() {
//        if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation))
//        {
//            //print("landscape")
//        }
//        
//        if(UIDeviceOrientationIsPortrait(UIDevice.currentDevice().orientation))
//        {
//            //print("Portrait")
//        }
        if(AppSettings.IS_IPAD == true){
            redrawLayout()
        }
    }
    
    func hasConnectivity() -> Bool {
        let reachability: Bool = Reachability.isConnectedToNetwork()
        return reachability
    }
    
    func showNotConnectedAlert(){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "No Internet Connection", message: "The application requires internet connection to perform this task", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: nil))//{ action in self.reloadLayout()}))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showInternetConnectionLost(){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "No Internet Connection", message: "Internet connection lost.  Loading new content requires internet connection", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: nil))//{ action in self.reloadLayout()}))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: get data methods

    func refreshMLJSON(){
        getMLJSON()
    }
    
    func getMLJSON(){
        //println("<< containerViewController getMLJSON <<")
        //println("getMLJSON")
        startMLJsonConnection()
    }
    
    func startMLJsonConnection(){
        let urlPath: String = String(format:"%@/API/ML/User?company_id=%@", shareData.myUrl, shareData.company_id)
        //print(urlPath)
        if let url = URL(string: urlPath) {
            let urlRequest: NSMutableURLRequest = NSMutableURLRequest(url: url)

//            urlRequest.setValue(shareData.userAgent, forHTTPHeaderField:"User-Agent")
            
            let session = URLSession.shared
            let task = session.dataTask(with: urlRequest as URLRequest) {
                (data, response, error) -> Void in
                if(response != nil) {
                    let httpResponse = response as! HTTPURLResponse
                    let statusCode = httpResponse.statusCode

                    if (statusCode == 200) {
                        self.responseData = NSMutableData()
                        self.responseData.append(data!)
//                        if(self.responseData != nil) {
                            self.mlTryCount = 0
                            self.parseMLJSON(self.responseData) {
                                (parsedMLData: Dictionary<String, Array<MLItem>>) in
                                self.parseCompleted(parsedMLData)
                                //println("*X*X*parseMLJSON completed*X*X*")
                                //println("got back: (result)")
                            }
//                        }
                    } else {
                        //print("xx--fail--xx")
                        DispatchQueue.main.async {
                            self.centerViewController.leftMenuBtn.isEnabled = true
                        }
                    }
                } else {
                    //print("xx--fail--xx response nil")
                    self.getMLJSON()
//                    DispatchQueue.main.async {
//                        self.centerViewController.leftMenuBtn.isEnabled = true
//                    }
                }
                
            }
            task.resume()
        }
        
    }
    
    func updateLastSelected(_ mlItem: MLItem){
        //needs to add mlItem
        //print("updateLastSelected=>don't call")
        startUpdateLastConnection(mlItem, actionUrl: "/API/ML/")
    }
    
    func convertMLItemToJson(_ mlItem: MLItem) -> NSString
    {
        let serializer: SerializableMLItem = SerializableMLItem()
        //var mlItemArray: Array<MLItem> = []
        //mlItemArray.append(mlItem)
        //serializer.item = mlItem
        //return serializer.toJsonString()
        return serializer.toMLItemJsonString(mlItem)
    }
    
    func startUpdateLastConnection(_ mlItem: MLItem, actionUrl: String){
        let urlPath: String = shareData.myUrl + actionUrl + shareData.company_id
        let url: URL = URL(string: urlPath)!
        //print(String(format: "startUpdateLastConnection urlPath=>%@", url as CVarArg))
        
        let cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        let request = NSMutableURLRequest(url: url, cachePolicy: cachePolicy, timeoutInterval: 2.0)
        request.setValue(shareData.userAgent, forHTTPHeaderField:"User-Agent")
        request.httpMethod = "PATCH"

        // set data
        //var err: NSError?
        
        let dataString: String = convertMLItemToJson(mlItem) as String
        request.httpBody = dataString.data(using: String.Encoding.utf8, allowLossyConversion: true)
        
        if(dataString.count > 0){
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            // set content length
            //NSURLProtocol.setProperty(requestBodyData.length, forKey: "Content-Length", inRequest: request)

            var response: URLResponse? = nil
            var error: NSError? = nil
            var reply: Data?
            do {
                reply = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning:&response)
            } catch let error1 as NSError {
                error = error1
                reply = nil
            }
            if(error != nil){
                //print("!!--Error updating last selected--!!")
                //print(err!.localizedDescription)
                //let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
            } else {
//                let results = NSString(data:reply!, encoding:NSUTF8StringEncoding)
//                //println("last selected API Response: \(results)")
            }
        } else {
            
        }
    }
    
    func parseMLJSON(_ responseData: NSMutableData, completion: (_ parsedMLData: Dictionary<String, Array<MLItem>>) -> Void){
        
//        //print("-=-=-parseMLJSON====")
        var parsedMLData = Dictionary<String, Array<MLItem>>()
        
//        var shouldLoadId: String!
//        var loadMLItemIndex: Int!
    
        var returnJSON: NSDictionary!
        
        do {
            returnJSON = try JSONSerialization.jsonObject(with: responseData as Data, options: []) as? NSDictionary
        } catch {
            
        }
        
        if returnJSON != nil {
            
            print("parseMLJSON=>")
            print(returnJSON)
            
            if let errorMsg = returnJSON["Message"] as? String {
                print("^^^==errorMsg=>" + errorMsg)
            }
            
            if let last_selectedTemp = returnJSON["last_selected"] as? Int {
                last_selected = last_selectedTemp
                //print("^^^==last_selected=>" + String(last_selected))
            }
            
//            var mlImage : UIImage = UIImage(named:"Menu_Hamburger.png")!
//            var mlImageSelected : UIImage = UIImage(named:"Menu_Hamburger.png")!
//
//            var itemCount: Int = 0
            
            if let homeItems = returnJSON["home_items"] as? NSArray {
                parseHomeItem("home_items", homeItems: homeItems) {
                    (returnItems: Array<MLItem>) in
                    if(last_selectedML == nil && last_selected < 0 && returnItems.count > 0) {
                        let firstML = returnItems[0]
                        if(firstML.name.lowercased() == "home workspace") {
                            last_selectedML = firstML
                            last_selected = last_selectedML.id
                            returnItems[0].selected_ind = true
                        }
                    }
                    parsedMLData["home_items"] = returnItems
                }
            }
            
            if let headerHomeItems = returnJSON["header_home_items"] as? NSArray {
                //print("!!--header_home_items found--!!")
                //print(headerHomeItems)
                //print("")
                parseHomeItem("header_home_items", homeItems: headerHomeItems) {
                    (returnItems: Array<MLItem>) in
//                    parsedMLData["header_home_items"] = returnItems
                }
            }
            
            //hard coded shit for user_home_items / My Workspaces
            if let userHomeItems = returnJSON["user_home_items"] as? NSArray {
                let folder_ind: Bool = true
                //let selected_ind: Bool = true
                let id: Int = -1
                let company_id: Int = 0
                let name: String  = "My Workspaces"
                let order_ind: Int = 0
                let settings: String = ""
                let suggested_ind: Bool = false
                let shd_v: Bool = false
                let shd_e: Bool = false
                let shd_d: Bool = false
                let shd_c: Bool = false
                let set_hw_c: Bool = false
                let set_hw_v: Bool = false
                let code_class_name: String = "workspaces"
                let css_class_name: String = "workspaces"
                
                
                let deployed_status: String = "NOT DEPLOYED"
                let layout_id: Int = 0
                let directive_name: String = "workspaces"
                
                let mlAttributes = MLAttributes(layout_id: layout_id)
                
                let mlDirective = MLDirective(directive_name: directive_name, attributes: mlAttributes, webUrl: "")
                
                let mlStack = MLStack(directive: mlDirective)
                
                var userHomeItemsArray: Array<MLItem> = []
                //userHomeItemsArray = parseHomeItem("user_home_items", homeItems: userHomeItems)
                
                parseHomeItem("user_home_items", homeItems: userHomeItems) {
                    (returnItems: Array<MLItem>) in
                    userHomeItemsArray = returnItems
                }
                
                if(last_selectedML == nil && userHomeItemsArray.count > 0) {
                    last_selectedML = userHomeItemsArray[0]
                    last_selected = last_selectedML.id
                    userHomeItemsArray[0].selected_ind = true
                }
                
                let ml = MLItem(folder_ind: folder_ind,
                    selected_ind: true, //last_selected_is_workspace,
                    id: id,
                    company_id: company_id,
                    name: name,
                    order_ind: order_ind,
                    settings: settings,
                    suggested_ind: suggested_ind,
                    shd_v: shd_v,
                    shd_e: shd_e,
                    shd_d: shd_d,
                    shd_c: shd_c,
                    set_hw_c: set_hw_c,
                    set_hw_v: set_hw_v,
                    code_class_name: code_class_name,
                    css_class_name: css_class_name,
                    deployed_status: deployed_status,
                    layout_id: layout_id,
                    children: userHomeItemsArray,
                    mlStack: mlStack)
                
                //print("JSON--parse--last_selected=>" + String(last_selected))
                //print("JSON--parse--id=>" + String(id))
                
                //print("JSON--setCheck->last_selected=>" + String(last_selected))
                //print("JSON--setCheck->id=>" + String(id))
                
                /*
                this only works here if the last selected was on the 1st (or folder) level, otherwise it does nothing
                 */
                if(id == last_selected){
                    //print("Main-last_selected found for name=>" + name)
                    last_selectedML = ml
                }
                
                var workspaceItemsArray: Array<MLItem> = []
                workspaceItemsArray.append(ml)
                parsedMLData["user_home_items"] = workspaceItemsArray
            }
            
            /*
            if let suggestedHomeItems = mlDataJSON["suggested_home_items"] as? NSArray {
                suggested_home_items = suggestedHomeItems as! Array<MLItem>
            }
            */
            
            
        }
        
        completion(parsedMLData)
    }
    
    func parseCompleted(_ parsedMLData: Dictionary<String, Array<MLItem>>) {
        //print("CVC--parseCompleted-mlData=>")
        //print(mlData)
        if(parsedMLData.count > 0) {
            mlData = parsedMLData
//            //print("CVC--parseCompleted-XX-set-mlData=>")
//            //print(mlData)
//            //print("CVC--parseCompleted-XX-last_selected=>")
//            //print(last_selected)
            
            DispatchQueue.main.async {
                self.centerViewController.leftMenuBtn.isEnabled = true
            }
            
//            leftMenuBtn()
            
            if(last_selectedML != nil){
                centerViewController.mlItemSelected(last_selectedML)
            }
            
        } else {
            //print("!!!*** parseCompleted -- parsedMLData == nil ***!!!")
        }
    }
    
    func parseHomeItem(_ group: String, homeItems: NSArray, completion:(_ returnItems: Array<MLItem>) -> Void) {
        var returnItems: Array<MLItem> = []
        for item in homeItems.objectEnumerator().allObjects as! [[String:Any]]
        {
            var folder_ind: Bool!// = false
            var id: Int = 0
            let company_id: Int = 0
            var name: String  = ""
            var order_ind: Int = 0
            var settings: String = ""
            let suggested_ind: Bool = false
            var shd_v: Bool = false
            var shd_e: Bool = false
            var shd_d: Bool = false
            var shd_c: Bool = false
            var set_hw_c: Bool = false
            var set_hw_v: Bool = false
            var code_class_name: String = ""
            var css_class_name: String = ""
            
            var deployed_status: String = ""
//            var childrenData: Array<Any> = []
            var children: Array<MLItem> = []
            
            var layout_id: Int = 0
            var directive_name: String = ""
            var webUrl: String = ""
            
            if let JSON_folder_ind = item["folder_ind"] as? Bool {
                folder_ind = JSON_folder_ind
            } else {
                folder_ind = false
            }
            
            if let JSON_id = item["id"] as? Int {
                id = JSON_id
//                if(id == last_selected && group.containsString("user_home_items")){
//                    last_selected_is_workspace = true
//                }
            }
            if let JSON_company_id = item["company_id"] as? Int {
                shareData.company_id = String(JSON_company_id)
            }
            if let JSON_name = item["name"] as? String {
                name = JSON_name
            }
            //print(">> " + group + "-" + name)
            if let JSON_order_ind = item["order_ind"] as? Int {
                order_ind = JSON_order_ind
            }
            
            if let settingsStr = item["settings"] as? String {
                settings = settingsStr
                let settingsData = settingsStr.data(using: String.Encoding.utf8, allowLossyConversion: false)
                if let settingsJSON = (try? JSONSerialization.jsonObject(with: settingsData!, options: [])) as? NSDictionary{
                    
                    if let directive = settingsJSON["directive"] as? NSDictionary {
                        if let JSON_directive_name = directive["directive"] as? String {
                            directive_name = JSON_directive_name
                        } else if let JSON_directive_name = directive["directive_name"] as? String {
                            directive_name = JSON_directive_name
                        }
                    
                        if let attributes = directive["attributes"] as? NSDictionary {
                            if let layout_idInt = attributes["layout_id"] as? Int {
                                layout_id = layout_idInt
                            }
                            
                            if let webUrl_Temp = attributes["webUrl"] as? String {
                                webUrl = shareData.myUrl + webUrl_Temp.replacingOccurrences(of: "%%COMPANY_ID%%", with: shareData.company_id)
                            }
                        }
                    }
                }
            } else {
                //print("settingsArray not found?")
            }
            
            if let JSON_shd_v = item["shd_v"] as? Bool {
                shd_v = JSON_shd_v
            }
            if let JSON_shd_e = item["shd_e"] as? Bool {
                shd_e = JSON_shd_e
            }
            if let JSON_shd_d = item["shd_d"] as? Bool {
                shd_d = JSON_shd_d
            }
            if let JSON_shd_c = item["shd_c"] as? Bool {
                shd_c = JSON_shd_c
            }
            if let JSON_set_hw_c = item["set_hw_c"] as? Bool {
                set_hw_c = JSON_set_hw_c
            }
            if let JSON_set_hw_v = item["set_hw_v"] as? Bool {
                set_hw_v = JSON_set_hw_v
            }
            if let JSON_code_class_name = item["code_class_name"] as? String {
                code_class_name = JSON_code_class_name
            } else {
                code_class_name = "MLStack"
            }
            if(directive_name.contains("iframe")){
                code_class_name = directive_name
            }
            
            if let JSON_css_class_name = item["css_class_name"] as? String {
                css_class_name = JSON_css_class_name
            }
            
            if let JSON_deployed_status = item["deployed_status"] as? String {
                deployed_status = JSON_deployed_status
            } else {
                deployed_status = "NOT DEPLOYED"
            }
            
            if let JSON_children = item["children"] as? NSArray {
                parseHomeItem(name + "_children", homeItems: JSON_children) {
                    (returnItems: Array<MLItem>) in
                    children = returnItems
                }
            } else {
            
            }
            
            let mlAttributes = MLAttributes(layout_id: layout_id)
            
            let mlDirective = MLDirective(directive_name: directive_name, attributes: mlAttributes, webUrl: webUrl)
            
            let mlStack = MLStack(directive: mlDirective)
            
            let ml = MLItem(folder_ind: folder_ind,
                selected_ind: id == last_selected ? true : false,
                id: id,
                company_id: company_id,
                name: name,
                order_ind: order_ind,
                settings: settings,
                suggested_ind: suggested_ind,
                shd_v: shd_v,
                shd_e: shd_e,
                shd_d: shd_d,
                shd_c: shd_c,
                set_hw_c: set_hw_c,
                set_hw_v: set_hw_v,
                code_class_name: code_class_name,
                css_class_name: css_class_name,
                deployed_status: deployed_status,
                layout_id: layout_id,
                children: children,
                mlStack: mlStack)
            
            if(id == last_selected){
                last_selectedML = ml
            }
            
//            let gtest = "header_home_items"
            let gtest = "home_items"
            
            returnItems.append(ml)
            
            if(group == gtest) {
                if (ml.name.lowercased().contains("insights") || ml.name.lowercased().contains("automated insights") || ml.id == -519) {
                    insightML = ml
                    shareData.notifEnabled = true
                    if(centerViewController != nil) {
                        centerViewController.showDebugControls()
                    }
                }
            }
            
            
//            //Peter Demo
            
//            if(myUrl.lowercased().contains("s6-social")) {
//                if(peterDemoMode == 1) {
//                    if (ml.name.lowercased() == "automated insights") {
//                        insightML = ml
//                        //print("!!--insightML found=>")
//                        //print(insightML.css_class_name)
//                        //print("id=>" + String(insightML.id))
//                    }
//                }
//            } else if(myUrl.lowercased().contains("s20-social")) {
//                if(peterDemoMode == 1 && group == gtest) {
//                    if (ml.name.lowercased().contains("insights") || ml.name.lowercased().contains("automated insights") || ml.id == -519) {
//                        insightML = ml
//                        //print("!!--insightML found=>")
//                        //print(insightML.css_class_name)
//                        //print("id=>" + String(insightML.id))
//                    }
//                }
//            }
        }
        completion(returnItems)
    }
    
    // MARK: CenterViewController delegate methods
    func redrawLayout() {
        if(self.leftViewController != nil) {
            let notAlreadyExpanded = (currentState == .leftPanelExpanded)

            if(AppSettings.IS_IPAD == false){
                animateLeftMenuPanelPhone(shouldExpand: notAlreadyExpanded)
            } else {
                animateLeftMenuPanel(shouldExpand: notAlreadyExpanded)
            }
        }
    }
    
    func leftMenuBtn() {
        if (leftViewController != nil) {
            leftViewController!.refreshMLMenuCompleted(mlData, updated_last_selected: last_selected)
        } else {
            //            if(AppSettings.IS_IPAD){
            //                let notAlreadyExpanded = (currentState != .LeftPanelExpanded)
            //                if notAlreadyExpanded {
            if(self.leftViewController == nil) {
                //TODO: put this back in fix layout engine errors
                addLeftPanelViewController()
            }
        }
    }
    
    func toggleLeftPanel() {
        serialQueue.sync {
            if(self.leftViewController == nil) {
                //println("C->L => notAlreadyExpanded")
                addLeftPanelViewController()
            } else {
                leftViewController!.refreshMLMenuCompleted(mlData, updated_last_selected: last_selected)
                let notAlreadyExpanded = (currentState != .leftPanelExpanded)
                if(AppSettings.IS_IPAD == false){
                    //            animateLeftScreenPanel(shouldExpand: notAlreadyExpanded)
                    animateLeftMenuPanelPhone(shouldExpand: notAlreadyExpanded)
                } else {
                    animateLeftMenuPanel(shouldExpand: notAlreadyExpanded)
                }
            }
        }
    }

    func toggleRightPanel() {
//        if(AppSettings.IS_IPAD == false){
//            let notAlreadyExpanded = (currentState != .RightPanelExpanded)
//            
//            if notAlreadyExpanded {
//                addRightPanelViewController()
//            }
//            animateRightPanel(shouldExpand: notAlreadyExpanded)
//        } else {
            if(self.rightViewController == nil) {
                addRightPanelViewController()
            } else {
                animateRightMenuPanel(shouldExpand: userMenuVis)
            }
//        }
    }
    
    func hideUserMenu() {
        DispatchQueue.main.async {
            if (self.userMenuVis == true){
                // viewController is visible
                self.userMenuVis = false
                self.rightViewController!.view.isHidden = true
            }
        }
    }

    func collapseSidePanels() {
//        //print("Container->collapseSidePanels")
//        let cookies = HTTPCookieStorage.shared.cookies!
//        for cookie in cookies {
//            //print("--c--" + cookie.name + "=" + cookie.value)
//        }
//        //print("--end collapseSidePanels cookies--")
        switch (currentState) {
            case .rightPanelExpanded:
                toggleRightPanel()
            case .leftPanelExpanded:
                toggleLeftPanel()
            default:
            break
        }
    }

    func addLeftPanelViewController() {
        //print("C->L => addLeftPanelViewController")
        if (leftViewController == nil) {
            //print("C->L => leftViewController == nil")
            leftViewController = UIStoryboard.leftViewController()
            leftViewController!.mlData = mlData
            leftViewController!.last_selected = last_selected
            leftViewController!.menuOffset = menuOffset
            DispatchQueue.main.async {
                self.addChildLeftPanelController(self.leftViewController!)
                let notAlreadyExpanded = (self.currentState != .leftPanelExpanded)
                if(AppSettings.IS_IPAD == false){
                    //            animateLeftScreenPanel(shouldExpand: notAlreadyExpanded)
                    self.animateLeftMenuPanelPhone(shouldExpand: notAlreadyExpanded)
                } else {
                    self.animateLeftMenuPanel(shouldExpand: notAlreadyExpanded)
                }
            }
        } else {
            DispatchQueue.main.async {
                self.leftViewController!.view.isHidden = false
            }
        }
    }

    func addRightPanelViewController() {
        if (rightViewController == nil) {
            rightViewController = UIStoryboard.rightViewController()
            //rightViewController!.mlItems = MLItem.allRightList()
            addChildRightPanelController(rightViewController!)
            userMenuVis = true
        } else {
            DispatchQueue.main.async {
                self.rightViewController!.view.isHidden = false
            }
            userMenuVis = true
        }
    }

    func addChildLeftPanelController(_ leftPanelController: LeftMenuViewController) {
        //print("C->L=>addChildLeftPanelController")
        
        
        //TODO: this is throwing big autolayout issues
        
        
        leftPanelController.delegate = centerViewController
        //print("AppSettings.IS_IPAD=>" + String(AppSettings.IS_IPAD))
        var xPos: CGFloat = -(menuWidth + menuOffset)
        var showShadow: Bool = true
        if(AppSettings.IS_IPAD == true){
            xPos = menuOffset
            showShadow = true
        }
        
        DispatchQueue.main.async {
//            var topPadding: CGFloat = 0.0
//            if #available(iOS 11.0, *) {
//                topPadding = self.view.safeAreaInsets.top
//                // ...
//            }
            
            let topSpace: CGFloat = UIApplication.shared.statusBarFrame.size.height + (self.centerViewController.navigationController?.navigationBar.frame.size.height)!
            let menuFrame: CGRect = CGRect(x: xPos, y: topSpace, width: self.menuWidth, height: self.view.frame.size.height - topSpace)
            //leftPanelController
        
        
            leftPanelController.view.frame = menuFrame

            self.view.insertSubview(leftPanelController.view, aboveSubview: self.centerViewController.view)
        
//        } else {
//            view.insertSubview(leftPanelController.view, atIndex: 0)
//        }
            self.addChildViewController(leftPanelController)
            
            self.gShouldLoad = true
            //println(String(format:"add child gShouldLoad=>%@", gShouldLoad))
            leftPanelController.didMove(toParentViewController: self)
            self.showShadowForViewController(showShadow, viewController: self.leftViewController!)
        }
        
        
        
    }
    
    func addChildRightPanelController(_ rightPanelController: RightPanelViewController) {
        rightPanelController.delegate = centerViewController
        
        //if(AppSettings.IS_IPAD == true){
            let topSpace: CGFloat = UIApplication.shared.statusBarFrame.size.height + (centerViewController.navigationController?.navigationBar.frame.size.height)!
//            let xPos: CGFloat = (centerViewController.navigationController?.navigationBar.frame.size.width)! - 200
            let menuMaskFrame: CGRect = CGRect(x: 0, y: topSpace, width: view.frame.size.width, height: view.frame.size.height - topSpace)
            //let menuFrame: CGRect = CGRectMake(xPos, 0, 200, view.frame.size.height - topSpace)
            //leftPanelController
            
            //TODO:  make a layer view background off of rightPanelController as a mask that will dismiss the menu if clicked
            rightPanelController.view.frame = menuMaskFrame
        
        DispatchQueue.main.async {
        
            //rightPanelController.wrapperView.frame = menuFrame
            rightPanelController.wrapperView.layer.shadowOpacity = 0.42
            rightPanelController.wrapperView.layer.masksToBounds = false
            rightPanelController.wrapperView.layer.shadowOffset = CGSize(width: 5, height: 5);
            rightPanelController.wrapperView.layer.shadowRadius = 10;
            
            
            
            self.view.insertSubview(rightPanelController.view, aboveSubview: self.centerViewController.view)
            let gesture = UITapGestureRecognizer(target: self, action: #selector(CenterViewControllerDelegate.toggleRightPanel))
            rightPanelController.maskView.addGestureRecognizer(gesture)
            
//            userMenuVis = true
//        } else {
//            view.insertSubview(rightPanelController.view, atIndex: 0)
//        }
        
            self.addChildViewController(rightPanelController)
        
//        gShouldLoad = true
        //println(String(format:"add child gShouldLoad=>%@", gShouldLoad))
            rightPanelController.didMove(toParentViewController: self)
            
        }
        userMenuVis = true
        gShouldLoad = true
        
    }

    func animateLeftScreenPanel(shouldExpand: Bool) {
        if (shouldExpand) {
            currentState = .leftPanelExpanded
            animateCenterPanelXPosition(targetPosition: centerNavigationController.view.frame.width - (centerNavigationController.view.frame.width > 400 ? centerNavigationController.view.frame.width - 360 : centerPanelExpandedOffset))
        } else {
            animateCenterPanelXPosition(targetPosition: 0) { finished in
                self.currentState = .bothCollapsed
                DispatchQueue.main.async {
                    self.leftViewController!.view.isHidden = true
                }
                //this forces a dump on closing and a reload each open
                //self.leftViewController!.view.removeFromSuperview()
                //self.leftViewController = nil;
            }
        }
    }
    
    func animateLeftMenuPanel(shouldExpand: Bool){
        //print("CVC--animateLeftMenuPanel shouldExpand=>" + String(shouldExpand))
        if (shouldExpand) {
            currentState = .leftPanelExpanded
            leftViewController!.toggleTableText(!shouldExpand)
            animateMenuWidth(targetWidth: menuWidth) { finished in
                DispatchQueue.main.async {
                    self.leftViewController!.view.frame.size.width = self.menuWidth
                }
                //self.leftViewController!.tableView.frame.size.width = 320
            }
        } else {
            animateMenuWidth(targetWidth: menuWidthCollapsed) { finished in
                //need to move this out of the animate call, and make a call back to start the animate call after this call is completed
                //self.leftViewController!.tableView.frame.size.width = 40
                DispatchQueue.main.async {
                    self.leftViewController!.view.frame.size.width = self.menuWidthCollapsed
                }
                self.leftViewController!.toggleTableText(!shouldExpand)
                self.currentState = .bothCollapsed
            }
        }
    }
    
    func animateMenuWidth(targetWidth: CGFloat, completion: ((Bool) -> Void)! = nil) {
        DispatchQueue.main.async {
            self.centerViewController.contentViewLeftConstraint.constant = targetWidth
            self.centerViewController.contentView.setNeedsUpdateConstraints()
            self.centerViewController.contentView.setNeedsLayout()
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIViewAnimationOptions(), animations: {
                self.leftViewController!.view.frame.size.width = targetWidth
                self.leftViewController!.tableView.frame.size.width = targetWidth
                self.centerViewController.contentView.updateConstraints()
                self.centerViewController.contentView.layoutIfNeeded()
                self.leftViewController!.view.updateConstraints()
                self.leftViewController!.view.layoutIfNeeded()
                }, completion: completion)
        }
    }
    
    func animateLeftMenuPanelPhone(shouldExpand: Bool){
        //print("CVC--animateLeftMenuPanelPhone shouldExpand=>" + String(shouldExpand))
        if (shouldExpand) {
            currentState = .leftPanelExpanded
            animateMenuPosition(targetPos: -(menuOffset)) { finished in
                
            }
        } else {
            animateMenuPosition(targetPos: -(menuWidth + menuOffset)) { finished in
                self.currentState = .bothCollapsed
            }
        }
    }
    
    func animateMenuPosition(targetPos: CGFloat, completion: ((Bool) -> Void)! = nil) {
        DispatchQueue.main.async {
            self.leftViewController!.view.setNeedsUpdateConstraints()
            self.leftViewController!.view.setNeedsLayout()
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIViewAnimationOptions(), animations: {
                //print("self.view.safeAreaInsets.top=>\(self.view.safeAreaInsets.top)")
                //print("UIApplication.shared.statusBarFrame.size.height=>\(UIApplication.shared.statusBarFrame.size.height)")
                let topPadding: CGFloat = UIApplication.shared.statusBarFrame.size.height
//                if #available(iOS 11.0, *) {
//                    topPadding = 32
////                    topPadding = self.view.safeAreaInsets.top - UIApplication.shared.statusBarFrame.size.height
//                    // ...
//                } else {
//                    topPadding = UIApplication.shared.statusBarFrame.size.height
//                }
                
                let topSpace: CGFloat = topPadding + (self.centerViewController.navigationController?.navigationBar.frame.size.height)!
                
                
                let menuFrame: CGRect = CGRect(x: targetPos, y: topSpace, width: self.menuWidth, height: self.view.frame.size.height - topSpace)
                self.leftViewController?.view.frame = menuFrame
                
    //            self.showShadowForViewController(targetPos < -10 ? false : true, viewController: self.leftViewController!)

                self.leftViewController!.view.updateConstraints()
                self.leftViewController!.view.layoutIfNeeded()
                }, completion: completion)
        }
    }

    func animateRightPanel(shouldExpand: Bool) {
        if (shouldExpand) {
            currentState = .rightPanelExpanded
            animateCenterPanelXPosition(targetPosition: -centerNavigationController.view.frame.width + centerPanelExpandedOffset)
        } else {
            animateCenterPanelXPosition(targetPosition: 0) { _ in
                self.currentState = .bothCollapsed
                DispatchQueue.main.async {
                    self.rightViewController!.view.isHidden = true
                }
                //self.rightViewController!.view.removeFromSuperview()
                //self.rightViewController = nil;
            }
        }
    }
    
    func animateRightMenuPanel(shouldExpand: Bool) {
        //let isVis: Bool = self.rightViewController!.view.hidden
        //self.rightViewController!.view.hidden = !isVis
        //print("animateRightMenuPanel=>" + String(userMenuVis))
        if (self.userMenuVis == true){
            // viewController is visible
            self.userMenuVis = false
            DispatchQueue.main.async {
                self.rightViewController!.view.isHidden = true
            }
        } else {
            self.userMenuVis = true
            DispatchQueue.main.async {
                self.rightViewController!.view.isHidden = false
            }
        }
        
        /*
        if (shouldExpand) {
            currentState = .RightPanelExpanded
            self.rightViewController!.view.hidden = false
            //animateCenterPanelXPosition(targetPosition: -CGRectGetWidth(centerNavigationController.view.frame) + centerPanelExpandedOffset)
        } else {
            //animateCenterPanelXPosition(targetPosition: 0) { _ in
                self.currentState = .BothCollapsed
                self.rightViewController!.view.hidden = true
                //self.rightViewController!.view.removeFromSuperview()
                //self.rightViewController = nil;
            //}
        }
        */
    }

    func animateCenterPanelXPosition(targetPosition: CGFloat, completion: ((Bool) -> Void)! = nil) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIViewAnimationOptions(), animations: {
                self.centerNavigationController.view.frame.origin.x = targetPosition
            }, completion: completion)
        }
    }

    func showShadowForCenterViewController(_ shouldShowShadow: Bool) {
        //print("C->L=>showShadowForCenterViewController")
        DispatchQueue.main.async {
//            if (shouldShowShadow) {
//                self.centerNavigationController.view.layer.shadowOpacity = 0.8
//            } else {
//                self.centerNavigationController.view.layer.shadowOpacity = 0.0
//            }
            if(self.leftViewController != nil){
                if (shouldShowShadow) {
                    self.leftViewController!.view.layer.shadowOpacity = 0.8
                } else {
                    self.leftViewController!.view.layer.shadowOpacity = 0.0
                }
            }
            
        }
    }
    
    func showShadowForViewController(_ shouldShowShadow: Bool, viewController: UIViewController) {
        //print("CVC--showShadowForViewController=>" + String(stringInterpolationSegment:shouldShowShadow))
        DispatchQueue.main.async {
            if (shouldShowShadow) {
                viewController.view.layer.shadowOpacity = 0.42
                viewController.view.layer.masksToBounds = false
                viewController.view.layer.shadowOffset = CGSize(width: 5, height: 10);
                viewController.view.layer.shadowRadius = 10;
            } else {
                viewController.view.layer.shadowOpacity = 0.0
            }
        }
    }

    // MARK: Gesture recognizer

    func handlePanGesture(_ recognizer: UIPanGestureRecognizer) {
        //print("ContainerVC--handlePanGesture")
        switch(recognizer.state) {
            case .began:
                gMenuPan = Bool((recognizer.location(in: self.view).y < centerNavigationController.navigationBar.frame.height + UIApplication.shared.statusBarFrame.size.height - 2))
                
                //println(String(format:"Began=>"))
                gStartX = Int(recognizer.location(in: self.view).x)
                gStartY = Int(recognizer.location(in: self.view).y)

                //println(String(format:"Began x==>%@", String(gStartX)))
                //println(String(format:"Began y=>%@", String(gStartY)))
                
                gShouldLoad = true
                
                if((currentState == .bothCollapsed && recognizer.location(in: self.view).y < centerNavigationController.navigationBar.frame.height + UIApplication.shared.statusBarFrame.size.height) || currentState != .bothCollapsed){
                    // If the user starts panning, and neither panel is visible
                    // then show the correct panel based on the pan direction
                    showShadowForCenterViewController(true)
                }
            case .changed:
//                let gChangeX = Int(recognizer.location(in: self.view).x)
                let gChangeXDiff = Int(recognizer.location(in: self.view).x) - gStartX
                
                //println(String(format:"Changed x %@ start=>%@ new=>%@ diff=>%@", gMenuPan, String(gChangeX), String(gStartX), String(gChangeXDiff)))
                
                //loads SidePanel Views
                if(gMenuPan == true && currentState == .bothCollapsed &&
                    ((gChangeXDiff > gThreshold) || (gChangeXDiff < (gThreshold * -1)))){
                    if(gShouldLoad == true){
                        gShouldLoad = false
                        if (gChangeXDiff > 0) {
                            addLeftPanelViewController()
                        } else if (gChangeXDiff < 0) {
                            addRightPanelViewController()
                        }
                    }
                }
            
                //decide whether CenterView should drag
                if((gMenuPan == true && currentState == .bothCollapsed) ||
                   (gMenuPan != true && currentState != .bothCollapsed)){
                    if ((leftViewController != nil && gChangeXDiff >= 0) ||
                        (0 >= gChangeXDiff && rightViewController != nil) ||
                        (leftViewController == nil && rightViewController == nil) ||
                        (gMenuPan != true && currentState != .bothCollapsed)) {
                        // If the user is already panning, translate the center view controller's
                        // view by the amount that the user has panned
                        recognizer.view!.center.x = recognizer.view!.center.x + recognizer.translation(in: view).x
                    } else {
                        recognizer.view!.center.x = self.view.center.x
                    }
                    recognizer.setTranslation(CGPoint.zero, in: view)
                }
            
            case .ended:
                // When the pan ends, check whether the left or right view controller is visible
                //println(String(format:"ended=>%@", recognizer.locationInView(self.view).y))
                
                if (leftViewController != nil) {
                    //animate if collapsed and moved within menu bar, or is open and moved anywhere
                    let hasMovedPassedThreshold = recognizer.view!.center.x > view.bounds.size.width
                    //println(String(format:"animateLeftScreenPanel=>%@", hasMovedPassedThreshold))
                    animateLeftScreenPanel(shouldExpand: currentState == .bothCollapsed ? hasMovedPassedThreshold : false)
                } else if (rightViewController != nil) {
                    let hasMovedPassedThreshold = recognizer.view!.center.x < 0
                    //println(String(format:"animateRightPanel=>%@", hasMovedPassedThreshold))
                    animateRightPanel(shouldExpand: currentState == .bothCollapsed ? hasMovedPassedThreshold : false)
                }
            default:
                break
        }
    }
    
    /*------------------------------------------*/
    
    func launchClassFromString(_ classString: String, mlItem: MLItem) {
        if let theClass = NSClassFromString(classString) as? UIViewController.Type {
            let viewController = theClass.init(nibName: classString, bundle: nil)
            //viewController.mlItem = mlItem
            DispatchQueue.main.async {
                self.present(viewController, animated: true, completion: nil)
            }
        } else {
            //no class found
        }
    }
    
    /*------------------------------------------*/
    
    func returnToLogin() {
        logOut(false)
    }
    
    func userLoggedOut() {
        let mobNot: MobileNotification = MobileNotification()
        mobNot.registerDevice(active_ind: false)
        if(centerViewController != nil) {
            centerViewController.insightBtn.removeBadge()
        }
        let application = UIApplication.shared
        //TODO: need DispatchQueue.main.async {?
        application.applicationIconBadgeNumber = 0
        logOut(false)
    }
    
    func sessionTimedOut() {
        logOut(false)
    }
    
    func logOut(_ autoLogin: Bool){
        hideUserMenu()
//        var actionUrl: String!
        ssoLogout()
    }
    
    func ssoLogout(){
        if let accessToken = UserDefaults.standard.object(forKey: "accessToken") as! String? {
            if accessToken.count > 0 {
                let urlPath = shareData.myUrl + "/account/ssologoff?token=" + accessToken + "&returnUrl=/"
    //            let urlPath = myUrl + "/api/user/" + String(company_id)
                //print("urlPath=>" + urlPath)
                if let url = URL(string: urlPath) {
                    //                // Open the operations queue after 1 second
                    //                DispatchQueue.main.asyncAfter(deadline: self.time, execute: {[weak self] in
                    //                    //print("Opening the OperationQueue")
                    //                    self?.opQueue.isSuspended = false
                    //                })
                    let urlRequest: NSMutableURLRequest = NSMutableURLRequest(url: url)
                    let session = URLSession.shared
                    let task = session.dataTask(with: urlRequest as URLRequest) {
                        (data, response, error) -> Void in
                        
                        let httpResponse = response as! HTTPURLResponse
                        let statusCode = httpResponse.statusCode
                        
                        if (statusCode == 200) {
                            //print("success")
                        } else {
                            //print("Load error getting notifications")
                        }
                        
                        self.showLogin(true)
                    }
                    
                    task.resume()
                }
            }
        }
    }
    
    func apiConnComplete(_ connData: NSMutableData) {
//        showLogin(false)
    }
    
    func loadInsightView() {
        if(insightML != nil && centerViewController != nil) {
            centerViewController.mlItemSelected(insightML)
            centerViewController.checkInsightsBadge()
            if(leftViewController != nil) {
                leftViewController?.deselectLastItem()
            }
        }
    }
}


extension ContainerViewController:URLSessionDelegate {
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        //accept all certs when testing, perform default handling otherwise
//        var isTestingUrl: Bool = true
//        if isTestingUrl == true {
//            //print("Accepting cert as always")
//            completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
//        }
//        else {
            //print("Using default handling")
            completionHandler(.performDefaultHandling, URLCredential(trust: challenge.protectionSpace.serverTrust!))
//        }
    }
    
    func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        // We've got an error
        if let err = error {
            print("CVC--Error: \(err.localizedDescription)")
        } else {
            print("CVC--Error. Giving up")
        }
        //            PlaygroundPage.current.finishExecution()
    }
}

extension ContainerViewController:URLSessionDataDelegate {
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didBecome streamTask: URLSessionStreamTask) {
        // The task became a stream task - start the task
        //print("CVC--didBecome streamTask")
        streamTask.resume()
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didBecome downloadTask: URLSessionDownloadTask) {
        // The task became a download task - start the task
        //print("CVC--didBecome downloadTask")
        downloadTask.resume()
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        // We've got a URLAuthenticationChallenge - we simply trust the HTTPS server and we proceed
        //print("CVC--didReceive challenge")
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: @escaping (URLRequest?) -> Void) {
        // The original request was redirected to somewhere else.
        // We create a new dataTask with the given redirection request and we start it.
//        if let urlString = request.url?.absoluteString {
//            print("CVC--willPerformHTTPRedirection to \(urlString)")
//        } else {
//            print("CVC--willPerformHTTPRedirection")
//        }
        if let task = self.session?.dataTask(with: request) {
            task.resume()
        }
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        // We've got an error
        if let err = error {
            print("CVC--Error: \(err.localizedDescription)")
        } else {
            //print("CVC--Session Complete")
            if(self.responseData.length > 0) {
                self.parseMLJSON(self.responseData) {
                    (parsedMLData: Dictionary<String, Array<MLItem>>) in
                    self.parseCompleted(parsedMLData)
                }
            }
        }
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        // We've got the response headers from the server.
        //print("CVC--didReceive response")
        self.response = response
        self.responseData = NSMutableData()
        completionHandler(URLSession.ResponseDisposition.allow)
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        // We've got the response body
        //print("CVC--didReceive data")
//        if let responseText = String(data: data, encoding: .utf8) {
//            print(self.response ?? "")
//            print("\nCVC--Server's response text")
//            print(responseText)
//        }
        
        self.responseData.append(data)
        self.session?.finishTasksAndInvalidate()
        //            PlaygroundPage.current.finishExecution()
    }
}



private extension UIStoryboard {
    class func mainStoryboard() -> UIStoryboard {
        if #available(iOS 11, *) {
            return UIStoryboard(name: "Main", bundle: Bundle.main)
        } else {
            return UIStoryboard(name: "MainiOS10", bundle: Bundle.main)
        }
    }
    
    class func expionViewController() -> ExpionViewController? {
        return mainStoryboard().instantiateViewController(withIdentifier: "ExpionViewController") as? ExpionViewController
    }
    
    class func loginViewController() -> LoginViewController? {
        return mainStoryboard().instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
    }
    
    class func leftViewController() -> LeftMenuViewController? {
        return mainStoryboard().instantiateViewController(withIdentifier: "LeftViewController") as? LeftMenuViewController
    }

    class func rightViewController() -> RightPanelViewController? {
        return mainStoryboard().instantiateViewController(withIdentifier: "RightViewController") as? RightPanelViewController
    }

  class func centerViewController() -> CenterViewController? {
    return mainStoryboard().instantiateViewController(withIdentifier: "CenterViewController") as? CenterViewController
  }
}
