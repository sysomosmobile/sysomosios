//
//  GalleryCreateNew.swift
//  Expion
//
//  Created by Mike Bagwell on 5/19/16.
//  Copyright © 2016 Expion. All rights reserved.
//

import Foundation
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


@objc
protocol GalleryCreateNewViewControllerDelegate {
    func creationViewCompleted()
}

class GalleryCreateNewViewController: UIViewController, UITextViewDelegate, FlatTreeViewControllerDelegate, UIGestureRecognizerDelegate {
    var delegate: GalleryCreateNewViewControllerDelegate?
    var isAlbumCreate: Bool = true

    var albumType: String!
    var newImage: UIImage!
    
    @IBOutlet var detailName: UITextField!
    @IBOutlet var detailDesc: UITextView!
    @IBOutlet var detailDescLabel: UILabel!
    @IBOutlet var detailImgView: UIImageView!
    
    var locationArray: Array<Dictionary<String, AnyObject>>!
    @IBOutlet var locToolbar: UIView!
    @IBOutlet var albumLocBtn: UIButton!
    @IBAction func locationButton() {
        showFlatLocTree()
    }
    
    
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var loadingLabel: UILabel!
    
    @IBOutlet var detailsView: UIView!
    @IBOutlet weak var detailsViewHConstraint: NSLayoutConstraint!
    var albumObject: Dictionary<String, AnyObject>!
    
    @IBAction func cancelBtn(_ sender: AnyObject) {
        //print("createToAlbums")
        self.performSegue(withIdentifier: "createToAlbums", sender: self)
    }
    
    @IBOutlet var titleButton: UIBarButtonItem!
    
    @IBOutlet var createButton: UIBarButtonItem!
    @IBAction func createBtn(_ sender: AnyObject) {
        loadingView.isHidden = false
        if (isAlbumCreate == true){
            createNewAlbum()
        } else {
            uploadImageCheck()
        }
//        self.performSegueWithIdentifier("createToAlbums", sender: self)
    }
    
    var uploadAlert: UIAlertController!
    
    let shareData = ShareData.sharedInstance
    override func viewDidLoad() {
        super.viewDidLoad()
        //print("GalleryCreateNewViewController=" + shareData.myUrl)
        //print("companyId=" + shareData.company_id)
        detailDesc.delegate = self
        DispatchQueue.main.async {
            if(self.newImage != nil) {
                self.detailImgView.image = self.newImage
                self.detailImgView.contentMode = .scaleAspectFit //.ScaleAspectFill
            } else {
                self.detailImgView.image = UIImage(named: "Full_Folder.png")!
            }
        
//            self.detailImgView.image = cellImage //UIImage(named: "IconGlobe.png")
            self.detailImgView.layer.borderWidth = 1.0
            self.detailImgView.layer.masksToBounds = true
            self.detailImgView.layer.borderColor = UIColor(red: 191/255, green: 191/255, blue: 191/255, alpha: 1).cgColor
        }
        if(locationArray != nil) {
            DispatchQueue.main.async {
                self.locToolbar.isHidden = false
            }
            setLocs()
        } else {
            setAlbumLocName()
//            locToolbar.hidden = true
        }
        
        if(isAlbumCreate == true) {
            DispatchQueue.main.async {
                self.detailsView.isHidden = false
                self.createButton.title = "Create"
                self.titleButton.title = "Album Creation"
            }
        } else {
            DispatchQueue.main.async {
                if(self.albumType=="e"){
                    self.detailsView.isHidden = false
                } else if(self.albumType=="fb"){
                    self.detailsView.isHidden = true
                    self.detailsView.frame.size.height = 0
                    self.detailsViewHConstraint.constant = 0
    //                detailCreateBtn.enabled = true
                }
                self.createButton.title = "Upload"
                self.titleButton.title = "Image Upload"
            }
        }
    }
    
    /* modal view controller dismiss on click outside of bounds - BEGIN */
    fileprivate var tapOutsideRecognizer: UITapGestureRecognizer!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(self.tapOutsideRecognizer == nil) {
            self.tapOutsideRecognizer = UITapGestureRecognizer(target: self, action: #selector(GalleryDetailViewController.handleTapBehind(_:)))
            self.tapOutsideRecognizer.numberOfTapsRequired = 1
            self.tapOutsideRecognizer.cancelsTouchesInView = false
            self.tapOutsideRecognizer.delegate = self
            self.view.window!.addGestureRecognizer(self.tapOutsideRecognizer)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if(self.tapOutsideRecognizer != nil) {
            self.view.window?.removeGestureRecognizer(self.tapOutsideRecognizer)
            self.tapOutsideRecognizer = nil
        }
    }
    
    func close(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func handleTapBehind(_ sender: UITapGestureRecognizer) {
        if (sender.state == UIGestureRecognizerState.ended) {
//            let location: CGPoint = sender.locationInView(nil)
//            
//            if (!self.view.pointInside(self.view.convertPoint(location, fromView: self.view.window), withEvent: nil)) {
//                self.view.window?.removeGestureRecognizer(sender)
//                self.close(sender)
//            }
            let location: CGPoint = sender.location(in: self.view)
            
            if(self.view.point(inside: location, with: nil)){
                //print("P--In view??")
            } else {
                //print("P--Not in view??")
                self.view.window?.removeGestureRecognizer(sender)
                self.close(sender)
                
            }
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    /* modal view controller dismiss on click outside of bounds - END */
    
    func setAlbumLocName() {
        var titleTxt: String = ""
        if let locObjTemp = albumObject["Location"] as? Dictionary<String, AnyObject> {
            if let location_nameTemp = locObjTemp["name"] as? String {
                titleTxt = location_nameTemp + " - "
            }
        }
        if let albumTemp = albumObject["title"] as? String {
            titleTxt = titleTxt + albumTemp
        } else if let albumTemp = albumObject["album_name"] as? String {
            titleTxt = titleTxt + albumTemp
        }
        DispatchQueue.main.async {
            self.albumLocBtn.isEnabled = false
            self.albumLocBtn.setImage(nil, for: UIControlState())
            self.albumLocBtn.setTitle("  " + titleTxt, for: UIControlState())
        }
    }
    
//    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
//        detailDescLabel.hidden = true
//        return true
//    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView == detailDesc {
            DispatchQueue.main.async {
                if textView.text?.count > 0 {
                    self.detailDescLabel.isHidden = true
                } else {
                    self.detailDescLabel.isHidden = false
                }
            }
        }
    }
    
    func setLocs() {
        let locs = LocationUtils().forceLocListSingle(self.locationArray)
        self.locationArray = locs.0
        let titleTxt = locs.1
        DispatchQueue.main.async {
            self.albumLocBtn.setTitle("  " + titleTxt, for: UIControlState())
        }
    }

    func showFlatLocTree() {
        var storyboard = UIStoryboard() //(name: "Main", bundle: nil)
        if #available(iOS 11, *) {
            storyboard = UIStoryboard(name: "Main", bundle: nil)
        } else {
            storyboard = UIStoryboard(name: "MainiOS10", bundle: nil)
        }
//        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let locSelect: FlatTreeViewController = storyboard.instantiateViewController(withIdentifier: "FlatTreeViewController") as! FlatTreeViewController
        
        locSelect.treeItemArray = self.locationArray
        locSelect.canSelectMultiples = false
        
        locSelect.delegate = self
        
        if(AppSettings.isPad() == true) {
            locSelect.modalPresentationStyle = UIModalPresentationStyle.formSheet
            locSelect.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        }
        DispatchQueue.main.async {
            self.present(locSelect, animated: true, completion: nil)
            self.albumLocBtn.setTitle("  ...", for: UIControlState())
        }
    }
    
    func locViewControllerDismissed(_ returnedLocs: Array<Dictionary<String, AnyObject>>, withTitle titleTxt: String){
        //TODO fill this out
        self.locationArray.removeAll()
        self.locationArray = returnedLocs
        DispatchQueue.main.async {
            self.albumLocBtn.setTitle("  " + titleTxt, for: UIControlState())
        }
    }
    
    func createNewAlbum() {
        let locs: Array<Int> = LocationUtils().getSelectedFromLocList(self.locationArray)
        if(detailName.text?.count > 0 && locs.count > 0){// || albumType=="fb"){
            DispatchQueue.main.async {
                self.detailName.resignFirstResponder()
                self.detailDesc.resignFirstResponder()
            }
            let urlString: String = String(format:"%@/api/eimages/albums/%@", shareData.myUrl, shareData.company_id)
            self.createAlbumPost(urlString)
        } else {
            uploadAlert = UIAlertController(title: "Field cannot be empty", message:"Please provide a name.", preferredStyle: .alert)
            uploadAlert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
            DispatchQueue.main.async {
                self.present(self.uploadAlert, animated: true){}
                self.loadingView.isHidden = true
            }
        }
    }
    
    func uploadImageCheck() {
        if(albumType=="fb" || (albumType=="e" && detailName.text?.count > 0)){// || albumType=="fb"){
            DispatchQueue.main.async {
                self.detailName.resignFirstResponder()
                self.detailDesc.resignFirstResponder()
            }
            self.uploadImage()
        } else {
            uploadAlert = UIAlertController(title: "Field cannot be empty", message:"Please provide a name.", preferredStyle: .alert)
            uploadAlert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
            DispatchQueue.main.async {
                self.present(self.uploadAlert, animated: true){}
                self.loadingView.isHidden = true
            }
        }
    }
    
    func uploadImage() {
        var cookieVal: String = ""
        let cookies = HTTPCookieStorage.shared.cookies
        for cookie in cookies! //[[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies])
        {
            //print("cookie name=>" + cookie.name);
            if(cookie.name.uppercased().range(of: "ASPXAUTH") != nil){
                cookieVal = cookie.value
                //print("cookie ASPXAUTH found=>" + cookie.value)
            }
        }
        //print("uploadImage cookieVal=>" + cookieVal)
        
        if(albumObject != nil){
            var loc_id: String = ""
            if let locDict = albumObject["Location"] as? Dictionary<String, AnyObject> {
                if let locId = locDict["id"] as? Int {
                    loc_id = String(locId)
                }
            }

            var currentAlbumId: String = ""
            if let albumId = albumObject["id"] as? Int {
                currentAlbumId = String(albumId)
            }
            
            let fbUrlString: String = String(format:"%@/Upload/FacebookFileMobile?location_id=%@&album_id=%@&company_id=%@&app=FBIMAGEV2&token=%@", shareData.myUrl, loc_id, currentAlbumId, shareData.company_id, cookieVal)
            
            //eimage
            let eUrlString: String = String(format:"%@/Upload/ExpionFileMobile?location_id=%@&album_id=%@&company_id=%@&app=EIMAGES&token=%@", shareData.myUrl, loc_id, currentAlbumId, shareData.company_id, cookieVal)
            
            let urlString: String = albumType == "fb" ? fbUrlString : eUrlString
            
            //print("uploadImage url=>" + urlString)
            uploadImagePost(urlString)
        }
    }
    
    func uploadImagePost(_ urlPath: String) {
        if(shareData.myUrl != nil && shareData.company_id != nil){
            let url: URL = URL(string: urlPath)!
            //print(String(format: "uploadImagePost urlPath=>%@", url as CVarArg))
            
            let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
            
            let boundary: String = "form101"
            let contentType = String(format:"multipart/form-data; boundary=%@", boundary)
            request.addValue(contentType, forHTTPHeaderField: "Content-Type")
            request.setValue(ShareData.sharedInstance.userAgent, forHTTPHeaderField: "User-Agent")
            request.httpMethod = "POST"
            
            
            let body: NSMutableData = NSMutableData()
            //dunno if this will work
            //[body appendData: jsonData];
            
            body.append(String(format:"\r\n--%@\r\n",boundary).data(using: String.Encoding.utf8)!)
            body.append(String(format:"Content-Disposition: form-data; name=\"file\"; filename=\"%@.jpg\"\r\n", (albumType == "fb" ? "facebookImage" : detailName.text!)).data(using: String.Encoding.utf8)!)
            body.append(String(format:"Content-Type: application/octet-stream\r\n\r\n").data(using: String.Encoding.utf8)!)
            
            body.append(UIImageJPEGRepresentation(newImage, 0.8)!)
            //print("-----appendData imageData-----")
            
            body.append(String(format:"\r\n--%@--\r\n",boundary).data(using: String.Encoding.utf8)!)
            
            request.httpBody = body as Data
            
            let connection: NSURLConnection = NSURLConnection(request: request as URLRequest, delegate: self, startImmediately: false)!
            
            connection.start()

//            uploadAlert = UIAlertController(title: "Uploading Image", message:"", preferredStyle: .Alert)
//            self.presentViewController(uploadAlert, animated: true){}
        }
    }
    
    func createAlbumPost(_ urlPath: String) {
        if(shareData.myUrl != nil && shareData.company_id != nil){
            let url: URL = URL(string: urlPath)!
            //print(String(format: "createAlbumPost urlPath=>%@", url as CVarArg))
            
            let locs: Array<Int> = LocationUtils().getSelectedFromLocList(self.locationArray)
            let loctionary: Dictionary<String, AnyObject> = ["id": locs[0] as AnyObject]
            
            let title: String = detailName.text!
            let description: String = detailDesc.text
            
            let jsonDict: Dictionary<String, AnyObject> = ["Location": loctionary as AnyObject,
                                                           "title": title as AnyObject,
                                                           "description": description as AnyObject,
                                                           "type": albumType as AnyObject]
            
            let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
            request.setValue(ShareData.sharedInstance.userAgent, forHTTPHeaderField: "User-Agent")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            
//            var jsonError: NSError
            request.httpBody = try! JSONSerialization.data(withJSONObject: jsonDict, options: [])
            
            let connection: NSURLConnection = NSURLConnection(request: request as URLRequest, delegate: self, startImmediately: false)!
            
            connection.start()
//            uploadAlert = UIAlertController(title: "Creating Album", message:"", preferredStyle: .Alert)
//            self.presentViewController(uploadAlert, animated: true){}
        }
    }
    
    var data: NSMutableData = NSMutableData()
    func connection(_ didReceiveResponse: NSURLConnection!, didReceiveResponse response: URLResponse!) {
        // Received a new request, clear out the data object
        if let httpResponse = response as? HTTPURLResponse {
            //print(httpResponse.statusCode)
            if(httpResponse.statusCode == 200){
                self.data = NSMutableData()
            } else {
                //print("didReceiveResponse != 200")
                //                returnToLogin()
                DispatchQueue.main.async {
                    self.loadingView.isHidden = true
                }
            }
        } else {
            assertionFailure("unexpected response")
        }
        
        
    }
    
    func connection(_ connection: NSURLConnection!, didReceiveData data: Data!){
        self.data.append(data)
//        let dataStr = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
//        print(String(format: "!!~~dataStr=>%@", dataStr!))
    }
    
    func connectionDidFinishLoading(_ connection: NSURLConnection!) {
//        var err: NSError
        // throwing an error on the line below (can't figure out where the error message is)
        //mlData.removeAll()
        //parseMLJSON(data)
        self.creationViewCompleted()
    }
    
    func creationViewCompleted() {
//        delegate?.creationViewCompleted()
//        self.dismissViewControllerAnimated(false, completion: nil)
//        NSOperationQueue.mainQueue().addOperationWithBlock {
//            [weak self] in
//            self?.performSegueWithIdentifier("createToAlbums", sender: self)
//        }
        if(isAlbumCreate == true){
            DispatchQueue.main.async{
                self.performSegue(withIdentifier: "createToAlbums", sender: self)
            }
        } else {
            DispatchQueue.main.async{
                self.performSegue(withIdentifier: "backToImages", sender: self)
            }
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "createToAlbums" {
            //print("GalleryCreateNewViewController>prepareForSegue createToAlbums")
        }
        if segue.identifier == "backToImages" {
            //print("GalleryCreateNewViewController>prepareForSegue backToImages")
        }
    }
    
}
