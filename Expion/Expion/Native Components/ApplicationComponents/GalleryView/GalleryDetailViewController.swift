//
//  GalleryDetailViewController.swift
//  Expion
//
//  Created by Mike Bagwell on 5/11/16.
//  Copyright © 2016 Expion. All rights reserved.
//

import Foundation

@objc
protocol GalleryDetailViewControllerDelegate {
    func showLoading()
    func hideLoading()
    func getImageDataForDetail(_ index: Int) //-> Dictionary<String, AnyObject>
    @objc optional func reloadTable()
    @objc optional func setNeedsUpdate()
}

class GalleryDetailViewController: UIViewController, UIGestureRecognizerDelegate {
    var delegate: GalleryDetailViewControllerDelegate?
    var displayMode: String!
    var showApprovalMode: Bool!
    
    @IBOutlet var skipBtn: UIBarButtonItem!
    
    @IBOutlet var imageNameLabel: UIBarButtonItem!

    @IBOutlet var imageByLabel: UILabel!
    @IBOutlet var imageLocationLabel: UILabel!
    @IBOutlet var imageAlbumLabel: UILabel!
    @IBOutlet var imageCell: UIImageView!
    
    @IBOutlet var rotateToolbar: UIToolbar!
    @IBOutlet var rotateCCWBtn: UIBarButtonItem!
    @IBOutlet var rotateCWBtn: UIBarButtonItem!
    
    @IBOutlet var actionsToolbar: UIToolbar!
    @IBOutlet var rejectActionBtn: UIBarButtonItem!
    @IBOutlet var approveActionBtn: UIBarButtonItem!
    
    @IBOutlet var loadingView: UIView!
    
    var imageRotate: Int = 0
    var rejectUrl: String!
    var approveUrl: String!
    
    var itemCount: Int = 0
    var currentItemIndex: Int = 0
    var itemData: Dictionary<String, AnyObject>!
    var itemImgage: UIImage = UIImage()
    
    var tableNeedsReload: Bool = false
    
    let shareData = ShareData.sharedInstance
    override func viewDidLoad() {
        super.viewDidLoad()
       //print("GalleryDetailViewController=" + shareData.myUrl)
       //print("companyId=" + shareData.company_id)
        setupActions()
        setupDetailView()
    }
    
    /* modal view controller dismiss on click outside of bounds - BEGIN */
    fileprivate var tapOutsideRecognizer: UITapGestureRecognizer!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(self.tapOutsideRecognizer == nil) {
            self.tapOutsideRecognizer = UITapGestureRecognizer(target: self, action: #selector(GalleryDetailViewController.handleTapBehind(_:)))
            self.tapOutsideRecognizer.numberOfTapsRequired = 1
            self.tapOutsideRecognizer.cancelsTouchesInView = false
            self.tapOutsideRecognizer.delegate = self
            self.view.window!.addGestureRecognizer(self.tapOutsideRecognizer)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if(self.tapOutsideRecognizer != nil) {
            self.view.window?.removeGestureRecognizer(self.tapOutsideRecognizer)
            self.tapOutsideRecognizer = nil
        }
    }
    
    func close(_ sender: AnyObject) {
//        self.dismissViewControllerAnimated(true, completion: nil)
        if(tableNeedsReload == true) {
            delegate?.reloadTable!()
        }
        
        if(showApprovalMode == true) {
            delegate?.setNeedsUpdate!()
            self.performSegue(withIdentifier: "backToApprovalImages", sender: self)
        } else {
            self.performSegue(withIdentifier: "backToImages", sender: self)
        }
    }
    
    //@objc
    @objc func handleTapBehind(_ sender: UITapGestureRecognizer) {
        if (sender.state == UIGestureRecognizerState.ended) {
//            let location: CGPoint = sender.locationInView(nil)
//            
//            if (!self.view.pointInside(self.view.convertPoint(location, fromView: self.view.window), withEvent: nil)) {
//                self.view.window?.removeGestureRecognizer(sender)
//                self.close(sender)
//            }
            let location: CGPoint = sender.location(in: self.view)
            
            if(self.view.point(inside: location, with: nil)){
               //print("P--In view??")
            } else {
               //print("P--Not in view??")
                self.view.window?.removeGestureRecognizer(sender)
                self.close(sender)
                
            }
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    /* modal view controller dismiss on click outside of bounds - END */
    
    @IBAction func backToImagesBtn(_ sender: AnyObject) {
       //print("backToImages")
        self.close(sender)
    }
    
    @IBAction func skipBtn(_ sender: AnyObject) {
       //print("skipBtn")
        detailNextImage()
    }
    
    func showLoading() {
        DispatchQueue.main.async {
            self.loadingView.isHidden = false
        }
    }
    
    func hideLoading() {
        DispatchQueue.main.async {
            self.loadingView.isHidden = true
        }
    }
    
    //Actions section
    func setupActions(){
        if(showApprovalMode == false){
            DispatchQueue.main.async {
                self.skipBtn.title = "Next"
            }
        }
        
        DispatchQueue.main.async {
            self.actionsToolbar.isHidden = true
            self.rotateToolbar.isHidden = true
        }
        if let imageActions = itemData["actions"] as? NSArray {
            if(showApprovalMode == true){
                DispatchQueue.main.async {
                    self.actionsToolbar.isHidden = false
                    self.rotateToolbar.isHidden = false
                }
                for a in 0 ..< imageActions.count { //***LOOP CHECK SHOULD THIS BE 0 or 1
                    if let action = imageActions[a] as? NSDictionary {
                        if let key = action["name"] as? String {
                            let link = action["link"] as? String
                            if((key.lowercased().range(of: "approve")) != nil){
                                approveUrl = String(format:"%@%@", self.shareData.myUrl, link!)
                            }
                            if((key.lowercased().range(of: "reject")) != nil){
                                rejectUrl = String(format:"%@%@", self.shareData.myUrl, link!)
                            }
                        }
                    }
                }
//                for a in imageActions.objectEnumerator().allObjects as! [[[String:Any]]] {
//                    if let key = a["name"] as? String {
//                        let link = a["link"] as? String
//                        if((key.lowercased().range(of: "approve")) != nil){
//                            approveUrl = String(format:"%@%@", self.shareData.myUrl, link!)
//                        }
//                        if((key.lowercased().range(of: "reject")) != nil){
//                            rejectUrl = String(format:"%@%@", self.shareData.myUrl, link!)
//                        }
//                    }
//                }
            }
        }
    }
    
    @IBAction func rejectAction(_ sender: AnyObject) {
       //print("rejectActionBtn")
        checkRotation(rejectUrl)
    }
    
    @IBAction func approveAction(_ sender: AnyObject) {
       //print("approveActionBtn")
        checkRotation(approveUrl)
    }
    
    @IBAction func rotateCCW(_ sender: AnyObject) {
       //print("rotateCCW")
        rotateImage(false)
    }
    
    @IBAction func rotateCW(_ sender: AnyObject) {
       //print("rotateCW")
        rotateImage(true)
    }
    
    func rotateImage(_ clockwise: Bool) {
        let rotation = clockwise ? 90 : -90
        DispatchQueue.main.async {
            self.imageCell.image = self.imageCell.image?.imageRotatedByDegrees(CGFloat(rotation), flip: false)
        }
        imageRotate += rotation
        imageRotate += (imageRotate >= 360 ? -360 : (imageRotate < 0 ? 360 : 0))
    }
    
    func checkRotation(_ actionUrlString: String) {
        if(imageRotate != 0){
            self.doRotateAction(actionUrlString)
        } else {
            self.doApprovalAction(actionUrlString)
        }
    }
    
    func doRotateAction(_ actionUrlString: String) {
        tableNeedsReload = true
        showLoading()
        
        if let currentImageId = itemData["image_id"] as? Int {
            let imgRotateUrlString: String = String(format:"%@/eimages/RotateImageBy90DInterval?company_id=%@&imageId=%@&degrees=%d", shareData.myUrl, shareData.company_id, String(currentImageId), imageRotate)
            
            //NSLog(@"* * albumType=>%@",albumType);
           //print("* * *imgApprUrlString=>" + imgRotateUrlString)
            
            let URL = Foundation.URL(string: imgRotateUrlString)!
            let request = NSMutableURLRequest(url:URL)
            request.httpMethod = "POST"
            NSURLConnection.sendAsynchronousRequest(
                request as URLRequest,
                queue: OperationQueue.main,
                completionHandler: { response, data, err in
                    if err != nil {
                        //error?(e)
                       //print("error occured=>" + err.debugDescription)
                        self.hideLoading()
                    } else {
                        if(data != nil) {
                            self.doApprovalAction(actionUrlString)
                        } else {
                            //ERROR
                            self.hideLoading()
                        }
                       //print("success")
                    }
            })
        }
    }
    
    func doApprovalAction(_ actionUrlString: String){
        showLoading()
        delegate?.setNeedsUpdate!()
        let URL = Foundation.URL(string: actionUrlString)!
        let request = NSMutableURLRequest(url:URL)
        request.httpMethod = "PATCH"
        NSURLConnection.sendAsynchronousRequest(
            request as URLRequest,
            queue: OperationQueue.main,
            completionHandler: { response, data, err in
                if err != nil {
                    //error?(e)
                   //print("error occured=>" + err.debugDescription)
                    self.hideLoading()
                } else {
                    if(data != nil) {
                        self.detailNextImage()
                    } else {
                        //ERROR
                        self.hideLoading()
                    }
                   //print("success")
                }
        })
    }
    
    func detailNextImage(){
//        clearDetailView()
        showLoading()
        DispatchQueue.main.async {
            self.imageNameLabel.title = ""
            self.imageByLabel.text = ""
            self.imageLocationLabel.text = ""
            self.imageAlbumLabel.text = ""
            self.imageCell.isHidden = true
            self.imageCell.image = UIImage(named:"blank1.png")
            self.imageCell.image = nil
        }
        let nextItemIndex: Int = (currentItemIndex + 1) < itemCount ? (currentItemIndex + 1) : 0
        delegate?.getImageDataForDetail(nextItemIndex)
    }
    
    //detail section
    func setupDetailView(){
        let imageFrame = self.imageCell.frame
        let imageWidth = imageFrame.size.width
        let imageHeight = imageFrame.size.height
        if let image_url = itemData["image_url"] as? String {
            let imageURLString = String(format:"%@%@?width=%@&height=%@", self.shareData.myUrl, image_url, String(format: "%g", imageWidth), String(format: "%g", imageWidth))
            NSURLConnection.sendAsynchronousRequest(
                URLRequest(url: URL (string: imageURLString)!),
                queue: OperationQueue.main,
                completionHandler: { response, data, err in
                    if err != nil {
                        //error?(e)
                       //print("error occured=>" + err.debugDescription)
                        self.hideLoading()
                    } else {
                        if let img: UIImage = UIImage(data: data!) {
                            self.itemImgage = img
                            DispatchQueue.main.async {
                                self.imageCell.image = img
                                self.imageCell.isHidden = false
                            }
                        }
                    }
            })
        }
        configureForItem(itemData, cellImage: itemImgage)
    }
    
    func clearDetailView() {
        DispatchQueue.main.async {
            self.imageNameLabel.title = ""
            self.imageByLabel.text = ""
            self.imageLocationLabel.text = ""
            self.imageAlbumLabel.text = ""
            self.imageCell.image = nil
            self.imageCell.image = UIImage()
        }
    }
    
    func configureForItem(_ itemData: Dictionary<String, AnyObject>, cellImage: UIImage) {
        var name: String = ""
        var uploaded_by: String = ""
        var location_name: String = ""
        var album_name: String = ""
        
        DispatchQueue.main.async {
            self.imageCell.image = cellImage //UIImage(named: "IconGlobe.png")
            self.imageCell.layer.borderWidth = 1.0
            self.imageCell.layer.masksToBounds = true
            self.imageCell.layer.borderColor = UIColor(red: 191/255, green: 191/255, blue: 191/255, alpha: 1).cgColor
        }
        if let nameTemp = itemData["name"] as? NSString {
            name = nameTemp as String
            //10 characters ~ 100 width
        } else {
            name = "Back to images"
        }
        let nameStr: NSString = name as NSString
        let maxChar: Int = Int(floor((self.view.frame.size.width - 100) / 10) - 1)
        let nameCount: Int = nameStr.length
        if(nameCount > (maxChar)){
            let text = nameStr.substring(to: maxChar)
            name = text + "..."
        }
        DispatchQueue.main.async {
            self.imageNameLabel.title = name
        }
        if let uploaded_byTemp = itemData["uploaded_by"] as? String {
            var upload_date: String = ""
            if let upload_dateTemp = itemData["upload_date"] as? String {
                //let strTime = "2015-07-27 19:29:50 +0000"
                let formatter = DateFormatter()
                let localeStr: String = (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode)! as! String
                let pre = Locale.preferredLanguages[0]

                formatter.locale = Locale(identifier: "us")
                if((upload_dateTemp.range(of: ".")) == nil){
                    formatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'" //"yyyy-MM-dd HH:mm:ss Z"
                } else {
                    formatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'"
                }
                let date = formatter.date(from: upload_dateTemp)
                formatter.dateFormat = "M'/'d'/'yyyy"
                let dateStr = formatter.string(from: date!)
                upload_date = ", " + dateStr
            }
            uploaded_by = "By: " + uploaded_byTemp + upload_date
        }
        DispatchQueue.main.async {
            self.imageByLabel.text = uploaded_by
        }
        if let location_nameTemp = itemData["location_name"] as? String {
            location_name = "For: " + location_nameTemp
        }
        DispatchQueue.main.async {
            self.imageLocationLabel.text = location_name
        }
        if let albumTemp = itemData["album_name"] as? String {
            album_name = "Album: " + albumTemp
        }
        DispatchQueue.main.async {
            self.imageAlbumLabel.text = album_name
        }
        self.hideLoading()
    }
}
