//
//  GalleryViewAlbumTableCellTableViewCell.swift
//  Expion
//
//  Created by Mike Bagwell on 5/4/16.
//  Copyright © 2016 Expion. All rights reserved.
//

import UIKit

class GalleryAlbumTableViewCell: UITableViewCell {

//    @IBOutlet weak var cellImage: UIImageView!
//    @IBOutlet strong var textLabel: UILabel?
//    @IBOutlet weak var detailTextLabel: UILabel?
    
    @IBOutlet var albumImage: UIImageView!
    @IBOutlet var albumNameLabel: UILabel!
    @IBOutlet var albumDateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
