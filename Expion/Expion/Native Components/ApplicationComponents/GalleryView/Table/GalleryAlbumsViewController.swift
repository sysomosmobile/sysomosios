//
//  GalleryAlbumsViewController.swift
//  Expion
//
//  Created by Mike Bagwell on 5/16/16.
//  Copyright © 2016 Expion. All rights reserved.
//

import Foundation

@objc
protocol GalleryAlbumsViewControllerDelegate {
    func showLoading()
    func hideLoading()
    func addGalleryAlbumsImagesViewController(_ albumObj: Dictionary<String, AnyObject>, albumType: String)
}

class GalleryAlbumsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, JSONLocationDelegate, FlatTreeViewControllerDelegate, PickerViewControllerDelegate, GalleryCreateNewViewControllerDelegate, UIPopoverPresentationControllerDelegate {
    
    //TODO:  Find a way to get rid of this
    var busyLoading: Bool = false
    
    var delegate: GalleryAlbumsViewControllerDelegate?
    var displayMode: String!
    
    //image connection and api return data
    var rowHeight: CGFloat = 79
    var itemPerPage: Int = 9
    var itemPage: Int = 0
    var data: NSMutableData = NSMutableData()
    
    //For image parser and image data
    var itemTotal: Int = 0
    var itemCount: Int = 0
    
    //    var imageData: Dictionary<String, AnyObject>!
    var itemDataArray: Array<Dictionary<String, AnyObject>>!
    var itemSmallImageDataArray: NSMutableArray!
    
    var currentItemData: Dictionary<String, AnyObject>!
    var currentItemImage: UIImage!
//    var currentDetailIndex: Int = 0
    
//    var shouldUpdateDetail: Bool = false
    
    //Uploader stuff
    var canFBAlbumImagePublish: Bool = false
    var canExpionImage: Bool = false
    
    
    //Album Type View stuff
    @IBOutlet var albumTypeView: UIView!
    @IBOutlet var albumTypeDone: UIBarButtonItem!
    @IBOutlet var albumTypePicker: UIPickerView!
    
    var albumType: String!
    var albumTypeDesc: String!
//    var albumTypeOptions: NSMutableArray!
//    var albumTypeValues: NSMutableArray!
    var albumPickerOptionsArray: Array<Dictionary<String, AnyObject>>!
    
    //Main View stuff
    @IBOutlet var albumToolbar: UIView!
    @IBOutlet var albumLocBtn: UIButton!
    @IBAction func locationButton() {
        showFlatLocTree()
    }
    @IBOutlet var albumTypeBtn: UIButton!
    
    var locationArray: Array<Dictionary<String, AnyObject>>!
    
    //tableview
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noneAvailView: UIView!
    //var tableViewController = UITableViewController()
    var refreshControl = UIRefreshControl()
    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet var createNewAlbumBtnView: UIView!
    @IBOutlet var createNewAlbumBtn: UIButton!
    @IBAction func createNewAlbumAction() {
        showCreateNewAlbum()
    }
    
    @IBAction func createToAlbums(_ segue: UIStoryboardSegue) {
        //print("createToAlbums")
        itemPage = 0
        getAlbumsItemData(1, limit: itemPerPage)
    }
    
    func creationViewCompleted() {
        setupAlbums()
    }
    
    @IBAction func albumTypeSelect() {
        showAlbumTypePicker()
    }
    
    let shareData = ShareData.sharedInstance
    var cookieStr: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //showLogin()
        //initCenterView()
        //print("GalleryAlbumsViewController=" + displayMode)
        //print("GalleryAlbumsViewController=" + shareData.myUrl)
        //print("companyId=" + shareData.company_id)
        
        tableView.register(UINib(nibName: "GalleryTableViewCell", bundle: nil), forCellReuseIdentifier: "GalleryTableViewCell")
        
        //let displayModeInit = displayMode + "Init:"
        let displayModeInit: Selector = NSSelectorFromString(displayMode + "ModeInit")
        itemPerPage = Int(self.view.frame.height / rowHeight)
        DispatchQueue.main.async {
            self.noneAvailView.isHidden = true
        }
        if self.responds(to: displayModeInit) {
            self.perform(displayModeInit)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        // Return no adaptive presentation style, use default presentation behaviour
        return .none
    }
    
    func showLoading() {
        delegate?.showLoading()
    }
    
    func hideLoading() {
        delegate?.hideLoading()
    }
    
    func showMiniLoading() {
        delegate?.showLoading()
    }
    
    func hideMiniLoading() {
        delegate?.hideLoading()
    }
    
    @objc func uploaderModeInit() {
        albumPickerOptionsArray = []//Array<Dictionary<String, AnyObject>>
        
        let prefs = UserDefaults.standard
        if let canFBImage = prefs.string(forKey: "canFBAlbumImagePublish"){
            canFBAlbumImagePublish = toBool(canFBImage)
        }
        if let canEImage = prefs.string(forKey: "canExpionImage"){
            canExpionImage = toBool(canEImage)
        }
        
        canExpionImage = true
        canFBAlbumImagePublish = true
        
        if(canExpionImage == true){
            let dict: Dictionary<String, AnyObject> = [
                "name": "Sysomos Albums" as AnyObject,
                "value": "e" as AnyObject,
                "selected": (albumPickerOptionsArray.count > 0 ? false : true) as AnyObject
            ]
            albumPickerOptionsArray.append(dict)
        }
        if(canFBAlbumImagePublish == true){
            let dict: Dictionary<String, AnyObject> = [
                "name": "Facebook Albums" as AnyObject,
                "value": "fb" as AnyObject,
                "selected": (albumPickerOptionsArray.count > 0 ? false : true) as AnyObject
            ]
            albumPickerOptionsArray.append(dict)
        }
        
        setupGallery()
    }
    
    func toBool(_ boolStr: String) -> Bool {
        switch boolStr {
        case "True", "true", "yes", "1":
            return true
        case "False", "false", "no", "0":
            return false
        default:
            return false
        }
    }
    
    func showAlbumTypePicker() {
        let storyboard = UIStoryboard(name: "GalleryView", bundle: Bundle.main)
        let pickerVC: PickerViewController = storyboard.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        
        pickerVC.delegate = self
        pickerVC.pickerOptionsArray = self.albumPickerOptionsArray
        pickerVC.modalPresentationStyle =   UIModalPresentationStyle.popover
        pickerVC.preferredContentSize = CGSize(width: self.view.frame.size.width, height: 260)
        let sourceRectFrame = self.albumTypeBtn.frame
        let popoverMenuViewController = pickerVC.popoverPresentationController
        popoverMenuViewController!.permittedArrowDirections = .any
        popoverMenuViewController!.delegate = self
        popoverMenuViewController!.sourceView = self.view
        popoverMenuViewController!.sourceRect = CGRect(x: sourceRectFrame.origin.x, y: sourceRectFrame.origin.y, width: sourceRectFrame.size.width, height: sourceRectFrame.size.height)
        present(pickerVC, animated: true, completion: nil)
    }
    
    //PickerViewDelegate
    func pickerValueSelected(_ pickerReturnArray: Array<Dictionary<String, AnyObject>>, pickerReturnObj: Dictionary<String, AnyObject>) {
        if(self.albumPickerOptionsArray != nil){
            self.albumPickerOptionsArray.removeAll()
        }
        self.albumPickerOptionsArray = pickerReturnArray
        
        if let pickerReturnTitle = pickerReturnObj["name"] as? String {
            DispatchQueue.main.async {
                self.albumTypeBtn.setTitle(String(format:"%@  \u{25be}", pickerReturnTitle), for: UIControlState())
            }
        }
        
        if let pickerReturnValue = pickerReturnObj["value"] as? String {
            self.albumType = pickerReturnValue
        }
        
        setupAlbums()
    }
    
    func setupGallery() {
        if(albumPickerOptionsArray.count > 0){
            albumType = albumPickerOptionsArray[0]["value"] as! String
            albumTypeDesc = albumPickerOptionsArray[0]["name"] as! String
            DispatchQueue.main.async {
                self.albumTypeBtn.setTitle(String(format:"%@  \u{25be}", self.albumTypeDesc), for: UIControlState())
            }
        }
        getLocJson()
    }
    
    func getLocJson()
    {
        let jsonLoc: JSONLocation = JSONLocation()
        jsonLoc.company_id = shareData.company_id
        jsonLoc.myUrl = shareData.myUrl
        jsonLoc.delegate = self
        
        jsonLoc.getLocJSON()
    }
    
    func locJSONReturned(_ jsonLocReturn: JSONLocation) {
        //TODO: use jsonLocReturn to set loc data
        if(self.locationArray != nil){
            self.locationArray.removeAll()
        }
        locationArray = jsonLocReturn.locationList
        setupAlbums()
    }
    
    func showFlatLocTree() {
        //print("LL--location Button displayMode=>%@", displayMode)
//        //print("LL--locIdArray Count=>%lu", (unsigned long)[locationIdArray count])
//        //print("LL--locNameArray Count=>%lu", (unsigned long)[locationNameArray count])
//        //print("LL--selectedLocs Count=>%lu", (unsigned long)[selectedItems count])
//        //print("LL--selectedLocList Count=>%lu", (unsigned long)[selectedLocList count])
//        //print("LL--locList Count=>%lu", (unsigned long)[locList count])
//        //print("LL--locationArray Count=>%lu", (unsigned long)[locationArray count])
        
        var storyboard = UIStoryboard() //(name: "Main", bundle: nil)
        if #available(iOS 11, *) {
            storyboard = UIStoryboard(name: "Main", bundle: nil)
        } else {
            storyboard = UIStoryboard(name: "MainiOS10", bundle: nil)
        }
        
//        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let locSelect: FlatTreeViewController = storyboard.instantiateViewController(withIdentifier: "FlatTreeViewController") as! FlatTreeViewController

        locSelect.treeItemArray = self.locationArray
        locSelect.canSelectMultiples = true
        
        locSelect.delegate = self

        if(AppSettings.isPad() == true) {
            locSelect.modalPresentationStyle = UIModalPresentationStyle.formSheet
            locSelect.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        }
        DispatchQueue.main.async {
            self.present(locSelect, animated: true, completion: nil)
            self.albumLocBtn.setTitle("  ...", for: UIControlState())
        }
    }
    
    func setupAlbums() {
        let titleTxt = LocationUtils().getTitleFromLocList(locationArray)
        DispatchQueue.main.async {
            self.albumLocBtn.setTitle("  " + titleTxt, for: UIControlState())
        }
        itemPage = 0
        if(self.locationArray != nil) {
            getAlbumsItemData(1, limit: itemPerPage)
        }
    }
    
    func locViewControllerDismissed(_ returnedLocs: Array<Dictionary<String, AnyObject>>, withTitle titleTxt: String){
        //TODO fill this out
        self.locationArray.removeAll()
        self.locationArray = returnedLocs
        DispatchQueue.main.async {
            self.albumLocBtn.setTitle("  " + titleTxt, for: UIControlState())
        }
        itemPage = 0
        getAlbumsItemData(1, limit: itemPerPage)
    }
    
    func showCreateNewAlbum() {
        if(AppSettings.isPad() == true) {
            self.performSegue(withIdentifier: "CreateNewViewSegueModal", sender: self)
        } else {
            self.performSegue(withIdentifier: "CreateNewViewSegue", sender: self)
        }
    }
    
    //Get Item Data section
    func getAlbumsItemData(_ page: Int, limit: Int) {
        let locs: Array<Int> = LocationUtils().getSelectedFromLocList(self.locationArray)
        
        if(locs.count > 0) {
            DispatchQueue.main.async {
                self.noneAvailView.isHidden = true
            }
            if(itemPage == 0) {
                clearItems()
            }
            itemPage = page
            //print("<< getItemData <<")
            //println("getMLJSON")
            showMiniLoading()
            let albumsUrlPath: String = String(format:"%@/api/eimages/albums/%@?excludeEmpty=False", shareData.myUrl, shareData.company_id)
            startAlbumDataConnection(albumsUrlPath, page: page, limit: limit)
        }
    }
    
    func startAlbumDataConnection(_ urlPath: String, page: Int, limit: Int){
        //print(urlPath)
        if let url = URL(string: urlPath) {
//            let urlRequest: NSMutableURLRequest = NSMutableURLRequest(url: url)
//            let session = URLSession.shared
            
            let locs: Array<Int> = LocationUtils().getSelectedFromLocList(self.locationArray)
            if(locs.count > 0) {
                
                let jsonDict: Dictionary<String, AnyObject> = ["locs": locs as AnyObject,
                                                               "page": page as AnyObject,
                                                               "limit": limit as AnyObject,
                                                               "type": albumType as AnyObject]
                
                let urlRequest: NSMutableURLRequest = NSMutableURLRequest(url: url)
                let session = URLSession.shared
                
                urlRequest.setValue(ShareData.sharedInstance.userAgent, forHTTPHeaderField: "User-Agent")
                urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
                urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                urlRequest.httpMethod = "PUT"
            
                var bodyData: NSData!
                
                do {
                    if let jsonData = try JSONSerialization.data(withJSONObject: jsonDict, options: JSONSerialization.WritingOptions.prettyPrinted) as? NSData {
                        bodyData = jsonData
                        if (try JSONSerialization.jsonObject(with: jsonData as Data, options: []) as? NSDictionary) != nil {
                            //print("get albums data=>")
                            //print(jsonData)
                        } else {
                            // here "jsonData" is the dictionary encoded in JSON data
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
                urlRequest.httpBody = bodyData as Data?
                
                let task = session.dataTask(with: urlRequest as URLRequest) {
                    (data, response, error) -> Void in
                    let httpResponse = response as! HTTPURLResponse
                    let statusCode = httpResponse.statusCode
                    
//                    //print(httpResponse)
                    if (statusCode == 200) {
                        //print("--success-->")
                        
                        //print("try parse=>")
                        
                        
                        if(data != nil) {
                            self.data = NSMutableData()
                            self.data.append(data!)
//                            let dataStr = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
//                            //print(String(format: "!!~~dataStr=>%@", dataStr!))
                            
                            self.parseJSON(self.data) {
                                (parsedJSONData: Array<Dictionary<String, AnyObject>>) in
                                self.parseCompleted(parsedJSONData)
                                //println("*X*X*parseMLJSON completed*X*X*")
                                //println("got back: (result)")
                            }
                        }
                    } else {
                        //print("xx--fail--xx")
                    }
                    
                }
                task.resume()
            }
        }
    }
    
    
    func startItemDataConnection(_ urlPath: String, page: Int, limit: Int){
        if(shareData.myUrl != nil && shareData.company_id != nil){
            let url: URL = URL(string: urlPath)!
            //print(String(format: "getAlbumsItemData urlPath=>%@", url as CVarArg))
            
            let locs: Array<Int> = LocationUtils().getSelectedFromLocList(self.locationArray)
            
            if(locs.count > 0) {
            
            let jsonDict: Dictionary<String, AnyObject> = ["locs": locs as AnyObject,
                                                           "page": page as AnyObject,
                                                           "limit": limit as AnyObject,
                                                           "type": albumType as AnyObject]
            
            let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
            request.setValue(ShareData.sharedInstance.userAgent, forHTTPHeaderField: "User-Agent")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "PUT"
//            request.HTTPMethod = "POST"
            
            var jsonError: NSError
            request.httpBody = try! JSONSerialization.data(withJSONObject: jsonDict, options: [])
            
            let connection: NSURLConnection = NSURLConnection(request: request as URLRequest, delegate: self, startImmediately: false)!
            
            connection.start()
            }
        }
    }
    
    func connection(_ didReceiveResponse: NSURLConnection!, didReceiveResponse response: URLResponse!) {
        // Received a new request, clear out the data object
        if let httpResponse = response as? HTTPURLResponse {
            //print(httpResponse.statusCode)
            if(httpResponse.statusCode == 200){
                self.data = NSMutableData()
            } else {
                //print("didReceiveResponse != 200")
                //                returnToLogin()
            }
        } else {
            assertionFailure("unexpected response")
        }
        
        
    }
    
    func connection(_ connection: NSURLConnection!, didReceiveData data: Data!){
        self.data.append(data)
        let dataStr = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        //print(String(format: "!!~~dataStr=>%@", dataStr!))
    }
    
    func connectionDidFinishLoading(_ connection: NSURLConnection!) {
        var err: NSError
        // throwing an error on the line below (can't figure out where the error message is)
        //mlData.removeAll()
        //parseMLJSON(data)
        
        let parseIt: Bool = true
        
        if(parseIt == true){
            parseJSON(data) {
                (parsedJSONData: Array<Dictionary<String, AnyObject>>) in
                self.parseCompleted(parsedJSONData)
                //println("*X*X*parseMLJSON completed*X*X*")
                //println("got back: (result)")
            }
        }
        
    }
    
    func parseJSON(_ responseData: NSMutableData, completion: (_ parsedJSONData: Array<Dictionary<String, AnyObject>>) -> Void){
        
        //println("-=-=-parseMLJSON====")
        var parsedJSONData = Array<Dictionary<String, AnyObject>>()
        
        var jsonError: NSError?
        //var last_selected_match: Bool = false
        var shouldLoadId: String!
        var loadMLItemIndex: Int!
        
        var returnJSON: NSDictionary!
        
        do {
            returnJSON = try JSONSerialization.jsonObject(with: responseData as Data, options: []) as? NSDictionary
        } catch {
            
        }
        
        if returnJSON != nil {
            //print("returnJSON")
            //print(returnJSON)
            if let total_resultsTemp = returnJSON["total_results"] as? Int {
                self.itemTotal = total_resultsTemp
            }
            if let countTemp = returnJSON["count"] as? Int {
                self.itemCount = countTemp
            }
            if let dataTemp = returnJSON["data"] as? NSArray {
                //                if let dataDictTemp = dataTemp[0] as? NSDictionary {
                //                    parsedJSONData = dataDictTemp.mutableCopy() as! Dictionary<String, AnyObject>
                //                }
                parsedJSONData = dataTemp as! Array<Dictionary<String, AnyObject>>
            } else {
                DispatchQueue.main.async {
                    self.noneAvailView.isHidden = false
                }
            }
        } else {
            //print("!!--returnJSON == nil")
        }
        completion(parsedJSONData)
    }
    
    func parseCompleted(_ parsedJSONData: Array<Dictionary<String, AnyObject>>) {
        //dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)) {
        
        if(parsedJSONData.count > 0) {
            for i in 0 ..< parsedJSONData.count { //***LOOP CHECK SHOULD THIS BE 0 or 1
    //            self.itemSmallImageDataArray.addObject(UIImage())
    //            let imageIndex = i + ((itemPage - 1) * (itemPerPage))
                if let dataDict = parsedJSONData[i] as? Dictionary<String, AnyObject> {
                    itemDataArray.append(dataDict)
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.tableView.reloadData()
                    })
                    //hardcoded image
    //                let img: UIImage = UIImage(named: "Full_Folder.png")!
    //                self.itemSmallImageDataArray.replaceObjectAtIndex(imageIndex, withObject: img)
    //                self.tableView.reloadData()
                }
            }
        }
        busyLoading = false
        hideMiniLoading()
    }
    
    
    func clearItems(){
        if(itemSmallImageDataArray != nil){
            itemSmallImageDataArray.removeAllObjects()
            itemSmallImageDataArray = nil
        }
//        itemSmallImageDataArray = NSMutableArray()
//        if(itemDataArray != nil){
//            itemDataArray.removeAll()
//            itemDataArray = nil
//        }
        itemDataArray = Array<Dictionary<String, AnyObject>>()
        itemCount = 0
        itemPage = 0
        itemTotal = 0
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
//        currentDetailIndex = 0
    }
    
    
    
    // MARK: Table View Data Source
    //override
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //override
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemDataArray != nil ? itemDataArray.count : 0
    }
    
    //override
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 79
    }
    
    //override
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var item: Dictionary<String, AnyObject>!
        if(itemDataArray.count > (indexPath as NSIndexPath).row){
            item = itemDataArray[(indexPath as NSIndexPath).row]
//            //print("TV--imageItem has value")
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "GalleryTableViewCell", for: indexPath) as! GalleryTableViewCell
            //let selectedCell: Bool = (last_selected == mlItem.id) ? true : false
//            var itemImage: UIImage = UIImage()
//            if(self.itemSmallImageDataArray.count > indexPath.row){
//                itemImage = self.itemSmallImageDataArray[indexPath.row] as! UIImage
//            }
            let itemImage: UIImage = UIImage(named: "Full_Folder.png")!
            cell.configureForAlbumItem(item, cellImage: itemImage)//collapsedInd[indexPath.section])
            
            return cell
        } else {
            //print("TV--!!--mlItem NULL--!!")
            return UITableViewCell()
        }
        
    }
    
    //    // Mark: Table View Delegate
    //    //override
    //    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
    //        let cellToDeSelect = tableView.cellForRowAtIndexPath(indexPath)! as! MLCell
    //        cellToDeSelect.imageNameLabel.textColor = colorWithHexString("#333333")//("#333333")
    //        cellToDeSelect.imageCreatorLabel.textColor = colorWithHexString("#333333")//("#333333")
    //    }
    
    //override
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let itemInfo = getCurrentItem(indexPath.row)
//        currentItemData = itemInfo.0
        //        currentItemImage = itemInfo.1
//        currentDetailIndex = indexPath.row
        //performSegueWithIdentifier("GalleryViewApproveDetailSegue", sender: self)
        delegate?.addGalleryAlbumsImagesViewController(itemDataArray[(indexPath as NSIndexPath).row], albumType: albumType)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //print("didScroll")
        if(busyLoading != true){
            //NSLog(@"scrollView != nil");
            let bottomEdge: CGFloat = scrollView.contentOffset.y + scrollView.frame.size.height
            if(scrollView.contentOffset.y < 0){
                //NSLog(@"top!");
                return
                //} else if(scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.bounds.size.height)){
            } else if (bottomEdge >= scrollView.contentSize.height && busyLoading != true) {
                //NSLog(@"bottom! albumCount");
                if(itemTotal > (itemPage * itemPerPage) && busyLoading == false && itemPage > 0){
                    //NSLog(@"bottom! getAlbumImages => %d -- %d * %d", imageCount, imagePage, imagePerPage);
                    busyLoading = true
                    showMiniLoading()
                    getAlbumsItemData(itemPage+1, limit: itemPerPage)
                }
            } else {
                //NSLog(@"else");
                return
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "CreateNewViewSegue" || segue.identifier == "CreateNewViewSegueModal") {
            
            //print("CreateNewViewSegue")
            let createView = (segue.destination as! GalleryCreateNewViewController)
            createView.delegate = self
            createView.albumType = self.albumType
            createView.locationArray = self.locationArray
//            createView.currentItemIndex = currentDetailIndex
        }
    }
}
