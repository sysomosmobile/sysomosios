//
//  GalleryApproveTableViewCell.swift
//  Expion
//
//  Created by Mike Bagwell on 5/10/16.
//  Copyright © 2016 Expion. All rights reserved.
//

import UIKit

class GalleryTableViewCell: UITableViewCell {

    @IBOutlet var imageCell: UIImageView!
    @IBOutlet var statusImageCell: UIImageView!
    @IBOutlet var imageNameLabel: UILabel!
    @IBOutlet var imageByLabel: UILabel!
    @IBOutlet var imageLocationLabel: UILabel!
    @IBOutlet var imageAlbumLabel: UILabel!
//    @IBOutlet var imageStatusLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureForImageItem(_ itemData: Dictionary<String, AnyObject>, cellImage: UIImage) {
        var name: String = ""
        var uploaded_by: String = ""
        var location_name: String = ""
        var album_name: String = ""
        let status: String = ""
        
        if let nameTemp = itemData["name"] as? String {
            name = nameTemp
        }
        if let uploaded_byTemp = itemData["uploaded_by"] as? String {
            var upload_date: String = ""
            if let upload_dateTemp = itemData["upload_date"] as? String {
                //let strTime = "2015-07-27 19:29:50 +0000"
                let formatter = DateFormatter()
                let localeStr: String = (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode)! as! String
                let pre = Locale.preferredLanguages[0]
                
                formatter.locale = Locale(identifier: "us")
                if((upload_dateTemp.range(of: ".")) == nil){
                    formatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'" //"yyyy-MM-dd HH:mm:ss Z"
                } else {
                    formatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'"
                }
                let date = formatter.date(from: upload_dateTemp)
                formatter.dateFormat = "MM'/'dd'/'yy"
                let dateStr = formatter.string(from: date!)
                upload_date = ", " + dateStr
            }
            uploaded_by = "By: " + uploaded_byTemp + upload_date
        }

        
        if let location_nameTemp = itemData["location_name"] as? String {
            location_name = "For: " + location_nameTemp
        }
        
        if let albumTemp = itemData["album_name"] as? String {
            album_name = "Album: " + albumTemp
        }
        
        var statusImg: UIImage!
        var statusImgName: String = "tag-color"
        if let statusTemp = itemData["status"] as? String {
            //status = "Status:" + statusTemp
            statusImgName = statusTemp.lowercased()
        }
        statusImg = UIImage(named:statusImgName)
        
        DispatchQueue.main.async {
            self.imageCell.image = cellImage //UIImage(named: "IconGlobe.png")
            self.statusImageCell.image = statusImg
            self.imageCell.layer.borderWidth = 1.0
            self.imageCell.layer.masksToBounds = true
            self.imageCell.layer.borderColor = UIColor(red: 191/255, green: 191/255, blue: 191/255, alpha: 1).cgColor
        
            self.imageNameLabel.text = name
            self.imageByLabel.text = uploaded_by
            self.imageLocationLabel.text = location_name
            self.imageAlbumLabel.text = status//album_name
        }
    }
    
    func configureForAlbumItem(_ itemData: Dictionary<String, AnyObject>, cellImage: UIImage) {
        var name: String = ""
        var uploaded_by: String = ""
        var location_name: String = ""
        var album_name: String = ""
        
        if let nameTemp = itemData["title"] as? String {
            name = nameTemp
        }
        
        if let uploaded_byTemp = itemData["created_by"] as? String {
            uploaded_by = "Created By: " + uploaded_byTemp + ", "
        } else {
            uploaded_by = "Created: "
        }
        var upload_date: String = ""
        if let upload_dateTemp = itemData["create_date"] as? String {
            //let strTime = "2015-07-27 19:29:50 +0000"
            let formatter = DateFormatter()
//            let localeStr: String = (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode)! as! String
//            let pre = Locale.preferredLanguages[0]
            
            formatter.locale = Locale(identifier: "us")
            if((upload_dateTemp.range(of: ".")) == nil){
                formatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'" //"yyyy-MM-dd HH:mm:ss Z"
            } else {
                formatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'"
            }
            let date = formatter.date(from: upload_dateTemp)
            formatter.dateFormat = "MM'/'dd'/'yy"
            let dateStr = formatter.string(from: date!)
            upload_date =  dateStr
        }
        uploaded_by += upload_date
        
        
        
        if let locObjTemp = itemData["Location"] as? Dictionary<String, AnyObject> {
            if let location_nameTemp = locObjTemp["name"] as? String {
                location_name = "For: " + location_nameTemp
            }
        }
        
        if let albumTemp = itemData["album_name"] as? String {
            album_name = "Album: " + albumTemp
        }
        DispatchQueue.main.async {
            self.imageCell.image = cellImage //UIImage(named: "IconGlobe.png")
            //        self.imageCell.layer.borderWidth = 1.0
            //        self.imageCell.layer.masksToBounds = true
            //        self.imageCell.layer.borderColor = UIColor(red: 191/255, green: 191/255, blue: 191/255, alpha: 1).CGColor
            
            self.imageNameLabel.text = name
            self.imageByLabel.text = uploaded_by
            self.imageLocationLabel.text = location_name
            self.imageAlbumLabel.text = album_name
        }
    }
}
