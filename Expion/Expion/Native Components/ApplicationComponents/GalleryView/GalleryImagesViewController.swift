//
//  GalleryImagesViewController.swift
//  Expion
//
//  Created by Mike Bagwell on 5/18/16.
//  Copyright © 2016 Expion. All rights reserved.
//

import Foundation
import MobileCoreServices

@objc
protocol GalleryImagesViewControllerDelegate {
    func showLoading()
    func hideLoading()
    func backToGalleryAlbums()
}

class GalleryImagesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIPopoverPresentationControllerDelegate, GalleryCreateNewViewControllerDelegate, GalleryDetailViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    //TODO:  Find a way to get rid of this
    var busyLoading: Bool = false
    
    var delegate: GalleryImagesViewControllerDelegate?
    var displayMode: String!

    //image connection and api return data
    let itemPerPage: Int = 9 //TODO:  calculate this value based off of screen size / row height
    var itemPage: Int = 0
    var data: NSMutableData = NSMutableData()
    
    //For image parser and image data
    var itemTotal: Int = 0
    var itemCount: Int = 0
    
    var itemDataArray: Array<Dictionary<String, AnyObject>>!
    var itemSmallImageDataArray: NSMutableArray!
    var newImage: UIImage!
    
    //Main View stuff
    @IBOutlet var imagesToolbar: UIView!
    @IBOutlet var imagesBackToAlbumBtn: UIButton!
    @IBAction func backToAlbums() {
        delegate?.backToGalleryAlbums()
    }
    
    //tableview
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noneAvailView: UIView!
    //var tableViewController = UITableViewController()
    var refreshControl = UIRefreshControl()
    @IBOutlet weak var shadowView: UIView!
    
    //Model Data stuff
    var albumObject: Dictionary<String, AnyObject>!
    var albumType: String!
    var excludeSubmitted: String = ""
    
    //segue to DetailView
    var galleryDetailViewController: GalleryDetailViewController!
    var currentItemData: Dictionary<String, AnyObject>!
    var currentItemImage: UIImage!
    var currentDetailIndex: Int = 0
    var shouldUpdateDetail: Bool = false
    
    @IBAction func backToImages(_ segue: UIStoryboardSegue) {
        galleryDetailViewController = nil
        currentDetailIndex = 0
        setupGallery()
    }
    
    func creationViewCompleted() {
        galleryDetailViewController = nil
        currentDetailIndex = 0
        setupGallery()
    }
    
    @IBAction func takePhotoButton() {
//        loadingView.hidden = NO;
        self.pickPhoto(UIImagePickerControllerSourceType.camera)
    //[self closeToolbox];
    }
    
    @IBAction func cameraRollButton() {
//        loadingView.hidden = NO;
        self.pickPhoto(UIImagePickerControllerSourceType.photoLibrary)
    }
    
    override var shouldAutorotate : Bool {
        return false
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.all
    }
    
    //UIImagePicker stuff
    func pickPhoto(_ sourceType: UIImagePickerControllerSourceType) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = sourceType
        imagePicker.modalTransitionStyle = .coverVertical
        
        
        
        if(sourceType == .camera) {
            imagePicker.shouldAutorotate
            imagePicker.cameraCaptureMode = .photo
            imagePicker.cameraFlashMode = .auto
            imagePicker.showsCameraControls = true
        } else {
            imagePicker.mediaTypes = [kUTTypeImage as String] //, kUTTypeMovie as String]
        }
        DispatchQueue.main.async {
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
           //print("imagePickerController success")
//            imageView.contentMode = .ScaleAspectFit
            self.newImage = fixOrientation(pickedImage)
//            showCreateNewImage()
        }
        
        dismiss(animated: true, completion: {
            self.showCreateNewImage()
        })
    }
    
    func fixOrientation(_ img:UIImage) -> UIImage {
        
        if (img.imageOrientation == UIImageOrientation.up) {
            return img;
        }
        
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale);
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
        
        let normalizedImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext();
        return normalizedImage;
        
    }
    
    func showCreateNewImage() {
        DispatchQueue.main.async {
            if(AppSettings.isPad() == true) {
                self.performSegue(withIdentifier: "CreateNewImageSegueModal", sender: self)
            } else {
                self.performSegue(withIdentifier: "CreateNewImageSegue", sender: self)
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    let shareData = ShareData.sharedInstance
    override func viewDidLoad() {
        super.viewDidLoad()
        //showLogin()
        //initCenterView()
       //print("GalleryImagesViewController=" + displayMode)
       //print("GalleryImagesViewController=" + shareData.myUrl)
       //print("companyId=" + shareData.company_id)
        
        tableView.register(UINib(nibName: "GalleryTableViewCell", bundle: nil), forCellReuseIdentifier: "GalleryTableViewCell")
        
        //let displayModeInit = displayMode + "Init:"
        let displayModeInit: Selector = NSSelectorFromString(displayMode + "ModeInit")
        DispatchQueue.main.async {
            self.noneAvailView.isHidden = true
        }
        if self.responds(to: displayModeInit) {
            self.perform(displayModeInit)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        // Return no adaptive presentation style, use default presentation behaviour
        return .none
    }
    
    func showLoading() {
        delegate?.showLoading()
    }
    
    func hideLoading() {
        delegate?.hideLoading()
    }
    
    func showMiniLoading() {
        delegate?.showLoading()
    }
    
    func hideMiniLoading() {
        delegate?.hideLoading()
    }
    
    @objc func uploaderModeInit() {
//        albumPickerOptionsArray = []//Array<Dictionary<String, AnyObject>>
//        
//        let prefs = NSUserDefaults.standardUserDefaults()
//        if let canFBImage = prefs.stringForKey("canFBAlbumImagePublish"){
//            canFBAlbumImagePublish = toBool(canFBImage)
//        }
//        if let canEImage = prefs.stringForKey("canExpionImage"){
//            canExpionImage = toBool(canEImage)
//        }
//        
//        if(canFBAlbumImagePublish == true){
//            let dict: Dictionary<String, AnyObject> = [
//                "name": "Facebook Albums",
//                "value": "fb",
//                "selected": albumPickerOptionsArray.count > 0 ? false : true
//            ]
//            albumPickerOptionsArray.append(dict)
//        }
//        if(canExpionImage == true){
//            let dict: Dictionary<String, AnyObject> = [
//                "name": "Expion Albums",
//                "value": "e",
//                "selected": albumPickerOptionsArray.count > 0 ? false : true
//            ]
//            albumPickerOptionsArray.append(dict)
//        }
        setupGallery()
    }
    
    func toBool(_ boolStr: String) -> Bool {
        switch boolStr {
        case "True", "true", "yes", "1":
            return true
        case "False", "false", "no", "0":
            return false
        default:
            return false
        }
    }
    
//    func showAlbumTypePicker() {
//        let storyboard = UIStoryboard(name: "GalleryView", bundle: NSBundle.mainBundle())
//        let pickerVC: PickerViewController = storyboard.instantiateViewControllerWithIdentifier("PickerViewController") as! PickerViewController
//        
//        //        pickerVC.delegate = self
//        //        pickerVC.pickerOptionsArray = self.albumPickerOptionsArray
//        
//        //        pickerVC.modalPresentationStyle = UIModalPresentationStyle.Popover
//        //        pickerVC.preferredContentSize = CGSizeMake(self.view.frame.size.width, 260)
//        
//        //        self.presentViewController(pickerView, animated: true, completion: nil)
//        
//        //        let popover = pickerVC.popoverPresentationController
//        //        if let _popover = popover {
//        //            _popover.sourceView = self.albumTypeBtn//self.textField
//        //            _popover.sourceRect = CGRectMake((self.albumTypeBtn.frame.origin.x + self.albumTypeBtn.frame.size.width/2), (self.albumTypeBtn.frame.origin.y + self.albumTypeBtn.frame.size.height), 0 ,0)
//        //            _popover.delegate = self // to force no adaptive
//        ////            self.dataChanged = dataChanged // save the closure
//        //            self.presentViewController(pickerVC, animated: true, completion: nil)
//        ////            presented = true
//        //        }
//        pickerVC.delegate = self
//        pickerVC.pickerOptionsArray = self.albumPickerOptionsArray
//        pickerVC.modalPresentationStyle =   UIModalPresentationStyle.Popover
//        pickerVC.preferredContentSize = CGSizeMake(self.view.frame.size.width, 260)
//        let sourceRectFrame = self.albumTypeBtn.frame
//        let popoverMenuViewController = pickerVC.popoverPresentationController
//        popoverMenuViewController!.permittedArrowDirections = .Any
//        popoverMenuViewController!.delegate = self
//        popoverMenuViewController!.sourceView = self.view
//        popoverMenuViewController!.sourceRect = CGRect(x: sourceRectFrame.origin.x, y: sourceRectFrame.origin.y, width: sourceRectFrame.size.width, height: sourceRectFrame.size.height)
//        presentViewController(pickerVC, animated: true, completion: nil)
//        
//        
//        //        self.albumLocBtn.setTitle("  ...", forState: UIControlState.Normal)
//    }
    
//    //PickerViewDelegate
//    func pickerValueSelected(pickerReturnArray: Array<Dictionary<String, AnyObject>>, pickerReturnObj: Dictionary<String, AnyObject>) {
//        if(self.albumPickerOptionsArray != nil){
//            self.albumPickerOptionsArray.removeAll()
//        }
//        self.albumPickerOptionsArray = pickerReturnArray
//        
//        if let pickerReturnTitle = pickerReturnObj["name"] as? String {
//            self.albumTypeBtn.setTitle(String(format:"%@  \u{25be}", pickerReturnTitle), forState: UIControlState.Normal)
//        }
//        
//        if let pickerReturnValue = pickerReturnObj["value"] as? String {
//            self.albumType = pickerReturnValue
//        }
//        
//        setupAlbums()
//    }
    
    func setupGallery() {
        if(albumObject != nil){
//            albumType = albumPickerOptionsArray[0]["value"] as! String
//            albumTypeDesc = albumPickerOptionsArray[0]["name"] as! String
            var albumTitle: String = ""
            if let albumTitleTemp = albumObject["title"] as? String {
                albumTitle = albumTitleTemp
            }
            if let locObjTemp = albumObject["Location"] as? Dictionary<String, AnyObject> {
                if let location_nameTemp = locObjTemp["name"] as? String {
                    albumTitle += (albumTitle.count > 0 ? " - " : "") + location_nameTemp
                }
            }
            DispatchQueue.main.async {
                self.imagesBackToAlbumBtn.setTitle(albumTitle, for: UIControlState())
            }
            itemPage = 0
            getAlbumsImagesItemData(1, limit: itemPerPage)
        }
    }

    //Get Item Data section
    func getAlbumsImagesItemData(_ page: Int, limit: Int) {
        noneAvailView.isHidden = true
        if(itemPage == 0) {
            clearItems()
        }
        itemPage = page
       //print("<< getAlbumsImagesItemData <<")
        //println("getMLJSON")
        showMiniLoading()
        var imgUrlString: String = ""
        if let albumId = albumObject["id"] as? Int {
            var credStr: String = ""
            if let cred = albumObject["cred"] as? Int {
                credStr = "&cred=" + String(cred)
            }
            imgUrlString = String(format:"%@/api/eimages/images/%@/%@?page=%@&limit=%@&type=%@&width=150&height=150%@%@&excludeSubmitted=False", shareData.myUrl, String(albumId), shareData.company_id, String(page), String(limit), self.albumType, credStr, self.excludeSubmitted)
            print(imgUrlString)
            startItemDataConnection(imgUrlString)
        }
    }
    
    func startItemDataConnection(_ urlPath: String){
        if(shareData.myUrl != nil && shareData.company_id != nil){
            let url: URL = URL(string: urlPath)!
           //print(String(format: "getAlbumsImagesItemData urlPath=>%@", url as CVarArg))
            
            let urlRequest: NSMutableURLRequest = NSMutableURLRequest(url: url)
            let session = URLSession.shared
            urlRequest.setValue(ShareData.sharedInstance.userAgent, forHTTPHeaderField: "User-Agent")
            urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.httpMethod = "GET"
            
//            let connection: NSURLConnection = NSURLConnection(request: request as URLRequest, delegate: self, startImmediately: false)!
//            connection.start()
            
            let task = session.dataTask(with: urlRequest as URLRequest) {
                (data, response, error) -> Void in
                let httpResponse = response as! HTTPURLResponse
                let statusCode = httpResponse.statusCode
                
                //                    //print(httpResponse)
                if (statusCode == 200) {
                    //print("--success-->")
                    
                    //print("try parse=>")
                    
                    
                    if(data != nil) {
                        self.data = NSMutableData()
                        self.data.append(data!)
                        
                        self.parseJSON(self.data) {
                            (parsedJSONData: Array<Dictionary<String, AnyObject>>) in
                            self.parseCompleted(parsedJSONData)
                        }
                    }
                } else {
                    //print("xx--fail--xx")
                }
                
            }
            task.resume()
        }
    }
    
//    func connection(_ didReceiveResponse: NSURLConnection!, didReceiveResponse response: URLResponse!) {
//        // Received a new request, clear out the data object
//        if let httpResponse = response as? HTTPURLResponse {
//            print(httpResponse.statusCode)
//            if(httpResponse.statusCode == 200){
//                self.data = NSMutableData()
//            } else {
//                print("didReceiveResponse != 200")
//                //                returnToLogin()
//            }
//        } else {
//            assertionFailure("unexpected response")
//        }
//
//
//    }
//
//    func connection(_ connection: NSURLConnection!, didReceiveData data: Data!){
//        self.data.append(data)
//        let dataStr = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
//        print(String(format: "!!~~dataStr=>%@", dataStr!))
//    }
//
//    func connectionDidFinishLoading(_ connection: NSURLConnection!) {
//        var err: NSError
//        // throwing an error on the line below (can't figure out where the error message is)
//        //mlData.removeAll()
//        //parseMLJSON(data)
//
//        let parseIt: Bool = true
//
//        if(parseIt == true){
//            parseJSON(data) {
//                (parsedJSONData: Array<Dictionary<String, AnyObject>>) in
//                self.parseCompleted(parsedJSONData)
//                //println("*X*X*parseMLJSON completed*X*X*")
//                //println("got back: (result)")
//            }
//        }
//
//    }
    
    func parseJSON(_ responseData: NSMutableData, completion: (_ parsedJSONData: Array<Dictionary<String, AnyObject>>) -> Void){
        
        var parsedJSONData = Array<Dictionary<String, AnyObject>>()
        
        var jsonError: NSError?
        //var last_selected_match: Bool = false
        var shouldLoadId: String!
        var loadMLItemIndex: Int!
        
        var returnJSON: NSDictionary!
        
        do {
            returnJSON = try JSONSerialization.jsonObject(with: responseData as Data, options: []) as? NSDictionary
        } catch {
            
        }
        //print(returnJSON)
        if returnJSON != nil {
//            let priority = DispatchQueue.GlobalQueuePriority.default
            
            if let total_resultsTemp = returnJSON["total_results"] as? Int {
                self.itemTotal = total_resultsTemp
            }
            if let countTemp = returnJSON["count"] as? Int {
                self.itemCount = countTemp
            }
            if let dataTemp = returnJSON["data"] as? NSArray {
                //                if let dataDictTemp = dataTemp[0] as? NSDictionary {
                //                    parsedJSONData = dataDictTemp.mutableCopy() as! Dictionary<String, AnyObject>
                //                }
                parsedJSONData = dataTemp as! Array<Dictionary<String, AnyObject>>
            } else {
                DispatchQueue.main.async {
                    self.noneAvailView.isHidden = false
                }
            }
        }
        completion(parsedJSONData)
    }
    
    func parseCompleted(_ parsedJSONData: Array<Dictionary<String, AnyObject>>) {
        //dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)) {
        if(parsedJSONData.count > 0){
            for i in 0 ..< parsedJSONData.count { //***LOOP CHECK SHOULD THIS BE 0 or 1
                self.itemSmallImageDataArray.add(UIImage())
                let imageIndex = i + ((itemPage - 1) * (itemPerPage))
                if let dataDict = parsedJSONData[i] as? Dictionary<String, AnyObject> {
                    itemDataArray.append(dataDict)
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.tableView.reloadData()
                    })
                    //this is image_url for images //cover_url for albums
                    if let image_url = dataDict["image_url"] as? String {
                        let imageURLString = String(format:"%@%@?width=75&height=75", self.shareData.myUrl, image_url)
                        NSURLConnection.sendAsynchronousRequest(
                            URLRequest(url: URL (string: imageURLString)!),
                            queue: OperationQueue.main,
                            completionHandler: { response, data, err in
                                if let e = err {
                                   //print("error for image " + String(imageIndex))
                                   //print("error imageURLString=>" + imageURLString)
                                } else {
                                    if(self.itemSmallImageDataArray.count > imageIndex){
                                        if let img: UIImage = UIImage(data: data!) {
                                            self.itemSmallImageDataArray.replaceObject(at: imageIndex, with: img)
                                            DispatchQueue.main.async {
                                                self.tableView.reloadData()
                                            }
                                        } else {
                                           //print("no return image for=>" + String(imageIndex))
                                           //print("no return image imageURLString=>" + imageURLString)
                                        }
                                    } else {
                                       //print("imageIndex out of bounds=>" + String(imageIndex))
                                       //print("imageIndex imageURLString=>" + imageURLString)
                                    }
                                }
                        })
                    } else {
                        if(self.itemSmallImageDataArray.count > imageIndex){
                            if let img: UIImage = UIImage(data: data as Data) {
                                self.itemSmallImageDataArray.replaceObject(at: imageIndex, with: img)
                                DispatchQueue.main.async {
                                    self.tableView.reloadData()
                                }
                            }
                        }
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                self.noneAvailView.isHidden = false
            }
        }
        
        busyLoading = false
        if(shouldUpdateDetail == true) {
            shouldUpdateDetail = false
            setImageDataForDetail(currentDetailIndex)
        }
        hideMiniLoading()
    }
    
    
    func clearItems(){
        if(itemSmallImageDataArray != nil){
            itemSmallImageDataArray.removeAllObjects()
            itemSmallImageDataArray = nil
        }
        itemSmallImageDataArray = NSMutableArray()
        if(itemDataArray != nil){
            itemDataArray.removeAll()
            itemDataArray = nil
        }
        itemDataArray = Array<Dictionary<String, AnyObject>>()
        itemCount = 0
        itemPage = 0
        itemTotal = 0
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        currentDetailIndex = 0
    }
    
    
    
    // MARK: Table View Data Source
    //override
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //override
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemDataArray != nil ? itemDataArray.count : 0
    }
    
    //override
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 79
    }
    
    //override
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var item: Dictionary<String, AnyObject>!
        if(itemDataArray.count > (indexPath as NSIndexPath).row){
            item = itemDataArray[(indexPath as NSIndexPath).row]
//           //print("TV--imageItem has value")
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "GalleryTableViewCell", for: indexPath) as! GalleryTableViewCell
            //let selectedCell: Bool = (last_selected == mlItem.id) ? true : false
            var itemImage: UIImage = UIImage()
            if(self.itemSmallImageDataArray.count > (indexPath as NSIndexPath).row){
                itemImage = self.itemSmallImageDataArray[(indexPath as NSIndexPath).row] as! UIImage
            }
            cell.configureForImageItem(item, cellImage: itemImage)//collapsedInd[indexPath.section])
            
            return cell
        } else {
            //print("TV--!!--mlItem NULL--!!")
            return UITableViewCell()
        }
        
    }
    
    //    // Mark: Table View Delegate
    //    //override
    //    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
    //        let cellToDeSelect = tableView.cellForRowAtIndexPath(indexPath)! as! MLCell
    //        cellToDeSelect.imageNameLabel.textColor = colorWithHexString("#333333")//("#333333")
    //        cellToDeSelect.imageCreatorLabel.textColor = colorWithHexString("#333333")//("#333333")
    //    }
    
    //override
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let itemInfo = getCurrentItem((indexPath as NSIndexPath).row)
        currentItemData = itemInfo.0
        //        currentItemImage = itemInfo.1
        currentDetailIndex = (indexPath as NSIndexPath).row
        DispatchQueue.main.async {
            if(AppSettings.isPad() == true) {
                self.performSegue(withIdentifier: "GalleryViewDetailSegueModal", sender: self)
            } else {
                self.performSegue(withIdentifier: "GalleryViewDetailSegue", sender: self)
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       //print("didScroll")
        if(busyLoading != true){
            //NSLog(@"scrollView != nil");
            let bottomEdge: CGFloat = scrollView.contentOffset.y + scrollView.frame.size.height
            if(scrollView.contentOffset.y < 0){
                //NSLog(@"top!");
                return
                //} else if(scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.bounds.size.height)){
            } else if (bottomEdge >= scrollView.contentSize.height && busyLoading != true) {
                //NSLog(@"bottom! albumCount");
                if(itemTotal > (itemPage * itemPerPage) && busyLoading == false && itemPage > 0){
                    //NSLog(@"bottom! getAlbumImages => %d -- %d * %d", imageCount, imagePage, imagePerPage);
                    busyLoading = true
                    showMiniLoading()
                    getAlbumsImagesItemData(itemPage+1, limit: itemPerPage)
                }
            } else {
                //NSLog(@"else");
                return
            }
        }
    }
    
    //end tableview
    
    
    func getCurrentItem(_ index: Int) -> (Dictionary<String, AnyObject>, UIImage) {
        var itemData: Dictionary<String, AnyObject>!
        if let itemDataTemp = itemDataArray[index] as? NSDictionary {
            itemData = itemDataTemp.mutableCopy() as! Dictionary<String, AnyObject>
        }
        var itemImage: UIImage = UIImage()
        if(self.itemSmallImageDataArray.count > index){
            itemImage = self.itemSmallImageDataArray[index] as! UIImage
        }
        return (itemData, itemImage)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "GalleryViewDetailSegue" || segue.identifier == "GalleryViewDetailSegueModal") {
           //print("GalleryViewDetailSegue")
            galleryDetailViewController = (segue.destination as! GalleryDetailViewController)
            galleryDetailViewController.delegate = self
            galleryDetailViewController.showApprovalMode = false
            galleryDetailViewController.itemData = currentItemData
            galleryDetailViewController.currentItemIndex = currentDetailIndex
            galleryDetailViewController.itemCount = itemTotal
        }
        if(segue.identifier == "CreateNewImageSegue" || segue.identifier == "CreateNewImageSegueModal") {
            let createView = (segue.destination as! GalleryCreateNewViewController)
            createView.delegate = self
            createView.albumType = self.albumType
            createView.newImage = self.newImage
            createView.isAlbumCreate = false
            createView.albumObject = self.albumObject
//            createView.locationArray = self.locationArray
            //            createView.currentItemIndex = currentDetailIndex
        }
    }
    
    func getImageDataForDetail(_ index: Int) {
        if(itemDataArray.count > index) {
            setImageDataForDetail(index)
        } else {
            detailUpdateData(index) {
                (index: Int) in
                self.detailDataUpdateComplete(index)
            }
        }
    }
    
    func setImageDataForDetail(_ index: Int) {
        if(itemDataArray.count > index) {
            currentDetailIndex = index
            let itemInfo = getCurrentItem(index)
            currentItemData = itemInfo.0
            galleryDetailViewController.itemData = currentItemData
            galleryDetailViewController.setupDetailView()
            galleryDetailViewController.currentItemIndex = currentDetailIndex
            galleryDetailViewController.itemCount = itemTotal
        } else {
           //print("setImageDataForDetail fail")
        }
    }
    
    //update from detail start
    func detailUpdateData(_ index: Int, completion: (_ index: Int) -> Void){
        var pageToGet: Int = 0
        if(itemPage * itemPerPage < itemTotal){
            pageToGet = itemPage + 1
        } else {
            itemPage = 0
        }
        shouldUpdateDetail = true
        currentDetailIndex = index
        getAlbumsImagesItemData(pageToGet, limit: itemPerPage)
        completion(index)
    }
    
    func detailDataUpdateComplete(_ index: Int) {
       //print("detailDataUpdateComplete")
        //        setImageDataForDetail(index)
    }
    
}
