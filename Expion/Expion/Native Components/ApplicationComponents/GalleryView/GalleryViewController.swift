//
//  GalleryViewController.swift
//  Expion
//
//  Created by Mike Bagwell on 5/4/16.
//  Copyright © 2016 Expion. All rights reserved.
//

import Foundation

@objc
protocol GalleryViewControllerDelegate {
    
}

@objcMembers class GalleryViewController: UIViewController, JSONLocationDelegate, GalleryAlbumsViewControllerDelegate, GalleryImagesViewControllerDelegate,  GalleryApprovalImagesViewControllerDelegate {
    
    var displayMode: String!
    var viewFrameHeight: NSNumber!
    var viewFrameWidth: NSNumber!
    var locationList: Array<Dictionary<String, AnyObject>>!
    
    var showApprovalMode: Bool!
    var excludeSubmitted: String!
    
    let shareData = ShareData.sharedInstance
    let locUtils: LocationUtils = LocationUtils()
    let jsonLoc: JSONLocation = JSONLocation()
    
    var galleryAlbumsViewController: GalleryAlbumsViewController?
    var galleryImagesViewController: GalleryImagesViewController?
    var galleryApprovalImagesViewController: GalleryApprovalImagesViewController?
    
    @IBOutlet var contentView: UIScrollView!
    @IBOutlet var loadingView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //showLogin()
        //initCenterView()
       //print("GalleryViewController=" + displayMode)
       //print("GalleryViewController=" + shareData.myUrl)
       //print("companyId=" + shareData.company_id)
        
        //let displayModeInit = displayMode + "Init:"
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let displayModeInit: Selector = NSSelectorFromString(displayMode + "ModeInit")
        
        if self.responds(to: displayModeInit) {
            self.perform(displayModeInit)
        }
        
    }
    
    func showLoading() {
        DispatchQueue.main.async {
            self.loadingView.isHidden = false
        }
    }
    
    func hideLoading() {
        DispatchQueue.main.async {
            self.loadingView.isHidden = true
        }
    }
    
    func approvalModeInit() {
//        galleryApproveView.hidden = false
//        imageTitles = [[NSMutableArray alloc] init];
//        imageIDs = [[NSMutableArray alloc] init];
//        imageUrl = [[NSMutableArray alloc] init];
//        imageSmallImageData = [[NSMutableArray alloc] init];
//        approvalImageActions = [[NSMutableArray alloc] init];
//        imageDetailArray = [[NSMutableArray alloc] init];
        
//        loadingView.hidden = YES;
//        galleryContentView.hidden = NO;
//        galleryApproveDetailView.hidden = YES;
//        imagePage = 0;
        
        //[self getApprovalImages:@"1" :[NSString stringWithFormat:@"%d", albumPerPage]];
        addGalleryApprovalImagesViewController()
    }
    
    func uploaderModeInit() {
        addGalleryAlbumsViewController()
    }
    
    func addGalleryAlbumsViewController() {
//       //print("GVC => addGalleryAlbumsViewController")
        if (galleryAlbumsViewController == nil) {
//           //print("GVC => galleryAlbumsViewController == nil")
            galleryAlbumsViewController = UIStoryboard.galleryAlbumsViewController()
            galleryAlbumsViewController?.delegate = self
            galleryAlbumsViewController?.displayMode = displayMode
            let frame = contentView.frame
            //print("++addGalleryAlbumsViewController++")
            //print(String(describing: frame.size.width))
            //print(String(describing: frame.size.height))
            DispatchQueue.main.async {
                self.galleryAlbumsViewController?.view.frame = frame
                self.addChildController(self.galleryAlbumsViewController!)
            }
        } else {
            DispatchQueue.main.async {
                self.galleryAlbumsViewController!.view.isHidden = false
            }
        }
    }
    
    func addGalleryAlbumsImagesViewController(_ albumObj: Dictionary<String, AnyObject>, albumType: String) {
//       //print("GVC => addGalleryAlbumImagesViewController")
        if (galleryImagesViewController == nil) {
//           //print("GVC => addGalleryAlbumImagesViewController == nil")
            galleryImagesViewController = UIStoryboard.galleryImagesViewController()
            galleryImagesViewController?.delegate = self
            galleryImagesViewController?.displayMode = displayMode
            galleryImagesViewController?.albumObject = albumObj
            galleryImagesViewController?.albumType = albumType
            let frame = contentView.frame
            //print("++addGalleryAlbumsViewController++")
            //print(String(describing: frame.size.width))
            //print(String(describing: frame.size.height))
            DispatchQueue.main.async {
                self.galleryImagesViewController?.view.frame = frame
                self.addChildController(self.galleryImagesViewController!)
            }
        } else {
            DispatchQueue.main.async {
                self.galleryImagesViewController!.view.isHidden = false
            }
        }
    }
    
    
    func addGalleryApprovalImagesViewController() {
       //print("GVC => addLeftPanelViewController")
        if (galleryApprovalImagesViewController == nil) {
           //print("GVC => GalleryApprovalImagesViewController == nil")
            galleryApprovalImagesViewController = UIStoryboard.galleryApprovalImagesViewController()
            galleryApprovalImagesViewController?.delegate = self
            galleryApprovalImagesViewController?.displayMode = displayMode
            let frame = contentView.frame
            DispatchQueue.main.async {
                self.galleryApprovalImagesViewController?.view.frame = frame
                self.addChildController(self.galleryApprovalImagesViewController!)
            }
        } else {
            DispatchQueue.main.async {
                self.galleryApprovalImagesViewController!.view.isHidden = false
            }
        }
    }
    
    func addChildController(_ viewController: UIViewController) {
       //print("GVC=>addChildController")
//       //print("AppSettings.IS_IPAD=>" + String(AppSettings.IS_IPAD))
//        if(AppSettings.IS_IPAD == true){
//            let topSpace: CGFloat = UIApplication.sharedApplication().statusBarFrame.size.height + (centerViewController.navigationController?.navigationBar.frame.size.height)!
//            let menuFrame: CGRect = CGRectMake(0, topSpace, 320, view.frame.size.height - topSpace)
//            //leftPanelController
//            leftPanelController.view.frame = menuFrame
//            view.insertSubview(leftPanelController.view, aboveSubview: centerViewController.view)
//        } else {
//            contentView.insertSubview(galleryViewController.view, atIndex: 0)
//        }
        DispatchQueue.main.async {
            self.addChildViewController(viewController)
            self.contentView.addSubview(viewController.view)
    //        gShouldLoad = true
            //println(String(format:"add child gShouldLoad=>%@", gShouldLoad))
            viewController.didMove(toParentViewController: self)
        }
    }
    
    
    func backToGalleryAlbums() {
        if(galleryImagesViewController != nil){
            hideContentController(galleryImagesViewController!)
            galleryImagesViewController = nil
        }
    }
    
    func hideContentController(_ content: UIViewController) {
        DispatchQueue.main.async {
            content.willMove(toParentViewController: nil)
            content.view.removeFromSuperview()
            content.removeFromParentViewController()
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    //Uploader start
    func uploaderInit(){
       //print("uploaderInit")
        self.getLocJson()
    }
    
    func getLocJson(){
//        JSONLocation *jsonLoc = [[JSONLocation alloc] init]
        jsonLoc.company_id = shareData.company_id
        jsonLoc.myUrl = shareData.myUrl
        jsonLoc.delegate = self
        
//        [jsonLoc getLocJSON]
        jsonLoc.getLocJSON()
    }
    
    func locJSONReturned(_ jsonLocReturn: JSONLocation){
       //print("locJSONReturned")
        if(locationList != nil){
            locationList.removeAll()
        }
        locationList = jsonLocReturn.locationList
        setLocs()
    }
    
    func setLocs() {
       //print("ESG--setupLocs")
        
//        let locBtnTitle: String = locUtils.getLocBtnTitle(locationList)
        let locBtnTitle: String = locUtils.getTitleFromLocList(locationList)
       //print(locBtnTitle)
       //print(locBtnTitle)
        
        let displayModeSetup: Selector = NSSelectorFromString(displayMode + "Setup")
        if self.responds(to: displayModeSetup) {
            self.perform(displayModeSetup)
        }
    }

    func uploaderSetup() {
        
    }
    
}

private extension UIStoryboard {
    class func galleryStoryboard() -> UIStoryboard { return UIStoryboard(name: "GalleryView", bundle: Bundle.main) }
    
    class func galleryAlbumsViewController() -> GalleryAlbumsViewController? {
        return galleryStoryboard().instantiateViewController(withIdentifier: "GalleryAlbumsViewController") as? GalleryAlbumsViewController
    }
    
    class func galleryImagesViewController() -> GalleryImagesViewController? {
        return galleryStoryboard().instantiateViewController(withIdentifier: "GalleryImagesViewController") as? GalleryImagesViewController
    }
    
    class func galleryApprovalImagesViewController() -> GalleryApprovalImagesViewController? {
        return galleryStoryboard().instantiateViewController(withIdentifier: "GalleryApprovalImagesViewController") as? GalleryApprovalImagesViewController
    }
}

