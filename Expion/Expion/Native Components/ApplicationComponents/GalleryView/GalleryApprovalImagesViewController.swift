//
//  GalleryApprovalImagesViewController.swift
//  Expion
//
//  Created by Mike Bagwell on 5/11/16.
//  Copyright © 2016 Expion. All rights reserved.
//

import Foundation

@objc
protocol GalleryApprovalImagesViewControllerDelegate {
    func showLoading()
    func hideLoading()
}

class GalleryApprovalImagesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, GalleryDetailViewControllerDelegate {
    //TODO:  Find a way to get rid of this
    var busyLoading: Bool = false
    
    var delegate: GalleryApprovalImagesViewControllerDelegate?
    var displayMode: String!
    var showApprovalMode: Bool!
    
    var excludeSubmitted: String!
    
    let shareData = ShareData.sharedInstance
    
    
    //image connection and api return data
    var rowHeight: CGFloat = 79
    var itemPerPage: Int = 9
    var itemPage: Int = 0
    var data: NSMutableData = NSMutableData()
    
    //For image parser and image data
    var itemTotal: Int = 0
    var itemCount: Int = 0
    
    var itemDataArray: Array<Dictionary<String, AnyObject>>!
    var itemSmallImageDataArray: NSMutableArray!
    
    //segue to DetailView
    var galleryDetailViewController: GalleryDetailViewController!
    var currentItemData: Dictionary<String, AnyObject>!
    var currentItemImage: UIImage!
    var currentDetailIndex: Int = 0
    var shouldUpdateDetail: Bool = false
    var actionUpdateNeeded: Bool = false
    
    //tableview
    @IBOutlet weak var tableView: UITableView!
    //var tableViewController = UITableViewController()
    var refreshControl = UIRefreshControl()
    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var noPhotosView: UIView!
    
    @IBAction func backToApprovalImages(_ segue: UIStoryboardSegue) {
        self.approvalDetailBack()
    }
    
    func approvalDetailBack(){
        galleryDetailViewController = nil
        currentDetailIndex = 0
        //print("actionUpdateNeeded=>\(actionUpdateNeeded)")
        if(actionUpdateNeeded == true) {
//            clearItems()
//            clearItems {
                //print("After clearItems")
                self.approvalModeInit()
//            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.noPhotosView.isHidden = true
        }
        //showLogin()
        //initCenterView()
        //print("GalleryApprovalImagesViewController=" + displayMode)
        //print("GalleryApprovalImagesViewController=" + shareData.myUrl)
        //print("companyId=" + shareData.company_id)
        
        tableView.register(UINib(nibName: "GalleryTableViewCell", bundle: nil), forCellReuseIdentifier: "GalleryTableViewCell")
        
        //let displayModeInit = displayMode + "Init:"
        let displayModeInit: Selector = NSSelectorFromString(displayMode + "ModeInit")
        itemPerPage = Int(self.view.frame.height / rowHeight)
//        noneAvailView.hidden = true
        if self.responds(to: displayModeInit) {
            self.perform(displayModeInit)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func showLoading() {
        delegate?.showLoading()
    }
    
    func hideLoading() {
        delegate?.hideLoading()
    }
    
    func showMiniLoading() {
        delegate?.showLoading()
    }
    
    func hideMiniLoading() {
        delegate?.hideLoading()
    }
    
    //approval
    @objc func approvalModeInit() {
        //TODO:  Show/hide components
        itemPage = 0
        getApprovalItemData(1, limit: itemPerPage)
    }
    
    func reloadTable() {
        approvalModeInit()
    }
    
    func getApprovalItemData(_ page: Int, limit: Int) {
        if(itemPage == 0) {
            clearItems {
                self.startApprovalItemData(page, limit: limit)
            }
        } else {
            self.startApprovalItemData(page, limit: limit)
        }
        
    }
    
    func startApprovalItemData(_ page: Int, limit: Int) {
        itemPage = page
        //print("<< getItemData <<")
        //println("getMLJSON")
        let urlPath: String = String(format: "%@/api/eimages/images/%@?page=%@&limit=%@", shareData.myUrl, shareData.company_id, String(page), String(limit))
        startApprovalItemDataConnection(urlPath)
    }
    
//    doThing1 {
//
//        //do something here after running your function
//        //print("After thing1!")
//    }
//
//    func doThing1(completion: () -> Void) {
//
//        //print("Doing thing1!")
//
//        completion()
//
//    }
    
    func clearItems(completion: (() -> Void)? = nil){
        DispatchQueue.main.async {
            if(self.itemSmallImageDataArray != nil){
                self.itemSmallImageDataArray.removeAllObjects()
            }
            self.itemSmallImageDataArray = NSMutableArray()
            if(self.itemDataArray != nil){
                self.itemDataArray.removeAll()
            }
            self.itemDataArray = Array<Dictionary<String, AnyObject>>()
            self.itemCount = 0
            self.itemPage = 0
            self.itemTotal = 0
            self.tableView.reloadData()
            self.currentDetailIndex = 0
            //print("clearItems complete")
            completion?()
        }
    }
    
    func setNeedsUpdate() {
        actionUpdateNeeded = true
    }
    
    func startApprovalItemDataConnection(_ urlPath: String){//page: Int, limit: Int){
        showMiniLoading()
        DispatchQueue.main.async {
            self.noPhotosView.isHidden = true
        }
        if(shareData.myUrl != nil && shareData.company_id != nil){
            let url: URL = URL(string: urlPath)!
            //print(String(format: "getMLJSON urlPath=>%@", url as CVarArg))
            
            let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
            request.setValue(ShareData.sharedInstance.userAgent, forHTTPHeaderField: "User-Agent")
            let connection: NSURLConnection = NSURLConnection(request: request as URLRequest, delegate: self, startImmediately: false)!
            connection.start()
        }
    }
    
    @objc func connection(_ didReceiveResponse: NSURLConnection!, didReceiveResponse response: URLResponse!) {
        // Received a new request, clear out the data object
        if let httpResponse = response as? HTTPURLResponse {
            //print(httpResponse.statusCode)
            if(httpResponse.statusCode == 200){
                self.data = NSMutableData()
            } else {
                //print("didReceiveResponse != 200")
//                returnToLogin()
            }
        } else {
            assertionFailure("unexpected response")
        }
        
        
    }
    
    
    @objc func connection(_ connection: NSURLConnection!, didReceiveData data: Data!){
        self.data.append(data)
//        let dataStr = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
//        //print(String(format: "!!~~dataStr=>%@", dataStr!))
    }
    
    @objc func connectionDidFinishLoading(_ connection: NSURLConnection!) {
//        var err: NSError
//        let parseIt: Bool = true
        
//        if(parseIt == true){
        if(self.data.length > 0){
            parseJSON(self.data) {
                (parsedJSONData: Array<Dictionary<String, AnyObject>>) in
                self.parseCompleted(parsedJSONData)
            }
        }
        
    }
    
    func parseJSON(_ responseData: NSMutableData, completion: (_ parsedJSONData: Array<Dictionary<String, AnyObject>>) -> Void){
        
        //println("-=-=-parseMLJSON====")
        var parsedJSONData = Array<Dictionary<String, AnyObject>>()
        
//        var jsonError: NSError?
//        //var last_selected_match: Bool = false
//        var shouldLoadId: String!
//        var loadMLItemIndex: Int!
        
        var returnJSON: NSDictionary!
        
        do {
            returnJSON = try JSONSerialization.jsonObject(with: responseData as Data, options: []) as? NSDictionary
        } catch {
            
        }
        //print("returnJSON.count=>" + String(returnJSON.count))
        if returnJSON != nil && returnJSON.count > 0 {
//            let priority = DispatchQueue.GlobalQueuePriority.default
            
            if let total_resultsTemp = returnJSON["total_results"] as? Int {
                self.itemTotal = total_resultsTemp
            }
            if let countTemp = returnJSON["count"] as? Int {
                self.itemCount = countTemp
            }
            if let dataTemp = returnJSON["data"] as? NSArray {
                parsedJSONData = dataTemp as! Array<Dictionary<String, AnyObject>>
            }
        } else {
            DispatchQueue.main.async {
                self.noPhotosView.isHidden = false
            }
        }
        completion(parsedJSONData)
    }
    
    func parseCompleted(_ parsedJSONData: Array<Dictionary<String, AnyObject>>) {
        for i in 0 ..< parsedJSONData.count { //***LOOP CHECK SHOULD THIS BE 0 or 1
            self.itemSmallImageDataArray.add(UIImage())
            //print("itemPage=\(itemPage)")
            let itemPageTemp = itemPage > 0 ? itemPage - 1 : 0
            let imageIndex = i + (itemPageTemp * (itemPerPage))
            //print("imageIndex=\(imageIndex)")
            if let dataDict = parsedJSONData[i] as? Dictionary<String, AnyObject> {
                itemDataArray.append(dataDict)
                DispatchQueue.main.async(execute: { () -> Void in
                    self.tableView.reloadData()
                })
                if let image_url = dataDict["image_url"] as? String {
                    let imageURLString = String(format:"%@%@?width=75&height=75", self.shareData.myUrl, image_url)
                    NSURLConnection.sendAsynchronousRequest(
                        URLRequest(url: URL (string: imageURLString)!),
                        queue: OperationQueue.main,
                        completionHandler: { response, data, err in
                            if let e = err {
                                print(e)
                            } else {
                                if let img: UIImage = UIImage(data: data!) {
                                    //print("self.itemSmallImageDataArray.count=\(self.itemSmallImageDataArray.count)")
                                    //print("imageIndex=\(imageIndex)")
                                    if(self.itemSmallImageDataArray.count > imageIndex){
                                        self.itemSmallImageDataArray.replaceObject(at: imageIndex, with: img)
                                    }
                                    DispatchQueue.main.async {
                                        self.tableView.reloadData()
                                    }
                                }
                            }
                    })
                }
            }
        }
        
        busyLoading = false
        if(shouldUpdateDetail == true) {
            shouldUpdateDetail = false
            setImageDataForDetail(currentDetailIndex)
        }
        hideMiniLoading()
    }
    
//    func requestImage(url: String, success: (UIImage?) -> Void) {
//        requestURL(url, success: { (data) -> Void in
//            if let d = data {
//                success(UIImage(data: d))
//            }
//        })
//    }
//    
//    func requestURL(url: String, success: (NSData?) -> Void, error: ((NSError) -> Void)? = nil) {
//        NSURLConnection.sendAsynchronousRequest(
//            NSURLRequest(URL: NSURL (string: url)!),
//            queue: NSOperationQueue.mainQueue(),
//            completionHandler: { response, data, err in
//                if let e = err {
//                    error?(e)
//                } else {
//                    success(data)
//                }
//        })
//    }
    
    // MARK: Table View Data Source
    //override
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //override
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemDataArray != nil ? itemDataArray.count : 0
    }
    
    //override
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
    
    //override
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var item: Dictionary<String, AnyObject>!
        if(itemDataArray.count > (indexPath as NSIndexPath).row){
            if let itemTemp = itemDataArray[(indexPath as NSIndexPath).row] as? NSDictionary {
                item = itemTemp.mutableCopy() as! Dictionary<String, AnyObject>
                //print("TV--imageItem has value")
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "GalleryTableViewCell", for: indexPath) as! GalleryTableViewCell
                //let selectedCell: Bool = (last_selected == mlItem.id) ? true : false
                var itemImage: UIImage = UIImage()
                if(self.itemSmallImageDataArray.count > (indexPath as NSIndexPath).row){
                    itemImage = self.itemSmallImageDataArray[(indexPath as NSIndexPath).row] as! UIImage
                }
                cell.configureForImageItem(item, cellImage: itemImage)//collapsedInd[indexPath.section])
                
                return cell
            }
        } else {
            //print("TV--!!--mlItem NULL--!!")
            return UITableViewCell()
        }
        
    }
    
    //override
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let itemInfo = getCurrentItem((indexPath as NSIndexPath).row)
        currentItemData = itemInfo.0
//        currentItemImage = itemInfo.1
        currentDetailIndex = (indexPath as NSIndexPath).row
        if(AppSettings.isPad() == true) {
            self.performSegue(withIdentifier: "GalleryViewApproveDetailSegueModal", sender: self)
        } else {
            self.performSegue(withIdentifier: "GalleryViewApproveDetailSegue", sender: self)
        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //print("didScroll")
        if(busyLoading != true){
            //NSLog(@"scrollView != nil");
            let bottomEdge: CGFloat = scrollView.contentOffset.y + scrollView.frame.size.height
            if(scrollView.contentOffset.y < 0){
                //NSLog(@"top!");
                return
            } else if (bottomEdge >= scrollView.contentSize.height && busyLoading != true) {
                //NSLog(@"bottom! albumCount");
                if(itemTotal > (itemPage * itemPerPage) && busyLoading == false && itemPage > 0){
                    busyLoading = true
                    showMiniLoading()
                    getApprovalItemData(itemPage+1, limit: itemPerPage)
                }
            } else {
                //NSLog(@"else");
                return
            }
        }
    }
    
    //end tableview
    
    
    func getCurrentItem(_ index: Int) -> (Dictionary<String, AnyObject>, UIImage) {
        var itemData: Dictionary<String, AnyObject>!
        if let itemDataTemp = itemDataArray[index] as? NSDictionary {
            itemData = itemDataTemp.mutableCopy() as! Dictionary<String, AnyObject>
        }
        var itemImage: UIImage = UIImage()
        if(self.itemSmallImageDataArray.count > index){
            itemImage = self.itemSmallImageDataArray[index] as! UIImage
        }
        return (itemData, itemImage)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "GalleryViewApproveDetailSegue" || segue.identifier == "GalleryViewApproveDetailSegueModal") {
            //print("GalleryViewApproveDetailSegue")
            galleryDetailViewController = (segue.destination as! GalleryDetailViewController)
            galleryDetailViewController.delegate = self
            galleryDetailViewController.showApprovalMode = true
            galleryDetailViewController.itemData = currentItemData
            galleryDetailViewController.currentItemIndex = currentDetailIndex
            galleryDetailViewController.itemCount = itemTotal
        }
    }
    
    func getImageDataForDetail(_ index: Int) {
        if(itemDataArray.count > index) {
            setImageDataForDetail(index)
        } else {
            detailUpdateData(index) {
                (index: Int) in
                self.detailDataUpdateComplete(index)
            }
        }
    }
    
    func setImageDataForDetail(_ index: Int) {
        if(itemDataArray.count > index) {
            currentDetailIndex = index
            let itemInfo = getCurrentItem(index)
            currentItemData = itemInfo.0
            galleryDetailViewController.itemData = currentItemData
            galleryDetailViewController.setupDetailView()
            galleryDetailViewController.currentItemIndex = currentDetailIndex
            galleryDetailViewController.itemCount = itemTotal
        } else {
            //print("setImageDataForDetail fail")
        }
    }
    
    //update from detail start
    func detailUpdateData(_ index: Int, completion: (_ index: Int) -> Void){
        var pageToGet: Int = 0
        if(itemPage * itemPerPage < itemTotal){
            pageToGet = itemPage + 1
        } else {
            pageToGet = 1
        }
        shouldUpdateDetail = true
        currentDetailIndex = index
        getApprovalItemData(pageToGet, limit: itemPerPage)
        completion(index)
    }
    
    func detailDataUpdateComplete(_ index: Int) {
        //print("detailDataUpdateComplete")
//        setImageDataForDetail(index)
    }
    
    
    
    
}
