//
//  GalleryImage1LineTableViewCell.swift
//  Expion
//
//  Created by Mike Bagwell on 5/5/16.
//  Copyright © 2016 Expion. All rights reserved.
//

import UIKit

class GalleryImageTableViewCell: UITableViewCell {

    @IBOutlet var imageCell: UIImageView!
    @IBOutlet var imageNameLabel: UILabel!
    @IBOutlet var imageDateLabel: UILabel!
    @IBOutlet var imageNameOnlyLabel: UILabel!
    @IBOutlet var imageStatusLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
