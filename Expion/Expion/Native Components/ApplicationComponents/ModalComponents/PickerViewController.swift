//
//  PickerViewController.swift
//  Expion
//
//  Created by Mike Bagwell on 5/17/16.
//  Copyright © 2016 Expion. All rights reserved.
//

import Foundation

@objc
protocol PickerViewControllerDelegate {
    func pickerValueSelected(_ pickerReturnArray: Array<Dictionary<String, AnyObject>>, pickerReturnObj: Dictionary<String, AnyObject>)
}

class PickerViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource  {
    
    var delegate: PickerViewControllerDelegate!
    var pickerOptionsArray: Array<Dictionary<String, AnyObject>>! = nil
    var pickerTitle: String = ""
    var returnObj: Dictionary<String, AnyObject>! = nil
    
    @IBOutlet var pickerBoxView: UIView!
//    @IBOutlet var pickerToolbar: UIToolbar!
    @IBOutlet var pickerDoneBtn: UIBarButtonItem!
    
    @IBAction func pickerDoneAction() {
        self.dismiss(animated: true, completion: {
            self.delegate?.pickerValueSelected(self.pickerOptionsArray, pickerReturnObj: self.returnObj)
        })
    }
    
    @IBOutlet weak var picker: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.dataSource = self
        picker.delegate = self
        var pickerSelectedIndex: Int = 0
        for i in 0 ..< self.pickerOptionsArray.count { //***LOOP CHECK SHOULD THIS BE 0 or 1
            let pickerObj = self.pickerOptionsArray[i] as Dictionary<String, AnyObject>
            if (pickerObj["selected"] as? Bool) == true {
                pickerSelectedIndex = i
                if let title = pickerObj["name"] as? String {
                    self.pickerTitle = title
                }
            }
        }
        setPickerObj(0)
        picker.selectRow(pickerSelectedIndex, inComponent: 0, animated: false)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return pickerOptionsArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var title: String = ""
        if let pickerObj = self.pickerOptionsArray[row] as? Dictionary<String, AnyObject> {
            if let pickerStr = pickerObj["name"] as? String {
                title = pickerStr
            }
        }
        return title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        self.view.endEditing(true)
        setPickerObj(row)
    }
    
    func setPickerObj(_ index: Int) {
        if(pickerOptionsArray.count > index){
            for i in 0 ..< self.pickerOptionsArray.count { //***LOOP CHECK SHOULD THIS BE 0 or 1
                let pickerObj = self.pickerOptionsArray[i] as Dictionary<String, AnyObject>
                let dict: Dictionary<String, AnyObject> = [
                    "name": pickerObj["name"]!,
                    "value": pickerObj["value"]!,
                    "selected": (index == i ? true : false) as AnyObject
                ]
                if i == index {
                    if let title = pickerObj["name"] as? String {
                        self.pickerTitle = title
                    }
                    self.returnObj = dict
                }
                self.pickerOptionsArray[i] = dict
            }
        }
    }
}
