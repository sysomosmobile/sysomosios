//
//  FlatTree.swift
//  Expion
//
//  Created by Mike Bagwell on 2/23/16.
//  Copyright © 2016 Expion. All rights reserved.
//

import UIKit

@objc
protocol FlatTreeViewControllerDelegate {
//    optional func locViewControllerDismissed(returnedLocs: NSMutableArray, withTitle titleTxt: NSString)
    @objc optional func locViewControllerDismissed(_ returnedLocs: Array<Dictionary<String, AnyObject>>, withTitle titleTxt: String)
}

@objcMembers class FlatTreeViewController: UIViewController, UIGestureRecognizerDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var selectAllBtn: UIBarButtonItem!
    @IBOutlet weak var deselectAllBtn: UIBarButtonItem!
    @IBOutlet weak var doneBtn: UIBarButtonItem!
    
    var delegate: FlatTreeViewControllerDelegate?
    
//    var treeItemArray: NSMutableArray = []
    var treeItemArray: Array<Dictionary<String, AnyObject>>!
    var treeTitle: String = ""
    var canSelectMultiples: Bool = false
    var enableNoLocSelect: Bool = false
    var lastIndexPath: IndexPath!
    
    class func newInstance() -> FlatTreeViewController {
        return FlatTreeViewController()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //print("FlatTreeViewController didLoad canSelectMultiples=>" + (canSelectMultiples == true ? "true" : "false"))
        if(canSelectMultiples != true){
            selectAllBtn.tintColor = UIColor.clear
            selectAllBtn.isEnabled = false
            deselectAllBtn.tintColor = UIColor.clear
            deselectAllBtn.isEnabled = false
            
//            var numSelected: Int = 0
//            var currentItem: Int = 0
//            for item in self.treeItemArray {
//                if item["selected"] as? String == "1" {
//                    if(numSelected < 1) {
//                        self.lastIndexPath = NSIndexPath(forRow: currentItem, inSection: 0)
//                    } else {
//                        if let locItem = self.treeItemArray[currentItem] as? NSMutableDictionary {
//                            let locDic: NSMutableDictionary = [
//                                "name": locItem["name"]!,
//                                "id": locItem["id"]!,
//                                "isDefault": locItem["isDefault"]!,
//                                "selected": "0"
//                            ]
//                            self.treeItemArray.replaceObjectAtIndex(currentItem, withObject: locDic)
//                        }
//                    }
//                    numSelected += 1
//                }
//                currentItem += 1
//            }
        }
    }
    
    /* modal view controller dismiss on click outside of bounds - BEGIN */
    fileprivate var tapOutsideRecognizer: UITapGestureRecognizer!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(self.tapOutsideRecognizer == nil) {
            self.tapOutsideRecognizer = UITapGestureRecognizer(target: self, action: #selector(GalleryDetailViewController.handleTapBehind(_:)))
            self.tapOutsideRecognizer.numberOfTapsRequired = 1
            self.tapOutsideRecognizer.cancelsTouchesInView = false
            self.tapOutsideRecognizer.delegate = self
            self.view.window!.addGestureRecognizer(self.tapOutsideRecognizer)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if(self.tapOutsideRecognizer != nil) {
            self.view.window?.removeGestureRecognizer(self.tapOutsideRecognizer)
            self.tapOutsideRecognizer = nil
        }
    }
    
    func close(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func handleTapBehind(_ sender: UITapGestureRecognizer) {
        if (sender.state == UIGestureRecognizerState.ended) {
//            let location: CGPoint = sender.locationInView(nil)
//            
//            if (!self.view.pointInside(self.view.convertPoint(location, fromView: self.view.window), withEvent: nil)) {
//                self.view.window?.removeGestureRecognizer(sender)
//                self.close(sender)
//            }
            let location: CGPoint = sender.location(in: self.view)
            
            if(self.view.point(inside: location, with: nil)){
                //print("P--In view??")
            } else {
                //print("P--Not in view??")
                self.view.window?.removeGestureRecognizer(sender)
                self.close(sender)
                
            }
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    /* modal view controller dismiss on click outside of bounds - END */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func selectAllBtn(_ sender:UIButton) {
        //print("logoutBtn tapped")
        markAll(true)
    }
    
    @IBAction func deselectAllBtn(_ sender:UIButton) {
        //print("logoutBtn tapped")
        markAll(false)
    }
    
    func markAll(_ selected: Bool) {
        for i in 0 ..< self.treeItemArray.count { //***LOOP CHECK SHOULD THIS BE 0 or 1
            let locItem = self.treeItemArray[i]
            let locDic: Dictionary<String, AnyObject> = [
                "name": locItem["name"]!,
                "id": locItem["id"]!,
                "isDefault": locItem["isDefault"]!,
                "selected": (selected == true ? "1" : "0") as AnyObject
            ]
            self.treeItemArray[i] = locDic
        }
        self.tableView.reloadData()
    }
    
    @IBAction func doneBtnAction(_ sender:UIButton) {
        //print("doneBtnAction tapped")

//        var numSelected: Int = 0
//        var titleTxt: String = ""
//        for item in self.treeItemArray {
//            if item["selected"] as? String == "1" {
//                numSelected += 1
//                if(numSelected == 1) {
//                    titleTxt = (item["name"] as? String)!
//                } else {
//                    titleTxt = String(numSelected) + " Locations"
//                }
//            }
//        }
//        let titleTxt = LocationUtils().getLocBtnTitle(self.treeItemArray)
        if(self.treeItemArray == nil) {
            self.treeTitle = "No locations available"
            let alert = UIAlertController(title: "Data Error", message: "Error retreiving locations", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: nil))//{ action in self.reloadLayout()}))
            DispatchQueue.main.async {
                self.present(alert, animated: true, completion: nil)
                self.dismiss(animated: true, completion: {
                    self.delegate?.locViewControllerDismissed!(self.treeItemArray, withTitle: self.treeTitle)
                    self.delegate = nil
                })
            }
        } else if(enableNoLocSelect == false && LocationUtils().validationCheck(self.treeItemArray) == false) {
            let alert = UIAlertController(title: "Selection Error", message: "Please choose at lease one location", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: nil))//{ action in self.reloadLayout()}))
            DispatchQueue.main.async {
                self.present(alert, animated: true, completion: nil)
            }
        } else {
            let titleTxt = LocationUtils().getTitleFromLocList(self.treeItemArray)
            self.treeTitle = titleTxt
            //print(treeTitle)
            
            self.dismiss(animated: true, completion: {
                self.delegate?.locViewControllerDismissed!(self.treeItemArray, withTitle: self.treeTitle)
                self.delegate = nil
            })
        }
    }
    
//    func getTitleFromLocList(locList: Array<Dictionary<String, AnyObject>>) -> String {
//        var numSelected: Int = 0
//        var titleTxt: String = "Select Locations"
//        for item in locList {
//            if item["selected"] as? String == "1" {
//                numSelected += 1
//                if(numSelected == 1) {
//                    titleTxt = (item["name"] as? String)!
//                } else {
//                    titleTxt = String(numSelected) + " Locations"
//                }
//            }
//        }
//        return titleTxt
//    }
    
    // MARK: Table View Data Source
    
    //override
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        return 1
    }

    //override
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0 && treeItemArray != nil && treeItemArray.count > 0){
            //print("FlatTree numberOfRowsInSection=>" + String(treeItemArray.count))
            return treeItemArray.count
        } else {
            //print("FlatTree numberOfRowsInSection else count=>0")
            return 0
        }
    }
    
    //override
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    
    //override
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        //println("titleForHeaderInSection")
        //return 50
        return 0
    }
    
    //override
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    //override
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    //override
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        var locName: String = ""
        var locSelected: String = "0"
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        
        let locItem = self.treeItemArray[(indexPath as NSIndexPath).row]
        if let JSON_locName = locItem["name"] as? String {
            locName = JSON_locName
        }
        if let JSON_locSelected = locItem["selected"] as? String {
            locSelected = JSON_locSelected
        }
        
        cell.textLabel?.text = locName
        
        if(locSelected == "1"){
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
            cell.isSelected = true
        } else {
            cell.accessoryType = UITableViewCellAccessoryType.none
            cell.isSelected = false
        }
        
        return cell
        
        //return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)!
        
        if(self.canSelectMultiples != true){
            //for var i = 0; i < treeItemArray.count; ++i {
            //print("!!--deselect old loc")
            if(lastIndexPath != nil) {
                let oldCell = tableView.cellForRow(at: lastIndexPath)!
                self.tableView.deselectRow(at: indexPath, animated: false)
                oldCell.accessoryType = UITableViewCellAccessoryType.none;
                oldCell.isSelected = false
                let locItem = self.treeItemArray[lastIndexPath.row]
                let locDic: Dictionary<String, AnyObject> = [
                    "name": locItem["name"]!,
                    "id": locItem["id"]!,
                    "isDefault": locItem["isDefault"]!,
                    "selected": "0" as AnyObject
                ]
                self.treeItemArray[lastIndexPath.row] = locDic
            }
        }
        
        //cell.selected = true
        cell.selectionStyle = UITableViewCellSelectionStyle.none;
        var selectedStr: String!
        
        if (cell.accessoryType == UITableViewCellAccessoryType.none) {
            cell.accessoryType = UITableViewCellAccessoryType.checkmark;
            selectedStr = "1"
        } else if (cell.accessoryType == UITableViewCellAccessoryType.checkmark) {
            cell.accessoryType = UITableViewCellAccessoryType.none;
            selectedStr = "0"
        }
        
        let locItem = self.treeItemArray[(indexPath as NSIndexPath).row]
        let locDic: Dictionary<String, AnyObject> = [
            "name": locItem["name"]!,
            "id": locItem["id"]!,
            "isDefault": locItem["isDefault"]!,
            "selected": selectedStr as AnyObject
        ]
        self.treeItemArray[(indexPath as NSIndexPath).row] = locDic
        
        self.lastIndexPath = indexPath
    }
    
    // Mark: Table View Delegate
    //override
    func tableView(_ tableView: UITableView, didDeselectRowAtIndexPath indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)!
        cell.accessoryType = UITableViewCellAccessoryType.none;
    }
}
