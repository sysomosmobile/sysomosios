//
//  SysomosLoginViewController.swift
//  Expion
//
//  Created by Mike Bagwell on 3/27/17.
//  Copyright © 2017 Expion. All rights reserved.
//

import Foundation

@objc
protocol SysomosLoginViewControllerDelegate {
    
}

class SysomosLoginViewController: NSObject, URLSessionDelegate, URLSessionDataDelegate, URLSessionTaskDelegate {
    
    let enteredUsername: String = "mguglietti@sysomos.com"
    let enteredPassword: String = "o,IiFo84"
    
    let startUrl: String = "https://oauth.sysomos.com/oauth/oauth/authorize?response_type=token&state=oauth-test-5a02d3829317&client_id=sysomos-ui&scope=read&redirect_uri=https://www.google.com"
    let domainUrl: URL = URL(string:"https://oauth.sysomos.com")!
    let cookieUrl: URL = URL(string:"https://oauth.sysomos.com/oauth/login.jsp")!
    
    var responseData: Data?
    var responseUrl: URL?
    
    let opQueue = OperationQueue()
    var response:URLResponse?
    var session:URLSession?
    
    var time:DispatchTime! {
        return DispatchTime.now() + 1.0 // seconds
    }
    
    func ssoBegin() {
        //print("***-ssoBegin")
        var configuration = URLSessionConfiguration.default
        configuration.httpShouldSetCookies = true
        configuration.httpCookieAcceptPolicy = .always
        //    configuration.HTTPCookieStorage?.cookieacceptPolicy = NSHTTPCookieAcceptPolicyAlways;
        var session = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)

        var url = URL(string: startUrl)
        var request = URLRequest(url: url!)

//        var task: URLSessionTask? = session.dataTask(with: request)
//        task?.resume()
        
//        if let task = self.session?.dataTask(with: request) {
//            // start the task, tasks are not started by default
//            task.resume()
//        }
        let task = session.dataTask(with: request) {
            (data, response, error) -> Void in
            
            let httpResponse = response as! HTTPURLResponse
            let statusCode = httpResponse.statusCode
            //print("statusCode=>\(statusCode)")
            if (statusCode == 200) {
                //print("success")
                self.postSysomosSSO(self.cookieUrl)
            } else {
                //print("Load error getting notifications")
            }
        }
        
        task.resume()
    }
    
    func postSysomosSSO(_ cookieUrl: URL) {
        //print("***-postSysomosSSO-cookieUrl=>\(cookieUrl.absoluteString)")
        var cookieStr: String = ""
        let cookieStorage = HTTPCookieStorage.shared
        let cookies: [HTTPCookie]? = cookieStorage.cookies(for: cookieUrl)
        for cookie: HTTPCookie in cookies! {
            //print("!!**--cookie.name=>\(cookie.name)")
            //print("!!**--cookie.value=>\(cookie.value)")
            cookieStr = cookieStr + ("\(cookie.name)=\(cookie.value); ")
        }
        //print("!!**--cookieStr=>\(cookieStr)")

        //[passwordField text];
        let usernameData: String = "j_username=" + (enteredUsername)
        let passwordData: String = "&j_password=" + (enteredPassword)
        let loginData: String = usernameData + (passwordData)
        var postData: String = loginData + ("&login=Submit")
        //var urlDomain: String = "https://oauth.sysomos.com"
        let urlAsString: String = "https://oauth.sysomos.com/oauth/login.do"
        var appVersion: String? = (Bundle.main.infoDictionary?["CFBundleVersion"] as? String)
        //@"CFBundleVersion"];//CFBundleShortVersionString =fix
        appVersion = appVersion?.replacingOccurrences(of: ".", with: "")
        
        let postData2 = postData.data(using: String.Encoding.ascii, allowLossyConversion: true)
        let postLength: String = "\(UInt(postData.count))"
        let url = URL(string: urlAsString)
        
        
        
        self.opQueue.isSuspended = true
        
        let configuration = URLSessionConfiguration.default
        configuration.httpShouldSetCookies = true
        configuration.httpCookieAcceptPolicy = .always
        configuration.urlCache = nil
        //    configuration.HTTPCookieStorage?.cookieacceptPolicy = NSHTTPCookieAcceptPolicyAlways;
//        var session = URLSession(configuration: configuration, delegate: self, delegateQueue: self.opQueue)
        
//        var task: URLSessionTask? = session.dataTask(withRequest: request)
//        task?.resume()
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        request.setValue(postLength, forHTTPHeaderField: "Content-Length")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = postData2
        //        request.setValue(userAgent, forHTTPHeaderField: "User-Agent")
        request.setValue("application/json, text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8", forHTTPHeaderField: "Accept")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        //"application/json; charset=utf-8"
        request.setValue(cookieStr, forHTTPHeaderField: "Cookie")
        request.setValue("1", forHTTPHeaderField: "DNT")
        request.setValue("oauth.sysomos.com", forHTTPHeaderField: "Host")
        request.setValue("https://oauth.sysomos.com", forHTTPHeaderField: "Origin")
        request.setValue("https://oauth.sysomos.com/oauth/login.jsp", forHTTPHeaderField: "Referer")
        request.setValue("1", forHTTPHeaderField: "Upgrade-Insecure-Requests")
        
        
        let task = URLSession.shared.dataTask(with: request) {
            // start the task, tasks are not started by default
            (data, response, error) -> Void in
            
            let httpResponse = response as! HTTPURLResponse
            let statusCode = httpResponse.statusCode
            //print("**-statusCode=>\(statusCode)")
            //print("**-response?.url=>\(String(describing: response?.url?.absoluteString))")
            
            if (statusCode == 200) {
                //print("**-success")
                //self.postSysomosSSO(self.cookieUrl)
            } else if (statusCode == 302) {
                //print("**-302")
            } else {
                //print("error logging in")
            }
        }
        task.resume()
        // Open the operations queue after 1 second
        DispatchQueue.main.asyncAfter(deadline: self.time, execute: {[weak self] in
            //print("Opening the OperationQueue")
            self?.opQueue.isSuspended = false
        })
    }
    
    func callFinalGet() {
        //print("***-callFinalGet")
        let enteredUsername: String = "mguglietti@sysomos.com"
        //[usernameField text];
        let enteredPassword: String = "o,IiFo84"
        //[passwordField text];
        let usernameData: String = "j_username=" + (enteredUsername)
        let passwordData: String = "&j_password=" + (enteredPassword)
//        let loginData: String = usernameData + (passwordData)
//        var postData: String = loginData + ("&login=Submit")
        let urlDomain: String = "https://oauth.sysomos.com"
        let urlAsString2: String = "https://oauth.sysomos.com/oauth/oauth/authorize?response_type=token&state=oauth-test-5a02d3829317&client_id=sysomos-ui&scope=read&redirect_uri=https://www.google.com"
        var appVersion: String? = (Bundle.main.infoDictionary?["CFBundleVersion"] as? String)
        //@"CFBundleVersion"];//CFBundleShortVersionString =fix
        appVersion = appVersion?.replacingOccurrences(of: ".", with: "")
        let url = URL(string: urlAsString2)
        var request = URLRequest(url: url!)
        request.setValue("text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8", forHTTPHeaderField: "Accept")
        request.setValue(urlDomain, forHTTPHeaderField: "Host")
        request.setValue("1", forHTTPHeaderField: "Upgrade-Insecure-Requests")
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
//        var task: URLSessionTask? = session.dataTask(withRequest: request)
//        task?.resume()
        
//        if let task = self.session?.dataTask(with: request) {
//            // start the task, tasks are not started by default
//            task.resume()
//        }
        let task = session.dataTask(with: request) {
            (data, response, error) -> Void in
            
            let httpResponse = response as! HTTPURLResponse
            let statusCode = httpResponse.statusCode
            //print("statusCode=>\(statusCode)")
            
            //print("!!--finalGet-response?.url=>\(String(describing: response?.url?.absoluteString))")
            if (statusCode == 200) {
                //print("success")
            } else {
                //print("Load error getting notifications")
            }
        }
        
        task.resume()
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: @escaping (_: URLRequest) -> Void) {
        //print("!-URLSession-RR--Redirect from \(response.url) to \(request.url)")
        completionHandler(request)
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceiveResponse response: URLResponse, completionHandler: @escaping (_: URLSession.ResponseDisposition) -> Void) {
        //print("!-URLSession--response")
        //print("\(response)")
        responseData = Data()
//        responseData?.removeAll()
        responseUrl = response.url
        self.checkResponseUrl(response.url!)
//        checkResponse(response)
        completionHandler(.allow)
    }
    
//    func urlSession(_ session: URLSession, task: URLSessionTask, didReceiveChallenge challenge: URLAuthenticationChallenge, completionHandler: @escaping (_: URLSession.AuthChallengeDisposition, _: URLCredential) -> Void) {
//        //print("!-URLSession--challenge \(challenge.protectionSpace.authenticationMethod)")
//        completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, nil)
//    }

    open func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Swift.Void){
        
        var disposition: URLSession.AuthChallengeDisposition = URLSession.AuthChallengeDisposition.performDefaultHandling
        
        var credential:URLCredential?
        //print("didReceive challenge")
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
            credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            //print("didReceive challenge if")
            if (credential != nil) {
                //print("didReceive challenge credential != nil")
                disposition = URLSession.AuthChallengeDisposition.useCredential
            }
            else{
                //print("didReceive challenge credential == nil")
                disposition = URLSession.AuthChallengeDisposition.performDefaultHandling
            }
        }
        else{
            //print("didReceive challenge else")
            disposition = URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge
        }
        
        if (completionHandler != nil) {
            completionHandler(disposition, credential);
        }
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        //print("!-URLSession--done")
        if (responseUrl != nil) {
            var responseURLString: String = responseUrl!.absoluteString
            //print("responseURLString=>\(responseURLString)")
        }
        //https://home.sysomos.com/
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceiveData data: Data) {
        //print("!-didReceiveData")
        let responseDataString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        //print("!-responseDataString=>\(responseDataString)")
        responseData?.append(data)
    }
    
    func checkResponseUrl(_ responseUrl: URL) {
        let responseURLString: String = (responseUrl.absoluteString)
        //print("!!--checkResponse-url=>\(responseURLString)")
        if (responseURLString.lowercased().contains("login.jsp")) {
            self.setCookies(responseUrl)
        }
        if (responseURLString.lowercased().contains("login.do")) {
            self.callFinalGet()
        }
        
    }
    
    func setCookies(_ response: URL) {
        postSysomosSSO(response)
    }
    //---------------------------------
    //* End NSURLSession */
    //---------------------------------
    
}
