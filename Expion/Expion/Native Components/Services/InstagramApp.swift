//
//  InstagramApp.swift
//  Expion
//
//  Created by Mike Bagwell on 2/9/17.
//  Copyright © 2017 Expion. All rights reserved.
//

import UIKit
import Foundation


import Photos
import Social
//@objc
//protocol InstagramAppDelegate {
//    //    func locJSONReturned(_ jsonLocReturn: JSONLocation)
//}

class InstagramManager: NSObject, UIDocumentInteractionControllerDelegate {
    
    private let kInstagramURL = "instagram://app"
    private let kUTI = "public.data, public.content" //"com.instagram.exclusivegram"
    private let kfileNameExtension = "instagram.igo"
    private let kAlertViewTitle = "Error"
    private let kAlertViewMessage = "Please install the Instagram application"
    
    var documentInteractionController = UIDocumentInteractionController()
    let serialQueue = DispatchQueue(label: "mobileShareQueue")
    
    // singleton manager
    class var sharedManager: InstagramManager {
        struct Static {
            static let instance: InstagramManager = InstagramManager()
        }
        return Static.instance
    }
    
    
    
    func postImageToInstagram(image: UIImage, instagramCaption: String, view: UIView) {
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(InstagramManager.image(image:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    func postVideoToInstagram(image: UIImage, instagramCaption: String, view: UIView) {
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(InstagramManager.image(image:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    //@objc
    @objc func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer) {
        if error != nil {
            //print(error ?? "")
        }
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        let fetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        if let lastAsset = fetchResult.firstObject! as? PHAsset {
            let localIdentifier = lastAsset.localIdentifier
            let u = "instagram://library?LocalIdentifier=" + localIdentifier
            let url = NSURL(string: u)!
            if UIApplication.shared.canOpenURL(url as URL) {
                UIApplication.shared.openURL(NSURL(string: u)! as URL)
            } else {
                // alert displayed when the instagram application is not available in the device
                UIAlertView(title: kAlertViewTitle, message: kAlertViewMessage, delegate:nil, cancelButtonTitle:"Ok").show()
            }
            
        }
    }
    
    
    func postImageToInstagramWithCaption(image: UIImage, instagramCaption: String, view: UIView) {
        // called to post image with caption to the instagram application
        
        UIPasteboard.general.string = instagramCaption
        
        let instagramURL = URL(string: kInstagramURL)
        if UIApplication.shared.canOpenURL(instagramURL! as URL) {
            
            
            //option 1 - photo with library under it
            
            do {
                try PHPhotoLibrary.shared().performChangesAndWait {
                    
                    let request = PHAssetChangeRequest.creationRequestForAsset(from: image)
                    let assetID = request.placeholderForCreatedAsset?.localIdentifier ?? ""
                    let escapedString = assetID.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
                    //                    let shareURL = "instagram://library?LocalIdentifier=" + assetID
                    let shareURL = "instagram://library?LocalIdentifier=" + escapedString!
                    
                    if UIApplication.shared.canOpenURL(instagramURL!) {
                        if let urlForRedirect = NSURL(string: shareURL) {
//                            UIApplication.shared.openURL(urlForRedirect as URL)
                            UIApplication.shared.open(urlForRedirect as URL, options: [:], completionHandler: nil)
                        }
                    }
                }
            } catch {
                //print(error)
//                if let result = result {
//                    result(false)
//                }
            }
        } else {
            // alert displayed when the instagram application is not available in the device
            UIAlertView(title: kAlertViewTitle, message: kAlertViewMessage, delegate:nil, cancelButtonTitle:"Ok").show()
        }
    }

    
    func postImageToInstagramFromDIC(fileURL:URL, instagramCaption: String, view: UIView) {
        // called to post image with caption to the instagram application
        
        UIPasteboard.general.string = instagramCaption
        
        let instagramURL = URL(string: kInstagramURL)
        
        DispatchQueue.main.async {
            if UIApplication.shared.canOpenURL(instagramURL!) {
                //print("**--instagramURL=>" + String(describing:instagramURL))
    //                    print("**--fileURL=>" + String(describing: fileURL))
                
                self.documentInteractionController.url = fileURL
                self.documentInteractionController.delegate = self
                self.documentInteractionController.uti = self.kUTI
    //                        self.documentInteractionController.presentOpenInMenu(from: rect, in: view, animated: true)
                self.documentInteractionController.presentOptionsMenu(from: view.frame, in: view, animated: true)
            } else {
                // alert displayed when the instagram application is not available in the device
                UIAlertView(title: self.kAlertViewTitle, message: self.kAlertViewMessage, delegate:nil, cancelButtonTitle:"Ok").show()
            }
        }
    }

    func postVideoToInstagramFromDIC(videoUrlStr: String, instagramCaption: String, view: UIView) {
        // called to post image with caption to the instagram application
        
        UIPasteboard.general.string = instagramCaption
        
        let instagramURL = URL(string: kInstagramURL)
        if (UIApplication.shared.canOpenURL(instagramURL! as URL) && (videoUrlStr.count) > 0){
            
            guard let videoURL = URL(string: videoUrlStr) else { return }
            
            let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
//            var igVidUrl: URL!
            
            
            serialQueue.sync {
                // check if the file already exist at the destination folder if you don't want to download it twice
//                if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent(videoURL.lastPathComponent).path) {
                
                //print("**--videoURL=>" + videoUrlStr)
                    // set up your download task
                    URLSession.shared.downloadTask(with: videoURL) { (location, response, error) -> Void in
                        
                        // use guard to unwrap your optional url
                        guard let location = location else { return }
                        
                        // create a deatination url with the server response suggested file name
//                        let destinationURL2 = documentsDirectoryURL.appendingPathComponent(response?.suggestedFilename ?? videoURL.lastPathComponent)
                        
                        var uniqueVideoID = ""
                        let uniqueID = ""
                        //Here we are writing the data to the Document Directory for use later on.
                        let docPaths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
                        let documentsDirectory: AnyObject = docPaths[0] as AnyObject
                        
                        let dateformatter = DateFormatter()
                        
                        dateformatter.dateFormat = "MM.dd.yy.h.mm"
                        
                        let now = dateformatter.string(from: Date())
                        
                        uniqueVideoID = uniqueID  + String(describing: now) + ".mp4"
                        
                        //print(uniqueVideoID)
                        let docDataPath = documentsDirectory.appendingPathComponent(uniqueVideoID) as String
                        
                        let destinationURL = URL(fileURLWithPath: docDataPath)
                        
//                        try? myVideoVarData.write(to: URL(fileURLWithPath: docDataPath), options: [])
                        //print("**--docDataPath=>" + docDataPath)
                        //print("**--documentsDirectoryURL=>" + String(describing:documentsDirectoryURL))

                        
                        
                        //print("**--videoURL=>" + String(describing: videoURL))
                        //print("**--videoURL.lastPathComponent=>" + String(describing: videoURL.lastPathComponent))
                        //print("**--destinationURL=>" + String(describing: destinationURL))
                        //print("**--destinationURL2=>" + String(describing: destinationURL2))
                        
                        do {
                            
                            
                            
                            try FileManager.default.moveItem(at: location, to: destinationURL)
                            
                            PHPhotoLibrary.requestAuthorization({ (authorizationStatus: PHAuthorizationStatus) -> Void in
                                
                                // check if user authorized access photos for your app
                                if authorizationStatus == .authorized {
                                    PHPhotoLibrary.shared().performChanges({
                                        PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: destinationURL)}) { completed, error in
                                            if completed {
                                                //print("Video asset created")
//                                                igVidUrl = destinationURL
                                                self.sendVidToIG(igVidUrl: destinationURL, view: view)
                                            } else {
                                                //print(error)
                                            }
                                    }
                                }
                            })
                            
                        } catch let error as NSError {
                            print(error.localizedDescription)
                        }
                        
                    }.resume()
                
//                } else {
//                    //print("File already exists at destination url")
//                }
            }
            
        } else {
            // alert displayed when the instagram application is not available in the device
            UIAlertView(title: kAlertViewTitle, message: kAlertViewMessage, delegate:nil, cancelButtonTitle:"Ok").show()
        }
    }


    func sendVidToIG(igVidUrl: URL, view: UIView){
        //print("check igVidUrl")
        
//        private let kUTI = "com.instagram.exclusivegram"
        
        
        DispatchQueue.main.sync {
            let instagramURL = URL(string: kInstagramURL)
            if UIApplication.shared.canOpenURL(instagramURL!) {
                //print("Video send to IG")
                let igvidStr = String(format: "instagram://library?AssetPath=%@", igVidUrl.path)
                //print(igvidStr)
//                let rect = CGRect(x: 0, y: 0, width: 612, height: 612)
//                self.documentInteractionController.url = igvidStr
//                self.documentInteractionController.delegate = self
//                self.documentInteractionController.uti = self.kUTI
//                
//                self.documentInteractionController.presentOpenInMenu(from: rect, in: view, animated: true)
                UIApplication.shared.openURL(URL(string: igvidStr)!)
            }
        }
    }

    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
}
