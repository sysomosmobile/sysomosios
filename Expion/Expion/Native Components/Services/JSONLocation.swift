//
//  JSONLocation.swift
//  Expion
//
//  Created by Mike Bagwell on 11/25/15.
//  Copyright © 2015 Expion. All rights reserved.
//

import Foundation

@objc
protocol JSONLocationDelegate {
    func locJSONReturned(_ jsonLocReturn: JSONLocation)
}

class JSONLocation: NSObject {
    var company_id: String!
    var myUrl: String!
    var source: String!
    var enableNoLocSelect: Bool = false
    var jsonLocData: NSMutableData = NSMutableData()
    
//    var locationList = NSMutableArray()
    var locationList = Array<Dictionary<String, AnyObject>>()
//    var location = NSMutableDictionary()
    var selectedLocs: Array<Int>!
    
    var delegate: JSONLocationDelegate?
    
    //TODO: remove this after new swift galleryview is completed
    var locNames = NSMutableArray()
    var locIds = NSMutableArray()
    var locDefaults = NSMutableArray()
    
//    init(company_id: String, myUrl: String, source: String){
//        self.company_id = company_id
//        self.myUrl = myUrl
//        self.source = source
//    }
    let shareData = ShareData.sharedInstance
    func getLocJSON() {
        if(source == nil){
            source = "FBPOSTV1"
        }
        
//        let urlAsString: String = String(format:"%@/api/location/%@/%@?selectAll=true", myUrl, source, company_id)
//        
//        //NSUserDefaults.standardUserDefaults().registerDefaults(["UserAgent": "Expion-iOS-6.3"])
//        UserDefaults.standard.register(defaults: ["User-Agent": ShareData.sharedInstance.userAgent])
//        
//       //print("getLocJSON=>" + urlAsString)
//        let url = URL(string: urlAsString)
//        
//        let request: NSMutableURLRequest = NSMutableURLRequest(url: url!)
//        request.setValue("Expion", forHTTPHeaderField: "User-Agent")
//        let connection: NSURLConnection = NSURLConnection(request: request as URLRequest, delegate: self, startImmediately: false)!
//        connection.start()
        
        
        
        
        let urlPath: String = String(format:"%@/api/location/%@/%@?selectAll=true", myUrl, source, company_id)
        //print(urlPath)
        if let url = URL(string: urlPath) {
            let urlRequest: NSMutableURLRequest = NSMutableURLRequest(url: url)
            let session = URLSession.shared
            
            
//            let cookies = HTTPCookieStorage.shared.cookies!
//            //print("--getLocJSON cookies=>")
//            for cookie in cookies {
//                //print("--c--" + cookie.name + "=" + cookie.value)
//                
//            }
            
            let task = session.dataTask(with: urlRequest as URLRequest) {
                (data, response, error) -> Void in
                let httpResponse = response as! HTTPURLResponse
                let statusCode = httpResponse.statusCode
                
                //print(httpResponse)
                if (statusCode == 200) {
                    //print("--success-->")
                    
                    //print("try parse=>")
                    
                    
                    if(data != nil) {
                        self.jsonLocData = NSMutableData()
                        self.jsonLocData.append(data!)
                        self.doJSONParse()
//                        self.parseMLJSON(self.responseData) {
//                            (parsedMLData: Dictionary<String, Array<MLItem>>) in
//                            self.parseCompleted(parsedMLData)
//                            //println("*X*X*parseMLJSON completed*X*X*")
//                            //println("got back: (result)")
//                        }
                    }
                } else {
                    //print("xx--fail--xx")
                }
                
            }
            task.resume()
        }
        
    }
    
    func connection(_ didReceiveResponse: NSURLConnection!, didReceiveResponse response: URLResponse!) {
        // Received a new request, clear out the data object
    }
    
    
    func connection(_ connection: NSURLConnection!, didReceiveData data: Data!){
        if(self.jsonLocData.length > 0){
//            self.jsonLocData.setData(NSData() as Data)
            self.jsonLocData = NSMutableData(data: NSMutableData() as Data)
        }
        if(data != nil && data.count > 0){
//            self.jsonLocData = data.mutableCopy() as! NSMutableData

            self.jsonLocData = NSMutableData(data: data)
//            let dataStr = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
//           print("didReceiveData=>" + String(format: "%@", dataStr!))
            
        }
    }
    
    func connectionDidFinishLoading(_ connection: NSURLConnection!) {
        doJSONParse() /*{
            (parseCompleted: NSMutableArray) in
            self.parseCompleted(self.locationList)
            //println("*X*X*parseMLJSON completed*X*X*")
            //println("got back: (result)")
        }*/
    }
    
    func doJSONParse(){//(completion: () -> Void) {
       //print("doJSONParse=>")
        
        var returnJSON: NSArray!
        
        do {
            returnJSON = try JSONSerialization.jsonObject(with: self.jsonLocData as Data, options: JSONSerialization.ReadingOptions()) as? NSArray
        } catch {
            
        }
        
        var usePreSelected: Bool = false
        if((self.selectedLocs != nil && self.selectedLocs.count > 0) || enableNoLocSelect){
            usePreSelected = true
        }
        
       //print("doJSONParse=>2")
        if returnJSON != nil {
           //print("returnJSON != nil")
            self.locationList = Array<Dictionary<String, AnyObject>>()
            self.locNames = NSMutableArray()
            self.locIds = NSMutableArray()
            self.locDefaults = NSMutableArray()
            
            if let locArray = returnJSON {
//                //print("locArray=>")
//                //print(locArray)
                //self.locationList = locArray
                //locationList = locArray
                
                for loc in locArray.objectEnumerator().allObjects as! [[String:Any]] {
                    //let locDic: NSDictionary = NSDictionary()
                    var locName: String = ""
                    var locId: Int = 0
                    var locDefault: String = "0"
                    
                    if let JSON_locName = loc["name"] as? String {
                        locName = JSON_locName
                    }
                    if let JSON_locId = loc["id"] as? Int {
                        locId = JSON_locId
                    }
                    if let JSON_locDefault = loc["street_address"] as? String {
                        locDefault = JSON_locDefault
                    }
                    
                    var locSelected: String = "0"
                    
                    if(usePreSelected == true) {
                        locSelected = self.selectedLocs != nil && self.selectedLocs.contains(locId) == true ? "1" : "0"
                    } else {
                        locSelected = locDefault
                    }
                    
                    let locDic: Dictionary<String, AnyObject> = [
                        "name": locName as AnyObject,
                        "id": locId as AnyObject,
                        "isDefault": locDefault as AnyObject,
                        "selected": locSelected as AnyObject
                    ]
                        
                   //print("!--loc=>" + locName + "-" + String(locId) + "-" + locDefault)
                    self.locNames.add(locName)
                    self.locIds.add(locId)
                    self.locDefaults.add(locDefault)
                    
                    self.locationList.append(locDic)
                }
            }
            parseCompleted()
        }
        
    }
    
    func parseCompleted(){
       //print("parseCompleted")
        delegate?.locJSONReturned(self)
    }
}


class LocationUtils {
    
//    func getLocBtnTitle(locationList: Array<Dictionary<String, AnyObject>>) -> String {
//        var locBtnTitle: String = "No Locations"
//        var numSelected: Int = 0
//        
//        if(locationList.count > 0) {
//            for l in 1 ..< locationList.count { //LOOP CHECK SHOULD THIS BE 0 or 1
//                if locationList.count > l {
//                    let locDic = locationList[l]
//                    if let locSelectedTemp = locDic["selected"] as? String {
//                        if locSelectedTemp == "1" {
//                            if numSelected < 1 {
//                                if let locNameTemp = locDic["name"] as? String {
//                                    locBtnTitle = locNameTemp
//                                }
//                                numSelected += 1
//                            } else {
//                                numSelected += 1
//                                locBtnTitle = String(format: "%@ Locations", String(numSelected))
//                            }
//                        }
//                    }
//
//                }
//            }
//        }
//        
//        return locBtnTitle
//    }
    
    func forceLocListSingle(_ locList: Array<Dictionary<String, AnyObject>>) -> (Array<Dictionary<String, AnyObject>>, String) {
        var newLocList: Array<Dictionary<String, AnyObject>> = []
        var numSelected: Int = 0
        var titleTxt: String = "Select " + ShareData.sharedInstance.locationLabel //Locations"
        for var item in locList {
            if item["selected"] as? String == "1" {
                if(numSelected == 0) {
                    numSelected += 1
                    if(numSelected == 1) {
                        titleTxt = (item["name"] as? String)!
                    } else {
                        titleTxt = String(numSelected) + " " + ShareData.sharedInstance.locationLabel //Locations"
                    }
                } else {
                    item.updateValue("0" as AnyObject, forKey: "selected")
                }
            }
            newLocList.append(item)
        }
        return (newLocList, titleTxt)
    }
    
    func getTitleFromLocList(_ locList: Array<Dictionary<String, AnyObject>>) -> String {
        var numSelected: Int = 0
        var titleTxt: String = "Select " + ShareData.sharedInstance.locationLabel + "s"
        for item in locList {
            if item["selected"] as? String == "1" {
                numSelected += 1
                if(numSelected == 1) {
                    titleTxt = (item["name"] as? String)!
                } else {
                    titleTxt = String(numSelected) + " " + ShareData.sharedInstance.locationLabel + "s" //Locations"
                }
            }
        }
        return titleTxt
    }
    
    func getSelectedFromLocList(_ locList: Array<Dictionary<String, AnyObject>>) -> Array<Int> {
        var selectedLocs: Array<Int> = []
        for item in locList {
            if item["selected"] as? String == "1" {
                if let idInt = item["id"] as? Int {
                    selectedLocs.append(idInt)
                } else if let idStr = item["id"] as? String {
                    selectedLocs.append(Int(idStr)!)
                }
            }
        }
        return selectedLocs
    }
    
    func validationCheck(_ locList: Array<Dictionary<String, AnyObject>>) -> Bool {
        var hasSelected: Bool = false
        for item in locList {
            if hasSelected != true && item["selected"] as? String == "1" {
                hasSelected = true
            }
        }
        return hasSelected
    }
}
