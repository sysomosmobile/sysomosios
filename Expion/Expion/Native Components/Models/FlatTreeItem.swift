//
//  FlatTreeItem.swift
//  Expion
//
//  Created by Mike Bagwell on 2/24/16.
//  Copyright © 2016 Expion. All rights reserved.
//

struct FlatTreeItem {
    
    let name: String
    let id: Int
    let isDefault: Bool
    let selected: Bool
    
    init(name: String,
        id: Int,
        isDefault: Bool,
        selected: Bool) {
        
            self.name = name
            self.id = id
            self.isDefault = isDefault
            self.selected = selected
    }
}
