//
//  MLItem.swift
//  Expion
//
//  Created by Mike Bagwell on 12/16/14.
//  Copyright (c) 2014 Expion. All rights reserved.
//

import UIKit

/**
 MLItem - Master List Item
 - parameter one: param one description
 - returns: Object that represents Master List Item
 */
@objcMembers class MLItem: Serializable {  //MLStack
    /*
    let name: String
    let id: String
    let layout_id: String
    let image: UIImage?
    let selected: Bool
    let mlItemJSON: String
    let settings: MLSettings
    */
    
    let folder_ind: Bool
    var selected_ind: Bool
    let id: Int
    let company_id: Int
    let name: String
    let order_ind: Int
    let settings: String
    let suggested_ind: Bool
    let shd_v: Bool
    let shd_e: Bool
    let shd_d: Bool
    let shd_c: Bool
    let set_hw_c: Bool
    let set_hw_v: Bool
    let code_class_name: String
    let css_class_name: String
    let deployed_status: String
    
    let layout_id: Int
    
    // code_class_name = object representation of deserialized settings string by code_class_name value
    let mlStack: MLStack
    
    let children: Array<MLItem>
    
    init(folder_ind: Bool, selected_ind: Bool, id: Int, company_id: Int, name: String, order_ind: Int, settings: String, suggested_ind: Bool, shd_v: Bool, shd_e: Bool, shd_d: Bool, shd_c: Bool, set_hw_c: Bool, set_hw_v: Bool, code_class_name: String, css_class_name: String, deployed_status: String, layout_id: Int, children: Array<MLItem>,
       
        mlStack: MLStack){
        
        self.folder_ind = folder_ind
        self.selected_ind = selected_ind
        self.id = id
        self.company_id = company_id
        self.name = name
        self.order_ind = order_ind
        self.settings = settings
        self.suggested_ind = suggested_ind
        self.shd_v = shd_v
        self.shd_e = shd_e
        self.shd_d = shd_d
        self.shd_c = shd_c
        self.set_hw_c = set_hw_c
        self.set_hw_v = set_hw_v
        self.code_class_name = code_class_name
        self.css_class_name = css_class_name
        
        self.deployed_status = deployed_status
        self.layout_id = layout_id
        
        self.children = children
        //this is defined by code_class_name
        self.mlStack = mlStack
    }
}

@objcMembers class MLStack: Serializable {
    //MLStack
    //"code_class_name": "home"
    //{\"directive\":{\"directive_name\":\"expion-iframe\",\"attributes\":{\"webUrl\":\"/Home/Home?company_id=%%COMPANY_ID%%\"},\"disable_cache\":true}}",
    //"{\"directive\":{\"directive_name\":\"stacks-layout\",\"attributes\":{\"layout_id\":4746}}}"
    
    let directive: MLDirective
    
    init(directive: MLDirective) {
        self.directive = directive
    }
}

@objcMembers class MLDirective: Serializable {
    
    let directive_name: String
    let attributes: MLAttributes
    let webUrl: String
//    let attributesArray: NSMutableArray
    
    //TODO: create attributesArray: NSMutableArray on both sides
    init(directive_name: String, attributes: MLAttributes) { //, attributesArray: NSMutableArray) {
        self.directive_name = directive_name
        self.attributes = attributes
        self.webUrl = ""
        //        self.attributesArray = attributesArray
    }
    
    init(directive_name: String, attributes: MLAttributes, webUrl: String) { //, attributesArray: NSMutableArray) {
        self.directive_name = directive_name
        self.attributes = attributes
        self.webUrl = webUrl
        //        self.attributesArray = attributesArray
    }
}

@objcMembers open class MLAttributes: Serializable {
    let layout_id: Int
    //let data_single_stack_id: Int
    //let module_id: Int
    let attributeValues: Array<MLAttributesKeyValue>!
    
    init(layout_id: Int){//, attributValues: Array<MLAttributesKeyValue>!) {
        //init(layout_id: Int, data_single_stack_id: Int, module_id: Int) {
        self.layout_id = layout_id
        //self.data_single_stack_id = data_single_stack_id
        //self.module_id = module_id
        self.attributeValues = nil
    }
    
    init(layout_id: Int, attributeValues: Array<MLAttributesKeyValue>!) {
        self.layout_id = layout_id
        self.attributeValues = attributeValues
    }
    
    /*
    let webUrl: String
    let disable_cache: Bool
    
    init(webUrl: String, disable_cache: Bool) {
    self.webUrl = webUrl
    self.disable_cache = disable_cache
    }*/
}

@objcMembers class MLAttributesKeyValue: Serializable {
    let key: String
    let value: String
    
    init(key: String, value: String){
        self.key = key
        self.value = value
    }
    
}

@objcMembers class SerializableMLItem: Serializable
{
    var item: MLItem!
    
    override init()
    {
        
    }
}







