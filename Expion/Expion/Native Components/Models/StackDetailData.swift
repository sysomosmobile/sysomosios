//
//  StackDetailData.swift
//  Workspaces
//
//  Created by Mike Bagwell on 4/13/15.
//  Copyright (c) 2015 Expion. All rights reserved.
//

import Foundation

class StackDetailData {
    
    let NativeUserPicture : String
    let UserName : String
    let LatestDateTime : String
    let Message : String
    let NativePictureUrl : String
    
    init(NativeUserPicture: String,
        UserName : String,
        LatestDateTime : String,
        Message: String,
        NativePictureUrl: String) {
        
            self.NativeUserPicture = NativeUserPicture
            self.UserName = UserName
            self.LatestDateTime = LatestDateTime
            self.Message = Message
            self.NativePictureUrl = NativePictureUrl
    }
    
    /*
    let posts = [
        stackPost(NativeUserPicture: "photo1", UserName: "Dark Wing Duck", LatestDateTime: "Apr 9, 2015 09:35 AM to Liberty Ca", Message: "Fan Message text will go here and it will go on on and on and on and on and apodudl aljd ou alkdnelr yd oadfi adsjlenpfoiy da dypfo iadfoi neapone pafya dfn apoefy adof anefp naof aydof nepfoneof yfoaneopfyef fad fdasf yasdofy adsoifue", NativePictureUrl: "http://m2.i.pbase.com/g6/36/718136/2/71754072.pn7yNHAu.jpg"),
        stackPost(NativeUserPicture: "photo1", UserName: "Dark Wing Duck", LatestDateTime: "Apr 9, 2015 09:35 AM to Liberty Ca", Message: "Fan Message text will go here and it will go on on and on and on and on and", NativePictureUrl: ""),
        stackPost(NativeUserPicture: "photo1", UserName: "Dark Wing Duck", LatestDateTime: "Apr 9, 2015 09:35 AM to Liberty Ca", Message: "on and", NativePictureUrl: ""),
        stackPost(NativeUserPicture: "photo1", UserName: "Dark Wing Duck", LatestDateTime: "Apr 9, 2015 09:35 AM to Liberty Ca", Message: "Fan Message text will go here and it will go on on and on and on and on and apodudl aljd ou alkdnelr yd oadfi adsjlenpfoiy da dypfo iadfoi neapone pafya dfn apoefy adof anefp naof aydof nepfoneof yfoaneopfyef fad fdasf yasdofy adsoifue", NativePictureUrl: ""),
        stackPost(NativeUserPicture: "photo1", UserName: "Dark Wing Duck", LatestDateTime: "Apr 9, 2015 09:35 AM to Liberty Ca", Message: "Fan Message text will go here", NativePictureUrl: ""),
        stackPost(NativeUserPicture: "photo1", UserName: "Dark Wing Duck", LatestDateTime: "Apr 9, 2015 09:35 AM to Liberty Ca", Message: "Fan Message text will go here and it will go on on and on and on and on and apodudl aljd ou alkdnelr yd oadfi adsjlenpfoiy da dypfo iadfoi neapone pafya dfn apoefy adof anefp naof aydof nepfoneof yfoaneopfyef fad fdasf yasdofy adsoifue", NativePictureUrl: ""),
        stackPost(NativeUserPicture: "photo1", UserName: "Dark Wing Duck", LatestDateTime: "Apr 9, 2015 09:35 AM to Liberty Ca", Message: "adsoifue", NativePictureUrl: ""),
        stackPost(NativeUserPicture: "photo1", UserName: "Dark Wing Duck", LatestDateTime: "Apr 9, 2015 09:35 AM to Liberty Ca", Message: "Fan Message text will go here and it will go on on and on and on and on and apodudl aljd ou alkdnelr yd oadfi adsjlenpfoiy da dypfo iadfoi neapone pafya dfn apoefy adof anefp naof aydof nepfoneof yfoaneopfyef fad fdasf yasdofy adsoifue", NativePictureUrl: ""),
        stackPost(NativeUserPicture: "photo1", UserName: "Dark Wing Duck", LatestDateTime: "Apr 9, 2015 09:35 AM to Liberty Ca", Message: "Fan Message text will go here and it will go on on and on and on and on and apodudl aljd ou alkdnelr yd oadfi adsjlenpfoiy da dypfo iadfoi neapone pafya dfn apoefy adof anefp naof aydof nepfoneof yfoaneopfyef fad fdasf yasdofy adsoifue Fan Message text will go here and it will go on on and on and on and on and apodudl aljd ou alkdnelr yd oadfi adsjlenpfoiy da dypfo iadfoi neapone pafya dfn apoefy adof anefp naof aydof nepfoneof yfoaneopfyef fad fdasf yasdofy adsoifue", NativePictureUrl: ""),
        stackPost(NativeUserPicture: "photo1", UserName: "Dark Wing Duck", LatestDateTime: "Apr 9, 2015 09:35 AM to Liberty Ca", Message: "Fan Message text will go here and it will go on on and on and on and on and apodudl aljd ou alkdnelr yd oadfi adsjlenpfoiy da dypfo iadfoi neapone pafya dfn apoefy adof anefp naof aydof nepfoneof yfoaneopfyef fad fdasf yasdofy adsoifue", NativePictureUrl: ""),
        
    ]*/
    
}
