//
//  HomeItem.swift
//  Workspaces
//
//  Created by Mike Bagwell on 5/19/15.
//  Copyright (c) 2015 Expion. All rights reserved.
//

/*
struct User {
    let id: Int
    let name: String
    let email: String
}*/

struct HomeItem {  //MLStack
    /*
    let name: String
    let id: String
    let layout_id: String
    let image: UIImage?
    let selected: Bool
    let mlItemJSON: String
    let settings: MLSettings
    */
    
    let folder_ind: Bool
    let id: Int
    let company_id: Int
    let name: String
    let order_ind: Int
    let settings: String
    let suggested_ind: Bool
    let shd_v: Bool
    let shd_e: Bool
    let shd_d: Bool
    let shd_c: Bool
    let set_hw_c: Bool
    let set_hw_v: Bool
    let code_class_name: String
    let layout_id: Int
    
    // code_class_name = object representation of deserialized settings string by code_class_name value
    let mlStack: Stack
    
}

struct Stack {
    //MLStack
    //"code_class_name": "home"
    //{\"directive\":{\"directive_name\":\"expion-iframe\",\"attributes\":{\"webUrl\":\"/Home/Home?company_id=%%COMPANY_ID%%\"},\"disable_cache\":true}}",
    //"{\"directive\":{\"directive_name\":\"stacks-layout\",\"attributes\":{\"layout_id\":4746}}}"
    
    let directive: LayoutDirective
    
    init(directive: LayoutDirective) {
        self.directive = directive
    }
}

struct LayoutDirective {
    
    let directive_name: String
    let attributes: LayoutAttributes
    
    init(directive_name: String, attributes: LayoutAttributes) {
        self.directive_name = directive_name
        self.attributes = attributes
    }
}

struct LayoutAttributes {
    let layout_id: Int
    
    init(layout_id: Int) {
        self.layout_id = layout_id
    }
    
    /*
    let webUrl: String
    let disable_cache: Bool
    
    init(webUrl: String, disable_cache: Bool) {
    self.webUrl = webUrl
    self.disable_cache = disable_cache
    }*/
}
