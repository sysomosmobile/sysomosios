//
//  ExpionViewController.h
//  Expion
//
//  Created by Drunkynstynn on 1/7/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeychainItemWrapper.h"

@protocol ExpionViewControllerDelegate <NSObject>
@optional
//- (void)galleryViewImageSelected:(NSString *)eImageId :(NSData *)eImageData;
- (void)loginComplete;//:(NSString *)myUrl :(NSString *)company_id :(NSString *)userName :(NSString *)expionName;
- (void)switchToSSOLogin;
@end

@interface ExpionViewController : UIViewController {
    IBOutlet UIButton *ssoEnableBtn;
    IBOutlet UIButton *loginLogo;
    IBOutlet UIScrollView *scrollthis;
    IBOutlet UIView *loginLoadingView;
    IBOutlet UIView *loginPage;
    IBOutlet UIView *homePage;
    IBOutlet UITextField *usernameField;
    IBOutlet UITextField *passwordField;
    IBOutlet UITextField *serverCodeField;
    IBOutlet UITextField *branchField;
    IBOutlet UIButton *loginButton;
    IBOutlet UIButton *showStoredInfoButton;
    IBOutlet UIButton *launchWebAppButton;
    IBOutlet UIButton *quickPostButton;
    IBOutlet UIButton *localIdeasButton;
    IBOutlet UIButton *fanConversationsButton;
    IBOutlet UIButton *photoUploadButton;
    IBOutlet UIButton *logoutButton;
    IBOutlet UIActivityIndicatorView *loginIndicator;
    IBOutlet UISwitch *debugMode;
    
    KeychainItemWrapper *keychain;
    NSURLConnection *homeConnection;
    NSMutableData *responseData;
    NSURL *responseURL;
    
    NSString *myUrl;
    NSString *actionUrl; //TODO: remove me?
    NSString *company_id;
    NSString *expionName;
    NSString *accessRoles;
    NSString *userAgent;
    BOOL isAdmin;
    BOOL canViewPublish;
    BOOL canViewContent;
    BOOL canMobileSuggestedContent;
    BOOL canViewModeration;
    BOOL canMobileQuickDraft;
    BOOL canMobileUpload;
    BOOL canMobileMediaCreate;
    BOOL canMobileMediaApprove;
    
    BOOL canMediaGallery;
    BOOL canFBAlbumImagePublish;
    BOOL canExpionImage;
    BOOL canViewExpionImage;
    
    BOOL isEImages;
    
    BOOL autoLogin;
    BOOL autoComplete;
    
    __weak id<ExpionViewControllerDelegate> delegate;
}

@property (nonatomic, retain) UIButton *loginLogo;

@property (nonatomic, retain) UITextField *usernameField;
@property (nonatomic, retain) UITextField *passwordField;
@property (nonatomic, retain) UITextField *serverCodeField;
@property (nonatomic, retain) UITextField *branchField;
@property (nonatomic, retain) UIButton *loginButton;
@property (nonatomic, retain) UIButton *showStoredInfoButton;
@property (nonatomic, retain) UIButton *launchWebAppButton;
@property (nonatomic, retain) UIButton *quickPostButton;
@property (nonatomic, retain) UIButton *localIdeasButton;
@property (nonatomic, retain) UIButton *fanConversationsButton;
@property (nonatomic, retain) UIButton *photoUploadButton;
@property (nonatomic, retain) UIButton *logoutButton;
@property (nonatomic, retain) UIActivityIndicatorView *loginIndicator;
@property (nonatomic, retain) UISwitch *debugMode;

@property (nonatomic, retain) NSURLConnection *homeConnection;
@property (nonatomic, retain) NSURL *responseURL;

@property (nonatomic, retain) NSString *myUrl;
@property (nonatomic, retain) NSString *actionUrl;
@property (nonatomic, retain) NSString *company_id;
@property (nonatomic, retain) NSString *expionName;
@property (nonatomic, retain) NSString *accessRoles;
@property (nonatomic, retain) NSString *userAgent;
@property (nonatomic, assign) BOOL isAdmin;
@property (nonatomic, assign) BOOL canViewPublish;
@property (nonatomic, assign) BOOL canViewContent;
@property (nonatomic, assign) BOOL canMobileSuggestedContent;
@property (nonatomic, assign) BOOL canViewModeration;
@property (nonatomic, assign) BOOL canMobileQuickDraft;
@property (nonatomic, assign) BOOL canMobileUpload;
@property (nonatomic, assign) BOOL canMobileMediaCreate;
@property (nonatomic, assign) BOOL canMobileMediaApprove;
@property (nonatomic, assign) BOOL canMediaGallery;
@property (nonatomic, assign) BOOL canFBAlbumImagePublish;
@property (nonatomic, assign) BOOL canExpionImage;
@property (nonatomic, assign) BOOL canViewExpionImage;
@property (nonatomic, assign) BOOL isEImages;
@property (nonatomic, assign) BOOL autoLogin;
@property (nonatomic, assign) BOOL autoComplete;

@property (weak, nonatomic) id<ExpionViewControllerDelegate> delegate;

- (IBAction) login: (id) sender;
- (void) getFromKeyChain;
- (void) setUserCreds;
- (void) saveUserDefaults;
- (void) storeToKeyChain;
- (void) showAlert:(NSString *)alertText :(NSString *)alertMessage;


- (void) serverCodeFieldDidChange;

- (void)loginExpionConnection:(NSString *)eUsername :(NSString *)ePW :(NSString *)eUrl;

@end
