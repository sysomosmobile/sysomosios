//
//  ExpionViewController.m
//  Expion
//
//  Created by Drunkynstynn on 1/7/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
#import <Security/Security.h>

#import "Expion-Swift.h"
#import "ExpionViewController.h"
//#import "ExpionWebViewController.h"
//#import "ExpionQuickPostViewController.h"
//#import "ExpionUploadPhotoViewController.h"

@interface ExpionViewController () <NSURLSessionDataDelegate>

@property (nonatomic, strong, readwrite) NSURLSession * session;
@end

@implementation ExpionViewController

@synthesize loginLogo, usernameField;
@synthesize passwordField;
@synthesize serverCodeField, branchField, debugMode;
@synthesize loginButton;
@synthesize showStoredInfoButton;
@synthesize launchWebAppButton;
@synthesize loginIndicator;

@synthesize quickPostButton;
@synthesize localIdeasButton;
@synthesize fanConversationsButton;
@synthesize photoUploadButton;
@synthesize logoutButton;

@synthesize homeConnection;
@synthesize responseURL;

@synthesize myUrl;
@synthesize actionUrl;
@synthesize expionName;
@synthesize company_id;
@synthesize accessRoles, userAgent;
@synthesize isAdmin;
@synthesize canViewPublish;
@synthesize canViewContent;
@synthesize canMobileSuggestedContent;
@synthesize canViewModeration;
@synthesize canMobileQuickDraft;
@synthesize canMobileUpload;
@synthesize canMobileMediaCreate;
@synthesize canMobileMediaApprove,
canMediaGallery,
canFBAlbumImagePublish,
canExpionImage,
delegate;
@synthesize isEImages;

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    //NSLog(@"!!!_!_!__!____!!!!!!!---viewDidLoad---!!!_!_!__!____!!!!!!!");
    
    
    //userAgent = "Expion-" + AppSettings.deviceStr + "-" + (Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String).replacingOccurrences(of: ".", with: "") + "-" + UIWebView().stringByEvaluatingJavaScript(from: "navigator.userAgent")!
    
    NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
    NSString *version = [info objectForKey:@"CFBundleShortVersionString"];
    NSString *device = [NSString stringWithFormat:@"iPhone"];
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        device = [NSString stringWithFormat:@"iPad"]; /* Device is iPad */
    }
    
    userAgent = [NSString stringWithFormat:@"Expion-%@-%@", device, [version stringByReplacingOccurrencesOfString:@"." withString:@""]];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            [scrollthis setScrollEnabled:YES];
            [scrollthis setContentSize:CGSizeMake(320, 500)];
            
            branchField.hidden = YES;
            debugMode.hidden = YES;
            
            NSUserDefaults *standardUserDefaults  = [NSUserDefaults standardUserDefaults];
            BOOL ssoEnabled = [standardUserDefaults boolForKey:@"ssoEnabled"];
            
            if(ssoEnabled == false) {
                [self getFromKeyChain];
            } else {
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    loginPage.hidden = NO;
                });
            }
            
            homePage.hidden = YES;
            
            quickPostButton.hidden = NO;
            localIdeasButton.hidden = YES;
            fanConversationsButton.hidden = NO;
            photoUploadButton.hidden = NO;
            quickPostButton.enabled = YES;
            localIdeasButton.enabled = NO;
            fanConversationsButton.enabled = YES;
            photoUploadButton.enabled = YES;
        });
    });
    
    
    
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    [serverCodeField addTarget:self
                  action:@selector(serverCodeFieldDidChange)
        forControlEvents:UIControlEventEditingChanged];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:nil];
    dispatch_async(dispatch_get_main_queue(), ^(void){
        loginIndicator.hidden = TRUE;
        [loginIndicator stopAnimating];
    });
}

-(void)serverCodeFieldDidChange
{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [branchField setText:@""];
        [self hideBranch];
    });
}

-(void)checkForBranch
{
    NSString *enteredServerCode = [[serverCodeField text] stringByReplacingOccurrencesOfString:@" " withString:@""];
    //NSLog(@"checkForBranch =>%@", enteredServerCode);
    if([enteredServerCode rangeOfString:@"7431"].location != NSNotFound || [enteredServerCode rangeOfString:@"0000"].location != NSNotFound){
        [self showBranch];
    } else {
        [self hideBranch];
    }
}

-(void)showBranch
{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        branchField.hidden = NO;
        debugMode.hidden = NO;
        debugMode.enabled = YES;
    });
}

-(void)hideBranch
{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        branchField.hidden = YES;
        branchField.text = @"";
        debugMode.hidden = YES;
        debugMode.enabled = NO;
        [debugMode setOn:false];
    });
}

- (IBAction) login: (id) sender
{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        loginLoadingView.hidden = NO;
    });
    [self login];
}

//---------------------------------
//* Start Login */
//---------------------------------

-(void)login
{
    //NSLog(@"!!!!!----LOGIN BUTTON PRESSED----!!!!!!");
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [usernameField resignFirstResponder];
        [passwordField resignFirstResponder];
        [serverCodeField resignFirstResponder];
        [branchField resignFirstResponder];
        loginIndicator.hidden = FALSE;
        [loginIndicator startAnimating];
        
        loginButton.enabled = NO;
        logoutButton.enabled = YES;
        loginLoadingView.hidden = NO;
    });
    //do login shit
    // Create instance of keychain wrapper
    keychain = [[KeychainItemWrapper alloc] initWithIdentifier:@"ExpionMobile" accessGroup:nil];
    
    ////NSLog(@"myUrl start output");
    NSString *enteredServerCode = [[serverCodeField text] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *enteredBranch = [[branchField text] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    //NSMutableData *commandToSend= [[NSMutableData alloc] init];
    NSString *output = @"";
    //unsigned char whole_byte;
    NSString *thisString = @"";
    //NSLog(@"myUrl start output");
    //NSLog(@"enteredServerCode=>%@", enteredServerCode);
    BOOL validCode = true;
    BOOL stagingCode = false;
    
    if(enteredServerCode && [enteredServerCode caseInsensitiveCompare:@"0000"] == NSOrderedSame){
        myUrl = [NSString stringWithFormat:@"https://t1-mobile.crm.xpon.us"];
    } else if(enteredServerCode && [enteredServerCode caseInsensitiveCompare:@"1111"] == NSOrderedSame){
        myUrl = [NSString stringWithFormat:@"https://mobile-demo.crm.xpon.us"];
    } else if(enteredServerCode && [enteredServerCode caseInsensitiveCompare:@"stage"] == NSOrderedSame){
        myUrl = [NSString stringWithFormat:@"https://stage.crm.xpon.us"];
    } else {
        NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        if ([enteredServerCode rangeOfCharacterFromSet:notDigits].location == NSNotFound)
        {
            // newString consists only of the digits 0 through 9
        } else {
            NSString *firstLetter = [enteredServerCode substringToIndex:1];
            NSLog(@"FIRSTLETTER=>%@", firstLetter);
            if(enteredServerCode && [enteredServerCode rangeOfString:@"x" options:NSCaseInsensitiveSearch].location != NSNotFound && [firstLetter caseInsensitiveCompare:@"x"] == NSOrderedSame && [[enteredServerCode substringFromIndex:1] rangeOfCharacterFromSet:notDigits].location == NSNotFound){
                NSLog(@"FIRSTLETTER is x=>%@", firstLetter);
                stagingCode = true;
                enteredServerCode = [enteredServerCode substringFromIndex:1];
                NSLog(@"new code is x=>%@", enteredServerCode);
            } else {
                validCode = false;
            }
        }
        
        if ( [enteredServerCode length] % 2 == 0) {
            // remainder 0
        } else {
            // b does not divide a evenly
            validCode = false;
        }
        
        if(validCode == true){
            for (int i=0; i < [enteredServerCode length]; i+=2) {
                thisString = [enteredServerCode substringWithRange: NSMakeRange (i, 2)];
                //NSLog(@"thisString=%@", thisString);

                int value = 0;
                sscanf([thisString cStringUsingEncoding:NSASCIIStringEncoding], "%x", &value);
                output = [output stringByAppendingFormat:@"%c", (char)value];
                
                //NSLog(@"output=%@", output);
            }
            //NSLog(@"myUrl output=%@", output);
           
            if([output rangeOfString:@"t1"].location != NSNotFound && enteredBranch.length > 0){
                myUrl = [NSString stringWithFormat:@"https://%@-%@.crm.expion.us", output, enteredBranch];
            } else if (stagingCode == true){
                myUrl = [NSString stringWithFormat:@"https://stage-%@.crm.expion.us", output];
            } else {
                myUrl = [NSString stringWithFormat:@"https://%@.crm.xpon.us", output];
            }
        } else {
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [self showAlert: @"Invalid Code" :@"The code you have entered is invalid.  Please check your entry and try again"];
                loginLoadingView.hidden = YES;
                loginButton.enabled=YES;
                logoutButton.enabled=NO;
            });
        }
    }
    
    //myUrl = [[NSString stringWithFormat:@"https://%@.crm.xpon.us", output] retain];
    //myUrl = [[NSString stringWithFormat:@"http://192.168.3.104:9999"] retain];
    //myUrl = [NSString stringWithFormat:@"http://192.168.1.108:9999"];
    
    NSLog(@"myUrl=%@", myUrl);
    ///
    
    [self loginExpion];
//    [self postSysomosSSO];
//    [self ssoBegin];
    
}

//---------------------------------
//* Start NSURLConnection */
//---------------------------------


- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    //NSLog(@"***handleOpenURL***");
    //your app specific code here for handling the launch
    
    return YES;
}

-(void)loginExpion
{
    NSString *enteredUsername = [usernameField text];
    NSString *enteredPassword = [passwordField text];
    NSString *usernameData = [@"username=" stringByAppendingString: enteredUsername];
    NSString *passwordData = [@"&password=" stringByAppendingString: enteredPassword];
    NSString *loginData = [usernameData stringByAppendingString:passwordData];
    NSString *postData = [loginData stringByAppendingString:@"&rememberme=false"];
    
    NSString *urlAsString = [NSString stringWithFormat:@"%@/Account/LogOn?ReturnUrl=%%2F", myUrl];
    //NSLog(@"urlAsString=%@", urlAsString);
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];//@"CFBundleVersion"];//CFBundleShortVersionString =fix
    appVersion = [appVersion stringByReplacingOccurrencesOfString:@"." withString: @""];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlAsString] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0];
    //    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlAsString] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    
    
    
    NSLog(@"LL--appVersion=>%@", appVersion);
    NSLog(@"userAgent=>>%@", userAgent);
    NSLog(@"urlAsString=>%@", urlAsString);
    [request setValue:userAgent forHTTPHeaderField:@"User-Agent"];
    //    [request setValue:[NSString stringWithFormat:@"Expion-iPhone-%@", appVersion] forHTTPHeaderField:@"User-Agent"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    homeConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

-(void)loginExpionConnection:(NSString *)eUsername :(NSString *)ePW :(NSString *)eUrl
{
    NSString *usernameData = [@"username=" stringByAppendingString: eUsername];
    NSString *passwordData = [@"&password=" stringByAppendingString: ePW];
    NSString *loginData = [usernameData stringByAppendingString:passwordData];
    NSString *postData = [loginData stringByAppendingString:@"&rememberme=false"];
    
    
    NSLog(@"loginExpionConnection strings=>%@", postData);
    
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];//@"CFBundleVersion"];//CFBundleShortVersionString =fix
    appVersion = [appVersion stringByReplacingOccurrencesOfString:@"." withString: @""];
    
    NSString *reqUrl = [NSString stringWithFormat:@"https://%@", eUrl];
    myUrl = reqUrl;
    
    NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
    NSString *version = [info objectForKey:@"CFBundleShortVersionString"];
    NSString *device = [NSString stringWithFormat:@"iPhone"];
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        device = [NSString stringWithFormat:@"iPad"]; /* Device is iPad */
    }
    
    userAgent = [NSString stringWithFormat:@"Expion-%@-%@", device, [version stringByReplacingOccurrencesOfString:@"." withString:@""]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:reqUrl] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlAsString] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    

    
    NSLog(@"LL--appVersion=>%@", appVersion);
    NSLog(@"userAgent=>>%@", userAgent);
    NSLog(@"eUrl=>%@", eUrl);
    [request setValue:userAgent forHTTPHeaderField:@"User-Agent"];
//    [request setValue:[NSString stringWithFormat:@"Expion-iPhone-%@", appVersion] forHTTPHeaderField:@"User-Agent"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    homeConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void)logOffExpion {
    NSString *urlAsString = [NSString stringWithFormat:@"%@/Account/LogOff?ReturnUrl=%%2F", myUrl];
    //NSLog(@"urlAsString=%@", urlAsString);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlAsString] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0];
    
    homeConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
    return nil;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    responseData = [[NSMutableData alloc] init];
    [responseData setLength:0];
    responseURL = [response URL];
    
//    NSLog(@"didrecieveresponse url=%@", [responseURL absoluteString]);
    
    ExpionVersion *exVer = [ExpionVersion new];
    exVer.vc = self;
    [exVer checkResponseUrl:[responseURL absoluteString]];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    NSString *strResult = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
//    NSLog(@"didReceiveData=>%@", strResult);
    [responseData appendData:data];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
//    NSLog(@"Connection failed! Error - %@ %@",[error localizedDescription], [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [self showAlert: @"Connection Error" :@"Expion could not be reached.  Please check network connection and try again"];
        
        //TODO: call login screen to kill loading
        loginLoadingView.hidden = YES;
        loginButton.enabled=YES;
        logoutButton.enabled=NO;
    });
    // inform the user
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // Use responseData
    //NSLog(@"Succeeded! Received %lu bytes of data",(unsigned long)[responseData length]);
    
    if(responseURL){
        NSString *responseURLString = [responseURL absoluteString];
        NSLog(@"responseURLString=>%@", responseURLString);
        if([responseURLString rangeOfString:@"Home" options:NSCaseInsensitiveSearch].location != NSNotFound) {
            
            if(responseURLString != nil && responseURLString.length > 0) {
                company_id = [NSString stringWithFormat:@"%@", [[[[responseURLString componentsSeparatedByString:@"company_id="] objectAtIndex:1] componentsSeparatedByString:@"&"] objectAtIndex:0]];
                [self setUserCreds];
                //NSLog(@"setUserCreds done");
                [self saveUserDefaults];
                //NSLog(@"saveUserDefaults done");
                [self storeToKeyChain];
                //NSLog(@"storeToKeyChain done");

                [self loginComplete];
//            [self startViewController];
            //loginPage.hidden = YES;
            //homePage.hidden = NO;
            /*
            dispatch_async(dispatch_get_main_queue(), ^(void){
                loginButton.enabled=YES;
                logoutButton.enabled=NO;
            });
             */
            //[self showAlert: @"Login Success" :@"Login was successful."];
            }
        }else if([responseURLString rangeOfString:@"MobileVersion" options:NSCaseInsensitiveSearch].location != NSNotFound){
//            NSLog(@"MobileVersion");
//            NSLog(@"MobileVersion");
            
        }else if([responseURLString rangeOfString:@"LogOff" options:NSCaseInsensitiveSearch].location != NSNotFound){
            //            NSLog(@"LogOff!!! Do nothing?");
        }else if([responseURLString rangeOfString:@"LogOn" options:NSCaseInsensitiveSearch].location != NSNotFound){
            //            NSLog(@"LogOff!!! Do nothing?");
        }else{
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [self showAlert: @"Login Failed" :@"Please Check Username and Password"];
                loginButton.enabled=YES;
                logoutButton.enabled=NO;
                loginLoadingView.hidden = YES;
            });
        }
    }
}

//---------------------------------
//* End NSURLConnection */
//---------------------------------

//-----------------------------------------------------//





-(void)loginComplete {
    NSString *userName = [NSString stringWithFormat:@"%@", [usernameField text]];
    NSLog(@"loginComplete=>%@ =>%@ =>%@ =>%@", myUrl, company_id, userName, userName);
    //[self.delegate loginComplete];//:myUrl :company_id :userName :userName];
    //[[[self parentViewController] parentViewController] dismissViewControllerAnimated:YES completion:nil];
    
//    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
//        //Background Thread
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            [self.presentingViewController dismissViewControllerAnimated:YES completion: ^{
//                NSLog(@"login is dismissed");
                [self.delegate loginComplete];
            }];
        });
//    });
    
}

-(void)startViewController{
    
    /*
    ContainerViewController *cViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ContainerViewController"];
    cViewController.company_id = company_id;
    cViewController.myUrl = myUrl;
    cViewController.debugMode = [NSNumber numberWithBool:debugMode.isOn];
    cViewController.canMobileMediaApprove = [NSNumber numberWithBool:canMobileMediaApprove];
    cViewController.isAdmin = [NSNumber numberWithBool:isAdmin];
    [self presentViewController:cViewController animated:YES completion:^{[self presentHomeScreenCompleted];}];
    */
    //launch old world
    //[self performSegueWithIdentifier:@"OldWorldPush" sender:self];
}

-(IBAction)prepareForUnwind:(UIStoryboardSegue *)segue {
    [self presentHomeScreenCompleted];
}

-(void)presentHomeScreenCompleted{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        loginLoadingView.hidden = YES;
    });
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    //ExpionViewController *transferViewController = segue.destinationViewController;

    //NSLog(@"prepareForSegue: %@", segue.identifier);
    /*
    if([segue.identifier isEqualToString:@"segue1"]) {
        transferViewController.varName = @"String";
    } else if([segue.identifier isEqualToString:@"segue2"]) {
        transferViewController.varName = @"String";
    }*/

}

-(void)showAlert:(NSString *)alertTitle :(NSString *)alertMessage{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: alertTitle message: alertMessage delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [alert show];
    });
    //[alert release];
}

-(void)getFromKeyChain{
    //NSLog(@"!!!_!_!_--getFromKeyChain--_!____!!!!!!!");
    // Create instance of keychain wrapper
    keychain = [[KeychainItemWrapper alloc] initWithIdentifier:@"ExpionMobile" accessGroup:nil];
    
    if ([keychain objectForKey:(__bridge id)(kSecAttrAccount)]) {
    //if(SecItemCopyMatching((__bridge CFDictionaryRef)keychain, NULL) == noErr){
        // Get username from keychain (if it exists)
        //NSLog(@"keychain exists");
        
        NSString *storedUsername = [NSString stringWithFormat:@"%@", [keychain objectForKey:(__bridge id)kSecAttrAccount]];
        NSData *storedPasswordData = [keychain objectForKey:(__bridge id)(kSecValueData)];
        //NSString *storedPassword = [NSString stringWithFormat:@"%@", [keychain objectForKey:(__bridge id)kSecValueData]];
        NSString *storedServerCode = [NSString stringWithFormat:@"%@", [keychain objectForKey:(__bridge id)kSecAttrDescription]];
        NSString *storedBranch = [NSString stringWithFormat:@"%@", [keychain objectForKey:(__bridge id)kSecAttrComment] != nil ? [keychain objectForKey:(__bridge id)kSecAttrComment] : @""];
        
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            if (storedUsername != nil && storedUsername.length > 0) {
                [usernameField setText:storedUsername];
                //NSLog(@"username: %@", [usernameField text]);
            }
            if (storedPasswordData != nil && storedPasswordData.length > 0) {
                NSString *storedPassword = [[NSString alloc] initWithData:storedPasswordData encoding:NSUTF8StringEncoding];
                
                [passwordField setText:storedPassword];
                //NSLog(@"passwordField: %@", [passwordField text]);
            }
            if (storedServerCode != nil && storedServerCode.length > 0) {
                [serverCodeField setText:storedServerCode];
                //NSLog(@"serverCodeField: %@", [serverCodeField text]);
            }
            if (storedBranch != nil && storedBranch.length > 0) {
                [branchField setText:storedBranch];
                if([storedServerCode rangeOfString:@"7431"].location != NSNotFound || [storedServerCode rangeOfString:@"0000"].location != NSNotFound){
                    branchField.hidden = NO;
                    debugMode.hidden = NO;
                    debugMode.enabled = YES;
                }
                //NSLog(@"serverCodeField: %@", [serverCodeField text]);
            }
        });
//        NSString *apns_token = [[NSUserDefaults standardUserDefaults] stringForKey:@"apns_token"];
//        
//        if(apns_token != nil && apns_token.length > 0) {
//            NSLog(@"apns_token=>");
//            NSLog(@"%@", apns_token);
//        } else {
//            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
//                [[UIApplication sharedApplication] registerForRemoteNotifications];
//        }
        
        NSDate *lastResignDate = [[NSUserDefaults standardUserDefaults] stringForKey:@"lastResignDate"];
        
        NSTimeInterval diff = [lastResignDate timeIntervalSinceNow];
//        NSLog(@"dateDiff=>%f", diff);
        
        if(autoLogin == true && diff < 86400 && storedUsername != nil && storedUsername.length > 0
           && storedPasswordData != nil && storedPasswordData.length > 0
           &&storedServerCode != nil && storedServerCode.length > 0) {
            [self login];
        } else {
            //show
            dispatch_async(dispatch_get_main_queue(), ^(void){
                loginPage.hidden = NO;
            });
        }
    }
}

-(void)storeToKeyChain{
    //NSLog(@"storeToKeyChain start");
    // Store username to keychain
    NSString *usernameTxt = [NSString stringWithFormat:@""];
    if ([usernameField text])
        usernameTxt = [NSString stringWithFormat:@"%@", [usernameField text]];
    
    // Store password to keychain
    NSString *passwordTxt = [NSString stringWithFormat:@""];
    if ([passwordField text]){
        //NSLog(@"storeToKeyChain password => %@", [passwordField text]);
        passwordTxt = [NSString stringWithFormat:@"%@", [passwordField text]];
    }
    // Store serverCode to keychain
    NSString *serverCodeTxt = [NSString stringWithFormat:@""];
    if ([serverCodeField text])
        serverCodeTxt = [NSString stringWithFormat:@"%@", [serverCodeField text]];
    
    // Store branch to keychain
    NSString *branchTxt = [NSString stringWithFormat:@""];
    if ([branchField text])
        branchTxt = [NSString stringWithFormat:@"%@", [branchField text]];
    
    [keychain setObject:usernameTxt forKey:(__bridge id)kSecAttrAccount];
    [keychain setObject:passwordTxt forKey:(__bridge id)kSecValueData];
    [keychain setObject:serverCodeTxt forKey:(__bridge id)kSecAttrDescription];
    [keychain setObject:@"ExpionMobile" forKey: (__bridge id)kSecAttrService];
    [keychain setObject:branchTxt forKey:(__bridge id)kSecAttrComment];
    
    // Store login shit
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [usernameField resignFirstResponder];
        [passwordField resignFirstResponder];
        [serverCodeField resignFirstResponder];
        [branchField resignFirstResponder];
    });
}
-(void)setUserCreds{
    isAdmin = NO;
    canViewPublish = NO;
    canViewContent = NO;
    canViewModeration = NO;
    canMobileUpload = NO;
    canMobileMediaCreate = NO;
    canMobileMediaApprove = NO;
    
    canMediaGallery = NO;
    canFBAlbumImagePublish = NO;
    canExpionImage = NO;
    canViewExpionImage = NO;
    canMobileQuickDraft = NO;
    isEImages = NO;
    
    //NSString* responseDataString= [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
    NSString* responseDataString= [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    if ([responseDataString rangeOfString:@"canViewPublish=true"].location != NSNotFound) {
        canViewPublish = YES;
    }
    
    if ([responseDataString rangeOfString:@"canMobileQuickDraft=true"].location != NSNotFound) {
        canMobileQuickDraft = YES;
    }
    if ([responseDataString rangeOfString:@"canViewContent=true"].location != NSNotFound) {
        canViewContent = YES;
    }
    if ([responseDataString rangeOfString:@"canViewModeration=true"].location != NSNotFound) {
        canViewModeration = YES;
    }
    if ([responseDataString rangeOfString:@"canImageAlbumUpload=true"].location != NSNotFound) {
        canMobileUpload = YES;
    }
    
    //same as canMobileUpload
    //canImageAlbumUpload
    
    if ([responseDataString rangeOfString:@"canMobileMediaCreate=true"].location != NSNotFound) {
        canMobileMediaCreate = YES;
    }
    if ([responseDataString rangeOfString:@"canMobileMediaApprove=true"].location != NSNotFound) {
        canMobileMediaApprove = YES;
    }
    if ([responseDataString rangeOfString:@"canMediaGallery=true"].location != NSNotFound) {
        canMediaGallery = YES;
    }
    if ([responseDataString rangeOfString:@"canFBAlbumImagePublish=true"].location != NSNotFound) {
        canFBAlbumImagePublish = YES;
    }
    if ([responseDataString rangeOfString:@"canExpionImage=true"].location != NSNotFound) {
        canExpionImage = YES;
    }
    if ([responseDataString rangeOfString:@"canViewExpionImage=true"].location != NSNotFound) {
        canViewExpionImage = YES;
        //NSLog(@"canViewExpionImage=true");
    } else {
        //NSLog(@"canViewExpionImage=true NOT FOUND!!!!");
    }
    //NSLog(@"expionName: %@", expionName);
    //NSLog (@"isAdmin= %@", isAdmin ? @"YES" : @"NO");
    //NSLog (@"canViewPublish= %@", canViewPublish ? @"YES" : @"NO");
    //NSLog (@"canViewContent= %@", canViewContent ? @"YES" : @"NO");
    //NSLog (@"canViewModeration= %@", canViewModeration ? @"YES" : @"NO");
    //NSLog (@"canMobileUpload= %@", canMobileUpload ? @"YES" : @"NO");
    //NSLog (@"canMobileMediaCreate= %@", canMobileMediaCreate ? @"YES" : @"NO");
    //NSLog (@"canMobileMediaApprove= %@", canMobileMediaApprove ? @"YES" : @"NO");
}

-(void)saveUserDefaults{
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setBool:false forKey:@"ssoEnabled"];
    [standardUserDefaults setObject:myUrl forKey:@"myUrl"];
    //NSLog(@"saveUserDefaults 1");
    [standardUserDefaults setObject:actionUrl forKey:@"actionUrl"];
    [standardUserDefaults setObject:company_id forKey:@"companyId"];
    [standardUserDefaults setObject:expionName forKey:@"expionName"];
    NSString *userName = [NSString stringWithFormat:@"%@", [usernameField text]];
    [standardUserDefaults setObject:userName forKey:@"userName"];
    
    [standardUserDefaults setObject:[NSNumber numberWithBool:debugMode.isOn] forKey:@"debugMode"];
    
    [standardUserDefaults setBool:isAdmin forKey:@"isAdmin"];
    //NSLog(@"saveUserDefaults 2");
    [standardUserDefaults setBool:canViewPublish forKey:@"canViewPublish"];
    [standardUserDefaults setBool:canMobileSuggestedContent forKey:@"canMobileSuggestedContent"];
    [standardUserDefaults setBool:canMobileQuickDraft forKey:@"canMobileQuickDraft"];
    [standardUserDefaults setBool:canViewModeration forKey:@"canViewModeration"];
    [standardUserDefaults setBool:canMobileUpload forKey:@"canMobileUpload"];
    [standardUserDefaults setBool:canMobileMediaCreate forKey:@"canMobileMediaCreate"];
    [standardUserDefaults setBool:canMobileMediaApprove forKey:@"canMobileMediaApprove"];
    
    [standardUserDefaults setBool:canMediaGallery forKey:@"canMediaGallery"];
    [standardUserDefaults setBool:canFBAlbumImagePublish forKey:@"canFBAlbumImagePublish"];
    [standardUserDefaults setBool:canExpionImage forKey:@"canExpionImage"];
    [standardUserDefaults setBool:canViewExpionImage forKey:@"canViewExpionImage"];
    //NSLog(@"saveUserDefaults 3");
    [standardUserDefaults setBool:isEImages forKey:@"isEImages"];
    
    //[standardUserDefaults setObject: cookiesData forKey: @"cookies"];
    //NSLog(@"saveUserDefaults sync");
    [standardUserDefaults synchronize];
}
-(void)getUserDefaults{
    NSUserDefaults *standardUserDefaults  = [NSUserDefaults standardUserDefaults];
    myUrl = [standardUserDefaults objectForKey:@"myUrl"];
    company_id = [standardUserDefaults objectForKey:@"companyId"];
    expionName = [standardUserDefaults objectForKey:@"expionName"];
    isAdmin = [standardUserDefaults boolForKey:@"isAdmin"];
    
    //sloppy ass right here needs to be rewritten
    //int visButtons=0;
    //int buttonPos = 136;
    
    canViewPublish = [standardUserDefaults boolForKey:@"canViewPublish"];
    canMobileQuickDraft = [standardUserDefaults boolForKey:@"canMobileQuickDraft"];
    canViewModeration = [standardUserDefaults boolForKey:@"canViewModeration"];
    canMobileUpload = [standardUserDefaults boolForKey:@"canMobileUpload"];
    canMobileSuggestedContent = [standardUserDefaults boolForKey:@"canMobileSuggestedContent"];
    canMobileMediaCreate = [standardUserDefaults boolForKey:@"canMobileMediaCreate"];
    canMobileMediaApprove = [standardUserDefaults boolForKey:@"canMobileMediaApprove"];
    
    canMediaGallery = [standardUserDefaults boolForKey:@"canMediaGallery"];
    canFBAlbumImagePublish = [standardUserDefaults boolForKey:@"canFBAlbumImagePublish"];
    canExpionImage = [standardUserDefaults boolForKey:@"canExpionImage"];
    canViewExpionImage = [standardUserDefaults boolForKey:@"canViewExpionImage"];
}

- (IBAction)quickPostButton:(id)sender {
    //NSLog(@"!!!!!----quickPostButton----!!!!!!");
//    ExpionQuickPostViewController *quickPostView = [[[ExpionQuickPostViewController alloc] init] autorelease];
    //[ExpionQuickPostView setModalTransitionStyle:UIModalTransitionStylePartialCurl];
//    [self presentModalViewController:quickPostView animated:YES];
}
- (IBAction)localIdeasButton:(id)sender {
    /*
     ExpionViewController *expionWebView = [[[ExpionViewController alloc] init] autorelease];
     [expionWebView setModalTransitionStyle:UIModalTransitionStylePartialCurl];
     [self presentModalViewController:expionWebView animated:YES];
     */
}
- (IBAction)fanConversationsButton:(id)sender {
    //NSLog(@"!!!!!----fanConversationsButton----!!!!!!");
//    ExpionWebViewController *expionWebView = [[[ExpionWebViewController alloc] init] autorelease];
//    [expionWebView setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
//    [self presentModalViewController:expionWebView animated:YES];
}
- (IBAction)photoUploadButton:(id)sender {
    //NSLog(@"!!!!!----photoUploadButton----!!!!!!");
//    ExpionUploadPhotoViewController *uploadPhotoView = [[[ExpionUploadPhotoViewController alloc] init] autorelease];
    //[expionWebView setModalTransitionStyle:UIModalTransitionStylePartialCurl];
//    [self presentModalViewController:uploadPhotoView animated:YES];
}
- (IBAction)logoutButton:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^(void){
        loginPage.hidden = NO;
        homePage.hidden = YES;
        loginButton.enabled=YES;
        logoutButton.enabled=NO;
    });
    /*
     ExpionViewController *expionWebView = [[[ExpionViewController alloc] init] autorelease];
     [expionWebView setModalTransitionStyle:UIModalTransitionStylePartialCurl];
     [self presentModalViewController:expionWebView animated:YES];
     */
}

- (IBAction)loginLogoButton:(id)sender {
    [self checkForBranch];
}

- (IBAction)ssoEnableBtn:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^(void){
        //Run UI Updates
        [self.presentingViewController dismissViewControllerAnimated:YES completion: ^{
            //                NSLog(@"login is dismissed");
            [self.delegate switchToSSOLogin];
        }];
    });
}

- (void)dealloc
{
    /*[usernameField release];
    [passwordField release];
    [serverCodeField release];
    [loginButton release];
    [showStoredInfoButton release];
    [launchWebAppButton release];
    [loginIndicator release];
    [quickPostButton release];
    [localIdeasButton release];
    [fanConversationsButton release];
    [photoUploadButton release];
    [logoutButton release];
    */
    //[keychain release];
    //[responseURL release];
    //[responseData release];
    
    //[myUrl release];
    //[expionName release];
    //[company_id release];
    //[accessRoles release];
    
    //[super dealloc];
}


@end

