//
//  MainViewController.swift
//  Workspaces
//
//  Created by Mike Bagwell on 5/29/15.
//  Copyright (c) 2015 Expion. All rights reserved.
//

import UIKit
import QuartzCore
import Foundation

class MainViewController: UIViewController, ContainerViewControllerDelegate {
    
    var company_id: String!
    var myUrl: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let storyboard = UIStoryboard(name: "", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ContainerViewController")
        DispatchQueue.main.async {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
}
