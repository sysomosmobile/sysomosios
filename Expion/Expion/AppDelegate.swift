//
//  AppDelegate.swift
//  Expion
//
//  Created by Mike Bagwell on 12/15/14.
//  Copyright (c) 2014 Expion. All rights reserved.
//

import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var strDeviceToken : String = ""
    var notifEnabled: Bool = true
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        // for notifications
        if(notifEnabled == true) {
            registerForRemoteNotification()
            application.applicationIconBadgeNumber = 0
        }
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        UserDefaults.standard.setValue(Date(), forKey: "lastResignActive")
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        //print("AD--applicationDidBecomeActive!!!")
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        //        [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
        
        HTTPCookieStorage.shared.cookieAcceptPolicy = .always
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        //        MatchedPetsManager.sharedManager.archivePets()
    }
    
    func application(_ application: UIApplication, shouldSaveApplicationState coder: NSCoder) -> Bool {
        return true
    }
    
    func application(_ application: UIApplication, shouldRestoreApplicationState coder: NSCoder) -> Bool {
        return true
    }
    
    
    // MARK: Remote Notification Methods // <= iOS 9.x
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        let chars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var token = ""
        
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", arguments: [chars[i]])
        }
        
        //print("!!--DeviceToken=>", token)
        self.strDeviceToken = token
        UserDefaults.standard.setValue(token.description, forKey: "apns_token")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error)
    {
        //print("Error = ",error.localizedDescription)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        //print(userInfo)
        
        if(UIApplication.shared.applicationState == UIApplicationState.active) {
            //app is currently active, can update badges count here
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationReceived"), object: userInfo)
        }else if(UIApplication.shared.applicationState == UIApplicationState.background){
            //app is in background, if content-available key of your notification is set to 1, poll to your backend to retrieve data and update your interface here
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationOpened"), object: userInfo)
        }else if(UIApplication.shared.applicationState == UIApplicationState.inactive){
            //app is transitioning from background to foreground (user taps notification), do what you need when user taps here
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationOpened"), object: userInfo)
        }
        
        //print("after")
        //        application.cancelAllLocalNotifications()
        //        application.applicationIconBadgeNumber = 0
        completionHandler(.newData)
        
    }
    
    // MARK: UNUserNotificationCenter Delegate // >= iOS 10
    //@available(iOS 10.0, *)
    //func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    //    @available(iOS 10.0, *)
    //    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options: UNNotificationPresentationOptions) -> Void) {
    //        //print("User Info = ",notification.request.content.userInfo)
    //        completionHandler([.alert, .badge, .sound])
    //    }
    //
    //    @available(iOS 10.0, *)
    //    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    //        //print("PUSH-->response.actionIdentifier=>", response.actionIdentifier)
    //        //print("User Info = ",response.notification.request.content.userInfo)
    //
    //        UIApplication.shared.applicationIconBadgeNumber = 0
    //        UIApplication.shared.cancelAllLocalNotifications()
    //        //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationReceived"), object: userInfo)
    //        let notificationName = Notification.Name("NotificationReceived")
    //        NotificationCenter.default.post(name: notificationName, object: nil)
    //
    //        completionHandler()
    //    }
    
    // MARK: Class Methods
    func registerForRemoteNotification() {
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil{
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
            }
        }
        else {
            DispatchQueue.main.async {
                UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
}


